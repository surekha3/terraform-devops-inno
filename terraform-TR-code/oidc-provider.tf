resource "aws_cloudformation_stack" "oidc-counterpart" {
  depends_on = [aws_eks_cluster.plexus]
  for_each   = { for acc, value in var.oidc-target-accounts : acc => value }
  name       = "${local.name-prefix}-oidc-${each.value}-stack"
  parameters = {
    clusterName = local.cluster-name
    accountId   = each.value
  }
  template_body = <<STACK
{
  "Parameters": {
    "clusterName": {
      "Type": "String",
      "Description": "the eks cluster name which contains the cluster.identity.oidc.issuer"
    },
    "accountId": {
      "Type": "String",
      "Description": "account_id is the AWS account id hosting the IDP"
    }
  },
  "Resources": {
    "CreateOpenIdIDPProvider": {
      "Type": "Custom::OpenIdProvider",
      "Properties": {
        "ServiceToken": {"Fn::Sub": "arn:aws:sns:${var.aws-region}:460300312212:a205257-create-openId-provider-topic"},
        "cluster_name": {"Ref": "clusterName"},
        "account_id": {"Ref": "accountId"}
      }
    }
  },
  "Outputs": {
    "IdpArn": {
      "Description": "Open Id Provider Arn",
      "Value": {"Fn::GetAtt": ["CreateOpenIdIDPProvider", "Arn"]}
    }
  }
}
STACK
  tags          = merge(local.required-tags, { Name = "${local.name-prefix}-oidc-${each.value}-stack" })
}

resource "aws_cloudformation_stack" "oidc-provider" {
  depends_on = [aws_eks_cluster.plexus]
  name       = "${local.name-prefix}-oidc-provider-stack"

  parameters = {
    clusterName = local.cluster-name
  }

  template_body = <<STACK
{
  "Parameters": {
    "clusterName": {
      "Type": "String",
      "Description": "the eks cluster name which contains the cluster.identity.oidc.issuer"
    }
  },
  "Resources": {
    "CreateOpenIdIDPProvider": {
      "Type": "Custom::OpenIdProvider",
      "Properties": {
        "ServiceToken": {"Fn::Sub": "arn:aws:sns:${var.aws-region}:460300312212:a205257-create-openId-provider-topic"},
        "cluster_name": {"Ref": "clusterName"}
      }
    }
  },
  "Outputs": {
    "IdpArn": {
      "Description": "Open Id Provider Arn",
      "Value": {"Fn::GetAtt": ["CreateOpenIdIDPProvider", "Arn"]}
    }
  }
}
STACK

  tags = merge(local.required-tags, { Name = "${local.name-prefix}-oidc-provider-stack" })

}
