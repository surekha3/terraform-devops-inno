provider "aws" {
  profile    = "preprod"
  region     = "us-east-1"
}

resource "aws_instance" "example" {
  ami           = "ami-07f28d0659c7d2eef"
  instance_type = "t2.micro"
}
