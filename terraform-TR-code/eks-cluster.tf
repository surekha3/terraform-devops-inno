data "aws_iam_policy_document" "plexus-cluster" {
  statement {
    sid     = ""
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "plexus-cluster" {
  name                 = "${local.name-prefix}-eks-role"
  permissions_boundary = local.permission-boundary
  assume_role_policy   = data.aws_iam_policy_document.plexus-cluster.json
  tags                 = merge(local.required-tags, { Name = "${local.name-prefix}-eks-role" })
}

resource "aws_iam_role_policy_attachment" "nlb-policy" {
  policy_arn = aws_iam_policy.nlb-policy.arn
  role       = aws_iam_role.plexus-cluster.name
}

data "aws_iam_policy_document" "nlb-policy" {
  statement {
    sid       = "kopsK8sNLBMasterPermsRestrictive"
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "ec2:DescribeVpcs",
      "elasticloadbalancing:AddTags",
      "elasticloadbalancing:CreateListener",
      "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener",
      "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:DescribeLoadBalancerPolicies",
      "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
    ]
  }

  statement {
    sid       = ""
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "ec2:DescribeVpcs",
      "ec2:DescribeRegions",
    ]
  }
}

resource "aws_iam_policy" "nlb-policy" {
  name   = "${local.name-prefix}-nlb-policy"
  path   = "/"
  policy = data.aws_iam_policy_document.nlb-policy.json
  tags   = local.required-tags
}

resource "aws_iam_role_policy_attachment" "plexus-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.plexus-cluster.name
}

resource "aws_iam_role_policy_attachment" "plexus-cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.plexus-cluster.name
}

resource "aws_security_group" "plexus-cluster" {
  name        = "${local.name-prefix}-terraform-eks-plexus-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = local.tr-vpc-id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all outbound traffic to any destination"
  }

  tags = merge(local.required-tags, { Name = "${local.name-prefix}-terraform-eks-plexus-cluster" })

}

resource "aws_security_group_rule" "plexus-nat-ips-ingress" {
  count             = var.public-nlb-enable ? 1 : 0
  description       = "Allow ingress from NAT IPs"
  from_port         = 30000
  to_port           = 32767
  protocol          = "tcp"
  security_group_id = data.aws_security_group.plexus-cluster-eks-generated-sg.id
  cidr_blocks       = local.nat-ips-list
  type              = "ingress"
}

resource "aws_security_group_rule" "plexus-private-access" {
  count             = var.endpoint_public_access ? 0 : 1
  description       = "Allow APIServer access from private network"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = data.aws_security_group.plexus-cluster-eks-generated-sg.id
  cidr_blocks       = ["10.0.0.0/8"]
  type              = "ingress"
}

resource "aws_eks_cluster" "plexus" {
  name                      = local.cluster-name
  role_arn                  = aws_iam_role.plexus-cluster.arn
  enabled_cluster_log_types = ["api", "controllerManager", "scheduler", "audit", "authenticator"]
  version                   = var.eks-cluster-version

  vpc_config {
    security_group_ids      = [aws_security_group.plexus-cluster.id]
    subnet_ids              = concat(local.public-subnets, local.private-subnets)
    endpoint_private_access = true
    endpoint_public_access  = var.endpoint_public_access
    public_access_cidrs     = local.public_access_cidrs
  }

  encryption_config {
    provider {
      key_arn = local.aws-kms-key-arn
    }
    resources = ["secrets"]
  }

  tags = merge(local.required-tags, { Name = local.cluster-name }, { Region = var.aws-region })

  depends_on = [
    aws_iam_role_policy_attachment.plexus-cluster-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.plexus-cluster-AmazonEKSServicePolicy,
    aws_cloudwatch_log_group.eks-log-group # So terraform creates the log group, not EKS
  ]
}

data "external" "current-context" {
  program = ["bash", "${path.module}/get-kubecontext.sh"]
}

resource "null_resource" "setup-kubeconfig" {

  count = data.external.current-context.result["context"] != "arn:aws:eks:${var.aws-region}:${var.account-number}:cluster/${aws_eks_cluster.plexus.name}" ? 1 : 0

  triggers = {
    always = timestamp()
  }

  provisioner "local-exec" {
    command = <<EOT
      eval $(aws sts assume-role --role-arn ${local.child-role} --role-session-name codebuild | jq -r '.Credentials | "export AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey)\nexport AWS_SESSION_TOKEN=\(.SessionToken)\n"')
      aws eks --region ${var.aws-region} update-kubeconfig --name ${aws_eks_cluster.plexus.name}
    EOT
  }
}

data "aws_security_group" "plexus-cluster-eks-generated-sg" {
  id = aws_eks_cluster.plexus.vpc_config[0].cluster_security_group_id
}

/*
 See https://docs.aws.amazon.com/eks/latest/userguide/sec-group-reqs.html
 for details on the EKS-generated SG
*/
resource "null_resource" "tag-eks-generated-cluster-sg" {
  depends_on = [aws_eks_cluster.plexus]
  triggers = {
    eks-generated-sg = data.aws_security_group.plexus-cluster-eks-generated-sg.id
  }

  provisioner "local-exec" {
    command = <<EOT
      eval $(aws sts assume-role --role-arn ${local.child-role} --role-session-name codebuild | jq -r '.Credentials | "export AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey)\nexport AWS_SESSION_TOKEN=\(.SessionToken)\n"')
      ${join(";", [for k, v in merge(local.required-tags, { Name = data.aws_security_group.plexus-cluster-eks-generated-sg.name }) :
"aws --region ${var.aws-region} ec2 create-tags --resources ${data.aws_security_group.plexus-cluster-eks-generated-sg.id} --tags Key='${k}',Value='${v}'"])}
    EOT
}
}

output "eks-generated-cluster-sg-id" {
  value = data.aws_security_group.plexus-cluster-eks-generated-sg.id
}

resource "aws_cloudwatch_log_group" "eks-log-group" {
  name              = "/aws/eks/${local.cluster-name}/cluster"
  retention_in_days = 90
  tags              = merge(local.required-tags, { Name = local.cluster-name })
}
