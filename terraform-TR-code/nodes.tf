locals {
  userdata-blue = replace(local.plexus-node-userdata,
    "$${color-code}", "blue"
  )
  userdata-green = replace(local.plexus-node-userdata,
    "$${color-code}", "green"
  )
}

resource "aws_launch_template" "plexus-blue" {
  iam_instance_profile {
    name = var.instance-profile-name
  }

  image_id      = local.blue-ami-id
  instance_type = var.instance-type
  name_prefix   = "${local.name-prefix}-blue-worker-nodes"

  /*
    Every update to the launch template creates a new version. We need to set
    the default version to the one we just applied. First available in aws 2.70.0
   */
  update_default_version = true

  lifecycle {
    create_before_destroy = true
  }

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size = var.ebs-volume-size
      volume_type = "gp3"
      encrypted   = true
      iops        = var.ebs-iops
      throughput  = var.ebs-throughput
      kms_key_id  = var.aws-kms-key-arn
    }
  }

  network_interfaces {
    security_groups       = concat([var.node-sg-id], [var.eks-sg-id], var.ingress-sgs, var.fsx-sgs)
    delete_on_termination = true
  }

  tag_specifications {
    resource_type = "volume"
    tags          = var.required-tags
  }

  user_data = base64encode(local.userdata-blue)
}

resource "aws_launch_template" "plexus-green" {
  iam_instance_profile {
    name = var.instance-profile-name
  }

  image_id      = local.green-ami-id
  instance_type = var.instance-type
  name_prefix   = "${local.name-prefix}-green-worker-nodes"

  /*
    Every update to the launch template creates a new version. We need to set
    the default version to the one we just applied. First available in aws 2.70.0
   */
  update_default_version = true

  lifecycle {
    create_before_destroy = true
  }

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size = var.ebs-volume-size
      volume_type = "gp3"
      encrypted   = true
      iops        = var.ebs-iops
      throughput  = var.ebs-throughput
      kms_key_id  = var.aws-kms-key-arn
    }
  }

  network_interfaces {
    security_groups       = concat([var.node-sg-id], [var.eks-sg-id], var.ingress-sgs, var.fsx-sgs)
    delete_on_termination = true
  }

  tag_specifications {
    resource_type = "volume"
    tags          = var.required-tags
  }

  user_data = base64encode(local.userdata-green)
}

resource "aws_autoscaling_group" "plexus-blue" {
  launch_template {
    id      = aws_launch_template.plexus-blue.id
    version = "$Latest"
  }
  max_size            = var.asg-max-size
  min_size            = var.asg-min-size
  desired_capacity    = var.active-asg == "blue" ? min(var.asg-max-size, var.asg-initial-size) : 0
  name                = "${aws_launch_template.plexus-blue.name}-asg"
  suspended_processes = ["AZRebalance"]
  vpc_zone_identifier = var.worker-node-subnets
  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingCapacity",
    "GroupPendingInstances",
    "GroupStandbyCapacity",
    "GroupStandbyInstances",
    "GroupTerminatingCapacity",
    "GroupTerminatingInstances",
    "GroupTotalCapacity",
    "GroupTotalInstances",
  ]

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [desired_capacity]
  }

  # ASG tags are snowflakes. They need special properties, hence the code below
  # Workaround for substitution tagged asset ID also included
  # Requires following custom permission on the PowerUser2:
  # {
  #     "Version": "2012-10-17",
  #     "Statement": [{
  #         "Effect": "Allow",
  #         "Action": [
  #             "autoscaling:CreateOrUpdateTags"
  #         ],
  #         "Resource": "*"
  #     }]
  # }
  # tags = concat([for k, v in var.required-tags : k == "tr:application-asset-insight-id" ? { key = k, value = var.tag-asset-id, propagate_at_launch = true } : { key = k, value = v, propagate_at_launch = true }], local.asg-tags, local.asg-blue-tags)
  # tags = concat([for k, v in var.required-tags : { key = k, value = v, propagate_at_launch = true }], local.asg-tags, local.asg-blue-tags)
  dynamic "tag" {
    for_each = var.required-tags
    content {
      key                 = tag.key
      value               = tag.key == "tr:client-id" ? var.client-id : tag.value
      propagate_at_launch = true
    }
  }
  dynamic "tag" {
    for_each = local.asg-tags
    content {
      key                 = tag.value.key
      value               = tag.value.value
      propagate_at_launch = tag.value.propagate_at_launch
    }
  }
  dynamic "tag" {
    for_each = local.asg-blue-tags
    content {
      key                 = tag.value.key
      value               = tag.value.value
      propagate_at_launch = tag.value.propagate_at_launch
    }
  }
  dynamic "tag" {
    for_each = var.labels
    content {
      key                 = "k8s.io/cluster-autoscaler/node-template/label/${tag.key}"
      value               = tag.value
      propagate_at_launch = true
    }
  }
  dynamic "tag" {
    for_each = var.taints
    content {
      key                 = "k8s.io/cluster-autoscaler/node-template/taint/${tag.value.key}"
      value               = "${tag.value.value}:${tag.value.effect}"
      propagate_at_launch = true
    }
  }
}

resource "aws_autoscaling_group" "plexus-green" {
  launch_template {
    id      = aws_launch_template.plexus-green.id
    version = "$Latest"
  }
  max_size            = var.asg-max-size
  min_size            = var.asg-min-size
  desired_capacity    = var.active-asg == "green" ? min(var.asg-max-size, var.asg-initial-size) : 0
  suspended_processes = ["AZRebalance"]
  name                = "${aws_launch_template.plexus-green.name}-asg"
  vpc_zone_identifier = var.worker-node-subnets
  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingCapacity",
    "GroupPendingInstances",
    "GroupStandbyCapacity",
    "GroupStandbyInstances",
    "GroupTerminatingCapacity",
    "GroupTerminatingInstances",
    "GroupTotalCapacity",
    "GroupTotalInstances",
  ]

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [desired_capacity]
  }

  # ASG tags are snowflakes. They need special properties, hence the code below
  # tags = concat([for k, v in var.required-tags : k == "tr:application-asset-insight-id" ? { key = k, value = var.tag-asset-id, propagate_at_launch = true } : { key = k, value = v, propagate_at_launch = true }], local.asg-tags, local.asg-green-tags)
  # tags = concat([for k, v in var.required-tags : { key = k, value = v, propagate_at_launch = true }], local.asg-tags, local.asg-green-tags)
  dynamic "tag" {
    for_each = var.required-tags
    content {
      key                 = tag.key
      value               = tag.key == "tr:client-id" ? var.client-id : tag.value
      propagate_at_launch = true
    }
  }
  dynamic "tag" {
    for_each = local.asg-tags
    content {
      key                 = tag.value.key
      value               = tag.value.value
      propagate_at_launch = tag.value.propagate_at_launch
    }
  }
  dynamic "tag" {
    for_each = local.asg-green-tags
    content {
      key                 = tag.value.key
      value               = tag.value.value
      propagate_at_launch = tag.value.propagate_at_launch
    }
  }
  dynamic "tag" {
    for_each = var.labels
    content {
      key                 = "k8s.io/cluster-autoscaler/node-template/label/${tag.key}"
      value               = tag.value
      propagate_at_launch = true
    }
  }
  dynamic "tag" {
    for_each = var.taints
    content {
      key                 = "k8s.io/cluster-autoscaler/node-template/taint/${tag.value.key}"
      value               = "${tag.value.value}:${tag.value.effect}"
      propagate_at_launch = true
    }
  }
}


locals {
  asg-tags = [
    {
      key                 = "kubernetes.io/cluster/${var.cluster.id}"
      value               = "owned"
      propagate_at_launch = true
    },
    {
      key                 = var.off-hours-opt-in ? "tr:scheduled-offhours" : "onhours"
      value               = "tz=et"
      propagate_at_launch = false
    },
    {
      key                 = "datadog-account"
      value               = var.datadog-account
      propagate_at_launch = true
    },
    {
      key                 = "k8s.io/cluster-autoscaler/enabled"
      value               = "true"
      propagate_at_launch = true
    },
    {
      key                 = "k8s.io/cluster-autoscaler/${var.cluster.id}"
      value               = "owned"
      propagate_at_launch = true
    }
  ]
  asg-blue-tags = [
    {
      key                 = "Name"
      value               = "${aws_launch_template.plexus-blue.name}-asg"
      propagate_at_launch = true
    },
    {
      key                 = "k8s.io/cluster-autoscaler/node-template/label/plexus-node-group"
      value               = "blue"
      propagate_at_launch = true
    },
    {
      key                 = "k8s.io/cluster-autoscaler/node-template/resources/ephemeral-storage"
      value               = "${aws_launch_template.plexus-blue.block_device_mappings[0].ebs[0].volume_size - 1}G"
      propagate_at_launch = true
    }
  ]
  asg-green-tags = [
    {
      key                 = "Name"
      value               = "${aws_launch_template.plexus-green.name}-asg"
      propagate_at_launch = true
    },
    {
      key                 = "k8s.io/cluster-autoscaler/node-template/label/plexus-node-group"
      value               = "green"
      propagate_at_launch = true
    },
    {
      key                 = "k8s.io/cluster-autoscaler/node-template/resources/ephemeral-storage"
      value               = "${aws_launch_template.plexus-green.block_device_mappings[0].ebs[0].volume_size - 1}G"
      propagate_at_launch = true
    }
  ]

  # EKS currently documents this required userdata for EKS worker nodes to
  # properly configure Kubernetes applications on the EC2 instance.
  # We utilize a Terraform local here to simplify Base64 encoding this
  # information into the AutoScaling Launch Configuration.
  # More information: https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/amazon-eks-nodegroup.yaml

  # bootstrap: https://github.com/awslabs/amazon-eks-ami/blob/master/files/bootstrap.sh

  plexus-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
sed -e '/net.ipv4.ip_forward/ s/^#*/#/' -i /etc/sysctl.conf
sysctl --system
sed -i 's/evictionHard": {"memory.available": "100Mi"/evictionHard": {"memory.available": "'${var.resource-reserve["mem-eviction"]}'"/' /etc/eks/bootstrap.sh
sed -i "/writes kubeReserved and evictionHard to the kubelet-config using the amount of CPU and memory to be reserved/a echo \"\$(jq '\. \+\= {\"systemReserved\": {\"cpu\": \"${var.resource-reserve["cpu-reserved"]}\", \"memory\": \"${var.resource-reserve["mem-reserved"]}\"}}' \$KUBELET_CONFIG)\" > \$KUBELET_CONFIG" /etc/eks/bootstrap.sh
/etc/eks/bootstrap.sh --apiserver-endpoint '${var.cluster.endpoint}' --b64-cluster-ca '${var.cluster.certificate_authority[0].data}' --container-runtime containerd --kubelet-extra-args '${local.labels-arg}${local.taint-arg}' '${var.cluster.id}'
USERDATA

  name-prefix  = "a${var.asset-id}-${var.name}-${terraform.workspace}"
  blue-ami-id  = var.blue-ami-id
  green-ami-id = var.green-ami-id
  labels-arg   = join(",", concat(["--node-labels=plexus-node-group=$${color-code}"], [for k, v in var.labels : "${k}=${v}"]))
  taint-arg    = length(var.taints) > 0 ? " --register-with-taints=${join(",", [for k, v in var.taints : "${v.key}=${v.value}:${v.effect}"])}" : ""

}

output "blue-asg" {
  value = aws_autoscaling_group.plexus-blue
}

output "green-asg" {
  value = aws_autoscaling_group.plexus-green
}

output "node-group" {
  value = { label = var.labels.node-group, name = var.name }
}

output "overprovisioner" {
  value = { replicas = var.overprovisioner-replicas }
}

output "taints" {
  value = var.taints
}
