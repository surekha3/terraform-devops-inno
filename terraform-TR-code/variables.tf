#
# Variables Configuration
#

/*
  This variable is used to list the NAT Gateway elastic IPs, to allow incoming
  traffic from various AWS accounts
*/
variable "nat-gateway-elastic-ips" {
  type = list(string)
  default = [
    //New nonprod CICD
    "54.83.196.42/32",
    "35.172.82.42/32",
    "3.81.241.93/32",
    // NAT gateway firewall migration - preprod us-east-1
    "18.208.8.210/32",
    "184.73.121.61/32"
  ]
}

data "aws_vpcs" "tr-vpc" {
  tags = {
    Name = "tr-vpc-1"
  }
}

data "aws_secretsmanager_secret_version" "plexus-secret" {
  secret_id = local.secret-id
}

variable "hosted-zones-suffixes" {
  type    = string
  default = "1554"
}

# this is additional IAM role to add to the k8s admin group, not the actual owner
variable "admin-user" {
  type    = string
  default = "PlexusClusterAdmin"
}

variable "ccoe-environment-type-tag-value" {
  type    = string
  default = "PRE-PRODUCTION"
}

variable "webcorp-cidr" {
  default = [
    //private
    //"10.0.0.0/8",
    //India	Delhi
    //"115.111.69.62/32",
    //India	Delhi
    //"118.185.221.226/32",
    //India	Mumbai
    //"118.185.228.130/32",
    //India	Mumbai
    "121.241.0.66/32",
    //US	IOWA // Zscaler
    "13.86.118.242/32",
    //US	NewYork
    "159.220.0.0/16",
    //US	NewYork
    "159.42.0.0/16",
    //US	Minnesota
    "163.231.0.0/16",
    //US	Kansas
    "164.57.0.0/16",
    //US	Kansas
    "167.68.0.0/16",
    //US	Minnesota
    "192.152.30.0/23",
    //Sweden	Stockholm
    //"192.165.208.0/20",
    //US	Kansas
    "198.179.137.0/24",
    //US	Massachusetts
    "198.80.128.0/18",
    //US	NewYork
    "199.224.128.0/17",
    //Netherlands	Amsterdam //ZScaler
    "20.50.193.234/32",
    //Australia	Sydney //ZScaler
    //"20.53.112.114/32",
    //Australia	Sydney //ZScaler
    //"20.53.112.75/32",
    //Australia	Sydney
    //"203.13.166.16/28",
    //Australia
    //"203.191.132.0/24",
    //Canada	Toronto
    "204.209.111.0/24",
    //Canada	Toronto
    "205.210.47.0/24",
    //US	Wisconsin
    "206.197.182.0/24",
    //US	NewYork
    "3.213.111.74/32",
    //US	Virginia
    "3.232.174.206/32",
    //US	Arizona
    "34.198.98.0/24",
    //GB	London //ZScaler
    "40.81.144.231/32",
    //GB	London //ZScaler
    "51.11.177.77/32",
    //Netherlands	Amsterdam //ZScaler
    "51.136.106.27/32",
    //Singapore	Singapore //ZScaler
    "52.139.216.47/32",
    //US	IOWA //Zscaler
    "52.165.81.100/32",
    //Singapore	singapore //ZScaler
    "52.237.89.169/32",
    //US	California //ZScaler
    "52.241.140.182/32",
    //US	California //ZScaler
    "52.241.141.187/32",
    //Ireland	Dublin
    //"84.18.160.0/19"
  ]
  type = list(string)
}

variable "asg-initial-size" {
  default = 6
  type    = number
}

variable "vpc-cni-addon-version" {
  type    = string
  default = "v1.12.0-eksbuild.1"
  validation {
    condition     = contains(["default", "latest"], var.vpc-cni-addon-version) || length(regexall("^v(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)-eksbuild.*", var.vpc-cni-addon-version)) > 0
    error_message = "The vpc-cni-addon-version must be 'default', 'value' or a specific version of the vpc-cni EKS addon."
  }
}

variable "kube-proxy-addon-version" {
  type    = string
  default = "v1.22.11-eksbuild.2"
  validation {
    condition     = contains(["default", "latest"], var.kube-proxy-addon-version) || length(regexall("^v(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)-eksbuild.*", var.kube-proxy-addon-version)) > 0
    error_message = "The kube-proxy-addon-version must be 'default', 'value' or a specific version of the kube-proxy EKS addon."
  }
}

variable "coredns-addon-version" {
  type        = string
  default     = "v1.8.7-eksbuild.1"
  description = "Either specify the exact coredns addon version, or `default` or `latest` to automatically use recommended versions from AWS"
  validation {
    condition     = contains(["default", "latest"], var.coredns-addon-version) || length(regexall("^v(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)-eksbuild.*", var.coredns-addon-version)) > 0
    error_message = "The coredns-addon-version must be 'default', 'value' or a specific version of the coredns EKS addon."
  }
}

variable "ebs-csi-addon-version" {
  type        = string
  default     = "v1.15.0-eksbuild.1"
  description = "Either specify the exact ebs-csi addon version, or `default` or `latest` to automatically use recommended versions from AWS"
  validation {
    condition     = contains(["default", "latest"], var.ebs-csi-addon-version) || length(regexall("^v(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)-eksbuild.*", var.ebs-csi-addon-version)) > 0
    error_message = "The ebs-csi-addon-version must be 'default', 'value' or a specific version of the ebs-csi EKS addon."
  }
}

variable "datadog-account" {
  type        = string
  default     = "tr-eps-preprod"
  description = "The name of your datadog account"
}

variable "eks-cluster-version" {
  type        = string
  description = "The version of AWS EKS to use for the cluster."
  default     = "1.23"
  validation {
    condition     = length(regexall("^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)$", var.eks-cluster-version)) > 0
    error_message = "The EKS version must be a two unit semantic version."
  }
}

variable "asset-id" {
  type        = number
  default     = 206215
  description = "The Asset Insights ID for the application."
}

variable "client-id" {
  type        = number
  default     = null
  description = "The Asset Insights ID for the application."
}

variable "hosted-zone-private-dns" {
  type    = string
  default = "aws-int.thomsonreuters.com"
}

variable "hosted-zone-public-dns" {
  type    = string
  default = "aws.thomsonreuters.com"
}

variable "account-number" {
  type    = number
  default = 674700511554
}

variable "ha-enabled" {
  type        = bool
  description = "Enable high availability, which scales the number of available replicas for some deployments."
  default     = false
}

variable "aws-region" {
  type        = string
  description = "The region in which to build the cluster."
  default     = "us-east-1"
}

variable "enable-sysdig" {
  type        = bool
  description = "Enable Sysdig on the cluster."
  default     = true
}

variable "off-hours-opt-in" {
  type        = bool
  default     = true
  description = "The EC2 instances only operate from 7am-7pm EST, Monday-Friday, unless set to false"
}

variable "echo-client-enabled" {
  type        = bool
  default     = true
  description = "Installs the echo-client deployment on the cluster when set to true"
}

variable "echo-server-enabled" {
  type        = bool
  default     = true
  description = "Installs the echo-server deployment on the cluster when set to true"
}

variable "echo-client-use-public" {
  type        = bool
  default     = false
  description = "Set echo-client to ping echo-server's public endpoint. Only has effect when echo-client-enabled is set to true"
}

variable "private-nlb-enable" {
  type        = bool
  default     = true
  description = "Enable the private NLB"
}

variable "public-nlb-enable" {
  type        = bool
  default     = true
  description = "Enable the public NLB"
}

variable "datadog-apm-enabled" {
  type        = bool
  default     = false
  description = "Enables Datadog APM collection"
}

## https://docs.datadoghq.com/tracing/setup_overview/open_standards/otlp_ingest_in_the_agent
variable "datadog-otlp-grpc" {
  type        = bool
  default     = false
  description = "Enabled Datadog OLTP Trace Ingestion over gRPC"
}

variable "datadog-otlp-http" {
  type        = bool
  default     = false
  description = "Enabled Datadog OLTP Trace Ingestion over http"
}

variable "datadog-site" {
  type        = string
  default     = "datadoghq.com"
  description = "Alternate Datadog site URL"
}

# Note: The Gremlin Team Id, Team secret needs to be added to the secrets before enabling gremlin as they are required for the installation.
variable "gremlin-enabled" {
  type        = bool
  default     = false
  description = "Install Gremlin on cluster"
}

variable "public-zone-enabled" {
  type        = bool
  default     = true
  description = "Enable use of public route53 zones"
}

variable "private-zone-enabled" {
  type        = bool
  default     = true
  description = "Enable use of private route53 zones"
}

variable "gatekeeper-disableValidatingWebhook" {
  type        = bool
  default     = false
  description = "Disable the gatekeeper validating webhook"
}

variable "netapp-trident-config" {
  type = object({
    enabled = bool
    backends = map(object({
      storageDriver = string
      managementLIF = string
      svm           = string
      usernameKey   = string
      passwordKey   = string
    }))
  })
  description = "Object containing configuration for NetApp trident"
  default = {
    enabled  = false
    backends = {}
  }
}

variable "fsx-attach-sg-main" {
  type        = bool
  default     = false
  description = "Attach the FSx SG to nodes in the main node group"
}

variable "endpoint_public_access" {
  type        = bool
  default     = true
  description = "Whether the Amazon EKS public API server endpoint is enabled"
}

# Values for memory/cpu/storage are defaults for overprovisioner resource requests/limits (they are identical)
variable "overprovisioner" {
  type = object({
    memory  = string
    cpu     = string
    storage = string
  })
  description = "Sets helm template values for the overprovisioner deployment"
  default = {
    memory  = "256Mi",
    cpu     = "200m",
    storage = "5Mi"
  }
}
variable "overprovisioner-replicas" {
  type    = number
  default = 3
}

# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volume-types.html
variable "ebs-throughput" {
  type        = number
  default     = 125
  description = "Max throughput of EBS volumes in MiB/s"
}

# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volume-types.html
variable "ebs-iops" {
  type        = number
  default     = 3000
  description = "Max IOPS of EBS volumes (16 KiB I/O)"
}

variable "secondary-private-subnets-enabled" {
  type        = bool
  default     = false
  description = "Enabling this value will create worker nodes in secondary private subnets(cgnat-subnets)"
}

variable "secondary-subnets" {
  type        = list(string)
  default     = null
  description = "Subnets to override the default cgnat/secondary subnets from IHN"
}

variable "private-subnets" {
  type        = list(string)
  default     = null
  description = "Subnets to override the default private subnets from IHN"
}

variable "public-subnets" {
  type        = list(string)
  default     = null
  description = "Subnets to override the default public subnets from IHN"
}

variable "resource-reserve" {
  type = object({
    mem-eviction = string
    cpu-reserved = string
    mem-reserved = string
  })
  default = {
    mem-eviction = "500Mi"
    cpu-reserved = "400m"
    mem-reserved = "800Mi"
  }
  description = "Values to generate plexus-node-userdata in the generic/nodes module"
}

variable "cname-prefix" {
  type        = string
  default     = ""
  description = "Enabling this value will create a CNAME records in Route53 with the given prefix"
}

variable "global-cname-prefix" {
  type        = string
  default     = ""
  description = "Enabling this value will create a global CNAME record in Route53 with the given prefix"
}

variable "enable-private-ingress-port-80" {
  type        = bool
  default     = false
  description = "Enable private ingress port 80 for istio"
}

variable "cname-weight" {
  type        = number
  default     = 0
  description = "Weight of the weighted routing policy cname"
}

variable "alt-secret-name" {
  type        = string
  default     = null
  description = "Set to use alternative secrets for different clusters in the same account. Not used by default"
}

variable "active-asg" {
  type        = string
  description = "Set the active asg group: 'green' or 'blue'"
  validation {
    condition     = contains(["green", "blue"], var.active-asg)
    error_message = "The active-asg must be 'green' or 'blue'."
  }
}

variable "app-namespaces" {
  type        = list(string)
  default     = []
  description = "Additional k8s namespaces for app teams"
}

variable "asg-blue" {
  type = object({
    instance-type = string
    size-min      = number
    size-max      = number
    ami-id-key    = string
  })
  default = {
    "instance-type" : "t3.xlarge",
    "size-min" : 0,
    "size-max" : 6,
    "ami-id-key" : "tr-gold-ami-new"
  }
}

variable "asg-green" {
  type = object({
    instance-type = string
    size-min      = number
    size-max      = number
    ami-id-key    = string
  })
  default = {
    "instance-type" : "t3.xlarge",
    "size-min" : 0,
    "size-max" : 6,
    "ami-id-key" : "tr-gold-ami-new"
  }
}

variable "karpenter-node-templates" {
  type = list(object({
    name           = string
    include-fsx-sg = bool
    blockDeviceMapping = object({
      volume-type = string
      volume-size = string
      iops        = number
      throughput  = number
    })
  }))

  default = [
    {
      name           = "default"
      include-fsx-sg = false
      blockDeviceMapping = {
        volume-type = "gp3"
        volume-size = "60Gi"
        iops        = 3000
        throughput  = 125
      }
    }
  ]
}

variable "karpenter-provisioners" {
  type = list(object({
    name                       = string
    template-name              = string
    instance-types             = list(string)
    instance-reservation-types = list(string)
    instance-size              = list(string)
    instance-category          = list(string)
    instance-generation        = list(string)
    cpuLimitvCPU               = number
    taints = list(object({
      key    = string
      value  = string
      effect = string
    }))
    labels = list(object({
      key   = string
      value = string
    }))
  }))

  default = [{
    name                       = "default"
    template-name              = "default"
    cpuLimitvCPU               = 48
    instance-types             = ["c5.2xlarge"]
    labels                     = []
    taints                     = []
    instance-reservation-types = ["on-demand"]
    instance-size              = ["2xlarge"]
    instance-category          = ["c", "m"]
    instance-generation        = ["4"]
  }]
}

variable "cluster-autoscaler-max-nodes-total" {
  type    = number
  default = 999
}

variable "node-drainer" {
  type = object({
    enabled = bool
  })
  default = {
    enabled = false
  }
  description = "Toggle automated node-drainer"
}

variable "oidc-target-accounts" {
  type        = list(string)
  default     = []
  description = "OIDC target account names. Needed to support cross account roles."
}

variable "cross-acct-inbound-sgs" {
  type = list(object({
    source-security-group-id = string
    description              = string
  }))
  default     = []
  description = "Creates additional AWS security group rules for allowing ingress to the cluster"
}

# Note: these CIDRs are dangerous, taking down the firewall in a given account
variable "cross-region-inbound-cidrs" {
  type = list(object({
    cidr        = string
    ports       = list(number)
    description = string
  }))
  default     = []
  description = "CIDRs for cross region connectivity"
}

variable "additional-ingress-ports" {
  type = list(object({
    name       = string
    port       = number
    targetPort = optional(number)
  }))
  default     = []
  description = "Additional ingress ports"
}

variable "efs-pvs" {
  type = list(object({
    name         = string
    fs-id        = string
    storage      = string
    access-modes = list(string)
  }))
  default     = []
  description = "Settings for EFS persistent volumes"
}

# Keep the name as an entry in the object as the name is unknown and therefore cannot be a object key
variable "secondary-node-groups" {
  type = list(object({
    name            = string
    client-id       = string
    ebs-throughput  = number
    ebs-iops        = number
    ebs-volume-size = number
    asg-size-min    = number
    asg-size-max    = number
    instance-type   = string
    taints = list(object({
      key    = string
      value  = string
      effect = string
    }))
    labels = object({
      modularized = bool
      node-group  = string
    })
  }))
  default     = []
  description = "Secondary node groups create an additional ASG with the given settings"
}

variable "asg-per-az" {
  type = object({
    instance-type = string
    labels        = optional(map(string), {})
    taints = optional(list(object({
      key    = string
      value  = string
      effect = string
    })), [])
  })
  default = {
    instance-type = ""
  }
  description = "Specify settings for AZ specific ASG"
}

variable "kubernetes-storage-classes" {
  type = list(object({
    name       = string
    type       = string
    fstype     = string
    iops       = string
    throughput = string
    encrypted  = optional(bool, true)
  }))
  default     = []
  description = "Create storage classes for EBS"
}

variable "aws-auth" {
  type        = string
  description = "A config map in K8s to map IAM roles to K8s groups or users"
}

variable "public-tls-certs" {
  type = map(
    object({
      secret-key-name = string
      secret-name     = optional(string)
    })
  )
  default = {}
}

variable "private-tls-certs" {
  type = map(
    object({
      secret-key-name = string
      secret-name     = optional(string)
    })
  )
  default = {}
}

variable "additional-secret-name" {
  type        = string
  default     = null
  description = "Temporary solution to reaching maximum Secrets Manager secret size"
}

locals {
  cluster-name    = "${local.asset-id-prefix}-${terraform.workspace}-plexus-cluster"
  tr-vpc-id       = sort(data.aws_vpcs.tr-vpc.ids)[0]
  asset-id-prefix = "a${var.asset-id}"
  client-id       = coalesce(tostring(var.client-id), tostring(var.asset-id))
  # name for cluster shouldn't include "plexus". should include region. "tax" is fine. discuss in meeting today.
  name-prefix         = "${local.asset-id-prefix}-${terraform.workspace}"
  environment-type    = var.ccoe-environment-type-tag-value
  child-role          = "arn:aws:iam::${var.account-number}:role/${local.asset-id-prefix}-terraform-target"
  public_access_cidrs = var.endpoint_public_access ? distinct(concat(var.webcorp-cidr, local.nat-ips-list)) : null

  required-tags = {
    "tr:resource-owner"               = "API-Platform-Service-Mesh@thomson.com"
    "tr:environment-type"             = local.environment-type
    "tr:application-asset-insight-id" = tostring(var.asset-id)
    "tr:terraform-workspace"          = terraform.workspace
    "tr:client-id"                    = local.client-id
  }

  // This local defines the rules per env to allow inbound traffic from the AWS accounts
  nat-ips-list = var.nat-gateway-elastic-ips

  // add suffix in cname for snowflake only
  cname-suffix = var.cname-prefix == "plexus-snowflake" ? "-${terraform.workspace}" : ""

  // echo-server settings in the settings files
  private_cname        = var.cname-prefix != "" ? "${var.cname-prefix}${local.cname-suffix}.${var.hosted-zones-suffixes}.${var.hosted-zone-private-dns}" : ""
  public_cname         = var.cname-prefix != "" && local.public-hosted-zone-id != null ? "${var.cname-prefix}${local.cname-suffix}.${var.hosted-zones-suffixes}.${var.hosted-zone-public-dns}" : ""
  global_cname         = var.global-cname-prefix != "" ? "${var.global-cname-prefix}${local.cname-suffix}.${var.hosted-zones-suffixes}.${var.hosted-zone-public-dns}" : ""
  cname_weighed_policy = var.cname-weight != 0 ? true : false

  // Use alternative secrets for different clusters in the same account
  secret-alt  = var.alt-secret-name != null
  secret-name = var.alt-secret-name != null ? "${local.asset-id-prefix}-${var.alt-secret-name}" : "${local.asset-id-prefix}-tr-eps-plexus"

  /*
     allows us to consistently update AMI versions, while keeping this configurable per cluster
     the keys in this map are arbitrarily named, as long as they match the value in the settings file
  */
  ami-settings-map = {
    tr-gold-ami-old = {
      # TR Golden AMI - EKS123 - Feb 16
      us-east-1      = "ami-0a87f6df5c231407c"
      us-east-2      = "ami-0e5d04a524b28f11f"
      us-west-2      = "ami-097636d5f85c2f518"
      eu-west-1      = "ami-053ec335c8c9eba48"
      eu-central-1   = "ami-02c3cf2c0a42adc72"
      ap-southeast-1 = "ami-07868559a430b9bab"
    }
    tr-gold-ami-new = {
      # TR Golden AMI - EKS122 - March 15
      us-east-1      = "ami-04e98d1ace6d48a0c"
      us-east-2      = "ami-03a7f0b7c2987092e"
      us-west-2      = "ami-0a6546b1ebb0c00f0"
      eu-west-1      = "ami-0fc53338f44164587"
      eu-central-1   = "ami-09b5852aa0c6b43a4"
      ap-southeast-1 = "ami-0543dc1031e9554f1"
    }
  }

  blue-ami-id      = local.ami-settings-map[var.asg-blue.ami-id-key][var.aws-region]
  green-ami-id     = local.ami-settings-map[var.asg-green.ami-id-key][var.aws-region]
  karpenter-ami-id = local.ami-settings-map["tr-gold-ami-new"][var.aws-region]

  ns-istio-access_roleName              = "plexus-ns-istio-access"
  secret-provider-class-access-roleName = "plexus-secret-provider-class-access"
  istioctl-version                      = "1-16-3"

  istio-hub = "gcr.io/istio-release"

  # For caching non-istio images required for our cluster
  # (work scheduled for future SVM-1188)
  asg-initial-size     = var.asg-initial-size
  helm_release_timeout = 600

  public-subnets         = coalesce(var.public-subnets, data.aws_subnets.public.ids)
  private-subnets        = coalesce(var.private-subnets, data.aws_subnets.private.ids)
  secondary-subnets      = coalesce(var.secondary-subnets, data.aws_subnets.cgnat.ids)
  worker-node-subnets    = var.secondary-private-subnets-enabled ? local.secondary-subnets : local.private-subnets
  secret-id              = local.secret-alt ? tolist(data.aws_secretsmanager_secrets.secret-id-alt.arns)[0] : data.aws_secretsmanager_secret.secret-id.id
  secret-add-id          = var.additional-secret-name != null ? tolist(data.aws_secretsmanager_secrets.secret-id-additional.arns)[0] : null
  ingress-sgs            = join(",", [data.aws_security_group.webcorp.id, data.aws_security_group.cloudfront-g-https.id, data.aws_security_group.cloudfront-r-https.id, data.aws_security_group.datadog-synthetic.id, data.aws_security_group.route53chk-https.id])
  nlb-nodeport-sgs       = [data.aws_security_group.cloudfront-g-nodeport.id, data.aws_security_group.cloudfront-r-nodeport.id, data.aws_security_group.route53chk-nodeport.id, data.aws_security_group.datadog-synthetic-nodeport.id]
  aws-kms-key-arn        = data.aws_kms_alias.aws-kms-key-arn.target_key_arn
  private-hosted-zone-id = one(data.aws_route53_zone.private[*].id)
  public-hosted-zone-id  = one(data.aws_route53_zone.public[*].id)
  admin-user             = "arn:aws:iam::${var.account-number}:role/${local.asset-id-prefix}-${var.admin-user}"
  permission-boundary    = "arn:aws:iam::${var.account-number}:policy/tr-permission-boundary"


  cluster-identity-oidc-issuer = trimprefix(aws_cloudformation_stack.oidc-provider.outputs["IdpArn"], "arn:aws:iam::${var.account-number}:oidc-provider/")

  # Note: these CIDRs are dangerous, taking down the firewall in a given account
  cross-region-inbound-cidrs-list = flatten([
    for rule in var.cross-region-inbound-cidrs : [
      for port in rule.ports : {
        cidr : rule.cidr
        port : port
        description : rule.description
      }
    ]
  ])

  base-taint-tolerations = [
    { key = "dedicated", operator = "Exists", effect = "NoSchedule" },
    { key = "DeletionCandidateOfClusterAutoscaler", operator = "Exists" },
    { key = "ToBeDeletedByClusterAutoscaler", operator = "Exists" },
    { key = "inactive", operator = "Exists", effect = "NoSchedule" }
  ]

  all-taint-tolerations = concat(local.base-taint-tolerations, var.asg-per-az.taints)

  taint-tolerations = <<-EOT
    - key: "dedicated"
      operator: "Exists"
      effect: "NoSchedule"
    - key: "DeletionCandidateOfClusterAutoscaler"
      operator: "Exists"
    - key: "ToBeDeletedByClusterAutoscaler"
      operator: "Exists"
    - key: "inactive"
      operator: "Exists"
      effect: "NoSchedule"
  EOT
}

############ per-env-and-region replacement

locals {
  short_env = local.environment-type == "PRE-PRODUCTION" ? "preprod" : local.environment-type == "PRODUCTION" ? "prod" : ""
}

# aws-kms-key-arn
data "aws_kms_alias" "aws-kms-key-arn" {
  name = "alias/${local.asset-id-prefix}-${var.aws-region}-${local.short_env}-${var.account-number}-cmk"
}

# secret-id
data "aws_secretsmanager_secret" "secret-id" {
  name = "${local.asset-id-prefix}-tr-eps-plexus"
}

# secret-id-alt for the second cluster in the same area
data "aws_secretsmanager_secrets" "secret-id-alt" {
  filter {
    name   = "name"
    values = [local.secret-name]
  }
}

# temp additional secret to get around secrets manager max size
data "aws_secretsmanager_secrets" "secret-id-additional" {
  filter {
    name   = "name"
    values = [var.additional-secret-name != null ? var.additional-secret-name : local.secret-name]
  }
}

# ingress-sgs
data "aws_security_group" "webcorp" {
  tags = {
    "aws:cloudformation:logical-id" = "WebCorp"
  }
}

# ingress-sgs
data "aws_security_group" "cloudfront-g-https" {
  name = "${local.asset-id-prefix}-cloudfront-g-https"
}

# ingress-sgs
data "aws_security_group" "cloudfront-r-https" {
  name = "${local.asset-id-prefix}-cloudfront-r-https"
}

# ingress-sgs
data "aws_security_group" "datadog-synthetic" {
  name = "${local.asset-id-prefix}-datadog-synthetic"
}

# ingress-sgs
data "aws_security_group" "route53chk-https" {
  name = "${local.asset-id-prefix}-route53chk-https"
}

# nlb-nodeport-sgs
data "aws_security_group" "cloudfront-g-nodeport" {
  name = "${local.asset-id-prefix}-cloudfront-g-nodeport"
}

# nlb-nodeport-sgs
data "aws_security_group" "cloudfront-r-nodeport" {
  name = "${local.asset-id-prefix}-cloudfront-r-nodeport"
}

# nlb-nodeport-sgs
data "aws_security_group" "route53chk-nodeport" {
  name = "${local.asset-id-prefix}-route53chk-nodeport"
}

# nlb-nodeport-sgs
data "aws_security_group" "datadog-synthetic-nodeport" {
  name = "${local.asset-id-prefix}-datadog-synthetic-nodeport"
}

# secondary-private-subnets
data "aws_subnets" "cgnat" {
  filter {
    name = "tag:Name"
    # including Compute_Private_Subnet* because of subnets created in preprod us-east-1
    values = ["*cgnat*"] # insert values here
  }
}

# public-subnets
data "aws_subnets" "public" {
  tags = {
    "tr:application-asset-insight-id" = "205257"
  }
  filter {
    name   = "tag:Name"
    values = ["tr-vpc-*public*"]
  }
}

# private-subnets
data "aws_subnets" "private" {
  tags = {
    "tr:application-asset-insight-id" = "205257"
  }
  filter {
    name   = "tag:Name"
    values = ["tr-vpc-*private*"]
  }
}

# private-hosted-zone-id
data "aws_route53_zone" "private" {
  count        = var.private-zone-enabled ? 1 : 0
  name         = "${var.hosted-zones-suffixes}.${var.hosted-zone-private-dns}"
  private_zone = true
}

# public-hosted-zone-id
data "aws_route53_zone" "public" {
  count        = var.public-zone-enabled ? 1 : 0
  name         = "${var.hosted-zones-suffixes}.${var.hosted-zone-public-dns}"
  private_zone = false
