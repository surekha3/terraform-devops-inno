# Base.tfvars
aws_region              = "us-east-1"
aws_profile             = "proterra-exp"
environment             = "ets-test"
StateTable              = "ets_test_terraform_state"
project                 = "proterra-exp"


#VPC Setting
vpc_cidr              = "172.30.1.0/24"
vpc_public_subnets    = ["172.30.1.0/27", "172.30.1.32/27"]
vpc_private_subnets   = ["172.30.1.64/27", "172.30.1.96/27"]
vpc_availabilityzones = ["us-east-1a", "us-east-1b"]

env_short             = "ets-test"


environment_short           = "qa"
rds_db_name                 = "expdbqa"
rds_username                = "dbadmin"
rds_password                = "Expqa123"
rds_engine_version          = "5.7.22"
rds_instance_class          = "db.t2.medium"
rds_cidr1                   = "[10.11.122.0/23]"
rds_subnet_group_name       = "exp-qa-rds-private-subnet"
rds_retention_period        = 1
rds_identifier              = "exp-db-qa"

# API Gateway
cp_api_swagger_file_name       = "control-panel-swagger-qa.json"
cp_api_name                    = "control-panel-qa"
cp_api_stage_name              = "exp-qa-cp"

mp_api_swagger_file_name       = "multiplexer-rest-swagger-qa.json"
mp_api_name                    = "multiplexer-rest-qa"
mp_api_stage_name              = "exp-qa-mp"

#Edmonton APIs

ams_ccss_api_swagger_file_name                 = "exp-ccss-rest-swagger-ets-test.json"
ams_ccss_api_name                              = "Exp_ETS_TEST_CCSS"
ams_ccss_api_stage_name                        = "exp-ets-test"

ams_data_lake_api_swagger_file_name            = "exp-data-lake-rest-swagger-ets-test.json"
ams_data_lake_api_name                         = "Exp_ETS_TEST_Data_Lake"
ams_data_lake_api_stage_name                   = "exp-ets-test"

ams_file_api_swagger_file_name                 = "exp-file-rest-swagger-ets-test.json"
ams_file_api_name                              = "Exp_ETS_TEST_File_API"
ams_file_api_stage_name                        = "exp-ets-test"

ams_file_proterra_ams_api_swagger_file_name    = "exp-file-proterra-ams-rest-swagger-ets-test.json"
ams_file_proterra_ams_api_name                 = "Exp_ETS_TEST_File_Proterra_AMS"
ams_file_proterra_ams_api_stage_name           = "exp-ets-test"

ams_iq_api_swagger_file_name                   = "exp-iq-rest-swagger-ets-test.json"
ams_iq_api_name                                = "Exp_ETS_TEST_IQ"
ams_iq_api_stage_name                          = "exp-ets-test"

ams_miscel_api_swagger_file_name               = "exp-miscel-rest-swagger-ets-test.json"
ams_miscel_api_name                            = "Exp_ETS_TEST_Miscel_API"
ams_miscel_api_stage_name                      = "exp-ets-test"

ams_ocpp_api_swagger_file_name                 = "exp-ocpp-rest-swagger-ets-test.json"
ams_ocpp_api_name                              = "Exp_ETS_TEST_OCPP"
ams_ocpp_api_stage_name                        = "exp-ets-test"

ams_proterra_ams_api_swagger_file_name         = "exp-proterra-ams-rest-swagger-ets-test.json"
ams_proterra_ams_api_name                      = "Exp_ETS_TEST_Proterra_AMS"
ams_proterra_ams_api_stage_name                = "exp-ets-test"

ams_runs_api_swagger_file_name                 = "exp-runs-rest-swagger-ets-test.json"
ams_runs_api_name                              = "Exp_ETS_TEST_Runs"
ams_runs_api_stage_name                        = "exp-ets-test"

ams_cim_api_swagger_file_name                 = "exp-cim-rest-swagger-ets-test.json"
ams_cim_api_name                              = "Exp_ETS_TEST_CIM"
ams_cim_api_stage_name                        = "exp-ets-test"


#API Keys

cp_api_key_name                = "exp_qa_cp_api_key"

ams_ams_api_key_name            = "exp_ams_ets_test_api"
ams_cim_api_key_name            = "exp_cim_ets_test_api"
ams_iq_admin_api_key_name       = "exp_ccss_iq_ets_test_admin"
ams_iq_api_key_name             = "exp_ccss_iq_ets_test_api"
ams_mgr_api_key_name            = "exp_ccss_mgr_ets_test_api"
ams_track_api_key_name          = "exp_ccss_track_ets_test_api"



### Multiplexer Control Panel Cognito
cp_cognito_app_client_name      = "proterra_qa_control_panel_app_client"
cp_cognito_user_pool_name       = "proterra_qa_control_panel_cognito_pool"
cp_cognito_email_verif_subject  = "Your verification code"
cp_cognito_email_verif_message  = "Your verification code is {####}."
cp_cognito_unused_account_days  = 7
cp_cognito_email_subject        = "Welcome to Proterra Control Panel"

### Edmonton AMS Cognito

ams_cognito_app_client_name      = "exp_proterra_ams_app_client_ets_test"
ams_cognito_user_pool_name       = "exp_proterra_ams_cognito_pool_edmonton_ets_test"
ams_cognito_email_verif_subject  = "APEX verification code"
ams_cognito_email_verif_message  = "APEX verification code is {####}"
ams_cognito_unused_account_days  = 30
ams_cognito_email_subject        = "Welcome to Proterra APEX"


# S3 Buckets
cp_s3_bucket_name                    = "multiplexer-qa-proterra.com"
cp_s3_bucket_policy_file_name        = "cp_s3_bucket_policy.json"

apex_ui_s3_bucket_name               = "exp-ets-test-apex.proterra.com"
apex_ui_s3_bucket_policy_file_name   = "../files/s3/apex_ui_s3_bucket_policy.json"

ccss_ui_s3_bucket_name               = "exp-ccss-ecms-ets-test.connected.proterra.com"
ccss_ui_s3_bucket_policy_file_name   = "../files/s3/ccss_ui_s3_bucket_policy.json"



