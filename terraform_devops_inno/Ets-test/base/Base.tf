#####################################################
# Base Level Terraform Code.
# - Modify Account specific items.
#####################################################
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}


terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
    bucket         = "exp-ets-test-foundation"
    key            = "terraform/state/qa-us-east-1/terraform.tfstate"
    region         = "us-east-1"
    profile        = "proterra-exp"
  }
}

#Set up the state locking table.
#module "TF_Module_StateLockingTable" {
#  source = "../modules/TF_Module_StateLockingTable"
#  table_name        = "${var.StateTable}"
#  table_environment = "${var.environment}"
#  vpc_id            = "${module.TF_Module_VPC.vpc_id}"

#}

module "TF_Module_VPC" {
  source = "../modules/TF_Module_VPC"

  vpc_cidr              = var.vpc_cidr
  vpc_public_subnets    = var.vpc_public_subnets
  vpc_private_subnets   = var.vpc_private_subnets
  vpc_availabilityzones = var.vpc_availabilityzones
  vpc_account_owner_ip  = var.vpc_account_owner_ip

  vpc_tag_project       = var.project
  vpc_tag_environment   = var.environment
}


module "TF_Module_RDS" {
  source = "../modules/TF_Module_RDS"
  rds_engine_version         = var.rds_engine_version
  rds_subnet_group_name      = var.rds_subnet_group_name
  rds_sg_tag_sec_ect         = var.rds_sg_tag_sec_ect
  rds_tag_sec_assets_db      = var.rds_tag_sec_assets_db
  rds_vpc_bastion_sg_id      = var.rds_vpc_bastion_sg_id
  rds_tag_sec_assets         = var.rds_tag_sec_assets
  rds_sg_tag_sec_gw          = var.rds_sg_tag_sec_gw
  rds_tag_sec_assets_pii     = var.rds_tag_sec_assets_pii
  project                    = var.project
  region                     = var.aws_region
  env_short                  = var.environment_short
  rds_vpc_id                 = module.TF_Module_VPC.vpc_id
  #rds_vpc_common_sg_id       = "${module.TF_Module_VPC.vpc_common_sg_db_id}"
  #rds_vpc_internal_zone_id   = module.TF_Module_VPC.vpc_internal_zone_id
  rds_vpc_internal_zone_id   = ""
  rds_vpc_private_subnet_ids = "${module.TF_Module_VPC.vpc_private_subnet_ids}"
  rds_db_name                = var.rds_db_name
  rds_username               = var.rds_username
  rds_identifier             = var.rds_identifier
  rds_password               = var.rds_password
  rds_retention_period       = var.rds_retention_period
  rds_availabilityzones      = var.vpc_availabilityzones
}

module "TF_Module_API_CP" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.cp_api_swagger_file_name
  api_stage_name        = var.cp_api_stage_name
  api_name              = var.cp_api_name
}

module "TF_Module_API_MP" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.mp_api_swagger_file_name
  api_stage_name        = var.mp_api_stage_name
  api_name              = var.mp_api_name
}



module "TF_Module_API_CCSS" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_ccss_api_swagger_file_name
  api_stage_name        = var.ams_ccss_api_stage_name 
  api_name              = var.ams_ccss_api_name
}


module "TF_Module_API_DATA_LAKE" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_data_lake_api_swagger_file_name
  api_stage_name        = var.ams_data_lake_api_stage_name
  api_name              = var.ams_data_lake_api_name
}


module "TF_Module_API_FILE" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_file_api_swagger_file_name 
  api_stage_name        = var.ams_file_api_stage_name
  api_name              = var.ams_file_api_name
}


module "TF_Module_API_FILE_PROTERRA" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_file_proterra_ams_api_swagger_file_name
  api_stage_name        = var.ams_file_proterra_ams_api_stage_name
  api_name              = var.ams_file_proterra_ams_api_name
}


module "TF_Module_API_IQ" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_iq_api_swagger_file_name
  api_stage_name        = var.ams_iq_api_stage_name
  api_name              = var.ams_iq_api_name
}


module "TF_Module_API_MISCEL" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_miscel_api_swagger_file_name
  api_stage_name        = var.ams_miscel_api_stage_name
  api_name              = var.ams_miscel_api_name
}


module "TF_Module_API_OCPP" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_ocpp_api_swagger_file_name
  api_stage_name        = var.ams_ocpp_api_stage_name
  api_name              = var.ams_ocpp_api_name
}


module "TF_Module_API_AMS" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_proterra_ams_api_swagger_file_name
  api_stage_name        = var.ams_proterra_ams_api_stage_name
  api_name              = var.ams_proterra_ams_api_name
}


module "TF_Module_API_MGR" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_runs_api_swagger_file_name
  api_stage_name        = var.ams_runs_api_stage_name
  api_name              = var.ams_runs_api_name
}

module "TF_Module_API_CIM" {
  source = "../modules/TF_Module_API"

  api_swagger_file_name = var.ams_cim_api_swagger_file_name
  api_stage_name        = var.ams_cim_api_stage_name
  api_name              = var.ams_cim_api_name
}


# API Keys

module "TF_Module_API_KEYS_CP" {
  source = "../modules/TF_Module_API_KEYS"

  api_key_name              = var.cp_api_key_name
}

module "TF_Module_API_KEYS_AMS" {
  source = "../modules/TF_Module_API_KEYS"

  api_key_name              = var.ams_ams_api_key_name
}

module "TF_Module_API_KEYS_CIM" {
  source = "../modules/TF_Module_API_KEYS"

  api_key_name              = var.ams_cim_api_key_name
}

module "TF_Module_API_KEYS_IQ_ADMIN" {
  source = "../modules/TF_Module_API_KEYS"

  api_key_name              = var.ams_iq_admin_api_key_name
}

module "TF_Module_API_KEYS_IQ" {
  source = "../modules/TF_Module_API_KEYS"

  api_key_name              = var.ams_iq_api_key_name 
}

module "TF_Module_API_KEYS_MGR" {
  source = "../modules/TF_Module_API_KEYS"

  api_key_name              = var.ams_mgr_api_key_name
}

module "TF_Module_API_KEYS_TRACK" {
  source = "../modules/TF_Module_API_KEYS"

  api_key_name              = var.ams_track_api_key_name 
}


# Cognito 

module "TF_Module_COGNITO_CP" {
  source = "../modules/TF_Module_COGNITO_CP"

  cognito_user_pool_name          = var.cp_cognito_user_pool_name
  cognito_email_verif_subject     = var.cp_cognito_email_verif_subject
  cognito_email_verif_message     = var.cp_cognito_email_verif_message
  cognito_unused_account_days     = var.cp_cognito_unused_account_days
  cognito_email_subject           = var.cp_cognito_email_subject
  cognito_sms_email_message       = var.cp_cognito_sms_email_message
  cognito_app_client_name         = var.cp_cognito_app_client_name

}


module "TF_Module_COGNITO_AMS" {
  source = "../modules/TF_Module_COGNITO_AMS"

  cognito_user_pool_name          = var.ams_cognito_user_pool_name
  cognito_email_verif_subject     = var.ams_cognito_email_verif_subject
  cognito_email_verif_message     = var.ams_cognito_email_verif_message
  cognito_unused_account_days     = var.ams_cognito_unused_account_days
  cognito_email_subject           = var.ams_cognito_email_subject
  cognito_sms_email_message       = var.ams_cognito_sms_email_message
  cognito_app_client_name         = var.ams_cognito_app_client_name

}


module "TF_Module_S3_Buckets_CP" {
  source = "../modules/TF_Module_S3_Buckets"

  s3_bucket_name             = var.cp_s3_bucket_name
  s3_bucket_policy_file_name = var.cp_s3_bucket_policy_file_name
}

module "TF_Module_S3_Buckets_APEX_UI" {
  source = "../modules/TF_Module_S3_Buckets"

  s3_bucket_name             = var.apex_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.apex_ui_s3_bucket_policy_file_name
}

module "TF_Module_S3_Buckets_CCSS_UI" {
  source = "../modules/TF_Module_S3_Buckets"

  s3_bucket_name             = var.ccss_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.ccss_ui_s3_bucket_policy_file_name
}

#####################################################

#output "rds.username" {
#  value = "${module.TF_Module_RDS.user}"
#}
#output "rds.password" {
#  value = "${module.TF_Module_RDS.password}"
#}

#output "vpc_provate_subnets_ids" {
#  value = "${module.TF_Module_VPC.vpc_private_subnet_ids}"
#}


