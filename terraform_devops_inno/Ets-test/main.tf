#####################################################
# Base Level Terraform Code.
# - Modify Account specific items.
#####################################################
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
  }
}

#################### S3 ######################

module "TF_Module_S3_Buckets_APEX_UI" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.apex_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.apex_ui_s3_bucket_policy_file_name
}

module "TF_Module_S3_Buckets_CCSS_UI" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.ccss_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.ccss_ui_s3_bucket_policy_file_name
}

#################### API ###################### 

module "TF_Module_API_IQ" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_IQ
  template_file_map = var.template_file_map_iq
  api_stage_name = var.template_file_map_iq["stageName"]
  api_name = var.template_file_map_iq["title"]
  api_description = var.template_file_map_iq["description"]
  stage_variables = var.stage_variables_iq
}
module "TF_Module_API_CCSS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_CCSS
  template_file_map = var.template_file_map_ccss
  api_stage_name = var.template_file_map_ccss["stageName"]
  api_name = var.template_file_map_ccss["title"]
  api_description = var.template_file_map_ccss["description"]
  stage_variables = var.stage_variables_ccss
}

module "TF_Module_API_AMS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_AMS
  template_file_map = var.template_file_map_ams
  api_stage_name = var.template_file_map_ams["stageName"]
  api_name = var.template_file_map_ams["title"]
  api_description = var.template_file_map_ams["description"]
  stage_variables = var.stage_variables_ams
}
module "TF_Module_API_FILE" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_FILE
  template_file_map = var.template_file_map_file
  api_stage_name = var.template_file_map_file["stageName"]
  api_name = var.template_file_map_file["title"]
  api_description = var.template_file_map_file["description"]
  stage_variables = var.stage_variables_file
}
module "TF_Module_API_FILE_PROTERRA" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_FILE_AMS
  template_file_map = var.template_file_map_file_ams
  api_stage_name = var.template_file_map_file_ams["stageName"]
  api_name = var.template_file_map_file_ams["title"]
  api_description = var.template_file_map_file_ams["description"]
  stage_variables = var.stage_variables_file_ams
}
module "TF_Module_API_MGR" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_RUNS
  template_file_map = var.template_file_map_runs
  api_stage_name = var.template_file_map_runs["stageName"]
  api_name = var.template_file_map_runs["title"]
  api_description = var.template_file_map_runs["description"]
  stage_variables = var.stage_variables_runs
}
module "TF_Module_API_CIM" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_CIM
  template_file_map = var.template_file_map_cim
  api_stage_name = var.template_file_map_cim["stageName"]
  api_name = var.template_file_map_cim["title"]
  api_description = var.template_file_map_cim["description"]
  stage_variables = var.stage_variables_cim
}
module "TF_Module_API_DATA_LAKE" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_DATALAKE
  template_file_map = var.template_file_map_datalake
  api_stage_name = var.template_file_map_datalake["stageName"]
  api_name = var.template_file_map_datalake["title"]
  api_description = var.template_file_map_datalake["description"]
  stage_variables = var.stage_variables_datalake
}
module "TF_Module_API_OCPP" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_OCPP
  template_file_map = var.template_file_map_ocpp
  api_stage_name = var.template_file_map_ocpp["stageName"]
  api_name = var.template_file_map_ocpp["title"]
  api_description = var.template_file_map_ocpp["description"]
  stage_variables = var.stage_variables_ocpp
}
module "TF_Module_API_MISCEL" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_MISCEL
  template_file_map = var.template_file_map_miscel
  api_stage_name = var.template_file_map_miscel["stageName"]
  api_name = var.template_file_map_miscel["title"]
  api_description = var.template_file_map_miscel["description"]
  stage_variables = var.stage_variables_miscel
}

# Cognito 
## Commented this because using QA env proterra_qa_control_panel_cognito_pool in this env

#module "TF_Module_COGNITO_CP" {
#  source = "../modules/TF_Module_COGNITO_CP"
#
#  cognito_user_pool_name          = var.cp_cognito_user_pool_name
#  cognito_email_verif_subject     = var.cp_cognito_email_verif_subject
#  cognito_sms_verif_message       = var.cp_cognito_sms_verif_message
#  cognito_email_verif_message     = var.cp_cognito_email_verif_message
#  cognito_unused_account_days     = var.cp_cognito_unused_account_days
#  cognito_email_subject           = var.cp_cognito_email_subject
#  cognito_sms_email_message       = var.cp_cognito_sms_email_message
#  cognito_app_client_name         = var.cp_cognito_app_client_name
#
#}


module "TF_Module_COGNITO_AMS" {
  source = "../modules/TF_Module_COGNITO_AMS"

  cognito_user_pool_name          = var.ams_cognito_user_pool_name
  cognito_email_verif_subject     = var.ams_cognito_email_verif_subject
  cognito_sms_verif_message       = var.ams_cognito_sms_verif_message
  cognito_email_verif_message     = var.ams_cognito_email_verif_message
  cognito_unused_account_days     = var.ams_cognito_unused_account_days
  cognito_email_subject           = var.ams_cognito_email_subject
  cognito_sms_email_message       = var.ams_cognito_sms_email_message
  cognito_app_client_name         = var.ams_cognito_app_client_name
  callback_urls                   = var.ams_callback_urls
  logout_urls                     = var.ams_logout_urls
  allowed_oauth_scopes            = var.ams_allowed_oauth_scopes
  explicit_auth_flows             = var.ams_explicit_auth_flows
  supported_identity_providers    = var.ams_supported_identity_providers

}