provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}


terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
    bucket         = "exp-ets-test-foundation"
    key            = "terraform/state/ets-test-us-east-1/terraform.tfstate"
    region         = "us-east-1"
    profile        = "proterra-exp"
  }
}

