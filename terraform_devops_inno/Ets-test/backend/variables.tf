variable "aws_region" {
  type = string
}

variable "aws_profile" {
  type = string
}

variable "environment" {
  type = string
}

variable "StateTable" {
  type = string
}
variable "project" {
  type = string
}

