# Base.tfvars
aws_region              = "us-east-1"
aws_profile             = "proterra-exp"
environment             = "ets-test"
StateTable              = "ets_test_terraform_state"
project                 = "proterra-exp"


####### S3 ####

apex_ui_s3_bucket_name               = "exp-ets-test-apex.proterra.com"
apex_ui_s3_bucket_policy_file_name   = "files-s3/apex_ui_s3_bucket_policy.json"
ccss_ui_s3_bucket_name               = "exp-ccss-ecms-ets-test.connected.proterra.com"
ccss_ui_s3_bucket_policy_file_name   = "files-s3/ccss_ui_s3_bucket_policy.json"


#Edmonton APIs

template_file_name_API_IQ = "IQ-swagger-apigateway.json"
template_file_map_iq = {
  title = "Exp_ETS_TEST_IQ"
  basePath = "/exp-ams-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for IQ"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}

stage_variables_iq = {
apiKey	= "34hilFQ2459qTrVCiWFEN6j7YNaKrCpr2icc4puS"
baseURL	= "a1bf0f09b078b498dbdf2b45df3699aa-989421164.us-east-1.elb.amazonaws.com:8080"
adminApiKey	= "7mAgbaUp8qahz4GUpWxH04XZf6jPoOVa4IKKt4RB"
}

template_file_name_API_CCSS = "CCSS-swagger-apigateway.json"
template_file_map_ccss = {
  title = "Exp_ETS_TEST_CCSS"
  basePath = "/exp-ccss-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for CCSS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}

stage_variables_ccss = {
m5statusApiKey	= "1tD3wwPCtaaSqDOs4B3ek8YaOe0nFT602twdiEeE"
baseURL	= "a5b056584bbe44361a1b75e040574bd6-707053554.us-east-1.elb.amazonaws.com:8080/ccss"	 
runsApiKey	= "0Kbr8oqv963Ieht7XtzNr7wynBREODhM2JajaYEn"
m5statusURL =	"a0bda3c978af0463598f957964f6f73a-584938980.us-east-1.elb.amazonaws.com:8080"	 
apiKey =	"XspDLBTQzs3u5FuJHnlcb62s8Jy2Acrc19UZturX"
runsURL =	"a6042ed66468643b48904d2b6a1944df-876312819.us-east-1.elb.amazonaws.com:8080/api"
}

template_file_name_API_AMS = "AMS-swagger-apigateway.json"
template_file_map_ams = {
  title = "Exp_ETS_TEST_Proterra_AMS"
  basePath = "/exp-ams-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for AMS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}

stage_variables_ams = {
baseURL	="a47ed99fbf7d64f729e08c7489fb03fd-1843553467.us-east-1.elb.amazonaws.com:8080/ams"
}

template_file_name_API_FILE = "FILE-swagger-apigateway.json"
template_file_map_file = {
  title = "Exp_ETS_TEST_File_API"
  basePath = "/exp-file-api-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for FILE"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}
stage_variables_file = {
baseURL = "34.238.189.75:8080/ams"
dataLakeURL	= "34.234.186.206:8080"
}

template_file_name_API_FILE_AMS = "FILE-AMS-swagger-apigateway.json"
template_file_map_file_ams = {
  title = "Exp_ETS_TEST_File_Proterra_AMS"
  basePath = "/exp-file-ams-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for FILE PROTERRA AMS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}
stage_variables_file_ams = {
baseCustomReportURL	="35.153.105.80:3000/api"
baseURL	= "54.146.37.242:8080/ams"
dataLakeURL	= "34.234.186.206:8080"
}

template_file_name_API_DATALAKE = "DATALAKE-swagger-apigateway.json"
template_file_map_datalake = {
  title = "Exp_ETS_TEST_Data_Lake"
  basePath = "/exp-datalake-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for DATALAKE"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}
stage_variables_datalake = {
baseURL	= "52.23.215.14:8080"
apikey	= "dYstPSEhRq3cYqTxcDvS07VUfbgSaqHN7yBUmADQ"
readOnlyApiKey	= "Basic cHJvdGVycmEtcmVhZG9ubHk6cHJvdGVycmEtcmVhZG9ubHk="
chargeSessionAPIKey	= "6323872f-1637-4ebd-aaf6-61cd1239d155"
reportBaseUrl	= "34.234.186.206:8080/dataLake/v1"
dataLakeUrl	= "34.234.186.206:8080/dataLake/v1"
chargeSessionsURL	= "ad3b4cffac5174e5c80bffaf676e42e0-1821811585.us-east-1.elb.amazonaws.com:8080/dataLake/v1"
runsURL	= "a15660516b9874bf7885d05ef4e9f1ed-315084267.us-east-1.elb.amazonaws.com:8080/api/v1"
latestAuth	= "Basic ZWxhc3RpYzpKeGdWMlpXZG9KMU9mSkN2c0licVdxYmk="
edmontonApiKey	= "b09389c4-fd73-4cad-ad5a-c443a963d1f7"
rtApiKey	= "7eb92260-e5c8-11ea-adc1-0242ac120002"
}

template_file_name_API_CIM = "CIM-swagger-apigateway.json"
template_file_map_cim = {
  title = "Exp_ETS_TEST_CIM"
  basePath = "/exp-cim-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for CIM"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}
stage_variables_cim = {
apiKey	= "1tD3wwPCtaaSqDOs4B3ek8YaOe0nFT602twdiEeE"
baseURL	= "a0bda3c978af0463598f957964f6f73a-584938980.us-east-1.elb.amazonaws.com:8080"
}

template_file_name_API_RUNS = "RUNS-swagger-apigateway.json"
template_file_map_runs = {
  title = "Exp_ETS_TEST_Runs"
  basePath = "/exp-runs-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for RUNS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}
stage_variables_runs = {
apiKey = "0Kbr8oqv963Ieht7XtzNr7wynBREODhM2JajaYEn" 
baseURL	= "a6042ed66468643b48904d2b6a1944df-876312819.us-east-1.elb.amazonaws.com:8080/api"
}

template_file_name_API_MISCEL = "MISCEL-swagger-apigateway.json"
template_file_map_miscel = {
  title = "Exp_ETS_TEST_Miscel_API"
  basePath = "/exp-no-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for MISCEL"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}
stage_variables_miscel = {

}

template_file_name_API_OCPP = "OCPP-swagger-apigateway.json"
template_file_map_ocpp = {
  title = "Exp_ETS_TEST_OCPP"
  basePath = "/exp-ocpp-ets-test"
  stageName = "exp-ets-test"
  description = "ETS-TEST API gateway for OCPP"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_L4BYvkXEs"
  host = "exp-api.proterra.com"
}
stage_variables_ocpp = {
baseOCPPURL	= "54.224.248.38:8080"
baseURL	= "52.45.139.95:8080"
}

#API Keys

cp_api_key_name                = "exp_qa_cp_api_key"

ams_ams_api_key_name            = "exp_ams_ets_test_api"
ams_cim_api_key_name            = "exp_cim_ets_test_api"
ams_iq_admin_api_key_name       = "exp_ccss_iq_ets_test_admin"
ams_iq_api_key_name             = "exp_ccss_iq_ets_test_api"
ams_mgr_api_key_name            = "exp_ccss_mgr_ets_test_api"
ams_track_api_key_name          = "exp_ccss_track_ets_test_api"



### Multiplexer Control Panel Cognito
cp_cognito_app_client_name      = "proterra_qa_control_panel_app_client"
cp_cognito_user_pool_name       = "proterra_qa_control_panel_cognito_pool"
cp_cognito_email_verif_subject  = "Your verification code"
cp_cognito_email_verif_message  = "Your verification code is {####}."
cp_cognito_unused_account_days  = 7
cp_cognito_email_subject        = "Welcome to Proterra Control Panel"
cp_cognito_sms_verif_message    = "Your authentication code is {####}. "

### Edmonton AMS Cognito

ams_cognito_app_client_name      = "exp_proterra_ams_app_client_ets_test"
ams_cognito_user_pool_name       = "exp_proterra_ams_cognito_pool_edmonton_ets_test"
ams_cognito_email_verif_subject  = "APEX verification code"
ams_cognito_email_verif_message  = "APEX verification code is {####}"
ams_cognito_unused_account_days  = 30
ams_cognito_email_subject        = "Welcome to Proterra APEX"
ams_cognito_sms_verif_message    = "Your authentication code is {####}. "
ams_callback_urls                =   ["http://localhost:9000", "https://exp-ets-test.proterra.com"]
ams_logout_urls                  =   ["http://localhost:9000", "https://exp-ets-test.proterra.com"]
ams_allowed_oauth_scopes         =   [ "email","openid","profile" ]
ams_explicit_auth_flows          =   [ "ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_SRP_AUTH"]
ams_supported_identity_providers =   ["COGNITO", "edmonton","okta"]

# S3 Buckets
cp_s3_bucket_name                    = "multiplexer-qa-proterra.com"
cp_s3_bucket_policy_file_name        = "cp_s3_bucket_policy.json"



