variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "aws_profile" {
  type = string
  default = "default"
}

variable "cognito_app_client_name" {
  type = string
  default = "proterra_control_panel_app_client"
}

variable "cognito_user_pool_name" {
  type = string
  default = "proterra_control_panel_cognito_pool"
}

variable "cognito_email_verif_subject" {
  type = string
  default = "Your verification code"
}

variable "cognito_email_verif_message" {
  type = string
  default = "Your verification code is {####}."
}

variable "cognito_sms_email_message" {
  type = string
  default = ""
}

variable "cognito_unused_account_days" {
  type = number
  default = 30
}
variable "cognito_email_subject" {
  type = string
  default = ""
}