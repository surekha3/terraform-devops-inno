resource "aws_db_subnet_group" "subnet_rds_grp" {
  
  #name       = "subnet-rds-grp-${var.project}.${var.env_short}-subnet"
  name       = var.rds_subnet_group_name
  #subnet_ids = data.terraform_remote_state.vpc.vpc_private_subnet_ids
  subnet_ids = var.rds_vpc_private_subnet_ids
  #subnet_ids = module.TF_Module_VPC.vpc_private_subnet_ids

  tags = {
    Name = "${var.project} ${var.env_short}  mysql"
  }
}


resource "aws_db_instance" "rds_instance" {
  allocated_storage    = 50
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = var.rds_engine_version
  instance_class       = var.rds_instance_class
  name                 = var.rds_db_name
  username             = var.rds_username
  password             = var.rds_password
  identifier           = var.rds_identifier
  parameter_group_name = "default.mysql5.7"
  db_subnet_group_name = aws_db_subnet_group.subnet_rds_grp.name
  vpc_security_group_ids   = [aws_security_group.rds.id]

  tags = {
    Name = "${var.project} ${var.env_short}  mysql"
    Provisioned = "terraform"
  }
}
