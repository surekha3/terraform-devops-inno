resource "aws_security_group" "rds" {
    #name        = "rds-security-group-${var.project}-${var.region}"
    name        = var.rds_security_group_name
    description = "rds Security Group"
    #vpc_id      = data.terraform_remote_state.vpc.vpc_id
    vpc_id      = var.rds_vpc_id
    tags = {
      Name = "${var.project}-${var.env_short}-mysql-security-group"
      Provisioned = "terraform"
    }

}

resource "aws_security_group_rule" "rds-ingress" {
  type              = "ingress"
  from_port         = var.rds_port
  to_port           = var.rds_port
  protocol          = "tcp"
  cidr_blocks       = [var.rds_cidr1]
  security_group_id = aws_security_group.rds.id
  description       = "rds-ingress-proterra-cidr"
}