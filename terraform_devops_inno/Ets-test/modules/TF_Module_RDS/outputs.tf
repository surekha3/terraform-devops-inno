
output "sg_rds_id" {
  value = "${aws_security_group.rds.id}"
}

output "sg_rds_name" {
  value = "${aws_security_group.rds.name}"
}
