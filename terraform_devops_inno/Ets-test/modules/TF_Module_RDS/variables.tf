variable "project" {
  description = "The project tag for RDS."
  type        = string
}

variable "region" {
  description = "The AWS Region where the RDS and stack reside."
  type        = string
}

variable "env_short" {
  description = "A short version of the environment name."
  type        = string
}

variable "rds_vpc_id" {
  description = "The VPC identifier."
  type        = string
}


variable "rds_engine_version" {
  description = "RDS Engine Version."
  type        = string
}

variable "rds_instance_class" {
  description = "The instance class to use."
  type        = string
  default     = "db.t2.medium"
}

variable "rds_db_name" {
  description = "RDS Database Name"
  type        = string
}

variable "rds_username" {
  description = "RDS Database User Name"
  type        = string
}

variable "rds_password" {
  description = "RDS Database Password"
  type        = string
}

variable "rds_subnet_group_name" {
  description = "RDS Subnet Group"
  type        = string
}

variable "rds_security_group_name" {
  description = "RDS Security Group"
  type        = string
  default     = "proterra-exp-qa-db-sgp"
}


variable "rds_identifier" {
  type = string
}


variable "rds_cidr1" {
  description = "RDS security group Ingress rule"
  type        = string
  default     = "10.11.122.0/23"
}

variable "rds_vpc_bastion_sg_id" {
  description = "The bastion security group ID, which is used to open ports with."
  type        = string
}

variable "rds_vpc_internal_zone_id" {
  description = "The route53 internal hosted zone, created by the VPC."
  type        = string
}


variable "rds_vpc_private_subnet_ids" {
  description = "The subnets where RDS will reside."
  type        = list
}



variable "rds_monitoring_interval" {
  description = "The interval, in seconds, between points when metrics are collected."
  type        = string
  default     = "30"
}



variable "rds_retention_period" {
  description = "The number of days to retain backups for."
  type        = string
  default     = "35"
}

variable "rds_tag_sec_assets" {
  description = "GENERAL, DB"
  type        = string
}

variable "rds_tag_sec_assets_db" {
  description = "GENERAL, DB"
  type        = string
}

variable "rds_tag_sec_assets_pii" {
  description = "IT is Y or N depending upon if we are storing personal identification information"
  type        = string

}

variable "rds_sg_tag_sec_gw" {
  description = "Tags for RDS Security Group"
  type        = string
}

variable "rds_sg_tag_sec_ect" {
  description = "Tags for RDS Security Group"
  type        = string
}

variable "rds_port" {
  description = "Port Number for communication"
  default     = 3306
}

variable "rds_availabilityzones" {
  type        = list
  description = "availability zone"
}

variable "storage_encrypted" {
  default     = true
  description = "encryption at rest"
}

variable "performance_insights_enabled" {
  default     = false
  description = "performance insights"
}

variable "rds_snapshot_identifier" {
  description = "The name of an RDS snapshot to use when creating the cluster."
  type        = string
  default     = ""
}



