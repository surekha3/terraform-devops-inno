#provider "aws" {
#  region  = var.aws_region
#  profile = var.aws_profile
#}

resource "aws_cognito_user_pool" "pool" {
  name                       = var.cognito_user_pool_name
  email_verification_subject = var.cognito_email_verif_subject
  email_verification_message = var.cognito_email_verif_message
  sms_verification_message   = "{####}"
  username_attributes      = ["email"]
  auto_verified_attributes = ["email"]
  #alias_attributes           = ["email", "preferred_username"]


  verification_message_template {
    default_email_option = "CONFIRM_WITH_CODE"
  }

 admin_create_user_config {
    allow_admin_create_user_only = true
    unused_account_validity_days = 30

    invite_message_template {

     email_subject = "Welcome to Proterra APEX"
     sms_message   = "Your username is {username} and temporary password is {####}. "
     email_message = "\u003c!DOCTYPE html\n  PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"\u003e\n\u003chtml xmlns=\"http://www.w3.org/1999/xhtml\"\u003e\n\n\u003chead\u003e\n  \u003cmeta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /\u003e\n  \u003cmeta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /\u003e\n  \u003ctitle\u003eSetup a new password\u003c/title\u003e\n  \u003c!-- \n    The style block is collapsed on page load to save you some scrolling.\n    Postmark automatically inlines all CSS properties for maximum email client \n    compatibility. You can just update styles here, and Postmark does the rest.\n    --\u003e\n  \u003cstyle type=\"text/css\" rel=\"stylesheet\" media=\"all\"\u003e\n    /* Base ------------------------------ */\n\n    *:not(br):not(tr):not(html) {\n      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n      box-sizing: border-box;\n    }\n\n    body {\n      width: 100% !important;\n      height: 100%;\n      margin: 0;\n      line-height: 1.4;\n      background-color: #F2F4F6;\n      color: #74787E;\n      -webkit-text-size-adjust: none;\n    }\n\n    p,\n    ul,\n    ol,\n    blockquote {\n      line-height: 1.4;\n      text-align: left;\n    }\n\n    a {\n      color: #3869D4;\n    }\n\n    a img {\n      border: none;\n    }\n\n    td {\n      word-break: break-word;\n    }\n\n    /* Layout ------------------------------ */\n\n    .email-wrapper {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #F2F4F6;\n    }\n\n    .email-content {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    /* Masthead ----------------------- */\n\n    .email-masthead {\n      padding: 25px 0;\n      text-align: center;\n    }\n\n    .email-masthead_logo {\n      width: 94px;\n    }\n\n    .email-masthead_name {\n      font-size: 16px;\n      font-weight: bold;\n      color: #bbbfc3;\n      text-decoration: none;\n      text-shadow: 0 1px 0 white;\n    }\n\n    /* Body ------------------------------ */\n\n    .email-body {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      border-top: 1px solid #EDEFF2;\n      border-bottom: 1px solid #EDEFF2;\n      background-color: #FFFFFF;\n    }\n\n    .email-body_inner {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #FFFFFF;\n    }\n\n    .email-footer {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .email-footer p {\n      color: #AEAEAE;\n    }\n\n    .body-action {\n      width: 100%;\n      margin: 30px auto;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .body-sub {\n      margin-top: 25px;\n      padding-top: 25px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .content-cell {\n      padding: 35px;\n    }\n\n    .preheader {\n      display: none !important;\n      visibility: hidden;\n      mso-hide: all;\n      font-size: 1px;\n      line-height: 1px;\n      max-height: 0;\n      max-width: 0;\n      opacity: 0;\n      overflow: hidden;\n    }\n\n    /* Attribute list ------------------------------ */\n\n    .attributes {\n      margin: 0 0 21px;\n    }\n\n    .attributes_content {\n      background-color: #EDEFF2;\n      padding: 16px;\n    }\n\n    .attributes_item {\n      padding: 0;\n    }\n\n    /* Related Items ------------------------------ */\n\n    .related {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .related_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .related_item-title {\n      display: block;\n      margin: .5em 0 0;\n    }\n\n    .related_item-thumb {\n      display: block;\n      padding-bottom: 10px;\n    }\n\n    .related_heading {\n      border-top: 1px solid #EDEFF2;\n      text-align: center;\n      padding: 25px 0 10px;\n    }\n\n    /* Discount Code ------------------------------ */\n\n    .discount {\n      width: 100%;\n      margin: 0;\n      padding: 24px;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #EDEFF2;\n      border: 2px dashed #9BA2AB;\n    }\n\n    .discount_heading {\n      text-align: center;\n    }\n\n    .discount_body {\n      text-align: center;\n      font-size: 15px;\n    }\n\n    /* Social Icons ------------------------------ */\n\n    .social {\n      width: auto;\n    }\n\n    .social td {\n      padding: 0;\n      width: auto;\n    }\n\n    .social_icon {\n      height: 20px;\n      margin: 0 8px 10px 8px;\n      padding: 0;\n    }\n\n    /* Data table ------------------------------ */\n\n    .purchase {\n      width: 100%;\n      margin: 0;\n      padding: 35px 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_content {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .purchase_heading {\n      padding-bottom: 8px;\n      border-bottom: 1px solid #EDEFF2;\n    }\n\n    .purchase_heading p {\n      margin: 0;\n      color: #9BA2AB;\n      font-size: 12px;\n    }\n\n    .purchase_footer {\n      padding-top: 15px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .purchase_total {\n      margin: 0;\n      text-align: right;\n      font-weight: bold;\n      color: #2F3133;\n    }\n\n    .purchase_total--label {\n      padding: 0 15px 0 0;\n    }\n\n    /* Utilities ------------------------------ */\n\n    .align-right {\n      text-align: right;\n    }\n\n    .align-left {\n      text-align: left;\n    }\n\n    .align-center {\n      text-align: center;\n    }\n\n    /*Media Queries ------------------------------ */\n\n    @media only screen and (max-width: 600px) {\n\n      .email-body_inner,\n      .email-footer {\n        width: 100% !important;\n      }\n    }\n\n    @media only screen and (max-width: 500px) {\n      .button {\n        width: 100% !important;\n      }\n    }\n\n    /* Buttons ------------------------------ */\n\n    .button {\n      background-color: #3869D4;\n      border-top: 10px solid #3869D4;\n      border-right: 18px solid #3869D4;\n      border-bottom: 10px solid #3869D4;\n      border-left: 18px solid #3869D4;\n      display: inline-block;\n      color: #FFF;\n      text-decoration: none;\n      border-radius: 3px;\n      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);\n      -webkit-text-size-adjust: none;\n    }\n\n    .button--green {\n      background-color: #62a744;\n      border-top: 10px solid #62a744;\n      border-right: 18px solid #62a744;\n      border-bottom: 10px solid #62a744;\n      border-left: 18px solid #62a744;\n    }\n\n    .button--red {\n      background-color: #FF6136;\n      border-top: 10px solid #FF6136;\n      border-right: 18px solid #FF6136;\n      border-bottom: 10px solid #FF6136;\n      border-left: 18px solid #FF6136;\n    }\n\n    /* Type ------------------------------ */\n\n    h1 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 19px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h2 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 16px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h3 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 14px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    p {\n      margin-top: 0;\n      color: #74787E;\n      font-size: 16px;\n      line-height: 1.5em;\n      text-align: left;\n    }\n\n    p.sub {\n      font-size: 12px;\n    }\n\n    p.center {\n      text-align: center;\n    }\n  \u003c/style\u003e\n\u003c/head\u003e\n\n\u003cbody\u003e\n  \u003cspan class=\"preheader\"\u003eUse this link to reset your password. The link is only valid for 24 hours.\u003c/span\u003e\n  \u003ctable class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n    \u003ctr\u003e\n      \u003ctd align=\"center\"\u003e\n        \u003ctable class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-masthead\"\u003e\n              \u003cdiv class=\"navbar-header\" style=\"width:100%; height:100px\"\u003e\n\n                \u003ca class=\"navbar-brand\" rel=\"home\" href=\"#\" title=\"\"\u003e\n                  \u003cimg style=\"margin-top: 10px;\" src=\"http://exp-apex.proterra.com.s3-website-us-east-1.amazonaws.com/images/logo.png\"\u003e\n                \u003c/a\u003e\n              \u003c/div\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003c!-- Email Body --\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n              \u003ctable class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003c!-- Body content --\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\"\u003e\n                    \u003ch1\u003eHi {username},\u003c/h1\u003e\n                    \u003cp\u003eYour Proterra APEX administrator has created an account for you.\u003c/p\u003e\n                    \u003cp\u003eClick below to complete your account setup. You will be asked to set your password.\u003c/p\u003e\n                    \u003c!-- Action --\u003e\n                    \u003ctable class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd align=\"center\"\u003e\n                          \u003c!-- Border based button\n                       https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design --\u003e\n                          \u003ctable width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                            \u003ctr\u003e\n                              \u003ctd align=\"center\"\u003e\n                                \u003ctable border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                                  \u003ctr\u003e\n                                    \u003ctd\u003e\n                                      \u003ca href=\"http://exp-apex.proterra.com.s3-website-us-east-1.amazonaws.com/changePassword.html?username={username}\u0026temporaryPassword={####}\"\n                                        class=\"button button--green\" target=\"_blank\"\n                                        style=\"color: white !important;\"\u003eSet up new password\u003c/a\u003e\n                                    \u003c/td\u003e\n                                  \u003c/tr\u003e\n                                \u003c/table\u003e\n                              \u003c/td\u003e\n                            \u003c/tr\u003e\n                          \u003c/table\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                    \u003cp\u003eIf you have issues or questions please contact your admin or \u003ca\n                        href=\"mailto:connected@proterra.com\"\u003eAPEX support\u003c/a\u003e.\u003c/p\u003e\n                    \u003cp\u003eThank you,\n                      \u003cbr\u003eThe APEX Team\u003c/p\u003e\n                    \u003c!-- Sub copy --\u003e\n                    \u003ctable class=\"body-sub\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd\u003e\n                          \u003cp class=\"sub\"\u003eIf you’re having trouble with the button above, copy and paste the URL below\n                            into your web browser.\u003c/p\u003e\n                          \u003cp class=\"sub\"\u003e\n                            http://exp-apex.proterra.com.s3-website-us-east-1.amazonaws.com/changePassword.html?username={username}\u0026temporaryPassword={####}\n                          \u003c/p\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003ctr\u003e\n            \u003ctd\u003e\n              \u003ctable class=\"email-footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\" align=\"center\"\u003e\n                    \u003cp class=\"sub align-center\"\u003e\u0026copy; 2019 Proterra. All rights reserved.\u003c/p\u003e\n\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n        \u003c/table\u003e\n      \u003c/td\u003e\n    \u003c/tr\u003e\n  \u003c/table\u003e\n\u003c/body\u003e\n\n\u003c/html\u003e"
    }


 }

 
 email_configuration {
     email_sending_account  = "COGNITO_DEFAULT"
     reply_to_email_address = ""
     source_arn             = ""
 }

 password_policy {
    minimum_length    = 8
    require_lowercase = true
    require_numbers   = true
    require_symbols   = true
    require_uppercase = true
  }
 
   schema {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "createdTime"
    required                 = false

    string_attribute_constraints {
      min_length = 0
      max_length = 9518590593466
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "department"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "firstName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "internalUser"
    required                 = false

    string_attribute_constraints {
      min_length = 0
      max_length = 1
    }
   }

   schema {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "lastModifiedTime"
    required                 = false

    string_attribute_constraints {
      min_length = 0
      max_length = 9518590593466
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "lastName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "middleName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "phone"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "roleInformation"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userId"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userType"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userRole"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressAbbreviation"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressId"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressLine1"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressLine2"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "city"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "country"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "latitude"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "longitude"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "zipCode"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "tenantName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "tenantId"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "zoneinfo"
    required                 = true

    string_attribute_constraints {
      min_length = 0
      max_length = 2048
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "email"
    required                 = true

    string_attribute_constraints {
      min_length = 0
      max_length = 2048
    }
   }

  tags = {
    "Name"    = "proterra-exp"
    "Project" = "terraform"
  }
}

 resource "aws_cognito_user_pool_client" "client" {
     name = var.cognito_app_client_name
     user_pool_id = aws_cognito_user_pool.pool.id
     generate_secret = false
     explicit_auth_flows = ["ADMIN_NO_SRP_AUTH"] #allowed values [ADMIN_NO_SRP_AUTH CUSTOM_AUTH_FLOW_ONLY USER_PASSWORD_AUTH]

 }