#resource "aws_s3_bucket" "proterraS3" {
#  bucket = var.s3_bucket_name
#  acl    = "public-read"
#  website {
#    index_document = "index.html"
#    error_document = "index.html"
#
#  }
#  #policy = "${file(var.s3_bucket_policy_file_name)}"
#  policy  = <<POLICY
#  {
#    "Version": "2012-10-17",
#    "Statement": [
#        {
#            "Sid": "PublicReadGetObject",
#            "Effect": "Allow",
#            "Principal": "*",
#            "Action": "s3:GetObject",
#            "Resource": "arn:aws:s3:::${var.s3_bucket_name}/*"
#        }
#    ]
#  }
#  POLICY
#}

resource "aws_s3_bucket" "proterraS3" {
  bucket = var.s3_bucket_name
  acl    = "public-read"
  website {
    index_document = "index.html"
    error_document = "index.html"

  }
}

resource "aws_s3_bucket_policy" "proterraS3policy" {
  bucket = aws_s3_bucket.proterraS3.id
  policy = file(var.s3_bucket_policy_file_name)
}

