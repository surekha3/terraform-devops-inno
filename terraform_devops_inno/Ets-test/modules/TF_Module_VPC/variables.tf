variable "vpc_cidr" {
  description = "The CIDR range for the VPC."
  type        = string
}

variable "vpc_ssh_port" {
  description = "The ssh port to use."
  type        = string
  default     = "22"
}

variable "vpc_private_subnets" {
  description = "A list of CIDR ranges to apply to the private subnets."
  type        = list
}

variable "vpc_public_subnets" {
  description = "A list of CIDR ranges to apply to the public subnets."
  type        = list
}

variable "vpc_availabilityzones" {
  description = "A list of AWS availability zones to build the VPC in."
  type        = list
}

variable "vpc_enable_dns_hostnames" {
  description = "Turn on/off DNS hostnames in the VPC."
  type        = string
  default     = "true"
}

variable "vpc_enable_dns_support" {
  description = "Turn on/off DNS support in the VPC."
  type        = string
  default     = "true"
}

variable "vpc_enable_nat_gateway" {
  description = "Turn on/off the NAT Gateway in the VPC."
  type        = string
  default     = "true"
}

variable "vpc_sg_common_prefix" {
  description = "A prefix for the common security group used for ec2 ssh connections."
  type        = string
  default     = "EC2"
}

variable "vpc_account_owner_ip" {
  type = list
}
variable "vpc_terraform_state_bucket" {
  default = ""
}

variable "vpc_tag_project" {
  description = "Project name such as SPAY, KPGW"
  default     = "proterra-exp"
}

variable "vpc_tag_environment" {
  description = "Environment such as dev, qa, prod"
  default     = "qa"
}
