###########################################################################
# aws_subnet
#
###########################################################################
resource "aws_subnet" "public" {
  vpc_id            = aws_vpc.mod.id
  cidr_block        = var.public_subnets[count.index]
  availability_zone = element(var.azs, count.index)
  count             = length(var.public_subnets)

  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags = {
    #"Name"        = "${format("net-public%.1d-%.1s", count.index + 1, substr(element(var.azs, count.index), length(element(var.azs, count.index)) - 1, 1))}"
    provisioned = "terraform"
    project     = var.tag_project
    environment = var.tag_environment
    type        = "public"
  }
}

resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.mod.id
  cidr_block        = var.private_subnets[count.index]
  availability_zone = element(var.azs, count.index)
  count             = length(var.private_subnets)

  tags = {
    #"Name"        = "${format("net-private%.1d-%.1s", count.index + 1, substr(element(var.azs, count.index), length(element(var.azs, count.index)) - 1, 1))}"
    #"zone"        = "${format("%.1s%.1s%s", element(split("-", element(var.azs, count.index)), 0), element(split("-", element(var.azs, count.index)), 1), element(split("-", element(var.azs, count.index)), 2))}"
    provisioned = "terraform"
    project     = var.tag_project
    environment = var.tag_environment
  }
}



###########################################################################
# aws_route_table
#
###########################################################################
resource "aws_route_table" "public" {
  vpc_id           = aws_vpc.mod.id
  propagating_vgws = var.public_propagating_vgws

  tags = {
    #"Name"        = "${format("rt-public-%.1s", substr(element(var.azs, count.index), length(element(var.azs, count.index)) - 1, 1))}"
    provisioned = "terraform"
    project     = var.tag_project
    environment = var.tag_environment
  }
}

resource "aws_route_table" "private" {
  vpc_id           = aws_vpc.mod.id
  propagating_vgws = var.private_propagating_vgws
  count            = length(var.private_subnets)

  tags = {
    #"Name"        = "${format("rt-private-%.1s", substr(element(var.azs, count.index), length(element(var.azs, count.index)) - 1, 1))}"
    provisioned = "terraform"
    project     = var.tag_project
    environment = var.tag_environment
  }
}

###########################################################################
# aws_route
#
###########################################################################
resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.mod.id
}

resource "aws_route" "private_nat_gateway" {
  route_table_id         = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.natgw.*.id, count.index)
  count                  = length(var.private_subnets) * lookup(map(var.enable_nat_gateway, 1), "true", 0)
}

###########################################################################
# aws_route_table_association
#
###########################################################################
resource "aws_route_table_association" "public" {
  count          = length(var.public_subnets)
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private" {
  count          = length(var.private_subnets)
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

