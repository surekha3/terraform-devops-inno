resource "aws_vpc" "mod" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  tags = {
    Name        = upper(join("_", list("VPC", var.tag_project, var.tag_environment)))
    provisioned = "terraform"
    project     = var.tag_project
    environment = var.tag_environment
  }

}

resource "aws_internet_gateway" "mod" {
  vpc_id = aws_vpc.mod.id

  tags = {
    Name        = format("%s-%s-igw", var.tag_project, var.tag_environment)
    provisioned = "terraform"
    project     = var.tag_project
    environment = var.tag_environment
  }
}

resource "aws_eip" "nateip" {
  vpc   = true
  count = length(var.private_subnets) * lookup(map(var.enable_nat_gateway, 1), "true", 0)
}

resource "aws_nat_gateway" "natgw" {
  allocation_id = element(aws_eip.nateip.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)
  count         = length(var.private_subnets) * lookup(map(var.enable_nat_gateway, 1), "true", 0)

  depends_on = [aws_internet_gateway.mod]
}

data "aws_region" "current" {
  #current = true
}
