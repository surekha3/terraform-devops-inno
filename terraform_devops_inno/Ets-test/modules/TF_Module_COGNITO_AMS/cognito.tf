#provider "aws" {
#  region  = var.aws_region
#  profile = var.aws_profile
#}

resource "aws_cognito_user_pool" "pool" {
  name                       = var.cognito_user_pool_name
  email_verification_subject = var.cognito_email_verif_subject
  email_verification_message = var.cognito_email_verif_message
  sms_verification_message   = "{####}"
  username_attributes      = ["email"]
  auto_verified_attributes = ["email"]
  #alias_attributes           = ["email", "preferred_username"]


  verification_message_template {
    default_email_option = "CONFIRM_WITH_CODE"
  }

 admin_create_user_config {
    allow_admin_create_user_only = true
    #moved below to password policy
    #unused_account_validity_days =  var.cognito_unused_account_days

    invite_message_template {

     email_subject = var.cognito_email_subject
     sms_message   = "Your username is {username} and temporary password is {####}. "
     email_message = var.cognito_sms_email_message
    }


 }

 
 email_configuration {
     email_sending_account  = "COGNITO_DEFAULT"
     reply_to_email_address = ""
     source_arn             = ""
 }

 password_policy {
    minimum_length    = 8
    require_lowercase = true
    require_numbers   = true
    require_symbols   = true
    require_uppercase = true
    temporary_password_validity_days = var.cognito_unused_account_days
  }
 

   schema {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "createdTime"
    required                 = false

    string_attribute_constraints {
      min_length = 0
      max_length = 9518590593466
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "department"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "firstName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "internalUser"
    required                 = false

    string_attribute_constraints {
      min_length = 0
      max_length = 1
    }
   }

   schema {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "lastModifiedTime"
    required                 = false

    string_attribute_constraints {
      min_length = 0
      max_length = 9518590593466
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "lastName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "middleName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "phone"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "roleInformation"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userId"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userType"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userRole"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressAbbreviation"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressId"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressLine1"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressLine2"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "city"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "country"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "latitude"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "longitude"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "zipCode"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "tenantName"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "tenantId"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "state"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "zoneinfo"
    required                 = true

    string_attribute_constraints {
      min_length = 0
      max_length = 2048
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "email"
    required                 = true

    string_attribute_constraints {
      min_length = 0
      max_length = 2048
    }
   }

  tags = {
    "Name"    = "proterra-exp"
    "Project" = "terraform"
  }
}

 resource "aws_cognito_user_pool_client" "client" {
     name = var.cognito_app_client_name
     user_pool_id = aws_cognito_user_pool.pool.id
     generate_secret = false
     explicit_auth_flows = ["ADMIN_NO_SRP_AUTH"] #allowed values [ADMIN_NO_SRP_AUTH CUSTOM_AUTH_FLOW_ONLY USER_PASSWORD_AUTH]
     read_attributes = ["address","birthdate","email","email_verified","family_name","gender","given_name","locale","middle_name","name","nickname","phone_number","phone_number_verified","picture","preferred_username","profile","zoneinfo","updated_at","website","custom:userType","custom:internalUser","custom:addressLine1","custom:addressAbbreviation","custom:latitude","custom:longitude","custom:country","custom:zipCode","custom:tenantName","custom:userId","custom:department","custom:firstName","custom:lastName","custom:lastModifiedTime","custom:city","custom:middleName","custom:roleInformation","custom:tenantId","custom:addressLine2","custom:userRole","custom:createdTime","custom:userName","custom:addressId","custom:phone"]
     write_attributes = ["address","birthdate","email","family_name","gender","given_name","locale","middle_name","name","nickname","phone_number","picture","preferred_username","profile","zoneinfo","updated_at","website","custom:userType","custom:internalUser","custom:addressLine1","custom:addressAbbreviation","custom:latitude","custom:longitude","custom:country","custom:zipCode","custom:tenantName","custom:userId","custom:department","custom:firstName","custom:lastName","custom:lastModifiedTime","custom:city","custom:middleName","custom:tenantId","custom:addressLine2","custom:userRole","custom:createdTime","custom:userName","custom:addressId","custom:phone"]
 }