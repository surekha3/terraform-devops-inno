# Base.tfvars
aws_region              = "us-east-1"
aws_profile             = "proterra-exp"

####### S3 ####
s3_bucket_env                      = "qa"
toucan_s3_bucket_name              = "exp-qa-toucan.proterra.com"
backward_toucan_s3_bucket_name     = "exp-qa-backward.toucan.proterra.com"
toucan_data_s3_bucket_name         = "exp-qa-toucan-data.proterra.com"
data_connected_s3_bucket_name      = "exp-qa-data.connected.proterra.com"
############## SQS ##############

# The default for visibility_timeout_seconds  is 30
# The default for message_retention_seconds is 345600 (4 days).
# The default for max_message_size is 262144 (256 KiB)
# The default for delay_seconds is 0 seconds.
# The default for receive_wait_time_seconds is 0, meaning that the call will return immediately.
sqs_env_tag                        = "qa"

sqs1_name                          =  "qa-ToucanBackward"
sqs1_message_retention_seconds     = 1209600 

sqs2_name                          =  "qa-ToucanRawDataStream"
sqs2_message_retention_seconds     = 604800 

sqs3_name                          =  "qa-RoadRunnerDataStream"
sqs3_message_retention_seconds     = 1209600 

sqs4_name                          =  "qa-BusShadowUpdates"
sqs4_message_retention_seconds     = 86400 

############## Lambda ###############

# For DecompressLambda
lambda_dl_iam_role_name    = "qa-DecompressLambda"
lambda_dl_iam_role_policy    = "files/iam/iam-policy-DecompressLambda.json"
lambda_dl_filename         = "files/lambda/DecompressLambda.zip"
lambda_dl_function_name    = "qa-DecompressLambda"
lambda_dl_timeout	       = 30
lambda_dl_mem_size	       = 256
lambda_dl_handler	        = "lambda_function.lambda_handler"
lambda_dl_runtime          = "python3.6"
#lambda_dl_s3_bucket        = "di-lambda-source"
#lambda_dl_s3_key           =  "DecompressLambda.zip"

# For ToucanRawProcessor
lambda_trp_iam_role_name    = "qa-ToucanRawProcessor"
lambda_trp_iam_role_policy    = "files/iam/iam-policy-ToucanRawProcessor.json"
lambda_trp_filename         = "files/lambda/ToucanRawProcessor.zip"
lambda_trp_function_name    = "qa-ToucanRawProcessor"
lambda_trp_timeout	       = 30
lambda_trp_mem_size	       = 256
lambda_trp_handler	        = "lambda_function.lambda_handler"
lambda_trp_runtime          = "python3.6"

#For BackwardToucanProcessor
lambda_btw_iam_role_name    = "qa-BackwardToucanProcessor"
lambda_btw_iam_role_policy    = "files/iam/iam-policy-BackwardToucanProcessor.json"
lambda_btw_filename         = "files/lambda/BackwardToucanProcessor.zip"
lambda_btw_function_name    = "qa-BackwardToucanProcessor"
lambda_btw_timeout	       = 30
lambda_btw_mem_size	       = 256
lambda_btw_handler	        = "lambda_function.lambda_handler"
lambda_btw_runtime          = "python3.6"

#RoadRunnerDataProcessor
lambda_rrdp_iam_role_name    = "qa-RoadRunnerDataProcessor"
lambda_rrdp_iam_role_policy    = "files/iam/iam-policy-RoadRunnerDataProcessor.json"
lambda_rrdp_filename         = "files/lambda/RoadRunnerDataProcessor.zip"
lambda_rrdp_function_name    = "qa-RoadRunnerDataProcessor"
lambda_rrdp_timeout	       = 300
lambda_rrdp_mem_size	       = 256
lambda_rrdp_handler	        = "lambda_function.lambda_handler"
lambda_rrdp_runtime          = "python3.7"

