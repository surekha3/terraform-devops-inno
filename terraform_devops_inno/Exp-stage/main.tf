#####################################################
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
  }
}

################### S3 ######################
module "TF_Module_S3_Buckets_APEX_UI" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.apex_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.apex_ui_s3_bucket_policy_file_name
}

module "TF_Module_S3_Buckets_CCSS_UI" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.ccss_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.ccss_ui_s3_bucket_policy_file_name
}

################### API #######################
module "TF_Module_API_IQ" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_IQ
  template_file_map = var.template_file_map_iq
  api_stage_name = var.template_file_map_iq["stageName"]
  stage_variables = var.stage_variables_iq
  api_name = var.template_file_map_iq["title"]
  api_description = var.template_file_map_iq["description"]
}
module "TF_Module_API_CCSS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_CCSS
  template_file_map = var.template_file_map_ccss
  api_stage_name = var.template_file_map_ccss["stageName"]
  stage_variables = var.stage_variables_ccss
  api_name = var.template_file_map_ccss["title"]
  api_description = var.template_file_map_ccss["description"]
}
module "TF_Module_API_AMS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_AMS
  template_file_map = var.template_file_map_ams
  api_stage_name = var.template_file_map_ams["stageName"]
  stage_variables = var.stage_variables_ams
  api_name = var.template_file_map_ams["title"]
  api_description = var.template_file_map_ams["description"]
}
module "TF_Module_API_FILE" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_FILE
  template_file_map = var.template_file_map_file
  api_stage_name = var.template_file_map_file["stageName"]
  stage_variables = var.stage_variables_file
  api_name = var.template_file_map_file["title"]
  api_description = var.template_file_map_file["description"]
}
module "TF_Module_API_FILE_PROTERRA" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_FILE_AMS
  template_file_map = var.template_file_map_file_ams
  api_stage_name = var.template_file_map_file_ams["stageName"]
  stage_variables = var.stage_variables_file_ams
  api_name = var.template_file_map_file_ams["title"]
  api_description = var.template_file_map_file_ams["description"]
}
module "TF_Module_API_MGR" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_RUNS
  template_file_map = var.template_file_map_runs
  api_stage_name = var.template_file_map_runs["stageName"]
  stage_variables = var.stage_variables_runs
  api_name = var.template_file_map_runs["title"]
  api_description = var.template_file_map_runs["description"]
}
module "TF_Module_API_CIM" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_CIM
  template_file_map = var.template_file_map_cim
  api_stage_name = var.template_file_map_cim["stageName"]
  stage_variables = var.stage_variables_cim
  api_name = var.template_file_map_cim["title"]
  api_description = var.template_file_map_cim["description"]
}
module "TF_Module_API_DATA_LAKE" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_DATALAKE
  template_file_map = var.template_file_map_datalake
  api_stage_name = var.template_file_map_datalake["stageName"]
  stage_variables = var.stage_variables_datalake
  api_name = var.template_file_map_datalake["title"]
  api_description = var.template_file_map_datalake["description"]
}
module "TF_Module_API_OCPP" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_OCPP
  template_file_map = var.template_file_map_ocpp
  api_stage_name = var.template_file_map_ocpp["stageName"]
  stage_variables = var.stage_variables_ocpp
  api_name = var.template_file_map_ocpp["title"]
  api_description = var.template_file_map_ocpp["description"]
}
module "TF_Module_API_MISCEL" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_MISCEL
  template_file_map = var.template_file_map_miscel
  api_stage_name = var.template_file_map_miscel["stageName"]
  api_name = var.template_file_map_miscel["title"]
  api_description = var.template_file_map_miscel["description"]
}
################### API #######################

########################## Cognito ##################

module "TF_Module_COGNITO_AMS" {
  source = "../modules/TF_Module_COGNITO_AMS"

  cognito_user_pool_name          = var.ams_cognito_user_pool_name
  cognito_email_verif_subject     = var.ams_cognito_email_verif_subject
  cognito_sms_verif_message       = var.ams_cognito_sms_verif_message
  cognito_email_verif_message     = var.ams_cognito_email_verif_message
  cognito_unused_account_days     = var.ams_cognito_unused_account_days
  cognito_email_subject           = var.ams_cognito_email_subject
  cognito_sms_email_message       = var.ams_cognito_sms_email_message
  cognito_app_client_name         = var.ams_cognito_app_client_name
  callback_urls                   = var.ams_callback_urls
  logout_urls                     = var.ams_logout_urls
  allowed_oauth_scopes            = var.ams_allowed_oauth_scopes
  explicit_auth_flows             = var.ams_explicit_auth_flows
  supported_identity_providers    = var.ams_supported_identity_providers
  read_attributes                 = var.ams_read_attributes
  write_attributes                = var.ams_write_attributes

}
