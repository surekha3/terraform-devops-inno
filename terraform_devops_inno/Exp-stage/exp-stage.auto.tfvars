aws_region              = "us-east-1"
aws_profile             = "proterra-exp"

environment             = "stage"

####### S3 ####

apex_ui_s3_bucket_name               = "exp-stage-apex.proterra.com"
apex_ui_s3_bucket_policy_file_name   = "files-s3/apex_ui_s3_bucket_policy.json"
ccss_ui_s3_bucket_name               = "exp-ccss-ecms.connected.proterra.com"
ccss_ui_s3_bucket_policy_file_name   = "files-s3/ccss_ui_s3_bucket_policy.json"


#################### API ####################
template_file_name_API_IQ = "IQ-swagger-apigateway.json"
template_file_map_iq = {
  title = "Exp_Prod_IQ"
  basePath = "/exp-iq-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for IQ"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}

stage_variables_iq = {
  apiKey = "tvG6ME1p4M20RHq1gdJAX50tQBLOQPqu2Mf91Gif"
  baseURL = "a2e42310e21154ee089a9eaa96cce7fb-472650784.us-east-1.elb.amazonaws.com:8080"
  adminApiKey  = "ur4HMQGCMJ6KlPjMH4aKc7MzZPmnVtsj1UeLkdX3"

}

template_file_name_API_CCSS = "CCSS-swagger-apigateway.json"
template_file_map_ccss = {
  title = "Exp_Prod_CCSS"
  basePath = "/exp-ccss-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for CCSS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}
stage_variables_ccss = {
baseURL = "a3dca0b5703f74ef790b03b957c5f3e6-1068774368.us-east-1.elb.amazonaws.com:8080/ccss"
m5statusApiKey = "2c0273e3-11c6-41c0-9bdf-0f90c03a4b02"
runsApiKey = "CXcqUyrrFU9i74lsO8E202rspw0XvAlz7cqt5iq3"
m5statusURL = "a39f41b9e169441a0b985ddeeef3cb0a-1776523960.us-east-1.elb.amazonaws.com:8080"
apiKey = "B9Xq60E6tR2fkjhu7XzOz4EpHH0tLm8J3UzwAcHG"
runsURL = "a154d74404b224394a4227de56ab3911-104241565.us-east-1.elb.amazonaws.com:8080/api"

}
template_file_name_API_AMS = "AMS-swagger-apigateway.json"
template_file_map_ams = {
  title = "Exp_Prod_Proterra_AMS"
  basePath = "/exp-ams-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for AMS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}
stage_variables_ams = {
  baseURL = "aa34dd54879fa449fb75a85872e77853-1526349665.us-east-1.elb.amazonaws.com:8080/ams" 
}

template_file_name_API_FILE = "FILE-swagger-apigateway.json"
template_file_map_file = {
  title = "Exp_Prod_File_API"
  basePath = "/exp-file-api-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for FILE"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}

stage_variables_file = {
  baseURL = "34.238.189.75:8080/ams" 
  dataLakeURL = "34.234.186.206:8080"
}

template_file_name_API_FILE_AMS = "FILE-AMS-swagger-apigateway.json"
template_file_map_file_ams = {
  title = "Exp_Prod_File_Proterra_AMS"
  basePath = "/exp-file-ams-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for FILE PROTERRA AMS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}
stage_variables_file_ams = {
  baseCustomReportURL = "35.153.105.80:3000/api" 
  baseURL = "54.146.37.242:8080/ams"
  dataLakeURL = "34.234.186.206:8080"
}

template_file_name_API_DATALAKE = "DATALAKE-swagger-apigateway.json"
template_file_map_datalake = {
  title = "Exp_Prod_Data_Lake"
  basePath = "/exp-datalake-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for DATALAKE"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}
stage_variables_datalake = {
  baseURL = "52.23.215.14:8080" 
  readOnlyApiKey = "Basic cHJvdGVycmEtcmVhZG9ubHk6cHJvdGVycmEtcmVhZG9ubHk="
  apikey = "dOhWfhuO472EKb9oE6vxe5PFBXh6TUQ07gD41byA"
  chargeSessionAPIKey = "6323872f-1637-4ebd-aaf6-61cd1239d155"
  dataLakeUrl = "34.234.186.206:8080/dataLake/v1"
  reportBaseUrl = "34.234.186.206:8080/dataLake/v1"
  chargeSessionsURL = "ad3b4cffac5174e5c80bffaf676e42e0-1821811585.us-east-1.elb.amazonaws.com:8080/dataLake/v1"
  runsURL = "a15660516b9874bf7885d05ef4e9f1ed-315084267.us-east-1.elb.amazonaws.com:8080/api/v1"
  apiKeyProd = "6323872f-1637-4ebd-aaf6-61cd1239d155"
  latestAuth = "Basic ZWxhc3RpYzpKeGdWMlpXZG9KMU9mSkN2c0licVdxYmk="
  edmontonApiKey = "b09389c4-fd73-4cad-ad5a-c443a963d1f7"
  rtApiKey = "7eb92260-e5c8-11ea-adc1-0242ac120002"
}

template_file_name_API_CIM = "CIM-swagger-apigateway.json"
template_file_map_cim = {
  title = "Exp_Prod_CIM"
  basePath = "/exp-cim-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for CIM"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}
stage_variables_cim = {
  baseURL = "a39f41b9e169441a0b985ddeeef3cb0a-1776523960.us-east-1.elb.amazonaws.com:8080" 
  apiKey =	"2c0273e3-11c6-41c0-9bdf-0f90c03a4b02"

}
template_file_name_API_RUNS = "RUNS-swagger-apigateway.json"
template_file_map_runs = {
  title = "Exp_Prod_Runs"
  basePath = "/exp-runs-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for RUNS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}
stage_variables_runs = {
  apiKey = "CXcqUyrrFU9i74lsO8E202rspw0XvAlz7cqt5iq3"
  baseURL = "a154d74404b224394a4227de56ab3911-104241565.us-east-1.elb.amazonaws.com:8080/api"
  

}
template_file_name_API_MISCEL = "MISCEL-swagger-apigateway.json"
template_file_map_miscel = {
  title = "Exp_Prod_Miscel_API"
  basePath = "/exp-no-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for MISCEL"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}

template_file_name_API_OCPP = "OCPP-swagger-apigateway.json"
template_file_map_ocpp = {
  title = "Exp_Prod_OCPP"
  basePath = "/exp-ocpp-stage"
  stageName = "exp-prod"
  description = "Exp Staging API gateway for OCPP"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_svZvjNiPJ"
  host = "exp-api.proterra.com"
}

stage_variables_ocpp = {
  baseOCPPURL = "54.224.248.38:8080" 
  baseURL = "52.45.139.95:8080"
}


### Cognito ########

### Multiplexer Control Panel Cognito
cp_cognito_app_client_name      = "proterra_qa_control_panel_app_client"
cp_cognito_user_pool_name       = "proterra_qa_control_panel_cognito_pool"
cp_cognito_email_verif_subject  = "Your verification code"
cp_cognito_email_verif_message  = "Your verification code is {####}."
cp_cognito_unused_account_days  = 7
cp_cognito_email_subject        = "Welcome to Proterra Control Panel"
cp_cognito_sms_verif_message    = "Your authentication code is {####}. "
cp_explicit_auth_flows          =    [ "ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH","ALLOW_USER_SRP_AUTH"]

### Edmonton AMS Cognito

ams_cognito_app_client_name      = "exp_proterra_ams_app_client"
#ams_cognito_app_client_name      = "exp_proterra_ams_cognito_user_pool_client"

ams_cognito_user_pool_name       = "exp_proterra_ams_cognito_pool_qa"
ams_cognito_email_verif_subject  = "APEX verification code"
ams_cognito_email_verif_message  = "APEX verification code is {####}"
ams_cognito_unused_account_days  = 30
ams_cognito_email_subject        = "Welcome to Proterra APEX"
ams_cognito_sms_verif_message    = "Your authentication code is {####}. "
ams_callback_urls                =   ["https://exp-stage.proterra.com"]
ams_logout_urls                  =   ["https://exp-stage.proterra.com"]
ams_allowed_oauth_scopes         =   [ "email","openid","profile" ]
ams_explicit_auth_flows          =   [ "ALLOW_CUSTOM_AUTH","ALLOW_REFRESH_TOKEN_AUTH","ALLOW_USER_PASSWORD_AUTH","ALLOW_USER_SRP_AUTH"]
ams_supported_identity_providers =   ["COGNITO", "edmonton","okta"]
ams_read_attributes              =   ["address","birthdate","email","email_verified","family_name","gender","given_name","locale","middle_name","name","nickname","phone_number","phone_number_verified","picture","preferred_username","profile","zoneinfo","updated_at","website","custom:userType","custom:internalUser","custom:addressLine1","custom:addressAbbreviation","custom:latitude","custom:longitude","custom:country","custom:zipCode","custom:tenantName","custom:userId","custom:department","custom:fedUserRole","custom:firstName","custom:lastName","custom:lastModifiedTime","custom:city","custom:middleName","custom:roleInformation","custom:tenantId","custom:addressLine2","custom:userRole","custom:createdTime","custom:userName","custom:addressId","custom:phone"]
ams_write_attributes             =   ["address","birthdate","email","family_name","gender","given_name","locale","middle_name","name","nickname","phone_number","picture","preferred_username","profile","zoneinfo","updated_at","website","custom:userType","custom:internalUser","custom:addressLine1","custom:addressAbbreviation","custom:latitude","custom:longitude","custom:country","custom:zipCode","custom:tenantName","custom:userId","custom:department","custom:firstName","custom:lastName","custom:lastModifiedTime","custom:city","custom:middleName","custom:tenantId","custom:addressLine2","custom:userRole","custom:createdTime","custom:userName","custom:addressId","custom:phone"]

