variable "iot_rule_provisioning_name"   {
    type = string
    default =""
}
variable "iot_rule_description" {
    type = string
    default =""
}
variable "is_iot_rule_enabled" {
    default = true
}
variable "iot_rule_sql_query"  {
    type = string
    default =""
}

variable "iot_rule_sql_version"  {
    type = string
    default = "2016-03-23"
}

variable "iot_rule_action_sns"  {
    type = list
    default = []
}
variable "iot_rule_action_lambda"  {
    type = list
    default = []
}



