data "aws_iam_policy_document" "iot_assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      identifiers = [
        "iot.amazonaws.com",
      ]
      type = "Service"
    }
  }
}
resource "aws_iam_role" "iot_role" {
  name               = "${var.iot_rule_provisioning_name}-iot-role"
  assume_role_policy = data.aws_iam_policy_document.iot_assume_role.json
}

############################
###  IAM for SNS Actions ###
############################
data "aws_iam_policy_document" "sns_publish" {
  count = length(var.iot_rule_action_sns) != 0 ? 1 : 0
  statement {
    actions = [
      "sns:Publish",
    ]
    resources = var.iot_rule_action_sns.*.target_arn
    sid       = "SNSPublish"
  }
}

resource "aws_iam_policy" "sns_publish" {
  count  = length(var.iot_rule_action_sns) != 0 ? 1 : 0
  name   = "${var.iot_rule_provisioning_name}-sns-publish"
  policy = data.aws_iam_policy_document.sns_publish.0.json
}

resource "aws_iam_role_policy_attachment" "sns_publish" {
  count      = length(var.iot_rule_action_sns) != 0 ? 1 : 0
  policy_arn = aws_iam_policy.sns_publish.0.arn
  role       = aws_iam_role.iot_role.name
}


# aws_iot_topic_rule.rule:
resource "aws_iot_topic_rule" "rule" {
    description = var.iot_rule_description
    enabled     = var.is_iot_rule_enabled
    name        = var.iot_rule_provisioning_name
    sql         = var.iot_rule_sql_query
    sql_version = var.iot_rule_sql_version

   dynamic "sns" {
    for_each = var.iot_rule_action_sns

    content {
      message_format = sns.value.message_format
      role_arn       = aws_iam_role.iot_role.arn
      target_arn     = sns.value.target_arn
    }
  }
      
    dynamic "lambda" {
    for_each = var.iot_rule_action_lambda

    content {
      function_arn = lambda.value.arn
    }
  }

}


 