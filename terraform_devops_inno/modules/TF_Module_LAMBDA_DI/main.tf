
#resource "aws_iam_role" "iam_for_lambda" {
#  name = var.iam_role_name
#  path = "/service-role/"
#  tags = {}
#  assume_role_policy = <<EOF
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Action": "sts:AssumeRole",
#      "Principal": {
#        "Service": [ "lambda.amazonaws.com"]
#      },
#      "Effect": "Allow"
#    }
#  ]
#}
#EOF
#}

resource "aws_iam_role" "iam_for_lambda" {
  name = var.iam_role_name
  assume_role_policy = file(var.iam_role_policy)
}


#resource "aws_lambda_function" "lambda" {
#  filename      = var.lambda_filename
#  s3_bucket   = var.lambda_s3_bucket
#  s3_key      = var.lambda_s3_key
#  function_name = var.lambda_function_name
#  handler                        = var.handler
#  memory_size                    = var.mem_size
#  reserved_concurrent_executions = -1
#  role			   = aws_iam_role.iam_for_lambda.arn
#  source_code_hash = filebase64sha256(var.lambda_s3_key)
#  runtime = var.runtime
#  publish                        = true
#  timeout                        = var.timeout
#  environment {
#   	 variables = var.lambda_env
#  	}
#  timeouts {}
#    tracing_config {
#        mode = "PassThrough"
#    }
#}

resource "aws_lambda_function" "lambda" {
  filename      = var.lambda_filename
  function_name = var.lambda_function_name
  handler                        = var.handler
  memory_size                    = var.mem_size
  reserved_concurrent_executions = -1
  role			   = aws_iam_role.iam_for_lambda.arn
  source_code_hash = filebase64sha256(var.lambda_filename)
  runtime = var.runtime
  publish                        = true
  timeout                        = var.timeout
  environment {
   	 variables = var.lambda_env
  	}
  timeouts {}
    tracing_config {
        mode = "PassThrough"
    }
}
