############  S3 builds #######
variable "s3_bucket_name" {
  description = "S3 Bucket Name"
  type        = string
  default     = ""
}
variable "s3_bucket_env" {
  default = ""
  type    = string
}