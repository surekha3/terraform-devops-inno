########## S3 buckets ##########
## Builds artifactory
resource "aws_s3_bucket" "mdc-builds" {
  bucket = var.s3_bucket_name
  acl    = "private"	
  tags = {
    Name = var.s3_bucket_name
    Environment=var.s3_bucket_env
  }
}

