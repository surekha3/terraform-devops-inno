#######  Usage Plan #########

# resource "aws_api_gateway_usage_plan" "main" {
#   
#   count  =  var.is_usage_plan_required ? 1 : 0
#   name         = var.is_dr ?  join("_", list("DR", var.usage_plan_map_data["name"])) : var.usage_plan_map_data["name"]
#   description  = var.usage_plan_map_data["description"]
# 
#   dynamic "api_stages" {
#     #for_each = jsondecode(var.usage_plan_map_data["apiStages"])
#     for_each = var.is_dr ? var.dr_usage_plan_apiStages_data : jsondecode(var.usage_plan_map_data["apiStages"])
#      
#     content {
#       #api_id =  var.dr_usage_plan_apiStages_api_id : api_stages.value.apiId
#       #stage  = var.is_dr ? var.dr_usage_plan_apiStages_stage_id : api_stages.value.stage
#       api_id =  api_stages.value.apiId
#       stage  =  api_stages.value.stage
#     }
#   }
# 
#   quota_settings {
#     limit  = jsondecode(var.usage_plan_map_data["quota"])["limit"]
#     offset = jsondecode(var.usage_plan_map_data["quota"])["offset"]
#     period = jsondecode(var.usage_plan_map_data["quota"])["period"]
#   }
# 
#   throttle_settings {
#     burst_limit = jsondecode(var.usage_plan_map_data["throttle"])["burstLimit"]
#     rate_limit  = jsondecode(var.usage_plan_map_data["throttle"])["rateLimit"]
#   }
# }
# 

resource "aws_api_gateway_usage_plan" "main" {
  
  count  =  var.is_usage_plan_required ? 1 : 0
  name         = var.is_dr ?  join("_", list("DR", var.usage_plan_map_data["name"])) : var.usage_plan_map_data["name"]
  description  = lookup(var.usage_plan_map_data, "description", " From Terraform")
  
  dynamic "api_stages" {
    #for_each = jsondecode(var.usage_plan_map_data["apiStages"])
    for_each = var.is_dr ? var.dr_usage_plan_apiStages_data : jsondecode(var.usage_plan_map_data["apiStages"])
     
    content {
      #api_id =  var.dr_usage_plan_apiStages_api_id : api_stages.value.apiId
      #stage  = var.is_dr ? var.dr_usage_plan_apiStages_stage_id : api_stages.value.stage
      api_id =  api_stages.value.apiId
      stage  =  api_stages.value.stage
    }
  }

dynamic "quota_settings" {                                                                                  
   for_each = length(lookup(var.usage_plan_map_data, "quota", "")) > 0  ? [jsondecode(var.usage_plan_map_data["quota"])] : []
    content {                                                                                                 
      limit  = quota_settings.value["limit"]                                                                  
      offset = quota_settings.value["offset"]                                                                 
      period = quota_settings.value["period"]                                                                 
    }                                                                                                         
  }            

  
   dynamic "throttle_settings" {
       for_each = lookup(var.usage_plan_map_data, "throttle", "") != null ? [jsondecode(var.usage_plan_map_data["throttle"])] : []
     content {  
        burst_limit = throttle_settings.value["burstLimit"]
       rate_limit  = throttle_settings.value["rateLimit"]
   }
   }
}
#######  API Key Creation #######

# resource "aws_api_gateway_api_key" "api_key" {
#   count  =  var.is_api_key_required ? 1 : 0
#   name = var.is_dr ?  join("_", list("DR", var.api_keys_map_data["name"])) : var.api_keys_map_data["name"]
#   description = var.api_keys_map_data["description"] != null && var.api_keys_map_data["description"] != "" ? var.api_keys_map_data["description"] : null
#   enabled = var.api_keys_map_data["enabled"] != null && var.api_keys_map_data["enabled"] != "" ? var.api_keys_map_data["enabled"] : true
#   
#   #The value of the API key. If not specified, it will be automatically generated by AWS on creation.
#   value = var.api_keys_map_data["value"] != null && var.api_keys_map_data["value"] != "" ? var.api_keys_map_data["value"] : null
# }

resource "aws_api_gateway_api_key" "api_key" {
  count  =  var.is_api_key_required ? 1 : 0
  name = var.is_dr ?  join("_", list("DR", var.api_keys_map_data["name"])) : var.api_keys_map_data["name"]
  description = lookup(var.api_keys_map_data, "description", " From Terraform")
  enabled = lookup(var.api_keys_map_data, "enabled", true)
  #The value of the API key. If not specified, it will be automatically generated by AWS on creation.
  value = lookup(var.api_keys_map_data, "value", null)
  
}

resource "aws_api_gateway_usage_plan_key" "main" {
  count  =  var.is_associated_usage_plans_required && var.is_api_key_required ? 1 : 0
  key_id        = aws_api_gateway_api_key.api_key[0].id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.main[0].id
}