variable "is_dr" {
	default = false
}

variable "is_usage_plan_required" {
	default = false
}

variable "is_api_key_required" {
	default = false
}
variable "is_associated_usage_plans_required" {
	default = false
}



variable "usage_plan_map_data" {
   type = map(string)
 default = {}
}

variable "dr_usage_plan_apiStages_api_id" {
   type = string
 default = ""
}

variable "dr_usage_plan_apiStages_stage_id" {
   type = string
 default = ""
}

variable "dr_usage_plan_apiStages_data" {
   type = list
 default = []
}

variable "api_keys_map_data" {
   type = map(string)
 default = {}
}
