variable "enabled"  {
  default = "false"
}
variable "project" {
  description = "The project tag for RDS."
  type        = string
}

variable "region" {
  description = "The AWS Region where the RDS and stack reside."
  type        = string
  default     = ""
}

variable "env_short" {
  description = "A short version of the environment name."
  type        = string
}

variable "rds_vpc_id" {
  description = "The VPC identifier."
  type        = string
}


variable "rds_engine_version" {
  description = "RDS Engine Version."
  type        = string
}

variable "rds_storage_encrypted" {
  description = "RDS Storage Encrypted."
  type        = string
}

variable "rds_max_allocated_storage" {
  description = "RDS Max Allocated Storage."
  type        = string
  default     = ""
}

variable "rds_instance_class" {
  description = "The instance class to use."
  type        = string
}

variable "rds_db_name" {
  description = "RDS Database Name"
  type        = string
}


variable "rds_subnet_group_name" {
  description = "RDS Subnet Group"
  type        = string
}

variable "rds_security_group_name" {
  description = "RDS Security Group"
  type        = string
}

variable "rds_security_group_description" {
  description = "RDS Security Group"
  type        = string
}

variable "rds_copy_tags_to_snapshot" {
  description = "RDS copy_tags_to_snapshot "
  type        = string
}

variable "rds_parameter_group_name" {
  description = "RDS parameter group name"
  type        = string
}

variable "rds_skip_final_snapshot" {
  description = "RDS "
  type        = string
}


variable "rds_identifier" {
  type = string
}


variable "rds_cidr1" {
  description = "RDS security group Ingress rule"
  type        = string
  default     = "10.0.0.0/8"
}

variable "rds_vpc_bastion_sg_id" {
  description = "The bastion security group ID, which is used to open ports with."
  type        = string
  default     = ""
}

variable "rds_vpc_internal_zone_id" {
  description = "The route53 internal hosted zone, created by the VPC."
  type        = string
  default     = ""
}


variable "rds_vpc_private_subnet_ids" {
  description = "The subnets where RDS will reside."
  type        = list
}



variable "rds_monitoring_interval" {
  description = "The interval, in seconds, between points when metrics are collected."
  type        = string
  default     = "30"
}



variable "rds_retention_period" {
  description = "The number of days to retain backups for."
  type        = string
  default     = "35"
}

variable "rds_tag_sec_assets" {
  description = "GENERAL, DB"
  type        = string
  default     = ""
}

variable "rds_tag_sec_assets_db" {
  description = "GENERAL, DB"
  type        = string
  default     = ""
}

variable "rds_tag_sec_assets_pii" {
  description = "IT is Y or N depending upon if we are storing personal identification information"
  type        = string
  default     = ""

}

variable "rds_sg_tag_sec_gw" {
  description = "Tags for RDS Security Group"
  type        = string
  default     = ""
}

variable "rds_sg_tag_sec_ect" {
  description = "Tags for RDS Security Group"
  type        = string
default     = ""
}

variable "rds_port" {
  description = "Port Number for communication"
  default     = 3306
}

variable "rds_availabilityzones" {
  type        = list
  description = "availability zone"
}

variable "storage_encrypted" {
  default     = true
  description = "encryption at rest"
}

variable "performance_insights_enabled" {
  default     = false
  description = "performance insights"
}

variable "rds_snapshot_identifier" {
  description = "The name of an RDS snapshot to use when creating the cluster."
  type        = string
  default     = ""
}

######
variable "rds_username" {
 default = ""
}
variable "rds_password" {
 default = ""
}
