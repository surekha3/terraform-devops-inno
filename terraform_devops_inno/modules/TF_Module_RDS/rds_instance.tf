resource "aws_db_subnet_group" "subnet_rds_grp" {
  
  #name       = "subnet-rds-grp-${var.project}.${var.env_short}-subnet"
  name       = var.rds_subnet_group_name
  #subnet_ids = data.terraform_remote_state.vpc.vpc_private_subnet_ids
  subnet_ids = var.rds_vpc_private_subnet_ids
  #subnet_ids = module.TF_Module_VPC.vpc_private_subnet_ids

  tags = {
    Name = "${var.project} ${var.env_short}  mysql"
  }
}

resource "aws_db_parameter_group" "default" {
  name   = var.rds_parameter_group_name
  family = "mysql5.7"

}

resource "aws_db_instance" "rds_instance" {
  count		= var.enabled ? 0 : 1
  allocated_storage    = 50
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = var.rds_engine_version
  instance_class       = var.rds_instance_class
  storage_encrypted    = var.rds_storage_encrypted
  max_allocated_storage = var.rds_max_allocated_storage
  #name                 = var.rds_db_name
  username             = var.rds_username
  password             = var.rds_password
  identifier           = var.rds_identifier
  parameter_group_name = aws_db_parameter_group.default.id
  db_subnet_group_name = aws_db_subnet_group.subnet_rds_grp.name
  vpc_security_group_ids   = [aws_security_group.rds.id]
  copy_tags_to_snapshot    = var.rds_copy_tags_to_snapshot
  monitoring_interval      = var.rds_monitoring_interval
  skip_final_snapshot      = var.rds_skip_final_snapshot
  snapshot_identifier      = var.rds_snapshot_identifier
  tags = {
    Name = "${var.project} ${var.env_short}  mysql"
    Provisioned = "terraform"
  }
}


# aws_rds_cluster.mysql:
resource "aws_rds_cluster" "mysql" {
    count                       = var.enabled ? 1 : 0
    #arn                                 = "arn:aws:rds:us-east-1:029126476216:cluster:proterra-prod-db"
    availability_zones                  = var.rds_availabilityzones
    #backtrack_window                    = 0
    backup_retention_period             = 1
  #  cluster_identifier                  = "proterra-prod-db"
  #  cluster_members                     = [
  #      "proterra-prod-db-instance-1",
  #      "proterra-prod-db-instance-1-us-east-1a",
  #  ]
  #  cluster_resource_id                 = "cluster-IOITHE2XKA7BZ72MHVIFHM2P74"
    copy_tags_to_snapshot               = true
    db_cluster_parameter_group_name     = "default.aurora5.6"
    db_subnet_group_name                = aws_db_subnet_group.subnet_rds_grp.name
    deletion_protection                 = true
    enable_http_endpoint                = false
    enabled_cloudwatch_logs_exports     = []
    #endpoint                            = "proterra-prod-db.cluster-cfies5mie0mm.us-east-1.rds.amazonaws.com"
    engine                              = "aurora"
    engine_mode                         = "provisioned"
    engine_version			= var.rds_engine_version
   # engine_version                      = "5.6.mysql_aurora.1.22.2"
  #  hosted_zone_id                      = "Z2R2ITUGPM61AM"
  #  iam_database_authentication_enabled = false
  #  iam_roles                           = []
  #  id                                  = "proterra-prod-db"
  #  kms_key_id                          = "arn:aws:kms:us-east-1:029126476216:key/e0371075-193c-4997-8c97-29d81f6ab310"
  #  master_username                     = "admin"
  #  port                                = 3306
  #  preferred_backup_window             = "08:28-08:58"
  #  preferred_maintenance_window        = "tue:03:50-tue:04:20"
  #  reader_endpoint                     = "proterra-prod-db.cluster-ro-cfies5mie0mm.us-east-1.rds.amazonaws.com"
  #  skip_final_snapshot                 = true
    storage_encrypted                   = true
    tags                                = {}
    vpc_security_group_ids              =  [aws_security_group.rds.id]
    snapshot_identifier             = var.rds_snapshot_identifier
    timeouts {}
}


resource "aws_rds_cluster_instance" "this" {
  count                       = var.enabled ? 1 : 0

  #identifier                      = length(var.instances_parameters) > count.index ? lookup(var.instances_parameters[count.index], "instance_name", "${var.name}-${count.index + 1}") : "${var.name}-${count.index + 1}"
  cluster_identifier              = element(concat(aws_rds_cluster.mysql.*.id, [""]), 0)
  engine                          = "aurora"
  engine_version                  = var.rds_engine_version
  instance_class                  =  var.rds_instance_class
   publicly_accessible		  = "false"
  #publicly_accessible             = length(var.instances_parameters) > count.index ? lookup(var.instances_parameters[count.index], "publicly_accessible", var.publicly_accessible) : var.publicly_accessible
  db_subnet_group_name            = aws_db_subnet_group.subnet_rds_grp.name
  db_parameter_group_name         = var.rds_parameter_group_name
  #preferred_maintenance_window    = var.preferred_maintenance_window
  #apply_immediately               = var.apply_immediately
  #monitoring_role_arn             = local.rds_enhanced_monitoring_arn
  #monitoring_interval             = var.monitoring_interval
  #auto_minor_version_upgrade      = var.auto_minor_version_upgrade
  #promotion_tier                  = length(var.instances_parameters) > count.index ? lookup(var.instances_parameters[count.index], "instance_promotion_tier", count.index + 1) : count.index + 1
  #performance_insights_enabled    = var.performance_insights_enabled
  #performance_insights_kms_key_id = var.performance_insights_kms_key_id
  #ca_cert_identifier              = var.ca_cert_identifier
   ca_cert_identifier		   = "rds-ca-2019"

  # Updating engine version forces replacement of instances, and they shouldn't be replaced
  # because cluster will update them if engine version is changed
  lifecycle {
    ignore_changes = [
      engine_version
    ]
  }

  tags = {}
}




