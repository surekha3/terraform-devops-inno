
output "sg_rds_id" {
  value = "${aws_security_group.rds.id}"
}

output "sg_rds_name" {
  value = "${aws_security_group.rds.name}"
}


output "db_endpoint" {
 value = split(":","${aws_db_instance.rds_instance[0].endpoint}")[0]
}
output "rds_cluster_endpoint" {
value = join("", aws_rds_cluster.mysql.*.endpoint)
}
