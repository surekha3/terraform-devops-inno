resource "aws_lambda_function" "lambda" {
    count		= var.enable_role ? 0 : 1
    filename			  = var.lambda_filename
    function_name                  = var.lambda_funcname
    handler                        = var.handler
    memory_size                    = var.mem_size
    reserved_concurrent_executions = -1
    role			   = var.lambda_function_role
    runtime                        = var.runtime
   # source_code_hash               = "${filebase64sha256(var.lambda_filename)}"
    timeout                        = var.timeout
    environment {
   	 variables = var.lambda_env
  	}
    timeouts {}
    tracing_config {
        mode = "PassThrough"
    }
}

locals {
  enable_environment_variables = "${length(var.lambda_env) > 0 ? "true" : "false"}"
}

resource "aws_lambda_function" "lambda1" {
    count		= var.enable_role && local.enable_environment_variables ? 0 : 1
    s3_bucket = var.s3_bucket
    s3_key = var.s3_key
    function_name                  = var.lambda_funcname
    handler                        = var.handler
    memory_size                    = var.mem_size
    reserved_concurrent_executions = -1
    role			   = aws_iam_role.iam_for_lambda[0].arn
    runtime                        = var.runtime
    #source_code_hash               = "${filebase64sha256(var.lambda_filename)}"
    timeout                        = var.timeout
    timeouts {}
    tracing_config {
        mode = "PassThrough"
    }
}

resource "aws_lambda_function" "lambda2" {
    count		= var.enable_role && local.enable_environment_variables ? 1 : 0
    s3_bucket = var.s3_bucket
    s3_key = var.s3_key
    function_name                  = var.lambda_funcname
    handler                        = var.handler
    memory_size                    = var.mem_size
    reserved_concurrent_executions = -1
    role			   = aws_iam_role.iam_for_lambda[0].arn
    runtime                        = var.runtime
    #source_code_hash               = "${filebase64sha256(var.lambda_filename)}"
    timeout                        = var.timeout
    environment {
   	 variables = var.lambda_env
  	}
    timeouts {}
    tracing_config {
        mode = "PassThrough"
    }
}
resource "aws_lambda_permission" "lambda_permission" {
  count		= var.enable_role && local.enable_environment_variables ? 0: length(var.source_arn)
  statement_id  =  var.statement_id[count.index]
  action        =  var.action
  function_name =  aws_lambda_function.lambda1[0].arn
  principal     = var.principal[count.index]
  source_arn = var.source_arn[count.index]
}

resource "aws_lambda_permission" "lambda_permission1" {
  count		= var.enable_role && local.enable_environment_variables ? length(var.source_arn) : 0
  statement_id  =  var.statement_id[count.index]
  action        =  var.action
  function_name =  aws_lambda_function.lambda2[0].arn
  principal     = var.principal[count.index]
  source_arn = var.source_arn[count.index]
}

resource "aws_iam_role" "iam_for_lambda" {
  count		= var.enable_role ? 1 : 0
  name = var.iam_role_name
  assume_role_policy = file(var.iam_assume_role_policy)
  path = var.path
}

resource "aws_iam_policy" "policy" {
  count		= var.enable_role ? 1 : 0
  name        = var.policy_name
  description = "policy for lambda"
  policy = file(var.iam_role_policy)
}


resource "aws_iam_role_policy_attachment" "attach" {
  count		= var.enable_role ? 1 : 0
  role       = aws_iam_role.iam_for_lambda[0].name
  policy_arn = aws_iam_policy.policy[0].arn
}

