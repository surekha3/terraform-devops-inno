variable "lambda_function_role" {
    default = ""
}
variable "s3_bucket" {}
variable "s3_key" {}
variable "lambda_filename" {
  default = ""
}
variable "lambda_funcname" {}
variable "timeout" {}
variable "mem_size" {}
variable "handler" {}
variable "runtime" {}
variable "lambda_env" {
  type = map
  default = {
    }
  }
variable "iam_role_name" {}
variable "iam_assume_role_policy" {
  default = ""
}
variable "iam_role_policy" {}
variable "policy_name" {
  default = ""
 }

variable "path" {
  default = ""
}
variable "enable_role" {
  default = false
}
variable "statement_id" {
type = list(string)
}
variable "action"  {}
variable "principal" {
 type = list(string)
}
variable "source_arn" {
  type = list(string)
}
