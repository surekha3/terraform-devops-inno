variable "s3_bucket_name" {
  description = "S3 Bucket Name"
  type        = string
  default     = ""
}

variable "s3_bucket_policy_file_name" {
  description = "S3 Bucket Policy"
  type        = string
  default     = ""
}

variable "enable_ploicy" {
  default     = true
}