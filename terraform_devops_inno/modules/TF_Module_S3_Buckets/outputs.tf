output "s3_bucket_name" {
  value = aws_s3_bucket.proterraS3.id
}
output "s3_bucket_domain_name" {
  value = aws_s3_bucket.proterraS3.bucket_domain_name
}
