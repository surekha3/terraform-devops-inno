resource "aws_dynamodb_table" "dynamodb" {
    billing_mode   = "PROVISIONED"
    hash_key       = var.hash_key
    name           = var.dy_table_name
    read_capacity  = 10
    stream_enabled = false
    tags           = {}
    write_capacity = 10

    attribute {
        name = var.hash_key
        type = "S"
    }

    point_in_time_recovery {
        enabled = false
    }

    timeouts {}

    ttl {
        enabled = false
    }
}
