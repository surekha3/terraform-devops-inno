variable "origin_domain_name" {}
variable "origin_id" {}
variable "aliases" {
  type        = list(string)
  default     = []

}
variable "acm_certificate_arn" {
  type = string 
  default="arn:aws:acm:us-east-1:029126476216:certificate/929ad282-5367-48a8-9e9e-6bdf410dd405"

}
