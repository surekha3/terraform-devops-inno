variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "aws_profile" {
  type = string
  default = "default"
}

variable "cognito_user_pool_name" {
  type = string
  default = "exp_proterra_ams_cognito_pool_qa"
}

variable "cognito_email_verif_subject" {
  type = string
  default = "APEX verification code"
}

variable "cognito_email_verif_message" {
  type = string
  default = " APEX verification code is {####}"
}

variable "cognito_sms_verif_message" {
  type = string
  default = " Your authentication code is {####}. "
}

variable "cognito_sms_email_message" {
  type = string
  default = " "
}

variable "cognito_unused_account_days" {
  type = number
  default = 30
}

variable "cognito_email_subject" {
  type = string
  default = "Welcome to Proterra"
} 

# schema
variable "schemas" {
  description = "A container with the schema attributes of a user pool. Maximum of 50 attributes"
  type        = list
  default     = []
}

variable "string_schemas" {
  description = "A container with the string schema attributes of a user pool. Maximum of 50 attributes"
  type        = list
  default     = []
}

variable "number_schemas" {
  description = "A container with the number schema attributes of a user pool. Maximum of 50 attributes"
  type        = list
  default     = []
}






###### cognito  client######

variable "cognito_app_client_name" {
  type = string
  default = "exp_proterra_ams_app_client"
}

variable "callback_urls" {
  type = list
  default = []
}
variable "logout_urls" {
  type = list
  default = []
}

variable "allowed_oauth_scopes" {
  type = list
  default = []
}
variable "explicit_auth_flows" {
  type = list
  default = []
}
variable "supported_identity_providers" {
  type = list
  default = []
}

variable "read_attributes" {
  type = list
  default = ["address","birthdate","email","email_verified","family_name","gender","given_name","locale","middle_name","name","nickname","phone_number","phone_number_verified","picture","preferred_username","profile","zoneinfo","updated_at","website","custom:userType","custom:internalUser","custom:addressLine1","custom:addressAbbreviation","custom:latitude","custom:longitude","custom:country","custom:zipCode","custom:tenantName","custom:userId","custom:department","custom:firstName","custom:lastName","custom:lastModifiedTime","custom:city","custom:middleName","custom:roleInformation","custom:tenantId","custom:addressLine2","custom:userRole","custom:createdTime","custom:userName","custom:addressId","custom:phone"]
}
variable "write_attributes" {
  type = list
  default = ["address","birthdate","email","family_name","gender","given_name","locale","middle_name","name","nickname","phone_number","picture","preferred_username","profile","zoneinfo","updated_at","website","custom:userType","custom:internalUser","custom:addressLine1","custom:addressAbbreviation","custom:latitude","custom:longitude","custom:country","custom:zipCode","custom:tenantName","custom:userId","custom:department","custom:firstName","custom:lastName","custom:lastModifiedTime","custom:city","custom:middleName","custom:tenantId","custom:addressLine2","custom:userRole","custom:createdTime","custom:userName","custom:addressId","custom:phone"]
}