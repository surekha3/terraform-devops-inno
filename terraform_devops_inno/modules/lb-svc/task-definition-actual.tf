data "template_file" "task-definition" {
  template = file("${path.module}/${var.task_definition}")	
  vars = {
   ccss-manager-url = var.template_file_urls["ccss_manager_url"]
   iq-url           = var.template_file_urls["ccss_iq_url"]
   ams-url          = var.template_file_urls["ams_url"]
   cim-url          = var.template_file_urls["cim_url"]
   track-url        = var.template_file_urls["track_url"]
   db-endpoint      = var.template_file_urls["db_endpoint"]
   ecr-image        = var.ecr_image
 }
}

resource "aws_ecs_task_definition" "service" {
  family 		= var.family
  container_definitions = data.template_file.task-definition.rendered
}
