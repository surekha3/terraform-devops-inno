resource "aws_alb" "ecs-load-balancer" {
      name                = var.ecs-lb-name
      security_groups     = [ var.sg_id ]
      subnets             = var.subnet_ids
      #security_groups = [ aws_security_group.sg_for_ec2_instances.id ]
      #subnets             = module.vpc.public_subnets

      tags = {
        Name = "ecs-load-balancer"
      }
  }

resource "aws_alb_target_group" "ecs-target-group" {
      count		  = var.target_group_count
      name                = var.ecs-target-group[count.index]
      port                = var.listerner_port[count.index]
      protocol            = "HTTP"
      vpc_id              = var.vpc_id
      health_check {
          healthy_threshold   = "5"
          unhealthy_threshold = "2"
          interval            = "30"
          matcher             = var.health_check_port
          path                = var.path
          port                = "traffic-port"
          protocol            = "HTTP"
          timeout             = "5"
      }

      tags = {
        Name = var.ecs-target-group[count.index]
      }
      depends_on = [aws_alb.ecs-load-balancer]
  }
  resource "aws_alb_listener" "alb-listener" {
      count		  = var.target_group_count
      load_balancer_arn = aws_alb.ecs-load-balancer.arn
      #load_balancer_arn = var.ecs_lb_arn
      port              = var.listerner_port[count.index]
      protocol          = "HTTP"

      default_action {
          target_group_arn = aws_alb_target_group.ecs-target-group[count.index].arn
          type             = "forward"
      }
  depends_on = [aws_alb_target_group.ecs-target-group]
  }

## ALB Listener Rule for API
resource "aws_alb_listener_rule" "rule" {
  count		  = var.target_group_count
  #count               = var.enable_alb ? 1 : 0
  listener_arn = aws_alb_listener.alb-listener[count.index].arn
  priority = var.priority

  action {
    type = "forward"
    target_group_arn = aws_alb_target_group.ecs-target-group[count.index].arn
  }

  condition {
    path_pattern {
    values = [
      var.path_pattern]
    }
}
}

