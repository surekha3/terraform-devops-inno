resource "aws_ecs_service" "test-ecs-service" {
    #count		= var.enable_alb ? 1 : 0
  	name            = var.service
  	#iam_role        = var.ecs_role 
  	cluster         = var.cluster_id
  	#task_definition = aws_ecs_task_definition.wordpress.family:max(aws_ecs_task_definition.wordpress.revision, data.aws_ecs_task_definition.wordpress.revision)
    task_definition = "${aws_ecs_task_definition.service.family}:${aws_ecs_task_definition.service.revision}"
  	desired_count   = 1

    dynamic "load_balancer" {
    for_each = [for g in var.groups: {
	  #target_group_arn  = aws_alb_target_group.ecs-target-group[g.number].arn
      index   = g.number
      container_port = g.container_port
      container_name = g.container_name
    }]

    content {
    	target_group_arn  = aws_alb_target_group.ecs-target-group[load_balancer.value.index].arn
        #target_group_arn   = var.alb_target_group_arn
    	container_port    = load_balancer.value.container_port
    	container_name    = load_balancer.value.container_name
	}

    #load_balancer {
    	#target_group_arn  = aws_alb_target_group.ecs-target-group[sample.value.index].arn
        #target_group_arn   = var.alb_target_group_arn
    	#container_port    = sample.value.container_port
    	#container_name    = sample.value.container_name
	#}

    }
    }
	

