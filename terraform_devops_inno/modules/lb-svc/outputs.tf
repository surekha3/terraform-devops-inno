output "alb_dns_name" {
 value       = concat(aws_alb.ecs-load-balancer.*.dns_name, [""])[0]
}