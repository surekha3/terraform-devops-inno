output "api_gateway_rest_api_id" {
  value = aws_api_gateway_rest_api.api-gateway.id
}
output "api_gateway_rest_api_name" {
  value = aws_api_gateway_rest_api.api-gateway.name
}
output "api_gateway_stage_id" {
  value = aws_api_gateway_stage.api-stage.id
}
output "api_gateway_stage_url" {
  value = aws_api_gateway_stage.api-stage.invoke_url
}