import json
import os
import pathlib


def parse_json2():
    with os.scandir(".") as it:
        for entry in it:
            if entry.name.endswith(".json") and entry.is_file():
                print(entry.name, entry.path)
                a_file = open(entry.name, "r")
                json_object = json.load(a_file)
                a_file.close()
                print(json_object)
                json_object["info"]["title"] = "${title}"
                json_object["host"] = "${host}"
                json_object["basePath"] = "${basePath}"
                json_object["securityDefinitions"]["CognitoAuthorizer"]["x-amazon-apigateway-authorizer"][
                    "providerARNs"] = [
                    "${providerARNs}"]
                a_file = open(entry.name, "w")
                json.dump(json_object, a_file)
                a_file.close()
                from pathlib import Path
                path = Path(entry.name)
                text = path.read_text()
                text = text.replace("${stageVariables", "$${stageVariables")
                path.write_text(text)



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    parse_json2()
