resource "aws_api_gateway_rest_api" "api-gateway" {
  name        = var.api_name
  description = var.api_description 
  body = templatefile("${path.module}/${var.exp_api_or_prod}/${var.template_file_name}",
    var.template_file_map
  )
  endpoint_configuration {
    types = ["REGIONAL"]
  }  
}

resource "aws_api_gateway_deployment" "api-gateway-deployment" {
  rest_api_id = aws_api_gateway_rest_api.api-gateway.id
 # stage_name  = var.api_stage_name
   stage_name  = ""
  lifecycle {
    create_before_destroy = true
  }
   variables =  {
    deployed_at = "${timestamp()}"
  }
}

resource "aws_api_gateway_stage" "api-stage" {
  stage_name    = var.api_stage_name
  rest_api_id   = aws_api_gateway_rest_api.api-gateway.id
  deployment_id = aws_api_gateway_deployment.api-gateway-deployment.id
  variables = var.stage_variables
}



