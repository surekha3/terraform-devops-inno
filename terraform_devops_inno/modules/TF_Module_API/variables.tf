variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "aws_profile" {
  type = string
  default = "default"
}

variable "api_name" {
  description = "API Name in API Gateway"
  type        = string
  default     = "Proterra custom API"
}

variable "api_description" {
  description = "API Description"
  type        = string
  default     = "Proterra custom API for API Gateway"
}

variable "api_swagger_file_name" {
  description = "API Swagger file name to import"
  type        = string
  default     = "exp-swagger-apigateway.json"
}

variable "api_stage_name" {
  description = "API Stage name to import"
  type        = string
  default     = "exp-env"
}

variable "api_baseurl" {
  description = "Baseurl for stage integration"
  type        = string
  default     = "{stageVariables.baseurl}"
}

variable "template_file_name" {
  type = string
}

variable "template_file_map" {
  type = map(string)
  default = {}
}

variable "stage_name" {
  type = string
  default = "stage"
}

variable "stage_variables" {
  type = map(string)
  default = {}
}

variable "exp_api_or_prod" {
  type = string
  default="templates-exp"
}