
variable "vpc_cidr" {
  description = "The CIDR for the entire VPC."
  type        = string
  default     = ""
}

variable "public_subnets" {
  description = "A list of public subnets CIDRs."
  type        = list
  default     = []
}

variable "private_subnets" {
  description = "A list of private subnets CIDRs."
  type        = list
  default     = []
}


variable "azs" {
  description = "A list of Availability zones in the region"
  default     = []
}

variable "enable_dns_hostnames" {
  description = "should be true if you want to use private DNS within the VPC"
  default     = false
}

variable "enable_dns_support" {
  description = "should be true if you want to use private DNS within the VPC"
  default     = false
}

variable "enable_nat_gateway" {
  description = "should be true if you want to provision NAT Gateways for each of your private networks"
  default     = false
}

variable "map_public_ip_on_launch" {
  description = "should be false if you do not want to auto-assign public IP on launch"
  default     = true
}

variable "private_propagating_vgws" {
  description = "A list of VGWs the private route table should propagate."
  default     = []
}

variable "public_propagating_vgws" {
  description = "A list of VGWs the public route table should propagate."
  default     = []
}

variable "tag_project" {
  description = "Project name such as SPAY, KPGW"
  default     = "proterra-exp"
}

variable "tag_environment" {
  description = "Environment such as dev, qa, prod"
  default     = "qa"
}
variable "vpc_additional_tags" {
  type = map(string)
  default = {}
}

variable "ssh_port" {
  default = "22"
}
variable "flow_log_cloudwatch_log_group" {
  description = "Log group for flow logs of VPC on cloudwatch"
  type        = string
  default     = "VPCFlowLog"
}
variable "prefix_ec2" {
  default = "EC2"
}
variable "prefix_db" {
  default = "DB"
}
variable "account_owner_ip" {
  type = list
}
variable "terraform_state_bucket" {
  default = ""
}

variable "Ec2_role" {
  description = "EC2 Instance profile Role"
  type = string
  default = "EC2_General"
}

variable "subnet_additional_tags" {
  type = map(string)
  default = {}  
}
variable "vpc_endpoint_tag_name" {
  type = string
}
