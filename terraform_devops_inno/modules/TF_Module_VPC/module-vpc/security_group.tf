resource "aws_default_security_group" "vpc" {
  vpc_id = aws_vpc.mod.id

  tags = {
    Name        = "${upper(format("%s-%s-VPC-Default", var.tag_project, var.tag_environment))}"
    role        = "default"
    provisioned = "terraform"
  }
}

# VPC common security groups for EC2
resource "aws_security_group" "common" {
  name   = lower(format("%s-%s-%scommon-sg", var.tag_project, var.tag_environment,var.prefix_ec2))
  vpc_id = aws_vpc.mod.id

  tags = {
    Name        = "${upper(format("%s-%s-%s-common-sg", var.tag_project, var.tag_environment,var.prefix_ec2))}"
    role        = "common"
    provisioned = "terraform"
  }
}

# All ssh access by explicit IPs
resource "aws_security_group_rule" "ingress-common-ec2-ssh" {
  type        = "ingress"
  from_port   = var.ssh_port
  to_port     = var.ssh_port
  protocol    = "tcp"
  cidr_blocks = [var.vpc_cidr]

  security_group_id = aws_security_group.common.id
}
