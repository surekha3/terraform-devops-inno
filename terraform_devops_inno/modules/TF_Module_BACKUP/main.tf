
resource "aws_backup_vault" "default" {
  count       = var.enabled ? 1 : 0
  name        = var.backup_name
  kms_key_arn = var.kms_key_arn
  tags        = var.backup_tags
}

resource "aws_backup_plan" "default" {
  count = var.enabled ? 1 : 0
  name  = var.backup_name

  rule {
    rule_name           = var.rule_name
    target_vault_name   = join("", aws_backup_vault.default.*.name)
    schedule            = var.schedule
    start_window        = var.start_window
    completion_window   = var.completion_window
    recovery_point_tags = var.recovery_tags

    dynamic "lifecycle" {
      for_each = var.cold_storage_after != null || var.delete_after != null ? ["true"] : []
      content {
        cold_storage_after = var.cold_storage_after
        delete_after       = var.delete_after
      }
    }
  }
}

data "aws_iam_policy_document" "assume_role" {
  count = var.enabled ? 1 : 0
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "default" {
  count              = var.enabled ? 1 : 0
  name               = var.backup_role_name
  assume_role_policy = join("", data.aws_iam_policy_document.assume_role.*.json)
  tags               = var.backup_role_tags 
}

resource "aws_iam_role_policy_attachment" "default" {
  count      = var.enabled ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = join("", aws_iam_role.default.*.name)
}

resource "aws_backup_selection" "default" {
  count        = var.enabled ? 1 : 0
  name         = var.backup_name
  iam_role_arn = join("", aws_iam_role.default.*.arn)
  plan_id      = join("", aws_backup_plan.default.*.id)
  resources    = var.backup_resources
}