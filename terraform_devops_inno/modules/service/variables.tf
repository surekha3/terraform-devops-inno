variable "cluster_name" {
  description = "The name to use to create the cluster and the resources. Only alphanumeric characters and dash allowed (e.g. 'my-cluster')"
}

variable "cluster_id" {}

variable "aws_region" {
  default     = "us-west-1"
  description = "The AWS region where to launch the cluster and releated resources"
}

variable "vpc_id" {
  default     = "invalid-vpc"
  description = "A VPC where the EC2 instances will be launched in. Either pass this variable or \"create_vpc=true\""
}

variable "create_vpc" {
  description = "Whether or not to create a VPC to contain this cluster and its resources, or reuse an existing VPC (passed to the module as a vpc_id parameter)"
}
variable "container_name" {}
variable "task_definition" {}
variable "path_pattern" {
	default = "/"
}

variable "family" {}
variable "ecs_role" {}
variable "alb_listener_arn" {}
variable "alb_target_group_arn" {}
variable "priority" {}
variable "service" {}
variable "ecs-target-group" {}
variable "path" {
   default = "/"
}
variable "enable_alb" {
	default = true
}
variable "ams-url" {
 default = "http://exp-dev-svc.proterra.com/ams"
}
variable "iq-url" {
 default = "http://exp-dev-svc.proterra.com/iq"
}
variable "track-url" {
 default = "http://exp-dev-svc.proterra.com/ccss"

}
variable "ccss-manager-url" {
 default = "http://exp-dev-svc.proterra.com/api"
}

variable "cim-url" {
 default = "http://exp-dev-svc.proterra.com/cim"
}
variable "template_file_urls" {
 default ={}
 type = map(string)
}

variable "ecr_image" {
 type = string
}
