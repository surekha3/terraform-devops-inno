resource "aws_ecs_service" "test-ecs-service" {
        count		= var.enable_alb ? 1 : 0
  	name            = var.service
  	iam_role        = var.ecs_role 
  	cluster         = var.cluster_id
  	#task_definition = aws_ecs_task_definition.wordpress.family:max(aws_ecs_task_definition.wordpress.revision, data.aws_ecs_task_definition.wordpress.revision)
        task_definition = "${aws_ecs_task_definition.service.family}:${aws_ecs_task_definition.service.revision}"
  	desired_count   = 1

  	load_balancer {
    	target_group_arn  = aws_alb_target_group.ecs-target-group[0].arn
        #target_group_arn   = var.alb_target_group_arn
    	container_port    = 8080
    	container_name    = var.container_name
	}
}
resource "aws_ecs_service" "service" {
      count           = var.enable_alb ? 0 : 1
      name            = var.service
      cluster         = var.cluster_id
      task_definition = "${aws_ecs_task_definition.service.family}:${aws_ecs_task_definition.service.revision}"
      desired_count   = 1
}
