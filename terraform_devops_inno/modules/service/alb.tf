resource "aws_alb_target_group" "ecs-target-group" {
      count		  = var.enable_alb ? 1 : 0
      name                = var.ecs-target-group
      port                = "80"
      protocol            = "HTTP"
      vpc_id              = var.vpc_id
      health_check {
          healthy_threshold   = "5"
          unhealthy_threshold = "2"
          interval            = "30"
          matcher             = "404,303"
          path                = var.path
          port                = "traffic-port"
          protocol            = "HTTP"
          timeout             = "5"
      }

      tags = {
        Name = var.ecs-target-group
      }
  }
## ALB Listener Rule for API
resource "aws_alb_listener_rule" "rule" {
  count               = var.enable_alb ? 1 : 0
  listener_arn = var.alb_listener_arn
  priority = var.priority

  action {
    type = "forward"
    target_group_arn = aws_alb_target_group.ecs-target-group[0].arn
  }

  condition {
    path_pattern {
    values = [
      var.path_pattern]
    }
}
}

