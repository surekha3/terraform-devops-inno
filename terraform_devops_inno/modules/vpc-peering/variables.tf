variable "owner_profile" {
  description = "AWS Profile"
  default     = "proterra-exp"
}
variable "region" {
  description = "AWS region"
  default     = "us-west-1"
}
variable "peer_region" {
 default = "us-west-1"
}

variable "accepter_profile" {
  description = "AWS Profile"
  default     = "proterra-exp"
}

variable "owner_vpc_id" {
  description = "Owner VPC Id"
}

variable "accepter_vpc_id" {
  description = "Accepter VPC Id"
}
