#Declare the data source for creating subnet group

data "aws_subnet_ids" "default_vpc" {
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group" "redis-sg" {
  name = "${var.cluster_security_group_name}"
  ingress {
    from_port   = "6397"
    to_port     = "6397"
    protocol    = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  tags = {
    Name        = "${var.cluster_security_group_name}"
    Project     = "MDC"
  }
}


resource "aws_elasticache_subnet_group" "default_subnet_group" {
  name       = "${var.cluster_subnet_group}"
  subnet_ids = "${var.all-subnets-list}"
}

resource "aws_elasticache_cluster" "redis-mdc" {
  cluster_id           = "${lower(var.cluster_id)}"
  engine               = "${var.engine_type}"
  node_type            = "${var.node_type}"
  num_cache_nodes      = "${var.num_cache_nodes}"
  parameter_group_name = "${var.parameter_group_name}"
  engine_version       = "${var.engine_version}"
  port                 = "${var.port_number}"
  security_group_ids   = ["${aws_security_group.redis-sg.id}"]
  subnet_group_name    = "${aws_elasticache_subnet_group.default_subnet_group.name}"
}


