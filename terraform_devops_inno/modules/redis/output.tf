output "redis_address" {
  value = "aws_elasticache_cluster.redis-mdc.cache_nodes.0.address"
  description = "Endpoint of the main server instance."
}
output "redis_id" {
  value = "aws_elasticache_cluster.redis-mdc.cache_nodes.0.id"
}

