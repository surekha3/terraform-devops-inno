# Create a new temporary VPC, to be used for this example
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "vpc-for-example-ecs-cluster"
  cidr = "10.1.0.0/16"

  azs = [
    data.aws_availability_zones.available.names[0],
    data.aws_availability_zones.available.names[1],
    data.aws_availability_zones.available.names[2]
  ]
  private_subnets = [
    "10.1.1.0/24",
    "10.1.2.0/24",
  "10.1.3.0/24"]
  public_subnets = [
    "10.1.101.0/24",
    "10.1.102.0/24",
  "10.1.103.0/24"]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
