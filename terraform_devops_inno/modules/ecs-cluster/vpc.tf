# TODO create a new VPC if the variable vpc_id is empty

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "VPC of cluster ${var.cluster_name}"
  cidr = var.cidr
	azs = var.az
  private_subnets = var.private_subnets
  public_subnets = var.public_subnets

  enable_nat_gateway = true
  enable_vpn_gateway = false
  single_nat_gateway = true
  create_vpc         = var.create_vpc

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
locals {
  public_subnet_ids = var.create_vpc ? module.vpc.public_subnets : var.subnet_ids
}
