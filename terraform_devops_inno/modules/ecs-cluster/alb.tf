resource "aws_alb" "ecs-load-balancer" {
      name                = var.ecs-lb-name
      #security_groups     =  var.sg_id
      #subnets             = var.subnet_ids
      security_groups = [ aws_security_group.sg_for_ec2_instances.id ]
      subnets             = module.vpc.public_subnets

      tags = {
        Name = var.ecs-lb-name
      }
  }

  resource "aws_alb_target_group" "ecs-target-group" {
      name                = var.ecs_target_group_name
      port                = "80"
      protocol            = "HTTP"
      vpc_id              = module.vpc.vpc_id

      health_check {
          healthy_threshold   = "5"
          unhealthy_threshold = "2"
          interval            = "30"
          matcher             = "200"
          path                = "/"
          port                = "traffic-port"
          protocol            = "HTTP"
          timeout             = "5"
      }

      tags = {
        Name = "ecs-target-group"
      }
  depends_on = [aws_alb_target_group.ecs-target-group]
  }


  resource "aws_alb_listener" "alb-listener" {
      load_balancer_arn = aws_alb.ecs-load-balancer.arn
      #load_balancer_arn = var.ecs_lb_arn
      port              = "80"
      protocol          = "HTTP"

      default_action {
          target_group_arn = aws_alb_target_group.ecs-target-group.arn
          type             = "forward"
      }
  depends_on = [aws_alb_target_group.ecs-target-group]
  }
