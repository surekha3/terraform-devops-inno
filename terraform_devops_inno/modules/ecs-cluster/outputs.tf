output "cluster_id" {
  value = aws_ecs_cluster.ecs_cluster.id
}
output "cluster_name" {
 value = aws_ecs_cluster.ecs_cluster.name 
}
output "ecs_role" {
 value = aws_iam_role.ecs-service-role.name
}
output "vpc_id" {
  value = data.aws_vpc.main.id
}
output "ecs_lb_arn" {
 value = aws_alb.ecs-load-balancer.arn
}
#output "service_discovery_id" {
#  value = aws_service_discovery_private_dns_namespace.dns_namespace.id
#}

output "public_subnets" {
 value = module.vpc.public_subnets
}

output "private_subnets_id" {
 value = module.vpc.private_subnets
}


output "sg_id" {
 value = aws_security_group.sg_for_ec2_instances.id
}

output "alb_listener_arn" {
 value = aws_alb_listener.alb-listener.arn
}

output "alb_target_group_arn" {
 value = aws_alb_target_group.ecs-target-group.arn
}

output "alb_dns_name" {
 value       = concat(aws_alb.ecs-load-balancer.*.dns_name, [""])[0]
}
