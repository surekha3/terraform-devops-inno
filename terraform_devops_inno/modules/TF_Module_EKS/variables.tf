#
# Variables Configuration
#
variable "vpc_id" {
  default = " "
  type    = string
}
variable "cluster-name" {
  default = "proterra-mdc-eks-cluster"
  type    = string
}

variable "all-subnets-list" {
   type    = list
}

variable "private-subnets-list" {
  type    = list
}

variable "launch_template" {
  type    = map
  default = {
      default ={   
      id      = "lt-07dddd0d44bdd39a3"
      name    = "devsecops-custom-ami"
      version = "latest_version"
      }
  }

}

variable "worker-nodes-data" {
  type = map(string)
  default = {}
}

variable "node-groups-data" {
  type = list
  default=[]
}