output "sqs_queue" {
  value = "${aws_sqs_queue.queue.name}"
}
output "sqs_queue_arn" {
  value = "${aws_sqs_queue.queue.arn}"
}