############ Common for all SQS #####
variable "sqs_env_tag" {
  type        = string
   default     = ""
}
######### SQS #######
variable "sqs_name" {
  type        = string
   default     = ""
}
variable "sqs_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_receive_wait_time_seconds" {
  type        = number
   default     = 0
}

variable "sqs_fifo_queue" {
   default     = false
}

variable "sqs_content_based_deduplication" {
   default     = false
}
