###### For collecting AWS account ID
data "aws_caller_identity" "current" {}

##################  SQS  ############
resource "aws_sqs_queue" "queue" {
  name                      = var.sqs_name
  #The default for this attribute is 30
  visibility_timeout_seconds = var.sqs_visibility_timeout_seconds
  #The default for this attribute is 345600 (4 days).
  message_retention_seconds = var.sqs_message_retention_seconds
  #default for this attribute is 262144 (256 KiB)
  max_message_size          = var.sqs_max_message_size
  # The default for this attribute is 0 seconds.
  delay_seconds             = var.sqs_delay_seconds
  #The default for this attribute is 0, meaning that the call will return immediately.
  receive_wait_time_seconds = var.sqs_receive_wait_time_seconds
  # The default for this attribute is false, to enable FIFO set true.
  fifo_queue                = var.sqs_fifo_queue
  # The default for this attribute is false, to enable depulicate set true.
  content_based_deduplication = var.sqs_content_based_deduplication
  tags = {
    Environment = var.sqs_env_tag
  }
}

resource "aws_sqs_queue_policy" "queue-policy" {
  queue_url = aws_sqs_queue.queue.id

  policy = <<POLICY
{
  "Version": "2008-10-17",
  "Id": "__default_policy_ID",
  "Statement": [
    {
      "Sid": "__owner_statement",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:*",
      "Resource": "arn:aws:sqs:us-east-1:${data.aws_caller_identity.current.account_id}:${var.sqs_name}"
    }
  ]
}
POLICY
}

