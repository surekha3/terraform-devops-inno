
##################  SNS  ############

resource "aws_sns_topic" "main" {
  name = var.sns_name
}

resource "aws_sns_topic_subscription" "user_updates_sqs_target" {
  count = length(var.sns_subscription_arn_list) > 0 ? length(var.sns_subscription_arn_list) : 0
  topic_arn = aws_sns_topic.main.arn
  protocol  = var.sns_subscription_arn_list[count.index]["protocol"]
  endpoint  = var.sns_subscription_arn_list[count.index]["arn"]
}

