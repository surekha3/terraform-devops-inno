############ SNS  #####
variable "sns_name" {
  type        = string
   default     = ""
}

#variable "sns_subscription_protocol" {
#  type        = string
#   default     = "sqs"
#}

variable "sns_subscription_arn_list" {
  type        = list
   default     = []
}
