#provider "aws" {
#  region  = var.aws_region
#  profile = var.aws_profile
#}

resource "aws_cognito_user_pool" "pool" {
  name                       = var.cognito_user_pool_name
  email_verification_subject = var.cognito_email_verif_subject
  email_verification_message = var.cognito_email_verif_message
  sms_authentication_message = var.cognito_sms_verif_message
  sms_verification_message   = "{####}"
  username_attributes      = ["email"]
  auto_verified_attributes = ["email"]
  #alias_attributes           = ["email", "preferred_username"]


  verification_message_template {
    default_email_option = "CONFIRM_WITH_CODE"
  }

 admin_create_user_config {
    allow_admin_create_user_only = false
    #moved below to password policy
    #unused_account_validity_days = 7

    invite_message_template {

     email_subject = var.cognito_email_subject
     sms_message   = "Your username is {username} and temporary password is {####}. "
     email_message = var.cognito_sms_email_message
    }


 }

 
 email_configuration {
     email_sending_account  = "COGNITO_DEFAULT"
     reply_to_email_address = ""
     source_arn             = ""
 }

 password_policy {
    minimum_length    = 8
    require_lowercase = true
    require_numbers   = true
    require_symbols   = true
    require_uppercase = true
    temporary_password_validity_days = var.cognito_unused_account_days
  }
 

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "createdBy"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "lastModifiedBy"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "company"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = false
    name                     = "userRole"
    required                 = false

    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "family_name"
    required                 = true

    string_attribute_constraints {
      min_length = 0
      max_length = 2048
    }
   }

   schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "given_name"
    required                 = true

    string_attribute_constraints {
      min_length = 1
      max_length = 2048
    }
   }

  tags = {
    "Name"    = "proterra-exp"
    "Project" = "terraform"
  }
}

 resource "aws_cognito_user_pool_client" "client" {
    name = var.cognito_app_client_name
    user_pool_id = aws_cognito_user_pool.pool.id
    generate_secret = false
    explicit_auth_flows = var.explicit_auth_flows
    read_attributes = ["family_name","given_name","email","name","custom:company","custom:lastModifiedBy","custom:createdBy","custom:userRole"]
    write_attributes = ["family_name","given_name"]
 }



