variable "aws_region" {
  type = string
}

variable "aws_profile" {
  type = string
}

variable "state_bucket" {
  type = string
}

variable "state_bucket_key" {
  type = string
}


variable "environment" {
  type = string
}


############### S3 ################

variable "apex_ui_s3_bucket_name" {
    type = string
} 

variable "apex_ui_s3_bucket_policy_file_name" {
    type = string
}

variable "ccss_ui_s3_bucket_name" {
    type = string
}

variable "ccss_ui_s3_bucket_policy_file_name" {
    type = string
}
#################### VPC ###############

###################################
# Variables added with VPC module.
variable "vpc_cidr" {
  type = string
}

variable "vpc_public_subnets" {
  type = list
}

variable "vpc_private_subnets" {
  type = list
}

variable "vpc_availabilityzones" {
  type = list
}

variable "project" {
  type = string
}

#variable "vpc_flow_log_cloudwatch_log_group" {
#  type = string
#}

variable "vpc_account_owner_ip" {
  type = list
  default = [" "]
}

variable "subnet_additional_tags" {
 type = map(string)
 default = {}
}

##################### RDS ##############
# Variables added with RDS.
variable "environment_short" {
  type = string
}

variable "env_short" {
  type = string
}

variable "rds_cidr1" {
  type = string
}

variable "rds_engine_version" {
  type = string
}

variable "rds_subnet_group_name" {
  type = string
}

variable "rds_vpc_bastion_sg_id" {
  type = string
  default = ""
}


variable "rds_instance_class" {
  type = string
}

variable "rds_db_name" {
  type = string
}

variable "rds_username" {
  type = string
}

variable "rds_password" {
  type = string
}

variable "rds_identifier" {
  type = string
}

variable "rds_retention_period" {
  type = string
}

variable "rds_tag_sec_assets_gateway" {
  type    = string
  default = "undefined"
}


variable "rds_tag_sec_assets_db" {
  type    = string
  default = "undefined"
}

variable "rds_tag_sec_assets" {
  type    = string
  default = "undefined"
}

variable "rds_sg_tag_sec_ect" {
  type    = string
  default = "HGETWELL"
}

variable "rds_sg_tag_sec_gw" {
  type    = string
  default = "Gateway SSH connecting from DB Bastion"
}

variable "rds_tag_sec_assets_pii" {
  type    = string
  default = "RDS Security assets pii"
}
variable "rds_storage_encrypted" {
  description = "RDS Storage Encrypted."
  type        = string
}

variable "rds_max_allocated_storage" {
  description = "RDS Max Allocated Storage."
  type        = string
}

variable "rds_security_group_name" {
  description = "RDS Security Group"
  type        = string
}

variable "rds_security_group_description" {
  description = "RDS Security Group"
  type        = string
}

variable "rds_copy_tags_to_snapshot" {
  description = "RDS copy_tags_to_snapshot "
  type        = string
}

variable "rds_monitoring_interval" {
  description = "RDS monitoring interval"
  type        = string
}

variable "rds_parameter_group_name" {
  description = "RDS parameter group name"
  type        = string
}

variable "rds_skip_final_snapshot" {
  description = "RDS "
  type        = string
}


################ API #####################
variable "template_file_name_API_IQ" {
  type = string
}

variable "template_file_map_iq" {
  type = map(string)
  default = {}
}

variable "stage_variables_iq" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_CCSS" {
  type = string
}

variable "template_file_map_ccss" {
  type = map(string)
  default = {}
}

variable "stage_variables_ccss" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_AMS" {
  type = string
}

variable "template_file_map_ams" {
  type = map(string)
  default = {}
}

variable "stage_variables_ams" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_FILE" {
  type = string
}

variable "template_file_map_file" {
  type = map(string)
  default = {}
}

variable "stage_variables_file" {
 type = map(string)
 default = {}
}


variable "template_file_name_API_FILE_AMS" {
  type = string
}

variable "template_file_map_file_ams" {
  type = map(string)
  default = {}
}

variable "stage_variables_file_ams" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_RUNS" {
  type = string
}

variable "template_file_map_runs" {
  type = map(string)
  default = {}
}

variable "stage_variables_runs" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_CIM" {
  type = string
}

variable "template_file_map_cim" {
  type = map(string)
  default = {}
}
variable "stage_variables_cim" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_DATALAKE" {
  type = string
}

variable "template_file_map_datalake" {
  type = map(string)
  default = {}
}
variable "stage_variables_datalake" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_OCPP" {
  type = string
}

variable "template_file_map_ocpp" {
  type = map(string)
  default = {}
}
variable "stage_variables_ocpp" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_MISCEL" {
  type = string
}

variable "template_file_map_miscel" {
  type = map(string)
  default = {}
}
variable "stage_variables_miscel" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_MP" {
  type = string
}

variable "template_file_map_mp" {
  type = map(string)
  default = {}
}
variable "stage_variables_mp" {
 type = map(string)
 default = {}
}

variable "template_file_name_API_MP_CP" {
  type = string
}

variable "template_file_map_mp_cp" {
  type = map(string)
  default = {}
}
variable "stage_variables_mp_cp" {
 type = map(string)
 default = {}
}


################## Cognito #######################


##$ Cognito Variables


variable "cp_cognito_user_pool_name" {
  type = string
  default = ""
}

variable "cp_cognito_email_verif_subject" {
  type = string
  default = ""
}

variable "cp_cognito_email_verif_message" {
  type = string
  default = ""
}

variable "cp_cognito_sms_verif_message" {
  type = string
  default = ""
}

variable "cp_cognito_unused_account_days" {
  type = number
  default = 30
}

variable "cp_cognito_email_subject" {
  type = string
  default = ""
}



variable "cp_cognito_sms_email_message" {
  type = string
  default = "\u003c!DOCTYPE html\n  PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"\u003e\n\u003chtml xmlns=\"http://www.w3.org/1999/xhtml\"\u003e\n\n\u003chead\u003e\n  \u003cmeta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /\u003e\n  \u003cmeta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /\u003e\n  \u003ctitle\u003eSetup a new password\u003c/title\u003e\n  \u003c!-- \n    The style block is collapsed on page load to save you some scrolling.\n    Postmark automatically inlines all CSS properties for maximum email client \n    compatibility. You can just update styles here, and Postmark does the rest.\n    --\u003e\n  \u003cstyle type=\"text/css\" rel=\"stylesheet\" media=\"all\"\u003e\n    /* Base ------------------------------ */\n\n    *:not(br):not(tr):not(html) {\n      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n      box-sizing: border-box;\n    }\n\n    body {\n      width: 100% !important;\n      height: 100%;\n      margin: 0;\n      line-height: 1.4;\n      background-color: #F2F4F6;\n      color: #74787E;\n      -webkit-text-size-adjust: none;\n    }\n\n    p,\n    ul,\n    ol,\n    blockquote {\n      line-height: 1.4;\n      text-align: left;\n    }\n\n    a {\n      color: #3869D4;\n    }\n\n    a img {\n      border: none;\n    }\n\n    td {\n      word-break: break-word;\n    }\n\n    /* Layout ------------------------------ */\n\n    .email-wrapper {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #F2F4F6;\n    }\n\n    .email-content {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    /* Masthead ----------------------- */\n\n    .email-masthead {\n      padding: 25px 0;\n      text-align: center;\n    }\n\n    .email-masthead_logo {\n      width: 94px;\n    }\n\n    .email-masthead_name {\n      font-size: 16px;\n      font-weight: bold;\n      color: #bbbfc3;\n      text-decoration: none;\n      text-shadow: 0 1px 0 white;\n    }\n\n    /* Body ------------------------------ */\n\n    .email-body {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      border-top: 1px solid #EDEFF2;\n      border-bottom: 1px solid #EDEFF2;\n      background-color: #FFFFFF;\n    }\n\n    .email-body_inner {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #FFFFFF;\n    }\n\n    .email-footer {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .email-footer p {\n      color: #AEAEAE;\n    }\n\n    .body-action {\n      width: 100%;\n      margin: 30px auto;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .body-sub {\n      margin-top: 25px;\n      padding-top: 25px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .content-cell {\n      padding: 35px;\n    }\n\n    .preheader {\n      display: none !important;\n      visibility: hidden;\n      mso-hide: all;\n      font-size: 1px;\n      line-height: 1px;\n      max-height: 0;\n      max-width: 0;\n      opacity: 0;\n      overflow: hidden;\n    }\n\n    /* Attribute list ------------------------------ */\n\n    .attributes {\n      margin: 0 0 21px;\n    }\n\n    .attributes_content {\n      background-color: #EDEFF2;\n      padding: 16px;\n    }\n\n    .attributes_item {\n      padding: 0;\n    }\n\n    /* Related Items ------------------------------ */\n\n    .related {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .related_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .related_item-title {\n      display: block;\n      margin: .5em 0 0;\n    }\n\n    .related_item-thumb {\n      display: block;\n      padding-bottom: 10px;\n    }\n\n    .related_heading {\n      border-top: 1px solid #EDEFF2;\n      text-align: center;\n      padding: 25px 0 10px;\n    }\n\n    /* Discount Code ------------------------------ */\n\n    .discount {\n      width: 100%;\n      margin: 0;\n      padding: 24px;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #EDEFF2;\n      border: 2px dashed #9BA2AB;\n    }\n\n    .discount_heading {\n      text-align: center;\n    }\n\n    .discount_body {\n      text-align: center;\n      font-size: 15px;\n    }\n\n    /* Social Icons ------------------------------ */\n\n    .social {\n      width: auto;\n    }\n\n    .social td {\n      padding: 0;\n      width: auto;\n    }\n\n    .social_icon {\n      height: 20px;\n      margin: 0 8px 10px 8px;\n      padding: 0;\n    }\n\n    /* Data table ------------------------------ */\n\n    .purchase {\n      width: 100%;\n      margin: 0;\n      padding: 35px 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_content {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .purchase_heading {\n      padding-bottom: 8px;\n      border-bottom: 1px solid #EDEFF2;\n    }\n\n    .purchase_heading p {\n      margin: 0;\n      color: #9BA2AB;\n      font-size: 12px;\n    }\n\n    .purchase_footer {\n      padding-top: 15px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .purchase_total {\n      margin: 0;\n      text-align: right;\n      font-weight: bold;\n      color: #2F3133;\n    }\n\n    .purchase_total--label {\n      padding: 0 15px 0 0;\n    }\n\n    /* Utilities ------------------------------ */\n\n    .align-right {\n      text-align: right;\n    }\n\n    .align-left {\n      text-align: left;\n    }\n\n    .align-center {\n      text-align: center;\n    }\n\n    /*Media Queries ------------------------------ */\n\n    @media only screen and (max-width: 600px) {\n\n      .email-body_inner,\n      .email-footer {\n        width: 100% !important;\n      }\n    }\n\n    @media only screen and (max-width: 500px) {\n      .button {\n        width: 100% !important;\n      }\n    }\n\n    /* Buttons ------------------------------ */\n\n    .button {\n      background-color: #3869D4;\n      border-top: 10px solid #3869D4;\n      border-right: 18px solid #3869D4;\n      border-bottom: 10px solid #3869D4;\n      border-left: 18px solid #3869D4;\n      display: inline-block;\n      color: #FFF;\n      text-decoration: none;\n      border-radius: 3px;\n      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);\n      -webkit-text-size-adjust: none;\n    }\n\n    .button--green {\n      background-color: #62a744;\n      border-top: 10px solid #62a744;\n      border-right: 18px solid #62a744;\n      border-bottom: 10px solid #62a744;\n      border-left: 18px solid #62a744;\n    }\n\n    .button--red {\n      background-color: #FF6136;\n      border-top: 10px solid #FF6136;\n      border-right: 18px solid #FF6136;\n      border-bottom: 10px solid #FF6136;\n      border-left: 18px solid #FF6136;\n    }\n\n    /* Type ------------------------------ */\n\n    h1 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 19px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h2 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 16px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h3 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 14px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    p {\n      margin-top: 0;\n      color: #74787E;\n      font-size: 16px;\n      line-height: 1.5em;\n      text-align: left;\n    }\n\n    p.sub {\n      font-size: 12px;\n    }\n\n    p.center {\n      text-align: center;\n    }\n  \u003c/style\u003e\n\u003c/head\u003e\n\n\u003cbody\u003e\n  \u003cspan class=\"preheader\"\u003eUse this link to reset your password. The link is only valid for 24 hours.\u003c/span\u003e\n  \u003ctable class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n    \u003ctr\u003e\n      \u003ctd align=\"center\"\u003e\n        \u003ctable class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-masthead\"\u003e\n              \u003cdiv class=\"navbar-header\" style=\"width:100%; height:100px\"\u003e\n\n                \u003ca class=\"navbar-brand\" rel=\"home\" href=\"#\" title=\"\"\u003e\n                  \u003cimg style=\"margin-top: 10px;\" src=\"http://stag-connected.proterra.com/images/logo.png\"\u003e\n                \u003c/a\u003e\n              \u003c/div\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003c!-- Email Body --\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n              \u003ctable class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003c!-- Body content --\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\"\u003e\n                    \u003ch1\u003eHi {username},\u003c/h1\u003e\n                    \u003cp\u003eYour Proterra Control Panel administrator has created an account for you.\u003c/p\u003e\n                    \u003cp\u003eClick below to complete your account setup. You will be asked to set your password.\u003c/p\u003e\n                    \u003c!-- Action --\u003e\n                    \u003ctable class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd align=\"center\"\u003e\n                          \u003c!-- Border based button\n                       https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design --\u003e\n                          \u003ctable width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                            \u003ctr\u003e\n                              \u003ctd align=\"center\"\u003e\n                                \u003ctable border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                                  \u003ctr\u003e\n                                    \u003ctd\u003e\n                                      \u003ca href=\"http://multiplexer-cp-exp-qa.apex.com.s3-website-us-east-1.amazonaws.com/resetPassword?username={username}\u0026temporaryPassword={####}\"\n                                        class=\"button button--green\" target=\"_blank\"\n                                        style=\"color: white !important;\"\u003eSet up new password\u003c/a\u003e\n                                    \u003c/td\u003e\n                                  \u003c/tr\u003e\n                                \u003c/table\u003e\n                              \u003c/td\u003e\n                            \u003c/tr\u003e\n                          \u003c/table\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                    \u003cp\u003eIf you have issues or questions please contact your admin or \u003ca\n                        href=\"mailto:connected@proterra.com\"\u003eControl Panel support\u003c/a\u003e.\u003c/p\u003e\n                    \u003cp\u003eThank you,\n                      \u003cbr\u003eThe Control Panel Team\u003c/p\u003e\n                    \u003c!-- Sub copy --\u003e\n                    \u003ctable class=\"body-sub\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd\u003e\n                          \u003cp class=\"sub\"\u003eIf you’re having trouble with the button above, copy and paste the URL below\n                            into your web browser.\u003c/p\u003e\n                          \u003cp class=\"sub\"\u003e\n                            http://multiplexer-cp-exp-qa.apex.com.s3-website-us-east-1.amazonaws.com/resetPassword?username={username}\u0026temporaryPassword={####}\n                          \u003c/p\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003ctr\u003e\n            \u003ctd\u003e\n              \u003ctable class=\"email-footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\" align=\"center\"\u003e\n                    \u003cp class=\"sub align-center\"\u003e\u0026copy; 2019 Proterra. All rights reserved.\u003c/p\u003e\n\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n        \u003c/table\u003e\n      \u003c/td\u003e\n    \u003c/tr\u003e\n  \u003c/table\u003e\n\u003c/body\u003e\n\n\u003c/html\u003e"
}

###########

variable "ams_cognito_user_pool_name" {
  type = string
  default = ""
}

variable "ams_cognito_email_verif_subject" {
  type = string
  default = ""
}

variable "ams_cognito_email_verif_message" {
  type = string
  default = ""
}

variable "ams_cognito_sms_verif_message" {
  type = string
  default = ""
}

variable "ams_cognito_unused_account_days" {
  type = number
  default = 30
}

variable "ams_cognito_email_subject" {
  type = string
  default = ""
}

variable "ams_cognito_sms_email_message" {
  type = string
  default = "\u003c!DOCTYPE html\n  PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"\u003e\n\u003chtml xmlns=\"http://www.w3.org/1999/xhtml\"\u003e\n\n\u003chead\u003e\n  \u003cmeta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /\u003e\n  \u003cmeta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /\u003e\n  \u003ctitle\u003eSetup a new password\u003c/title\u003e\n  \u003c!-- \n    The style block is collapsed on page load to save you some scrolling.\n    Postmark automatically inlines all CSS properties for maximum email client \n    compatibility. You can just update styles here, and Postmark does the rest.\n    --\u003e\n  \u003cstyle type=\"text/css\" rel=\"stylesheet\" media=\"all\"\u003e\n    /* Base ------------------------------ */\n\n    *:not(br):not(tr):not(html) {\n      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n      box-sizing: border-box;\n    }\n\n    body {\n      width: 100% !important;\n      height: 100%;\n      margin: 0;\n      line-height: 1.4;\n      background-color: #F2F4F6;\n      color: #74787E;\n      -webkit-text-size-adjust: none;\n    }\n\n    p,\n    ul,\n    ol,\n    blockquote {\n      line-height: 1.4;\n      text-align: left;\n    }\n\n    a {\n      color: #3869D4;\n    }\n\n    a img {\n      border: none;\n    }\n\n    td {\n      word-break: break-word;\n    }\n\n    /* Layout ------------------------------ */\n\n    .email-wrapper {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #F2F4F6;\n    }\n\n    .email-content {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    /* Masthead ----------------------- */\n\n    .email-masthead {\n      padding: 25px 0;\n      text-align: center;\n    }\n\n    .email-masthead_logo {\n      width: 94px;\n    }\n\n    .email-masthead_name {\n      font-size: 16px;\n      font-weight: bold;\n      color: #bbbfc3;\n      text-decoration: none;\n      text-shadow: 0 1px 0 white;\n    }\n\n    /* Body ------------------------------ */\n\n    .email-body {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      border-top: 1px solid #EDEFF2;\n      border-bottom: 1px solid #EDEFF2;\n      background-color: #FFFFFF;\n    }\n\n    .email-body_inner {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #FFFFFF;\n    }\n\n    .email-footer {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .email-footer p {\n      color: #AEAEAE;\n    }\n\n    .body-action {\n      width: 100%;\n      margin: 30px auto;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .body-sub {\n      margin-top: 25px;\n      padding-top: 25px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .content-cell {\n      padding: 35px;\n    }\n\n    .preheader {\n      display: none !important;\n      visibility: hidden;\n      mso-hide: all;\n      font-size: 1px;\n      line-height: 1px;\n      max-height: 0;\n      max-width: 0;\n      opacity: 0;\n      overflow: hidden;\n    }\n\n    /* Attribute list ------------------------------ */\n\n    .attributes {\n      margin: 0 0 21px;\n    }\n\n    .attributes_content {\n      background-color: #EDEFF2;\n      padding: 16px;\n    }\n\n    .attributes_item {\n      padding: 0;\n    }\n\n    /* Related Items ------------------------------ */\n\n    .related {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .related_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .related_item-title {\n      display: block;\n      margin: .5em 0 0;\n    }\n\n    .related_item-thumb {\n      display: block;\n      padding-bottom: 10px;\n    }\n\n    .related_heading {\n      border-top: 1px solid #EDEFF2;\n      text-align: center;\n      padding: 25px 0 10px;\n    }\n\n    /* Discount Code ------------------------------ */\n\n    .discount {\n      width: 100%;\n      margin: 0;\n      padding: 24px;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #EDEFF2;\n      border: 2px dashed #9BA2AB;\n    }\n\n    .discount_heading {\n      text-align: center;\n    }\n\n    .discount_body {\n      text-align: center;\n      font-size: 15px;\n    }\n\n    /* Social Icons ------------------------------ */\n\n    .social {\n      width: auto;\n    }\n\n    .social td {\n      padding: 0;\n      width: auto;\n    }\n\n    .social_icon {\n      height: 20px;\n      margin: 0 8px 10px 8px;\n      padding: 0;\n    }\n\n    /* Data table ------------------------------ */\n\n    .purchase {\n      width: 100%;\n      margin: 0;\n      padding: 35px 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_content {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .purchase_heading {\n      padding-bottom: 8px;\n      border-bottom: 1px solid #EDEFF2;\n    }\n\n    .purchase_heading p {\n      margin: 0;\n      color: #9BA2AB;\n      font-size: 12px;\n    }\n\n    .purchase_footer {\n      padding-top: 15px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .purchase_total {\n      margin: 0;\n      text-align: right;\n      font-weight: bold;\n      color: #2F3133;\n    }\n\n    .purchase_total--label {\n      padding: 0 15px 0 0;\n    }\n\n    /* Utilities ------------------------------ */\n\n    .align-right {\n      text-align: right;\n    }\n\n    .align-left {\n      text-align: left;\n    }\n\n    .align-center {\n      text-align: center;\n    }\n\n    /*Media Queries ------------------------------ */\n\n    @media only screen and (max-width: 600px) {\n\n      .email-body_inner,\n      .email-footer {\n        width: 100% !important;\n      }\n    }\n\n    @media only screen and (max-width: 500px) {\n      .button {\n        width: 100% !important;\n      }\n    }\n\n    /* Buttons ------------------------------ */\n\n    .button {\n      background-color: #3869D4;\n      border-top: 10px solid #3869D4;\n      border-right: 18px solid #3869D4;\n      border-bottom: 10px solid #3869D4;\n      border-left: 18px solid #3869D4;\n      display: inline-block;\n      color: #FFF;\n      text-decoration: none;\n      border-radius: 3px;\n      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);\n      -webkit-text-size-adjust: none;\n    }\n\n    .button--green {\n      background-color: #62a744;\n      border-top: 10px solid #62a744;\n      border-right: 18px solid #62a744;\n      border-bottom: 10px solid #62a744;\n      border-left: 18px solid #62a744;\n    }\n\n    .button--red {\n      background-color: #FF6136;\n      border-top: 10px solid #FF6136;\n      border-right: 18px solid #FF6136;\n      border-bottom: 10px solid #FF6136;\n      border-left: 18px solid #FF6136;\n    }\n\n    /* Type ------------------------------ */\n\n    h1 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 19px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h2 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 16px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h3 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 14px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    p {\n      margin-top: 0;\n      color: #74787E;\n      font-size: 16px;\n      line-height: 1.5em;\n      text-align: left;\n    }\n\n    p.sub {\n      font-size: 12px;\n    }\n\n    p.center {\n      text-align: center;\n    }\n  \u003c/style\u003e\n\u003c/head\u003e\n\n\u003cbody\u003e\n  \u003cspan class=\"preheader\"\u003eUse this link to reset your password. The link is only valid for 24 hours.\u003c/span\u003e\n  \u003ctable class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n    \u003ctr\u003e\n      \u003ctd align=\"center\"\u003e\n        \u003ctable class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-masthead\"\u003e\n              \u003cdiv class=\"navbar-header\" style=\"width:100%; height:100px\"\u003e\n\n                \u003ca class=\"navbar-brand\" rel=\"home\" href=\"#\" title=\"\"\u003e\n                  \u003cimg style=\"margin-top: 10px;\" src=\"http://exp-qa-apex.proterra.com.s3-website-us-east-1.amazonaws.com/images/logo.png\"\u003e\n                \u003c/a\u003e\n              \u003c/div\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003c!-- Email Body --\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n              \u003ctable class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003c!-- Body content --\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\"\u003e\n                    \u003ch1\u003eHi {username},\u003c/h1\u003e\n                    \u003cp\u003eYour Proterra APEX administrator has created an account for you.\u003c/p\u003e\n                    \u003cp\u003eClick below to complete your account setup. You will be asked to set your password.\u003c/p\u003e\n                    \u003c!-- Action --\u003e\n                    \u003ctable class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd align=\"center\"\u003e\n                          \u003c!-- Border based button\n                       https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design --\u003e\n                          \u003ctable width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                            \u003ctr\u003e\n                              \u003ctd align=\"center\"\u003e\n                                \u003ctable border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                                  \u003ctr\u003e\n                                    \u003ctd\u003e\n                                      \u003ca href=\"http://exp-qa-apex.proterra.com.s3-website-us-east-1.amazonaws.com/changePassword.html?username={username}\u0026temporaryPassword={####}\"\n                                        class=\"button button--green\" target=\"_blank\"\n                                        style=\"color: white !important;\"\u003eSet up new password\u003c/a\u003e\n                                    \u003c/td\u003e\n                                  \u003c/tr\u003e\n                                \u003c/table\u003e\n                              \u003c/td\u003e\n                            \u003c/tr\u003e\n                          \u003c/table\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                    \u003cp\u003eIf you have issues or questions please contact your admin or \u003ca\n                        href=\"mailto:connected@proterra.com\"\u003eAPEX support\u003c/a\u003e.\u003c/p\u003e\n                    \u003cp\u003eThank you,\n                      \u003cbr\u003eThe APEX Team\u003c/p\u003e\n                    \u003c!-- Sub copy --\u003e\n                    \u003ctable class=\"body-sub\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd\u003e\n                          \u003cp class=\"sub\"\u003eIf you’re having trouble with the button above, copy and paste the URL below\n                            into your web browser.\u003c/p\u003e\n                          \u003cp class=\"sub\"\u003e\n                            http://exp-qa-apex.proterra.com.s3-website-us-east-1.amazonaws.com/changePassword.html?username={username}\u0026temporaryPassword={####}\n                          \u003c/p\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003ctr\u003e\n            \u003ctd\u003e\n              \u003ctable class=\"email-footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\" align=\"center\"\u003e\n                    \u003cp class=\"sub align-center\"\u003e\u0026copy; 2019 Proterra. All rights reserved.\u003c/p\u003e\n\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n        \u003c/table\u003e\n      \u003c/td\u003e\n    \u003c/tr\u003e\n  \u003c/table\u003e\n\u003c/body\u003e\n\n\u003c/html\u003e"

}



######### cognito client #########

variable "ams_cognito_app_client_name" {
  type = string
  default = ""
}


variable "ams_callback_urls" {
  type = list
  default = []
}
variable "ams_logout_urls" {
  type = list
  default = []
}

variable "ams_allowed_oauth_scopes" {
  type = list
  default = []
}
variable "ams_explicit_auth_flows" {
  type = list
  default = []
}
variable "ams_supported_identity_providers"  {
  type = list
  default = []
}

variable "cp_cognito_app_client_name" {
  type = string
  default = ""
}
variable "cp_explicit_auth_flows" {
  type = list
  default = []
}