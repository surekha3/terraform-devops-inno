#####################################################
# Base Level Terraform Code.
# - Modify Account specific items.
#####################################################
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
  }
}

#################### S3 ######################
module "TF_Module_S3_Buckets_APEX_UI" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.apex_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.apex_ui_s3_bucket_policy_file_name
}

module "TF_Module_S3_Buckets_CCSS_UI" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.ccss_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.ccss_ui_s3_bucket_policy_file_name
}

#################### VPC ####################

module "TF_Module_VPC" {
  source = "../modules/TF_Module_VPC"

  vpc_cidr              = var.vpc_cidr
  vpc_public_subnets    = var.vpc_public_subnets
  vpc_private_subnets   = var.vpc_private_subnets
  vpc_availabilityzones = var.vpc_availabilityzones
  vpc_account_owner_ip  = var.vpc_account_owner_ip

  vpc_tag_project       = var.project
  vpc_tag_environment   = var.environment
  subnet_additional_tags   = var.subnet_additional_tags
}

##################### RDS #####################
module "TF_Module_RDS" {
  source = "../modules/TF_Module_RDS"
  rds_engine_version         = var.rds_engine_version
  rds_subnet_group_name      = var.rds_subnet_group_name
  rds_sg_tag_sec_ect         = var.rds_sg_tag_sec_ect
  rds_tag_sec_assets_db      = var.rds_tag_sec_assets_db
  rds_vpc_bastion_sg_id      = var.rds_vpc_bastion_sg_id
  rds_tag_sec_assets         = var.rds_tag_sec_assets
  rds_sg_tag_sec_gw          = var.rds_sg_tag_sec_gw
  rds_tag_sec_assets_pii     = var.rds_tag_sec_assets_pii
  project                    = var.project
  region                     = var.aws_region
  env_short                  = var.environment_short
  rds_vpc_id                 = module.TF_Module_VPC.vpc_id
  rds_storage_encrypted      = var.rds_storage_encrypted
  rds_max_allocated_storage  = var.rds_max_allocated_storage
  rds_instance_class         = var.rds_instance_class
  rds_security_group_name    = var.rds_security_group_name
  rds_security_group_description = var.rds_security_group_description
  rds_copy_tags_to_snapshot  = var.rds_copy_tags_to_snapshot
  rds_monitoring_interval    = var.rds_monitoring_interval
  rds_parameter_group_name   = var.rds_parameter_group_name
  rds_skip_final_snapshot    = var.rds_skip_final_snapshot
  #rds_vpc_common_sg_id       = "${module.TF_Module_VPC.vpc_common_sg_db_id}"
  #rds_vpc_internal_zone_id   = module.TF_Module_VPC.vpc_internal_zone_id
  rds_vpc_internal_zone_id   = ""
  rds_vpc_private_subnet_ids = "${module.TF_Module_VPC.vpc_private_subnet_ids}"
  rds_db_name                = var.rds_db_name
  rds_username               = var.rds_username
  rds_identifier             = var.rds_identifier
  rds_password               = var.rds_password
  rds_retention_period       = var.rds_retention_period
  rds_availabilityzones      = var.vpc_availabilityzones
  
}


#################### API ####################
module "TF_Module_API_IQ" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_IQ
  template_file_map = var.template_file_map_iq
  api_stage_name = var.template_file_map_iq["stageName"]
  api_name = var.template_file_map_iq["title"]
  api_description = var.template_file_map_iq["description"]
  stage_variables = var.stage_variables_iq
}
module "TF_Module_API_CCSS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_CCSS
  template_file_map = var.template_file_map_ccss
  api_stage_name = var.template_file_map_ccss["stageName"]
  api_name = var.template_file_map_ccss["title"]
  api_description = var.template_file_map_ccss["description"]
  stage_variables = var.stage_variables_ccss
}
module "TF_Module_API_AMS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_AMS
  template_file_map = var.template_file_map_ams
  api_stage_name = var.template_file_map_ams["stageName"]
  api_name = var.template_file_map_ams["title"]
  api_description = var.template_file_map_ams["description"]
  stage_variables = var.stage_variables_ams
}
module "TF_Module_API_FILE" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_FILE
  template_file_map = var.template_file_map_file
  api_stage_name = var.template_file_map_file["stageName"]
  api_name = var.template_file_map_file["title"]
  api_description = var.template_file_map_file["description"]
  stage_variables = var.stage_variables_file
}
module "TF_Module_API_FILE_PROTERRA" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_FILE_AMS
  template_file_map = var.template_file_map_file_ams
  api_stage_name = var.template_file_map_file_ams["stageName"]
  api_name = var.template_file_map_file_ams["title"]
  api_description = var.template_file_map_file_ams["description"]
  stage_variables = var.stage_variables_file_ams
}
module "TF_Module_API_MGR" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_RUNS
  template_file_map = var.template_file_map_runs
  api_stage_name = var.template_file_map_runs["stageName"]
  api_name = var.template_file_map_runs["title"]
  api_description = var.template_file_map_runs["description"]
  stage_variables = var.stage_variables_runs
}
module "TF_Module_API_CIM" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_CIM
  template_file_map = var.template_file_map_cim
  api_stage_name = var.template_file_map_cim["stageName"]
  api_name = var.template_file_map_cim["title"]
  api_description = var.template_file_map_cim["description"]
  stage_variables = var.stage_variables_cim
}
module "TF_Module_API_DATA_LAKE" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_DATALAKE
  template_file_map = var.template_file_map_datalake
  api_stage_name = var.template_file_map_datalake["stageName"]
  api_name = var.template_file_map_datalake["title"]
  api_description = var.template_file_map_datalake["description"]
  stage_variables = var.stage_variables_datalake
}
module "TF_Module_API_OCPP" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_OCPP
  template_file_map = var.template_file_map_ocpp
  api_stage_name = var.template_file_map_ocpp["stageName"]
  api_name = var.template_file_map_ocpp["title"]
  api_description = var.template_file_map_ocpp["description"]
  stage_variables = var.stage_variables_ocpp
}
module "TF_Module_API_MISCEL" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_MISCEL
  template_file_map = var.template_file_map_miscel
  api_stage_name = var.template_file_map_miscel["stageName"]
  api_name = var.template_file_map_miscel["title"]
  api_description = var.template_file_map_miscel["description"]
  stage_variables = var.stage_variables_miscel
}

module "TF_Module_API_MP" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_MP
  template_file_map = var.template_file_map_mp
  api_stage_name = var.template_file_map_mp["stageName"]
  api_name = var.template_file_map_mp["title"]
  api_description = var.template_file_map_mp["description"]
  stage_variables = var.stage_variables_mp
}

module "TF_Module_API_MP_CP" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_API_MP_CP
  template_file_map = var.template_file_map_mp_cp
  api_stage_name = var.template_file_map_mp_cp["stageName"]
  api_name = var.template_file_map_mp_cp["title"]
  api_description = var.template_file_map_mp_cp["description"]
  stage_variables = var.stage_variables_mp_cp
}

########################## Cognito ##################

module "TF_Module_COGNITO_CP" {
  source = "../modules/TF_Module_COGNITO_CP"

  cognito_user_pool_name          = var.cp_cognito_user_pool_name
  cognito_email_verif_subject     = var.cp_cognito_email_verif_subject
  cognito_sms_verif_message       = var.cp_cognito_sms_verif_message
  cognito_email_verif_message     = var.cp_cognito_email_verif_message
  cognito_unused_account_days     = var.cp_cognito_unused_account_days
  cognito_email_subject           = var.cp_cognito_email_subject
  cognito_sms_email_message       = var.cp_cognito_sms_email_message
  cognito_app_client_name         = var.cp_cognito_app_client_name
  explicit_auth_flows             = var.cp_explicit_auth_flows
  
}




module "TF_Module_COGNITO_AMS" {
  source = "../modules/TF_Module_COGNITO_AMS"

  cognito_user_pool_name          = var.ams_cognito_user_pool_name
  cognito_email_verif_subject     = var.ams_cognito_email_verif_subject
  cognito_sms_verif_message       = var.ams_cognito_sms_verif_message
  cognito_email_verif_message     = var.ams_cognito_email_verif_message
  cognito_unused_account_days     = var.ams_cognito_unused_account_days
  cognito_email_subject           = var.ams_cognito_email_subject
  cognito_sms_email_message       = var.ams_cognito_sms_email_message
  cognito_app_client_name         = var.ams_cognito_app_client_name
  callback_urls                   = var.ams_callback_urls
  logout_urls                     = var.ams_logout_urls
  allowed_oauth_scopes            = var.ams_allowed_oauth_scopes
  explicit_auth_flows             = var.ams_explicit_auth_flows
  supported_identity_providers    = var.ams_supported_identity_providers

}
