aws_region              = "us-east-1"
aws_profile             = "proterra-exp"
state_bucket          	= "exp-qa-foundation"
state_bucket_key        = "terraform/state/qa-us-east-1/terraform.tfstate"


environment             = "qa"
project                 = "proterra-exp"

####### S3 ####

apex_ui_s3_bucket_name               = "exp-qa-apex.proterra.com"
apex_ui_s3_bucket_policy_file_name   = "files-s3/apex_ui_s3_bucket_policy.json"
ccss_ui_s3_bucket_name               = "exp-ccss-ecms-qa.connected.proterra.com"
ccss_ui_s3_bucket_policy_file_name   = "files-s3/ccss_ui_s3_bucket_policy.json"



#VPC Setting
vpc_cidr              = "172.30.1.0/24"
vpc_public_subnets    = ["172.30.1.0/27", "172.30.1.32/27", "172.30.1.128/27", "172.30.1.160/27"]
vpc_private_subnets   = ["172.30.1.64/27", "172.30.1.96/27", "172.30.1.192/27", "172.30.1.224/27"]
vpc_availabilityzones = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
subnet_additional_tags = {
  "kubernetes.io/cluster/c-6bhpp" = "shared"
  "kubernetes.io/cluster/c-kw7p2" = "shared"
  "kubernetes.io/cluster/c-vb2gh" = "shared"
}

env_short             = "qa"


environment_short           = "qa"
rds_db_name                 = "expdbqa"
rds_username                = "dbadmin"
rds_password                = "Expqa123"
rds_engine_version          = "5.7.22"
rds_instance_class          = "db.t2.medium"
rds_cidr1                   = "[10.11.122.0/23]"
rds_subnet_group_name       = "exp-qa-rds-private-subnet"
rds_retention_period        = 1
rds_identifier              = "exp-db-qa"
rds_storage_encrypted       = "false"
rds_max_allocated_storage   = "0"
rds_security_group_name     = "proterra-exp-qa-db-sgp"
rds_security_group_description  = "rds Security Group"
rds_copy_tags_to_snapshot  = "false"
rds_monitoring_interval    =  "0"
rds_parameter_group_name   = "default.mysql5.7"
rds_skip_final_snapshot    = "false"



template_file_name_API_IQ = "IQ-swagger-apigateway.json"
template_file_map_iq = {
  title = "Exp_QA_IQ"
  basePath = "/exp-iq-qa"
  stageName = "exp-qa"
  description = "QA API gateway for IQ"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}
stage_variables_iq = {
apiKey = 	"JtBA9AwzSq9owUPivtk4L8cq2vZjuk4R9hGzvuOt"	 
baseURL	= "exp-qa-svc-alb.proterra.com"	 
adminApiKey = "6ZoAxiyrDr6k6PdJhL5az9jEUtAbI4V1miaijBx0"
}

template_file_name_API_CCSS = "CCSS-swagger-apigateway.json"
template_file_map_ccss = {
  title = "Exp_QA_CCSS"
  basePath = "/exp-ccss-qa"
  stageName = "exp-qa"
  description = "QA API gateway for CCSS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}

stage_variables_ccss = {
m5statusApiKey = "QBRvZlEq1Ric9D08pgqd5rj1NPYGplX1jNSWWWW8"	 
baseURL = "exp-qa-svc-alb.proterra.com/ccss" 
runsApiKey = "auJXtUZXIj89oc5y4LxcO3scnqUwQaPc2gVIw6ey"	 
m5statusURL = "exp-qa-svc-alb.proterra.com/cim" 
apiKey = "USVD773SAl7Rj4NNanngn49D6wqalu7T4JtW3BvB"	 
runsURL = "exp-qa-svc-alb.proterra.com/api"
}

template_file_name_API_AMS = "AMS-swagger-apigateway.json"
template_file_map_ams = {
  title = "Exp_QA_Proterra_AMS"
  basePath = "/exp-ams-qa"
  stageName = "exp-qa"
  description = "QA API gateway for AMS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}

stage_variables_ams = {
baseURL = "exp-qa-svc-alb.proterra.com/ams"
}

template_file_name_API_FILE = "FILE-swagger-apigateway.json"
template_file_map_file = {
  title = "Exp_QA_File_API"
  basePath = "/exp-file-api-qa"
  stageName = "exp-qa"
  description = "QA API gateway for FILE"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}

stage_variables_file = {
baseURL = "34.238.189.75:8080/ams"	 
dataLakeURL = "34.234.186.206:8080"
}

template_file_name_API_FILE_AMS = "FILE-AMS-swagger-apigateway.json"
template_file_map_file_ams = {
  title = "Exp_QA_File_Proterra_AMS"
  basePath = "/exp-file-ams-qa"
  stageName = "exp-qa"
  description = "QA API gateway for FILE PROTERRA AMS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}

stage_variables_file_ams = {
baseCustomReportURL = "35.153.105.80:3000/api"	 
baseURL = "54.146.37.242:8080/ams"	 
dataLakeURL = "34.234.186.206:8080"
}

template_file_name_API_DATALAKE = "DATALAKE-swagger-apigateway.json"
template_file_map_datalake = {
  title = "Exp_QA_Data_Lake"
  basePath = "/exp-datalake-qa"
  stageName = "exp-qa"
  description = "QA API gateway for DATALAKE"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}

stage_variables_datalake = {
baseURL = "52.23.215.14:8080"	 
readOnlyApiKey = "Basic cHJvdGVycmEtcmVhZG9ubHk6cHJvdGVycmEtcmVhZG9ubHk="	 
chargeSessionAPIKey = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
apikey = "NFJYpvWOzf3Fq9XY4vr8o140fK4W7he03LYktquj"	 
dataLakeUrl = "34.234.186.206:8080/dataLake/v1"	 
reportBaseUrl = "34.234.186.206:8080/dataLake/v1"	 
chargeSessionsURL = "ad3b4cffac5174e5c80bffaf676e42e0-1821811585.us-east-1.elb.amazonaws.com:8080/dataLake/v1"
runsURL = "a15660516b9874bf7885d05ef4e9f1ed-315084267.us-east-1.elb.amazonaws.com:8080/api/v1" 
apiKeyProd = "6323872f-1637-4ebd-aaf6-61cd1239d155" 
latestAuth = "Basic ZWxhc3RpYzpKeGdWMlpXZG9KMU9mSkN2c0licVdxYmk="	 
edmontonApiKey = "b09389c4-fd73-4cad-ad5a-c443a963d1f7" 
rtApiKey = "7eb92260-e5c8-11ea-adc1-0242ac120002"
}

template_file_name_API_CIM = "CIM-swagger-apigateway.json"
template_file_map_cim = {
  title = "Exp_QA_CIM"
  basePath = "/exp-cim-qa"
  stageName = "exp-qa"
  description = "QA API gateway for CIM"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}

stage_variables_cim = {
apiKey = "2c0273e3-11c6-41c0-9bdf-0f90c03a4b02"	 
baseURL = "exp-qa-svc-alb.proterra.com/cim"
}

template_file_name_API_RUNS = "RUNS-swagger-apigateway.json"
template_file_map_runs = {
  title = "Exp_QA_Runs"
  basePath = "/exp-runs-qa"
  stageName = "exp-qa"
  description = "QA API gateway for RUNS"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}
stage_variables_runs = {
apiKey = "auJXtUZXIj89oc5y4LxcO3scnqUwQaPc2gVIw6ey"
baseURL = "exp-qa-svc-alb.proterra.com/api"
}

template_file_name_API_MISCEL = "MISCEL-swagger-apigateway.json"
template_file_map_miscel = {
  title = "Exp_QA_Miscel_API"
  basePath = "/exp-no-qa"
  stageName = "exp-qa"
  description = "QA API gateway for MISCEL"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}

stage_variables_miscel = {
}
template_file_name_API_OCPP = "OCPP-swagger-apigateway.json"
template_file_map_ocpp = {
  title = "Exp_QA_OCPP"
  basePath = "/exp-ocpp-qa"
  stageName = "exp-qa"
  description = "QA API gateway for OCPP"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "exp-api.proterra.com"
}

stage_variables_ocpp = {
baseOCPPURL = "54.224.248.38:8080"
baseURL = "52.45.139.95:8080"
}

template_file_name_API_MP = "MP-swagger-apigateway.json"
template_file_map_mp = {
  title = "Exp_QA_Multipler_API"
  basePath = "/exp-mp-qa"
  stageName = "exp-qa"
  description = "QA API gateway for MP"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_Uz5YdECpb"
  host = "exp-api.proterra.com"
}

stage_variables_mp = {
  baseURL = "a41bbff09caae44f4b2afa323326a828-1062115889.us-east-1.elb.amazonaws.com:8887"
}

template_file_name_API_MP_CP = "CP-swagger-apigateway.json"
template_file_map_mp_cp = {
  title = "Exp_QA_Control_panel_API"
  basePath = "/exp-mp-cp-qa"
  stageName = "exp-qa"
  description = "QA API gateway for MP-CP"
  providerARNs = ""
  host = "exp-api.proterra.com"
}

stage_variables_mp = {
  baseURL = "a00e48df3f4864f00bfb1316d6a2d765-548187628.us-east-1.elb.amazonaws.com:8088"
}

### Cognito ########

### Multiplexer Control Panel Cognito
cp_cognito_app_client_name      = "proterra_qa_control_panel_app_client"
cp_cognito_user_pool_name       = "proterra_qa_control_panel_cognito_pool"
cp_cognito_email_verif_subject  = "Your verification code"
cp_cognito_email_verif_message  = "Your verification code is {####}."
cp_cognito_unused_account_days  = 7
cp_cognito_email_subject        = "Welcome to Proterra Control Panel"
cp_cognito_sms_verif_message    = "Your authentication code is {####}. "
cp_explicit_auth_flows          =    [ "ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH","ALLOW_USER_SRP_AUTH"]
#cp_explicit_auth_flows          =    ["ADMIN_NO_SRP_AUTH"]

### Edmonton AMS Cognito

ams_cognito_app_client_name      = "exp_proterra_ams_app_client_qa"
ams_cognito_user_pool_name       = "exp_proterra_ams_cognito_pool_edmonton_qa"
ams_cognito_email_verif_subject  = "APEX verification code"
ams_cognito_email_verif_message  = "APEX verification code is {####}"
ams_cognito_unused_account_days  = 30
ams_cognito_email_subject        = "Welcome to Proterra APEX"
ams_cognito_sms_verif_message    = "Your authentication code is {####}. "
ams_callback_urls                =   ["https://exp-qa.proterra.com"]
ams_logout_urls                  =   ["https://exp-qa.proterra.com"]
ams_allowed_oauth_scopes         =   [ "email","openid","profile" ]
ams_explicit_auth_flows          =   [ "ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_SRP_AUTH"]
ams_supported_identity_providers =   ["COGNITO", "edmonton","okta"]
