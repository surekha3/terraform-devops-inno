variable "aws_region" {
  type = string
}

variable "aws_profile" {
  type = string
}

#################### VPC ###############

# Variables added with VPC module.
variable "vpc_cidr" {
  type = string
}

variable "vpc_public_subnets" {
  type = list
}

variable "vpc_private_subnets" {
  type = list
}

variable "vpc_availabilityzones" {
  type = list
}

variable "vpc_project" {
  type = string
}

variable "vpc_environment" {
  type = string
}
#variable "vpc_flow_log_cloudwatch_log_group" {
#  type = string
#}

variable "vpc_account_owner_ip" {
  type = list
  default = [" "]
}

variable "subnet_additional_tags" {
 type = map(string)
 default = {}
}
variable "vpc_endpoint_tag_name" {
  type = string
}




######################### EKS ###################
variable "vpc_id" {
  default = " "
  type    = string
}
variable "cluster-name" {
  default = " "
  type    = string
}

variable "cluster-node-name" {
  default = " "
  type    = string
}
variable "node-instance-type" {
  default = ""
  type    = string
}

variable "ec2_ssh_key" {
  type    = string
  default =""
}
variable "autoscaling-desired-capacity" { }
variable "autoscaling-max-size" { }
variable "autoscaling-min-size" { }


variable "ami_id" {
  type    = string
}
variable "node_group_ec2_ssh_key" {
  type    = string
  default =""
}
variable "node-group-autoscaling-desired-capacity" { }
variable "node-group-autoscaling-max-size" { }
variable "node-group-autoscaling-min-size" { }
variable "node_group_name" {
  type    = string
  default =""
}
variable "node-group-instance-types" {
  type    = list
  default = ["t3.medium"]
}

variable "create_launch_template" {
  default = false 
}
variable "node_group_ec2_tags" {
  type = map(string)
  default = {}
}
################### Redis #############3

variable "cluster_id" {
  type        = string
}
variable "engine_type" {
  type        = string
  default     = "redis"
}
variable "node_type" {
  type        = string
}
variable "num_cache_nodes" {
  type        = string
}
variable "parameter_group_name" {
  type        = string
}
variable "engine_version" {
  type        = string
}
variable "port_number" {
  type       = number
}
variable "cluster_security_group_name" {
  type        = string
}
variable "cluster_subnet_group" {
  type        = string
}
variable "replication_group_id" {
  type        = string
}
variable "number_cache_clusters" {
  type        = number
}