#####################################################
# Base Level Terraform Code.
# - Modify Account specific items.
#####################################################
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
  }
}



########################  VPC ########################
module "TF_Module_VPC" {
  source = "../modules/TF_Module_VPC"

  vpc_cidr              = var.vpc_cidr
  vpc_public_subnets    = var.vpc_public_subnets
  vpc_private_subnets   = var.vpc_private_subnets
  vpc_availabilityzones = var.vpc_availabilityzones
  vpc_account_owner_ip  = var.vpc_account_owner_ip

  vpc_tag_project       = var.vpc_project
  vpc_tag_environment   = var.vpc_environment
  subnet_additional_tags   = var.subnet_additional_tags
  # DynamoDB VPC End point
  vpc_endpoint_tag_name   = var.vpc_endpoint_tag_name
}


data "aws_subnet_ids" "all-subnets" {
  vpc_id = module.TF_Module_VPC.vpc_id
}  

module "TF_Module_EKS" {
  source = "../modules/TF_Module_EKS"
  cluster-name       = var.cluster-name
  cluster-node-name  = var.cluster-node-name
  node-instance-type = var.node-instance-type
  all-subnets-list   = data.aws_subnet_ids.all-subnets.ids
  private-subnets-list = module.TF_Module_VPC.vpc_private_subnet_ids
  vpc_id = module.TF_Module_VPC.vpc_id
  
  autoscaling-desired-capacity = var.autoscaling-desired-capacity
  autoscaling-max-size = var.autoscaling-max-size
  autoscaling-min-size = var.autoscaling-min-size
  node_group_name = var.node_group_name
  #ec2_ssh_key = var.ec2_ssh_key
  node_group_ec2_ssh_key= var.node_group_ec2_ssh_key
  node-group-autoscaling-desired-capacity = var.node-group-autoscaling-desired-capacity
  node-group-autoscaling-max-size = var.node-group-autoscaling-max-size
  node-group-autoscaling-min-size = var.node-group-autoscaling-min-size
  node-group-instance-types = var.node-group-instance-types
  create_launch_template = var.create_launch_template
  ami_id=var.ami_id
  node_group_ec2_tags = var.node_group_ec2_tags
}

module "TF_Module_Redis" {
  source = "../modules/TF_Module_Redis"
  vpc_id = module.TF_Module_VPC.vpc_id
  vpc_cidr_blocks      = [ "${module.TF_Module_VPC.vpc_cidr_block}"]
  cluster_id           = var.cluster_id
  node_type            = var.node_type
  num_cache_nodes      = var.num_cache_nodes
  parameter_group_name = var.parameter_group_name
  replication_group_id  = var.replication_group_id
  engine_version       = var.engine_version
  port_number          = var.port_number
  cluster_security_group_name       = var.cluster_security_group_name
  cluster_subnet_group          = var.cluster_subnet_group
  private-subnets-list          = module.TF_Module_VPC.vpc_private_subnet_ids
  #az-list                       = module.TF_Module_VPC.vpc_availability_zones
  number_cache_clusters          = var.number_cache_clusters
}

