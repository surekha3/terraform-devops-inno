# Base.tfvars
aws_region              = "us-east-1"
aws_profile             = "proterra-prod"

################## VPC #################

vpc_cidr              = "10.109.16.0/20"
vpc_public_subnets    = ["10.109.16.0/24", "10.109.17.0/24", "10.109.18.0/24", "10.109.19.0/24"]
vpc_private_subnets   = ["10.109.20.0/24", "10.109.21.0/24", "10.109.22.0/24", "10.109.23.0/24"]
vpc_availabilityzones = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
subnet_additional_tags = {
  "Environment" = "Stage",
  "kubernetes.io/cluster/proterra-mdc-launch2020-stage-eks-cluster" = "shared"

}
vpc_project               = "launch2020"
vpc_environment           = "stage"     
vpc_endpoint_tag_name         = "LAUNCH2020-STAGE-EC2-DynamoDB"

##############  EKS ##############
cluster-name       = "proterra-mdc-launch2020-stage-eks-cluster"
cluster-node-name  = "proterra-mdc-launch2020-stage-eks-node"
node-instance-type = "t2.large"
ami_id="ami-0c106008a50996d1c"
autoscaling-desired-capacity = 0
autoscaling-max-size =0
autoscaling-min-size = 0
#ec2_ssh_key = "mdc-stage"
#node_group_ec2_ssh_key= "mdc-stage"
node_group_name = "mdc"
node-group-autoscaling-desired-capacity = 2
node-group-autoscaling-max-size = 2
node-group-autoscaling-min-size = 1
node-group-instance-types = ["t2.medium"]
create_launch_template = true
node_group_ec2_tags = {

     Name                 =  "proterra-mdc-launch2020-stage-eks-cluster-mdc-nodegroup"
  }
##########  Redis ###############
cluster_id                    = "redis-mdc-stage-rg"
node_type                     = "cache.t2.micro"
num_cache_nodes               = 1
parameter_group_name          = "default.redis5.0"
engine_version                = "5.0.6"
port_number                   = 6397 
cluster_security_group_name   ="redis-mdc-stage-sg"
cluster_subnet_group          = "redis-mdc-stage-parameter-group"
replication_group_id          = "redis-mdc-stage-cluster"
number_cache_clusters         = 1