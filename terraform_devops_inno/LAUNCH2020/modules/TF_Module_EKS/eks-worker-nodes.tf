
resource "aws_iam_role" "proterra-eks-node" {
  name = var.cluster-node-name

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "proterra-eks-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.proterra-eks-node.name
}

resource "aws_iam_role_policy_attachment" "proterra-eks-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.proterra-eks-node.name
}

resource "aws_iam_role_policy_attachment" "proterra-eks-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.proterra-eks-node.name
}

resource "aws_iam_instance_profile" "proterra-eks-node" {
  name = var.cluster-name
  role = aws_iam_role.proterra-eks-node.name
}

resource "aws_security_group" "proterra-eks-node" {
  name        = var.cluster-node-name
  description = "Security group for all nodes in the cluster"
  vpc_id      = var.vpc_id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${
    map(
     "Name", "var.cluster-node-name",
     "kubernetes.io/cluster/${var.cluster-name}", "owned",
    )
  }"
}

resource "aws_security_group_rule" "proterra-eks-node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.proterra-eks-node.id
  source_security_group_id = aws_security_group.proterra-eks-node.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "proterra-eks-node-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = aws_security_group.proterra-eks-node.id
  source_security_group_id = aws_security_group.proterra-eks-cluster.id
  to_port                  = 65535
  type                     = "ingress"
}

data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-${aws_eks_cluster.proterra-eks.version}-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We utilize a Terraform local here to simplify Base64 encoding this
# information into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  proterra-eks-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
sudo yum install -y amazon-ssm-agent
sudo systemctl enable amazon-ssm-agent && systemctl start amazon-ssm-agent

/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.proterra-eks.endpoint}' --b64-cluster-ca '${aws_eks_cluster.proterra-eks.certificate_authority.0.data}' '${var.cluster-name}'
USERDATA
}

resource "aws_launch_configuration" "proterra-eks" {
  associate_public_ip_address = false
  iam_instance_profile        = aws_iam_instance_profile.proterra-eks-node.name
  image_id                    = data.aws_ami.eks-worker.id
  instance_type               = var.node-instance-type
  name_prefix                 = var.cluster-node-name
  security_groups             = ["${aws_security_group.proterra-eks-node.id}"]
  user_data_base64            = "${base64encode(local.proterra-eks-node-userdata)}"
  key_name                    = var.ec2_ssh_key
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "proterra-eks" {
  desired_capacity     = var.autoscaling-desired-capacity
  launch_configuration = aws_launch_configuration.proterra-eks.id
  max_size             = var.autoscaling-max-size
  min_size             = var.autoscaling-min-size
  name                 = var.cluster-node-name
  vpc_zone_identifier  = var.private-subnets-list
  tag {
    key                 = "Name"
    value               = var.cluster-node-name
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster-name}"
    value               = "owned"
    propagate_at_launch = true
  }
}


resource "aws_launch_template" "cluster" {
  count = var.create_launch_template ? 1 : 0
  image_id               = var.ami_id
  name                   = join("_", list(var.cluster-node-name, var.node_group_name))
  key_name = var.node_group_ec2_ssh_key
  vpc_security_group_ids = ["${aws_security_group.proterra-eks-node.id}"]
  user_data = base64encode(templatefile("${path.module}/node-group-userdata.tpl", { CLUSTER_NAME = aws_eks_cluster.proterra-eks.name, B64_CLUSTER_CA = aws_eks_cluster.proterra-eks.certificate_authority[0].data, API_SERVER_URL = aws_eks_cluster.proterra-eks.endpoint }))
}


resource "aws_eks_node_group" "main" {
  cluster_name    = var.cluster-name
  node_group_name = var.node_group_name == "" ? "main" : var.node_group_name
  node_role_arn   = aws_iam_role.proterra-eks-node.arn
  subnet_ids = var.private-subnets-list
  instance_types = var.node-group-instance-types

  scaling_config {
    desired_size = var.node-group-autoscaling-desired-capacity
    max_size     = var.node-group-autoscaling-max-size
    min_size     = var.node-group-autoscaling-min-size
  }
 tags = var.node_group_ec2_tags

  #dynamic "remote_access" {
  #  for_each = var.node_group_ec2_ssh_key != null && var.node_group_ec2_ssh_key != "" ? ["true"] : []
  #  content {
  #    ec2_ssh_key               = var.node_group_ec2_ssh_key
  #    source_security_group_ids = ["${aws_security_group.proterra-eks-node.id}"]
  #  }
  #}

  dynamic "launch_template" {
    for_each = var.create_launch_template ?  [var.launch_template] : []
    content {
      id      = aws_launch_template.cluster[0].id
      version = aws_launch_template.cluster[0].latest_version
    }
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [scaling_config.0.desired_size]
  }
}
