#
# Variables Configuration
#
variable "vpc_id" {
  default = " "
  type    = string
}
variable "cluster-name" {
  default = "proterra-mdc-eks-cluster"
  type    = string
}

variable "cluster-node-name" {
  default = "proterra-mdc-eks-node"
  type    = string
}
variable "node-instance-type" {
  default = "t2.medium"
  type    = string
}
variable "node-group-instance-types" {
  type    = list
  default = ["t3.medium"]
}
variable "all-subnets-list" {
   type    = list
}

variable "private-subnets-list" {
  type    = list
}

variable "ami_id" {
  type    = string
}
variable "ec2_ssh_key" {
  type    = string
  default =""
}

variable "node_group_ec2_ssh_key" {
 type    = string
 default =""
}


variable "autoscaling-desired-capacity" { }
variable "autoscaling-max-size" { }
variable "autoscaling-min-size" { }

variable "node-group-autoscaling-desired-capacity" { }
variable "node-group-autoscaling-max-size" { }
variable "node-group-autoscaling-min-size" { }

variable "node_group_name" {
  type    = string
  default =""
}
variable "launch_template" {
  type    = map
  default = {
      default ={   
      id      = "lt-07dddd0d44bdd39a3"
      name    = "devsecops-custom-ami"
      version = "latest_version"
      }
  }

}
variable "create_launch_template" {
  default = false
}

variable "node_group_ec2_tags" {
  type = map(string)
  default = {}
}