
resource "aws_security_group" "redis-sg" {
  name = var.cluster_security_group_name
  vpc_id      = var.vpc_id
  ingress {
    from_port   = var.port_number
    to_port     = var.port_number
    protocol    = "tcp"
    cidr_blocks = var.vpc_cidr_blocks
  }

  tags = {
    Name        = "${var.cluster_security_group_name}"
    Project     = "MDC"
  }
}
resource "aws_elasticache_subnet_group" "default_subnet_group" {
  name       = var.cluster_subnet_group
  description = " "
  subnet_ids = var.private-subnets-list
}

resource "aws_elasticache_replication_group" "redis" {
  automatic_failover_enabled    = var.automatic_failover_enabled
  replication_group_id          = lower(var.replication_group_id)
  replication_group_description = " "
  node_type                     = var.node_type
  engine_version                = var.engine_version
  number_cache_clusters         = var.number_cache_clusters
  parameter_group_name          = var.parameter_group_name
  port                          = var.port_number
  security_group_ids   = ["${aws_security_group.redis-sg.id}"]
  subnet_group_name    = aws_elasticache_subnet_group.default_subnet_group.name
  #cluster_mode {
  #  replicas_per_node_group = 1
  #  num_node_groups         = 1
  #}
}

resource "aws_elasticache_cluster" "redis-replica" {
  count = 1
  cluster_id           = lower(var.cluster_id)
  replication_group_id = aws_elasticache_replication_group.redis.id
  
}
