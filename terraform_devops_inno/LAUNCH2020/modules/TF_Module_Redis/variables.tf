variable "vpc_id" {
  default = " "
  type    = string
}
variable "vpc_cidr_blocks" {
  type        = list
}
variable "cluster_id" {
  description = "Group identifier. ElastiCache converts this name to lowercases.must contain only lowercase alphanumeric characters and hyphens"
  type        = string
}
variable "engine_type" {
  description = "Name of the cache engine to be used for this cache cluster. Valid values for this parameter are memcached or redis"
  type        = string
  default     = "redis"
}
variable "node_type" {
  description = "The compute and memory capacity of the nodes"
  type        = string
}
variable "num_cache_nodes" {
  description = "The initial number of cache nodes that the cache cluster will have"
  type        = string
}
variable "parameter_group_name" {
  description = "Name of the parameter group to associate with this cache cluster"
  type        = string
}
variable "engine_version" {
  description = "Version number of the cache engine to be used"
  type        = string
}
variable "port_number" {
  description = "The port number on which each of the cache nodes will accept connections,default port is 6379"
  type       = number
}
variable "cluster_security_group_name" {
  description = "Name of security group"
  type        = string
}
variable "cluster_subnet_group" {
  description = "Name of security group"
  type        = string
}

variable "private-subnets-list" {
	description = "List of all  private subnets for create subnet group"
    type    = list
}
#variable "az-list" {
#    type    = list
#}

variable "replication_group_id" {
  type        = string
}

variable "number_cache_clusters" {
  type        = number
}
variable "automatic_failover_enabled" {
  default        = false
}

