# List of resources exported:
output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

#output "vpc_zone_aliases" {
#  value = "${module.vpc.zone_aliases}"
#}

#output "vpc_internal_zone_id" {
#  value = "${module.vpc.zone_id}"
#}

#output "vpc_internal_zone_id" {
#  value = "${module.vpc.r53_internal_zone_id}"
#}

#output "vpc_internal_zone_ns" {
#  value = "${module.vpc.r53_internal_zone_ns}"
#}

output "vpc_cidr_block" {
  value = "${module.vpc.vpc_cidr_block}"
}

output "vpc_private_subnet_ids" {
  value = "${module.vpc.private_subnets_ids}"
}

output "vpc_public_subnet_ids" {
  value = "${module.vpc.public_subnets_ids}"
}

output "vpc_igw_id" {
  value = "${module.vpc.igw_id}"
}

output "vpc_nat_eips" {
  value = "${module.vpc.nat_eips}"
}

output "vpc_nat_eips_public_ips" {
  value = "${module.vpc.nat_eips_public_ips}"
}

output "vpc_natgw_ids" {
  value = "${module.vpc.nat_gw_ids}"
}

output "vpc_availability_zones" {
  value = "${module.vpc.vpc_azs}"
}

output "vpc_private_route_table_ids" {
  value = ["${module.vpc.private_route_table_ids}"]
}

output "vpc_public_route_table_ids" {
  value = ["${module.vpc.public_route_table_ids}"]
}
