resource "aws_vpc_endpoint" "dynamodb" {
  vpc_id       = aws_vpc.mod.id
  service_name = "com.amazonaws.us-east-1.dynamodb"
  route_table_ids = concat([aws_route_table.public.id],[aws_route_table.private.id])
  depends_on = [aws_vpc.mod,aws_route_table.public,aws_route_table.private]
  tags = {
    Name = var.vpc_endpoint_tag_name
  }
}