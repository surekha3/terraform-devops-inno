output "vpc_id" {
  value = "${aws_vpc.mod.id}"
}

output "vpc_azs" {
  value = "${var.azs}"
}

output "vpc_cidr_block" {
  value = "${aws_vpc.mod.cidr_block}"
}

output "private_subnets_ids" {
  value = "${aws_subnet.private.*.id}"
}

output "public_subnets_ids" {
  value = ["${aws_subnet.public.*.id}"]
}

#output "zone_aliases" {
#  value = ["${aws_subnet.private.*.tags.zone}"]
#}

output "public_route_table_ids" {
  value =["${aws_route_table.public.*.id}"]
}

output "private_route_table_ids" {
  value = "${aws_route_table.private.*.id}"
}

output "nat_eips" {
  value = ["${aws_eip.nateip.*.id}"]
}

output "nat_eips_public_ips" {
  value = ["${aws_eip.nateip.*.public_ip}"]
}

output "nat_gw_ids" {
  value = ["${aws_nat_gateway.natgw.*.id}"]
}

output "igw_id" {
  value = "${aws_internet_gateway.mod.id}"
}

output "vpc_default_security_group_id" {
  value = "${aws_default_security_group.vpc.id}"
}

output "vpc_default_security_group_name" {
  value = "${aws_default_security_group.vpc.name}"
}

