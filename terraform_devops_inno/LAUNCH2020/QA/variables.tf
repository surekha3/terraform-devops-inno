variable "aws_region" {
  type = string
}

variable "aws_profile" {
  type = string
}

#################### VPC ###############

# Variables added with VPC module.
variable "vpc_cidr" {
  type = string
}

variable "vpc_public_subnets" {
  type = list
}

variable "vpc_private_subnets" {
  type = list
}

variable "vpc_availabilityzones" {
  type = list
}

variable "vpc_project" {
  type = string
}

variable "vpc_environment" {
  type = string
}
#variable "vpc_flow_log_cloudwatch_log_group" {
#  type = string
#}

variable "vpc_account_owner_ip" {
  type = list
  default = [" "]
}

variable "subnet_additional_tags" {
 type = map(string)
 default = {}
}
variable "vpc_endpoint_tag_name" {
  type = string
}




######################### EKS ###################

variable "cluster-name" {
  default = " "
  type    = string
}

variable "worker-nodes-data" {
  type = map(string)
  default = {}
}

variable "launch2020-node-group-data" {
  type = map(string)
  default = {}
}
#variable "mdc-node-group-data" {
#  type = map(string)
#  default = {}
#}

#################### ECR #############
# For ECR
variable "ecr_name" {
  type    = string
}

################### Redis #############3

variable "cluster_id" {
  type        = string
}
variable "engine_type" {
  type        = string
  default     = "redis"
}
variable "node_type" {
  type        = string
}
variable "num_cache_nodes" {
  type        = string
}
variable "parameter_group_name" {
  type        = string
}
variable "engine_version" {
  type        = string
}
variable "port_number" {
  type       = number
}
variable "cluster_security_group_name" {
  type        = string
}
variable "cluster_subnet_group" {
  type        = string
}
variable "replication_group_id" {
  type        = string
}
variable "number_cache_clusters" {
  type        = number
}

######### SQS #######

variable "sqs_BusRealTimeDataStream_name" {
  type        = string
   default     = ""
}
variable "sqs_BusRealTimeDataStream_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_BusRealTimeDataStream_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_BusRealTimeDataStream_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_BusRealTimeDataStream_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_BusRealTimeDataStream_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_BusRealTimeDataStream_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_BusRealTimeDataStream_fifo_queue" {
   default     = false
}

variable "sqs_BusRealTimeDataStream_content_based_deduplication" {
   default     = false
}

##
variable "sqs_BusShadowUpdatesV2_name" {
  type        = string
   default     = ""
}
variable "sqs_BusShadowUpdatesV2_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_BusShadowUpdatesV2_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_BusShadowUpdatesV2_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_BusShadowUpdatesV2_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_BusShadowUpdatesV2_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_BusShadowUpdatesV2_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_BusShadowUpdatesV2_fifo_queue" {
   default     = false
}
variable "sqs_BusShadowUpdatesV2_content_based_deduplication" {
   default     = false
}

##
variable "sqs_LogstashLambdaTest_name" {
  type        = string
   default     = ""
}
variable "sqs_LogstashLambdaTest_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_LogstashLambdaTest_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_LogstashLambdaTest_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_LogstashLambdaTest_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_LogstashLambdaTest_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_LogstashLambdaTest_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_LogstashLambdaTest_fifo_queue" {
   default     = false
}
variable "sqs_LogstashLambdaTest_content_based_deduplication" {
   default     = false
}

#####################

variable "sqs_OCPPMessageQueue_name" {
  type        = string
   default     = ""
}
variable "sqs_OCPPMessageQueue_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_OCPPMessageQueue_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_OCPPMessageQueue_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_OCPPMessageQueue_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPMessageQueue_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPMessageQueue_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_OCPPMessageQueue_fifo_queue" {
   default     = false
}
variable "sqs_OCPPMessageQueue_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_OCPPMessageQueueGV_name" {
  type        = string
   default     = ""
}
variable "sqs_OCPPMessageQueueGV_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_OCPPMessageQueueGV_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_OCPPMessageQueueGV_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_OCPPMessageQueueGV_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPMessageQueueGV_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPMessageQueueGV_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_OCPPMessageQueueGV_fifo_queue" {
   default     = false
}
variable "sqs_OCPPMessageQueueGV_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_OCPPFaults_name" {
  type        = string
   default     = ""
}
variable "sqs_OCPPFaults_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_OCPPFaults_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_OCPPFaults_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_OCPPFaults_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPFaults_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPFaults_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_OCPPFaults_fifo_queue" {
   default     = false
}
variable "sqs_OCPPFaults_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_OCPPHistogramJobQueue_name" {
  type        = string
   default     = ""
}
variable "sqs_OCPPHistogramJobQueue_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_OCPPHistogramJobQueue_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_OCPPHistogramJobQueue_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_OCPPHistogramJobQueue_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPHistogramJobQueue_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPHistogramJobQueue_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_OCPPHistogramJobQueue_fifo_queue" {
   default     = false
}
variable "sqs_OCPPHistogramJobQueue_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_OCPPHourlyJobQueue_name" {
  type        = string
   default     = ""
}
variable "sqs_OCPPHourlyJobQueue_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_OCPPHourlyJobQueue_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_OCPPHourlyJobQueue_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_OCPPHourlyJobQueue_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPHourlyJobQueue_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPHourlyJobQueue_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_OCPPHourlyJobQueue_fifo_queue" {
   default     = false
}
variable "sqs_OCPPHourlyJobQueue_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_OCPPSessionJobQueue_name" {
  type        = string
   default     = ""
}
variable "sqs_OCPPSessionJobQueue_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_OCPPSessionJobQueue_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_OCPPSessionJobQueue_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_OCPPSessionJobQueue_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPSessionJobQueue_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OCPPSessionJobQueue_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_OCPPSessionJobQueue_fifo_queue" {
   default     = false
}
variable "sqs_OCPPSessionJobQueue_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_ReindexRawDataQueue_name" {
  type        = string
   default     = ""
}
variable "sqs_ReindexRawDataQueue_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_ReindexRawDataQueue_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_ReindexRawDataQueue_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_ReindexRawDataQueue_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ReindexRawDataQueue_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ReindexRawDataQueue_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_ReindexRawDataQueue_fifo_queue" {
   default     = false
}
variable "sqs_ReindexRawDataQueue_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_RoadRunnerDataStream_name" {
  type        = string
   default     = ""
}
variable "sqs_RoadRunnerDataStream_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_RoadRunnerDataStream_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_RoadRunnerDataStream_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_RoadRunnerDataStream_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_RoadRunnerDataStream_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_RoadRunnerDataStream_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_RoadRunnerDataStream_fifo_queue" {
   default     = false
}
variable "sqs_RoadRunnerDataStream_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_ToucanBackward_name" {
  type        = string
   default     = ""
}
variable "sqs_ToucanBackward_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_ToucanBackward_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_ToucanBackward_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_ToucanBackward_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ToucanBackward_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ToucanBackward_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_ToucanBackward_fifo_queue" {
   default     = false
}
variable "sqs_ToucanBackward_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_ToucanRawDataStream_name" {
  type        = string
   default     = ""
}
variable "sqs_ToucanRawDataStream_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_ToucanRawDataStream_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_ToucanRawDataStream_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_ToucanRawDataStream_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ToucanRawDataStream_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ToucanRawDataStream_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_ToucanRawDataStream_fifo_queue" {
   default     = false
}
variable "sqs_ToucanRawDataStream_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_chargereventfaults_name" {
  type        = string
   default     = ""
}
variable "sqs_chargereventfaults_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_chargereventfaults_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_chargereventfaults_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_chargereventfaults_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_chargereventfaults_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_chargereventfaults_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_chargereventfaults_fifo_queue" {
   default     = false
}
variable "sqs_chargereventfaults_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_ChargerEventFaultsAggregated_name" {
  type        = string
   default     = ""
}
variable "sqs_ChargerEventFaultsAggregated_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_ChargerEventFaultsAggregated_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_ChargerEventFaultsAggregated_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_ChargerEventFaultsAggregated_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ChargerEventFaultsAggregated_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ChargerEventFaultsAggregated_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_ChargerEventFaultsAggregated_fifo_queue" {
   default     = false
}
variable "sqs_ChargerEventFaultsAggregated_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_EnergyManagerOcppQueueProd_name" {
  type        = string
   default     = ""
}
variable "sqs_EnergyManagerOcppQueueProd_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_EnergyManagerOcppQueueProd_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_EnergyManagerOcppQueueProd_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_EnergyManagerOcppQueueProd_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_EnergyManagerOcppQueueProd_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_EnergyManagerOcppQueueProd_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_EnergyManagerOcppQueueProd_fifo_queue" {
   default     = false
}
variable "sqs_EnergyManagerOcppQueueProd_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_EnergyManagerOcppQueueTest_name" {
  type        = string
   default     = ""
}
variable "sqs_EnergyManagerOcppQueueTest_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_EnergyManagerOcppQueueTest_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_EnergyManagerOcppQueueTest_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_EnergyManagerOcppQueueTest_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_EnergyManagerOcppQueueTest_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_EnergyManagerOcppQueueTest_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_EnergyManagerOcppQueueTest_fifo_queue" {
   default     = false
}
variable "sqs_EnergyManagerOcppQueueTest_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_energymanagersqs_name" {
  type        = string
   default     = ""
}
variable "sqs_energymanagersqs_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_energymanagersqs_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_energymanagersqs_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_energymanagersqs_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_energymanagersqs_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_energymanagersqs_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_energymanagersqs_fifo_queue" {
   default     = false
}
variable "sqs_energymanagersqs_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_ExpOcppChargerStatus_name" {
  type        = string
   default     = ""
}
variable "sqs_ExpOcppChargerStatus_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_ExpOcppChargerStatus_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_ExpOcppChargerStatus_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_ExpOcppChargerStatus_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ExpOcppChargerStatus_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ExpOcppChargerStatus_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_ExpOcppChargerStatus_fifo_queue" {
   default     = false
}
variable "sqs_ExpOcppChargerStatus_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_ExpOcppMessages_name" {
  type        = string
   default     = ""
}
variable "sqs_ExpOcppMessages_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_ExpOcppMessages_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_ExpOcppMessages_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_ExpOcppMessages_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ExpOcppMessages_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_ExpOcppMessages_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_ExpOcppMessages_fifo_queue" {
   default     = false
}
variable "sqs_ExpOcppMessages_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_FromDevIngestor_name" {
  type        = string
   default     = ""
}
variable "sqs_FromDevIngestor_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_FromDevIngestor_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_FromDevIngestor_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_FromDevIngestor_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_FromDevIngestor_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_FromDevIngestor_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_FromDevIngestor_fifo_queue" {
   default     = false
}
variable "sqs_FromDevIngestor_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_FromOcppProdIngestorm_name" {
  type        = string
   default     = ""
}
variable "sqs_FromOcppProdIngestorm_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_FromOcppProdIngestorm_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_FromOcppProdIngestorm_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_FromOcppProdIngestorm_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_FromOcppProdIngestorm_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_FromOcppProdIngestorm_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_FromOcppProdIngestorm_fifo_queue" {
   default     = false
}
variable "sqs_FromOcppProdIngestorm_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_OcppWriteQueueAssetcustomerdetails_name" {
  type        = string
   default     = ""
}
variable "sqs_OcppWriteQueueAssetcustomerdetails_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_OcppWriteQueueAssetcustomerdetails_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_OcppWriteQueueAssetcustomerdetails_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_OcppWriteQueueAssetcustomerdetails_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OcppWriteQueueAssetcustomerdetails_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OcppWriteQueueAssetcustomerdetails_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_OcppWriteQueueAssetcustomerdetails_fifo_queue" {
   default     = false
}
variable "sqs_OcppWriteQueueAssetcustomerdetails_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_OcppWriteQueueCategories_name" {
  type        = string
   default     = ""
}
variable "sqs_OcppWriteQueueCategories_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_OcppWriteQueueCategories_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_OcppWriteQueueCategories_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_OcppWriteQueueCategories_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OcppWriteQueueCategories_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_OcppWriteQueueCategories_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_OcppWriteQueueCategories_fifo_queue" {
   default     = false
}
variable "sqs_OcppWriteQueueCategories_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_expChargerEventFaults1_name" {
  type        = string
   default     = ""
}
variable "sqs_expChargerEventFaults1_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_expChargerEventFaults1_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_expChargerEventFaults1_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_expChargerEventFaults1_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_expChargerEventFaults1_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_expChargerEventFaults1_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_expChargerEventFaults1_fifo_queue" {
   default     = false
}
variable "sqs_expChargerEventFaults1_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_expChargerEventFaultsAggregated_name" {
  type        = string
   default     = ""
}
variable "sqs_expChargerEventFaultsAggregated_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_expChargerEventFaultsAggregated_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_expChargerEventFaultsAggregated_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_expChargerEventFaultsAggregated_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_expChargerEventFaultsAggregated_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_expChargerEventFaultsAggregated_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_expChargerEventFaultsAggregated_fifo_queue" {
   default     = false
}
variable "sqs_expChargerEventFaultsAggregated_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_expChargerEvent1_name" {
  type        = string
   default     = ""
}
variable "sqs_expChargerEvent1_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_expChargerEvent1_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_expChargerEvent1_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_expChargerEvent1_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_expChargerEvent1_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_expChargerEvent1_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_expChargerEvent1_fifo_queue" {
   default     = false
}
variable "sqs_expChargerEvent1_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_name" {
  type        = string
   default     = ""
}
variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_fifo_queue" {
   default     = false
}
variable "sqs_NonAbbOCPPSubmitMessageRequestQueue_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_name" {
  type        = string
   default     = ""
}
variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_fifo_queue" {
   default     = false
}
variable "sqs_NonAbbOCPPSubmitMessageResponseQueue_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_NonAbbOCPPChargerEventFaults1_name" {
  type        = string
   default     = ""
}
variable "sqs_NonAbbOCPPChargerEventFaults1_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_NonAbbOCPPChargerEventFaults1_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_NonAbbOCPPChargerEventFaults1_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_NonAbbOCPPChargerEventFaults1_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPChargerEventFaults1_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPChargerEventFaults1_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_NonAbbOCPPChargerEventFaults1_fifo_queue" {
   default     = false
}
variable "sqs_NonAbbOCPPChargerEventFaults1_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_name" {
  type        = string
   default     = ""
}
variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_fifo_queue" {
   default     = false
}
variable "sqs_NonAbbOCPPChargerEventFaultsaggregated_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_NonAbbOCPPChargerEvents1_name" {
  type        = string
   default     = ""
}
variable "sqs_NonAbbOCPPChargerEvents1_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_NonAbbOCPPChargerEvents1_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_NonAbbOCPPChargerEvents1_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_NonAbbOCPPChargerEvents1_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPChargerEvents1_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPChargerEvents1_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_NonAbbOCPPChargerEvents1_fifo_queue" {
   default     = false
}
variable "sqs_NonAbbOCPPChargerEvents1_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_NonAbbOCPPMessages_name" {
  type        = string
   default     = ""
}
variable "sqs_NonAbbOCPPMessages_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_NonAbbOCPPMessages_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_NonAbbOCPPMessages_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_NonAbbOCPPMessages_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPMessages_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_NonAbbOCPPMessages_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_NonAbbOCPPMessages_fifo_queue" {
   default     = false
}
variable "sqs_NonAbbOCPPMessages_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gChargerEventFaults1_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gChargerEventFaults1_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gChargerEventFaults1_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gChargerEventFaults1_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gChargerEventFaults1_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gChargerEventFaults1_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gChargerEventFaults1_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gChargerEventFaults1_fifo_queue" {
   default     = false
}
variable "sqs_v2gChargerEventFaults1_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gChargerEventFaultsAggregated_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gChargerEventFaultsAggregated_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gChargerEventFaultsAggregated_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gChargerEventFaultsAggregated_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gChargerEventFaultsAggregated_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gChargerEventFaultsAggregated_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gChargerEventFaultsAggregated_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gChargerEventFaultsAggregated_fifo_queue" {
   default     = false
}
variable "sqs_v2gChargerEventFaultsAggregated_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gChargerEvents1_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gChargerEvents1_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gChargerEvents1_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gChargerEvents1_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gChargerEvents1_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gChargerEvents1_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gChargerEvents1_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gChargerEvents1_fifo_queue" {
   default     = false
}
variable "sqs_v2gChargerEvents1_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gDnp3Reporting_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gDnp3Reporting_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gDnp3Reporting_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gDnp3Reporting_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gDnp3Reporting_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gDnp3Reporting_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gDnp3Reporting_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gDnp3Reporting_fifo_queue" {
   default     = false
}
variable "sqs_v2gDnp3Reporting_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gLib_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gLib_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gLib_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gLib_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gLib_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gLib_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gLib_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gLib_fifo_queue" {
   default     = false
}
variable "sqs_v2gLib_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gOCPPChargerStatus_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gOCPPChargerStatus_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gOCPPChargerStatus_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gOCPPChargerStatus_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gOCPPChargerStatus_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gOCPPChargerStatus_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gOCPPChargerStatus_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gOCPPChargerStatus_fifo_queue" {
   default     = false
}
variable "sqs_v2gOCPPChargerStatus_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gOCPPMessages_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gOCPPMessages_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gOCPPMessages_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gOCPPMessages_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gOCPPMessages_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gOCPPMessages_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gOCPPMessages_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gOCPPMessages_fifo_queue" {
   default     = false
}
variable "sqs_v2gOCPPMessages_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gDevIngestor_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gDevIngestor_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gDevIngestor_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gDevIngestor_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gDevIngestor_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gDevIngestor_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gDevIngestor_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gDevIngestor_fifo_queue" {
   default     = false
}
variable "sqs_v2gDevIngestor_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gtest13_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gtest13_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gtest13_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gtest13_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gtest13_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gtest13_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gtest13_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gtest13_fifo_queue" {
   default     = false
}
variable "sqs_v2gtest13_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gtest16_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gtest16_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gtest16_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gtest16_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gtest16_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gtest16_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gtest16_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gtest16_fifo_queue" {
   default     = false
}
variable "sqs_v2gtest16_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gtest58_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gtest58_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gtest58_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gtest58_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gtest58_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gtest58_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gtest58_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gtest58_fifo_queue" {
   default     = false
}
variable "sqs_v2gtest58_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gTestReppaOutbound_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gTestReppaOutbound_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gTestReppaOutbound_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gTestReppaOutbound_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gTestReppaOutbound_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gTestReppaOutbound_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gTestReppaOutbound_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gTestReppaOutbound_fifo_queue" {
   default     = false
}
variable "sqs_v2gTestReppaOutbound_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_v2gTestReppa_name" {
  type        = string
   default     = ""
}
variable "sqs_v2gTestReppa_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_v2gTestReppa_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_v2gTestReppa_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_v2gTestReppa_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gTestReppa_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_v2gTestReppa_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_v2gTestReppa_fifo_queue" {
   default     = false
}
variable "sqs_v2gTestReppa_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_PCANFaultEvents_name" {
  type        = string
   default     = ""
}
variable "sqs_PCANFaultEvents_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_PCANFaultEvents_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_PCANFaultEvents_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_PCANFaultEvents_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_PCANFaultEvents_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_PCANFaultEvents_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_PCANFaultEvents_fifo_queue" {
   default     = false
}
variable "sqs_PCANFaultEvents_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_PCANServiceOperation_name" {
  type        = string
   default     = ""
}
variable "sqs_PCANServiceOperation_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_PCANServiceOperation_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_PCANServiceOperation_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_PCANServiceOperation_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_PCANServiceOperation_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_PCANServiceOperation_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_PCANServiceOperation_fifo_queue" {
   default     = false
}
variable "sqs_PCANServiceOperation_content_based_deduplication" {
   default     = false
}

###################

variable "sqs_APEX_MDC_TO_OTA_name" {
  type        = string
   default     = ""
}
variable "sqs_APEX_MDC_TO_OTA_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs_APEX_MDC_TO_OTA_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs_APEX_MDC_TO_OTA_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs_APEX_MDC_TO_OTA_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs_APEX_MDC_TO_OTA_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
variable "sqs_APEX_MDC_TO_OTA_env_tag" {
  type        = string
   default    = ""
}
variable "sqs_APEX_MDC_TO_OTA_fifo_queue" {
   default     = false
}
variable "sqs_APEX_MDC_TO_OTA_content_based_deduplication" {
   default     = false
}


##############  s3  #################

variable "s3_ccssEcms_bucket_name" {
    type = string
} 

variable "s3_ccssEcms_bucket_policy_file_name" {
    type = string
}

variable "s3_ams_bucket_name" {
    type = string
} 

variable "s3_ams_bucket_policy_file_name" {
    type = string
}

variable "s3_apex_bucket_name" {
    type = string
} 

variable "s3_apex_bucket_policy_file_name" {
    type = string
}

variable "s3_backwardToucan_bucket_name" {
    type = string
} 

variable "s3_backwardToucan_bucket_policy_file_name" {
    type = string
}

variable "s3_commissioning_bucket_name" {
    type = string
} 

variable "s3_commissioning_bucket_policy_file_name" {
    type = string
}

variable "s3_dataConnected_bucket_name" {
    type = string
} 

variable "s3_dataConnected_bucket_policy_file_name" {
    type = string
}

variable "s3_reports_bucket_name" {
    type = string
} 

variable "s3_reports_bucket_policy_file_name" {
    type = string
}

variable "s3_toucanData_bucket_name" {
    type = string
} 

variable "s3_toucanData_bucket_policy_file_name" {
    type = string
}

variable "s3_toucanDataTest_bucket_name" {
    type = string
} 

variable "s3_toucanDataTest_bucket_policy_file_name" {
    type = string
}

variable "s3_toucan_bucket_name" {
    type = string
} 

variable "s3_toucan_bucket_policy_file_name" {
    type = string
}

variable "s3_multiplexer_bucket_name" {
    type = string
} 

variable "s3_multiplexer_bucket_policy_file_name" {
    type = string
}

variable "s3_v2g_apex_bucket_name" {
    type = string
} 

variable "s3_v2g_apex_bucket_policy_file_name" {
    type = string
}


#################  Cloud Front ###############

variable "acm_certificate_arn" {
  type = string 
  default="arn:aws:acm:us-east-1:493000495847:certificate/4d0492b7-b063-4ce1-b231-c87286e66e2b"

}

# APEX 
variable "cf_apex_aliases" {
  type        = list(string)
  default     = []
}

# EOL 
variable "cf_eol_aliases" {
  type        = list(string)
  default     = []
}

######### cognito  #########

variable "ams_cognito_user_pool_name" {
  type = string
  default = ""
}

variable "ams_cognito_email_verif_subject" {
  type = string
  default = ""
}

variable "ams_cognito_email_verif_message" {
  type = string
  default = ""
}

variable "ams_cognito_sms_verif_message" {
  type = string
  default = ""
}

variable "ams_cognito_unused_account_days" {
  type = number
  default = 30
}

variable "ams_cognito_email_subject" {
  type = string
  default = ""
}


variable "ams_cognito_sms_email_message" {
  type = string
  default = "\u003c!DOCTYPE html\n  PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"\u003e\n\u003chtml xmlns=\"http://www.w3.org/1999/xhtml\"\u003e\n\n\u003chead\u003e\n  \u003cmeta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /\u003e\n  \u003cmeta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /\u003e\n  \u003ctitle\u003eSetup a new password\u003c/title\u003e\n  \u003c!-- \n    The style block is collapsed on page load to save you some scrolling.\n    Postmark automatically inlines all CSS properties for maximum email client \n    compatibility. You can just update styles here, and Postmark does the rest.\n    --\u003e\n  \u003cstyle type=\"text/css\" rel=\"stylesheet\" media=\"all\"\u003e\n    /* Base ------------------------------ */\n\n    *:not(br):not(tr):not(html) {\n      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n      box-sizing: border-box;\n    }\n\n    body {\n      width: 100% !important;\n      height: 100%;\n      margin: 0;\n      line-height: 1.4;\n      background-color: #F2F4F6;\n      color: #74787E;\n      -webkit-text-size-adjust: none;\n    }\n\n    p,\n    ul,\n    ol,\n    blockquote {\n      line-height: 1.4;\n      text-align: left;\n    }\n\n    a {\n      color: #3869D4;\n    }\n\n    a img {\n      border: none;\n    }\n\n    td {\n      word-break: break-word;\n    }\n\n    /* Layout ------------------------------ */\n\n    .email-wrapper {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #F2F4F6;\n    }\n\n    .email-content {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    /* Masthead ----------------------- */\n\n    .email-masthead {\n      padding: 25px 0;\n      text-align: center;\n    }\n\n    .email-masthead_logo {\n      width: 94px;\n    }\n\n    .email-masthead_name {\n      font-size: 16px;\n      font-weight: bold;\n      color: #bbbfc3;\n      text-decoration: none;\n      text-shadow: 0 1px 0 white;\n    }\n\n    /* Body ------------------------------ */\n\n    .email-body {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      border-top: 1px solid #EDEFF2;\n      border-bottom: 1px solid #EDEFF2;\n      background-color: #FFFFFF;\n    }\n\n    .email-body_inner {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #FFFFFF;\n    }\n\n    .email-footer {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .email-footer p {\n      color: #AEAEAE;\n    }\n\n    .body-action {\n      width: 100%;\n      margin: 30px auto;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .body-sub {\n      margin-top: 25px;\n      padding-top: 25px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .content-cell {\n      padding: 35px;\n    }\n\n    .preheader {\n      display: none !important;\n      visibility: hidden;\n      mso-hide: all;\n      font-size: 1px;\n      line-height: 1px;\n      max-height: 0;\n      max-width: 0;\n      opacity: 0;\n      overflow: hidden;\n    }\n\n    /* Attribute list ------------------------------ */\n\n    .attributes {\n      margin: 0 0 21px;\n    }\n\n    .attributes_content {\n      background-color: #EDEFF2;\n      padding: 16px;\n    }\n\n    .attributes_item {\n      padding: 0;\n    }\n\n    /* Related Items ------------------------------ */\n\n    .related {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .related_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .related_item-title {\n      display: block;\n      margin: .5em 0 0;\n    }\n\n    .related_item-thumb {\n      display: block;\n      padding-bottom: 10px;\n    }\n\n    .related_heading {\n      border-top: 1px solid #EDEFF2;\n      text-align: center;\n      padding: 25px 0 10px;\n    }\n\n    /* Discount Code ------------------------------ */\n\n    .discount {\n      width: 100%;\n      margin: 0;\n      padding: 24px;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #EDEFF2;\n      border: 2px dashed #9BA2AB;\n    }\n\n    .discount_heading {\n      text-align: center;\n    }\n\n    .discount_body {\n      text-align: center;\n      font-size: 15px;\n    }\n\n    /* Social Icons ------------------------------ */\n\n    .social {\n      width: auto;\n    }\n\n    .social td {\n      padding: 0;\n      width: auto;\n    }\n\n    .social_icon {\n      height: 20px;\n      margin: 0 8px 10px 8px;\n      padding: 0;\n    }\n\n    /* Data table ------------------------------ */\n\n    .purchase {\n      width: 100%;\n      margin: 0;\n      padding: 35px 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_content {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .purchase_heading {\n      padding-bottom: 8px;\n      border-bottom: 1px solid #EDEFF2;\n    }\n\n    .purchase_heading p {\n      margin: 0;\n      color: #9BA2AB;\n      font-size: 12px;\n    }\n\n    .purchase_footer {\n      padding-top: 15px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .purchase_total {\n      margin: 0;\n      text-align: right;\n      font-weight: bold;\n      color: #2F3133;\n    }\n\n    .purchase_total--label {\n      padding: 0 15px 0 0;\n    }\n\n    /* Utilities ------------------------------ */\n\n    .align-right {\n      text-align: right;\n    }\n\n    .align-left {\n      text-align: left;\n    }\n\n    .align-center {\n      text-align: center;\n    }\n\n    /*Media Queries ------------------------------ */\n\n    @media only screen and (max-width: 600px) {\n\n      .email-body_inner,\n      .email-footer {\n        width: 100% !important;\n      }\n    }\n\n    @media only screen and (max-width: 500px) {\n      .button {\n        width: 100% !important;\n      }\n    }\n\n    /* Buttons ------------------------------ */\n\n    .button {\n      background-color: #3869D4;\n      border-top: 10px solid #3869D4;\n      border-right: 18px solid #3869D4;\n      border-bottom: 10px solid #3869D4;\n      border-left: 18px solid #3869D4;\n      display: inline-block;\n      color: #FFF;\n      text-decoration: none;\n      border-radius: 3px;\n      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);\n      -webkit-text-size-adjust: none;\n    }\n\n    .button--green {\n      background-color: #62a744;\n      border-top: 10px solid #62a744;\n      border-right: 18px solid #62a744;\n      border-bottom: 10px solid #62a744;\n      border-left: 18px solid #62a744;\n    }\n\n    .button--red {\n      background-color: #FF6136;\n      border-top: 10px solid #FF6136;\n      border-right: 18px solid #FF6136;\n      border-bottom: 10px solid #FF6136;\n      border-left: 18px solid #FF6136;\n    }\n\n    /* Type ------------------------------ */\n\n    h1 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 19px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h2 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 16px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h3 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 14px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    p {\n      margin-top: 0;\n      color: #74787E;\n      font-size: 16px;\n      line-height: 1.5em;\n      text-align: left;\n    }\n\n    p.sub {\n      font-size: 12px;\n    }\n\n    p.center {\n      text-align: center;\n    }\n  \u003c/style\u003e\n\u003c/head\u003e\n\n\u003cbody\u003e\n  \u003cspan class=\"preheader\"\u003eUse this link to reset your password. The link is only valid for 24 hours.\u003c/span\u003e\n  \u003ctable class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n    \u003ctr\u003e\n      \u003ctd align=\"center\"\u003e\n        \u003ctable class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-masthead\"\u003e\n              \u003cdiv class=\"navbar-header\" style=\"width:100%; height:100px\"\u003e\n\n                \u003ca class=\"navbar-brand\" rel=\"home\" href=\"#\" title=\"\"\u003e\n                  \u003cimg style=\"margin-top: 10px;\" src=\"http://exp-qa-apex.proterra.com.s3-website-us-east-1.amazonaws.com/images/logo.png\"\u003e\n                \u003c/a\u003e\n              \u003c/div\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003c!-- Email Body --\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n              \u003ctable class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003c!-- Body content --\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\"\u003e\n                    \u003ch1\u003eHi {username},\u003c/h1\u003e\n                    \u003cp\u003eYour Proterra APEX administrator has created an account for you.\u003c/p\u003e\n                    \u003cp\u003eClick below to complete your account setup. You will be asked to set your password.\u003c/p\u003e\n                    \u003c!-- Action --\u003e\n                    \u003ctable class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd align=\"center\"\u003e\n                          \u003c!-- Border based button\n                       https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design --\u003e\n                          \u003ctable width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                            \u003ctr\u003e\n                              \u003ctd align=\"center\"\u003e\n                                \u003ctable border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                                  \u003ctr\u003e\n                                    \u003ctd\u003e\n                                      \u003ca href=\"http://exp-qa-apex.proterra.com.s3-website-us-east-1.amazonaws.com/changePassword.html?username={username}\u0026temporaryPassword={####}\"\n                                        class=\"button button--green\" target=\"_blank\"\n                                        style=\"color: white !important;\"\u003eSet up new password\u003c/a\u003e\n                                    \u003c/td\u003e\n                                  \u003c/tr\u003e\n                                \u003c/table\u003e\n                              \u003c/td\u003e\n                            \u003c/tr\u003e\n                          \u003c/table\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                    \u003cp\u003eIf you have issues or questions please contact your admin or \u003ca\n                        href=\"mailto:connected@proterra.com\"\u003eAPEX support\u003c/a\u003e.\u003c/p\u003e\n                    \u003cp\u003eThank you,\n                      \u003cbr\u003eThe APEX Team\u003c/p\u003e\n                    \u003c!-- Sub copy --\u003e\n                    \u003ctable class=\"body-sub\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd\u003e\n                          \u003cp class=\"sub\"\u003eIf you’re having trouble with the button above, copy and paste the URL below\n                            into your web browser.\u003c/p\u003e\n                          \u003cp class=\"sub\"\u003e\n                            http://exp-qa-apex.proterra.com.s3-website-us-east-1.amazonaws.com/changePassword.html?username={username}\u0026temporaryPassword={####}\n                          \u003c/p\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003ctr\u003e\n            \u003ctd\u003e\n              \u003ctable class=\"email-footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\" align=\"center\"\u003e\n                    \u003cp class=\"sub align-center\"\u003e\u0026copy; 2019 Proterra. All rights reserved.\u003c/p\u003e\n\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n        \u003c/table\u003e\n      \u003c/td\u003e\n    \u003c/tr\u003e\n  \u003c/table\u003e\n\u003c/body\u003e\n\n\u003c/html\u003e"

}

# schema
variable "schemas" {
  description = "A container with the schema attributes of a user pool. Maximum of 50 attributes"
  type        = list
  default     = []
}

variable "string_schemas" {
  description = "A container with the string schema attributes of a user pool. Maximum of 50 attributes"
  type        = list
  default     = []
}

variable "number_schemas" {
  description = "A container with the number schema attributes of a user pool. Maximum of 50 attributes"
  type        = list
  default     = []
}
######### cognito client #########

variable "ams_cognito_app_client_name" {
  type = string
  default = ""
}


variable "ams_callback_urls" {
  type = list
  default = []
}
variable "ams_logout_urls" {
  type = list
  default = []
}

variable "ams_allowed_oauth_scopes" {
  type = list
  default = []
}
variable "ams_explicit_auth_flows" {
  type = list
  default = []
}
variable "ams_supported_identity_providers"  {
  type = list
  default = []
}

################ Lambda #################

# for RoadRunnerDataProcessor
variable "RoadRunnerDataProcessor_s3_bucket" {}
variable "RoadRunnerDataProcessor_s3_key" {}
variable "lambda_RoadRunnerDataProcessor_iam_role_name" {}
variable "lambda_RoadRunnerDataProcessor_iam_role_policy" {}
variable "lambda_RoadRunnerDataProcessor_iam_assume_role_policy" {}
variable "RoadRunnerDataProcessor_path" {}
variable "enable_RoadRunnerDataProcessor_role" {}
variable "RoadRunnerDataProcessor_policy_name" {}
#variable "lambda_RoadRunnerDataProcessor_filename" {}
variable "lambda_RoadRunnerDataProcessor_function_name" {}
variable "lambda_RoadRunnerDataProcessor_timeout" {}
variable "lambda_RoadRunnerDataProcessor_mem_size" {}
variable "lambda_RoadRunnerDataProcessor_handler" {}
variable "lambda_RoadRunnerDataProcessor_runtime" {}
variable "RoadRunnerDataProcessor_statement_id" {
  type = list(string)
}
variable "RoadRunnerDataProcessor_action"  {}
variable "RoadRunnerDataProcessor_principal" {
  type = list(string)
}
variable "RoadRunnerDataProcessor_source_arn" {
  type = list(string)
}

# for DecompressLambda
variable "DecompressLambda_s3_bucket" {}
variable "DecompressLambda_s3_key" {}
variable "lambda_DecompressLambda_iam_role_name" {}
variable "lambda_DecompressLambda_iam_role_policy" {}
variable "lambda_DecompressLambda_iam_assume_role_policy" {}
variable "DecompressLambda_path" {}
variable "enable_DecompressLambda_role" {}
variable "DecompressLambda_policy_name" {}
#variable "lambda_DecompressLambda_filename" {}
variable "lambda_DecompressLambda_function_name" {}
variable "lambda_DecompressLambda_timeout" {}
variable "lambda_DecompressLambda_mem_size" {}
variable "lambda_DecompressLambda_handler" {}
variable "lambda_DecompressLambda_runtime" {}
variable "DecompressLambda_statement_id" {
  type = list(string)
}
variable "DecompressLambda_action"  {}
variable "DecompressLambda_principal" {
  type = list(string)
}
variable "DecompressLambda_source_arn" {
  type = list(string)
}

# for BackwardToucanProcessor
variable "BackwardToucanProcessor_s3_bucket" {}
variable "BackwardToucanProcessor_s3_key" {}
variable "lambda_BackwardToucanProcessor_iam_role_name" {}
variable "lambda_BackwardToucanProcessor_iam_role_policy" {}
variable "lambda_BackwardToucanProcessor_iam_assume_role_policy" {}
variable "BackwardToucanProcessor_path" {}
variable "enable_BackwardToucanProcessor_role" {}
variable "BackwardToucanProcessor_policy_name" {}
#variable "lambda_BackwardToucanProcessor_filename" {}
variable "lambda_BackwardToucanProcessor_function_name" {}
variable "lambda_BackwardToucanProcessor_timeout" {}
variable "lambda_BackwardToucanProcessor_mem_size" {}
variable "lambda_BackwardToucanProcessor_handler" {}
variable "lambda_BackwardToucanProcessor_runtime" {}
variable "BackwardToucanProcessor_statement_id" {
  type = list(string)
}
variable "BackwardToucanProcessor_action"  {}
variable "BackwardToucanProcessor_principal" {
  type = list(string)
}
variable "BackwardToucanProcessor_source_arn" {
  type = list(string)
}

# for ToucanRawProcessor
variable "ToucanRawProcessor_s3_bucket" {}
variable "ToucanRawProcessor_s3_key" {}
variable "lambda_ToucanRawProcessor_iam_role_name" {}
variable "lambda_ToucanRawProcessor_iam_role_policy" {}
variable "lambda_ToucanRawProcessor_iam_assume_role_policy" {}
variable "ToucanRawProcessor_path" {}
variable "enable_ToucanRawProcessor_role" {}
variable "ToucanRawProcessor_policy_name" {}
#variable "lambda_ToucanRawProcessor_filename" {}
variable "lambda_ToucanRawProcessor_function_name" {}
variable "lambda_ToucanRawProcessor_timeout" {}
variable "lambda_ToucanRawProcessor_mem_size" {}
variable "lambda_ToucanRawProcessor_handler" {}
variable "lambda_ToucanRawProcessor_runtime" {}
variable "ToucanRawProcessor_statement_id" {
  type = list(string)
}
variable "ToucanRawProcessor_action"  {}
variable "ToucanRawProcessor_principal" {
  type = list(string)
}
variable "ToucanRawProcessor_source_arn" {
  type = list(string)
}

##########  API gateway #################

variable "templates_exp_path" {
   type = string
   default = "templates-launch2020"
}


#QA_LAUNCH2020_EOL_STARPOINT
variable "template_file_name_api_eol_startpoint" {
   type = string
   default = ""
}
variable "template_file_map_eol_startpoint" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_eol_startpoint" {
 type = map(string)
 default = {}
}

### QA_LAUNCH2020_BUSINESS_TIER
variable "template_file_name_api_business_tier" {
   type = string
   default = ""
}
variable "template_file_map_business_tier" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_business_tier" {
 type = map(string)
 default = {}
}


#QA_LAUNCH2020_CCSS
variable "template_file_name_api_ccss" {
   type = string
   default = ""
}
variable "template_file_map_ccss" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_ccss" {
 type = map(string)
 default = {}
}

#QA_LAUNCH2020_CCSS_TRACK
variable "template_file_name_api_ccss_track" {
   type = string
   default = ""
}
variable "template_file_map_ccss_track" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_ccss_track" {
 type = map(string)
 default = {}
}

#QA_LAUNCH2020_CHARGEMONITOR
variable "template_file_name_api_chargemonitor" {
   type = string
   default = ""
}
variable "template_file_map_chargemonitor" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_chargemonitor" {
 type = map(string)
 default = {}
}


#QA_LAUNCH2020_CIM
variable "template_file_name_api_cim" {
   type = string
   default = ""
}
variable "template_file_map_cim" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_cim" {
 type = map(string)
 default = {}
}


#QA_LAUNCH2020_DATALAKE
variable "template_file_name_api_datalake" {
   type = string
   default = ""
}
variable "template_file_map_datalake" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_datalake" {
 type = map(string)
 default = {}
}

#QA_LAUNCH2020_DEMAND_MANAGEMENT
variable "template_file_name_api_demand_management" {
   type = string
   default = ""
}
variable "template_file_map_demand_management" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_demand_management" {
 type = map(string)
 default = {}
}

#QA_LAUNCH2020_IQ
variable "template_file_name_api_iq" {
   type = string
   default = ""
}
variable "template_file_map_iq" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_iq" {
 type = map(string)
 default = {}
}



#QA_LAUNCH2020_MISCEL_API
variable "template_file_name_api_miscel" {
   type = string
   default = ""
}
variable "template_file_map_miscel" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_miscel" {
 type = map(string)
 default = {}
}

#QA_LAUNCH2020_MULTIPLEXER
variable "template_file_name_api_multiplexer" {
   type = string
   default = ""
}
variable "template_file_map_multiplexer" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_multiplexer" {
 type = map(string)
 default = {}
}


#QA_LAUNCH2020_MULTIPLEXER_CP
variable "template_file_name_api_multiplexer_cp" {
   type = string
   default = ""
}
variable "template_file_map_multiplexer_cp" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_multiplexer_cp" {
 type = map(string)
 default = {}
}


#QA_LAUNCH2020_OCPP
variable "template_file_name_api_ocpp" {
   type = string
   default = ""
}
variable "template_file_map_ocpp" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_ocpp" {
 type = map(string)
 default = {}
}


#QA_LAUNCH2020_PDF_FILE_PROTERRA_AMS
variable "template_file_name_api_pdf_file_proterra_ams" {
   type = string
   default = ""
}
variable "template_file_map_pdf_file_proterra_ams" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_pdf_file_proterra_ams" {
 type = map(string)
 default = {}
}


#QA_LAUNCH2020_PROTERRA_AMS
variable "template_file_name_api_proterra_ams" {
   type = string
   default = ""
}
variable "template_file_map_proterra_ams" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_proterra_ams" {
 type = map(string)
 default = {}
}

#QA_LAUNCH2020_V2G_WEBSERVICES

variable "template_file_name_api_v2g_webservices" {
   type = string
   default = ""
}
variable "template_file_map_v2g_webservices" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_v2g_webservices" {
 type = map(string)
 default = {}
}

#QA_LAUNCH2020_RUNS
variable "template_file_name_api_runs" {
   type = string
   default = ""
}
variable "template_file_map_runs" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_runs" {
 type = map(string)
 default = {}
}


############ SNS  #####
#ocpppowersetsns 
variable "ocpppowersetsns_sns_name" {
  type        = string
   default     = ""
}
variable "ocpppowersetsns_sns_subscription_arn_list" {
 type = list
 default = []
}

#ocpp-messages
variable "ocpp_messages_sns_name" {
  type        = string
   default     = ""
}
variable "ocpp_messages_sns_subscription_arn_list" {
 type = list
 default = []
}

#v2g_ocpp-messages
variable "v2g_ocpp_messages_sns_name" {
  type        = string
   default     = ""
}
variable "v2g_ocpp_messages_sns_subscription_arn_list" {
 type = list
 default = []
}

#v2g-sns
variable "v2g_sns_sns_name" {
  type        = string
   default     = ""
}
variable "v2g_sns_sns_subscription_arn_list" {
 type = list
 default = []
}

#v2g-sns-reppa
variable "v2g_sns_reppa_sns_name" {
  type        = string
   default     = ""
}
variable "v2g_sns_reppa_sns_subscription_arn_list" {
 type = list
 default = []
}