#####################################################
# Base Level Terraform Code.
# - Modify Account specific items.
#####################################################
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
  }
}


########################  VPC ########################
module "TF_Module_VPC" {
  source = "../../modules/TF_Module_VPC"

  vpc_cidr              = var.vpc_cidr
  vpc_public_subnets    = var.vpc_public_subnets
  vpc_private_subnets   = var.vpc_private_subnets
  vpc_availabilityzones = var.vpc_availabilityzones
  vpc_account_owner_ip  = var.vpc_account_owner_ip

  vpc_tag_project       = var.vpc_project
  vpc_tag_environment   = var.vpc_environment
  subnet_additional_tags   = var.subnet_additional_tags
  # DynamoDB VPC End point
  vpc_endpoint_tag_name   = var.vpc_endpoint_tag_name
}


data "aws_subnet_ids" "all-subnets" {
  vpc_id = module.TF_Module_VPC.vpc_id
}  

module "TF_Module_EKS" {
  source = "../../modules/TF_Module_EKS"
  cluster-name       = var.cluster-name
  all-subnets-list   = data.aws_subnet_ids.all-subnets.ids
  private-subnets-list = module.TF_Module_VPC.vpc_private_subnet_ids
  vpc_id = module.TF_Module_VPC.vpc_id
  worker-nodes-data = var.worker-nodes-data
  #node-groups-data = [var.launch2020-node-group-data,var.mdc-node-group-data]
  node-groups-data = [var.launch2020-node-group-data]
}


module "TF_Module_ECR" {
  source = "../../modules/TF_Module_ECR"
  ecr_name       = var.ecr_name
}


module "TF_Module_Redis" {
  source = "../../modules/TF_Module_Redis"
  vpc_id = module.TF_Module_VPC.vpc_id
  vpc_cidr_blocks      = [ "${module.TF_Module_VPC.vpc_cidr_block}"]
  cluster_id           = var.cluster_id
  node_type            = var.node_type
  num_cache_nodes      = var.num_cache_nodes
  parameter_group_name = var.parameter_group_name
  replication_group_id  = var.replication_group_id
  engine_version       = var.engine_version
  port_number          = var.port_number
  cluster_security_group_name       = var.cluster_security_group_name
  cluster_subnet_group          = var.cluster_subnet_group
  private-subnets-list          = module.TF_Module_VPC.vpc_private_subnet_ids
  #az-list                       = module.TF_Module_VPC.vpc_availability_zones
  number_cache_clusters          = var.number_cache_clusters
}

################ SQS #################

module "TF_Module_SQS_BusRealTimeDataStream" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_BusRealTimeDataStream_env_tag
  sqs_name                           = var.sqs_BusRealTimeDataStream_name
  sqs_visibility_timeout_seconds     = var.sqs_BusRealTimeDataStream_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_BusRealTimeDataStream_message_retention_seconds
  sqs_max_message_size               = var.sqs_BusRealTimeDataStream_max_message_size
  sqs_delay_seconds                  = var.sqs_BusRealTimeDataStream_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_BusRealTimeDataStream_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_BusRealTimeDataStream_fifo_queue
  sqs_content_based_deduplication    = var.sqs_BusRealTimeDataStream_content_based_deduplication
}

module "TF_Module_SQS_BusShadowUpdatesV2" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_BusShadowUpdatesV2_env_tag
  sqs_name                           = var.sqs_BusShadowUpdatesV2_name
  sqs_visibility_timeout_seconds     = var.sqs_BusShadowUpdatesV2_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_BusShadowUpdatesV2_message_retention_seconds
  sqs_max_message_size               = var.sqs_BusShadowUpdatesV2_max_message_size
  sqs_delay_seconds                  = var.sqs_BusShadowUpdatesV2_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_BusShadowUpdatesV2_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_BusShadowUpdatesV2_fifo_queue
  sqs_content_based_deduplication    = var.sqs_BusShadowUpdatesV2_content_based_deduplication
}

module "TF_Module_SQS_LogstashLambdaTest" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_LogstashLambdaTest_env_tag
  sqs_name                           = var.sqs_LogstashLambdaTest_name
  sqs_visibility_timeout_seconds     = var.sqs_LogstashLambdaTest_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_LogstashLambdaTest_message_retention_seconds
  sqs_max_message_size               = var.sqs_LogstashLambdaTest_max_message_size
  sqs_delay_seconds                  = var.sqs_LogstashLambdaTest_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_LogstashLambdaTest_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_LogstashLambdaTest_fifo_queue
  sqs_content_based_deduplication    = var.sqs_LogstashLambdaTest_content_based_deduplication
}

module "TF_Module_SQS_OCPPMessageQueue" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_OCPPMessageQueue_env_tag
  sqs_name                           = var.sqs_OCPPMessageQueue_name
  sqs_visibility_timeout_seconds     = var.sqs_OCPPMessageQueue_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_OCPPMessageQueue_message_retention_seconds
  sqs_max_message_size               = var.sqs_OCPPMessageQueue_max_message_size
  sqs_delay_seconds                  = var.sqs_OCPPMessageQueue_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_OCPPMessageQueue_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_OCPPMessageQueue_fifo_queue
  sqs_content_based_deduplication    = var.sqs_OCPPMessageQueue_content_based_deduplication
}

module "TF_Module_SQS_OCPPMessageQueueGV" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_OCPPMessageQueueGV_env_tag
  sqs_name                           = var.sqs_OCPPMessageQueueGV_name
  sqs_visibility_timeout_seconds     = var.sqs_OCPPMessageQueueGV_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_OCPPMessageQueueGV_message_retention_seconds
  sqs_max_message_size               = var.sqs_OCPPMessageQueueGV_max_message_size
  sqs_delay_seconds                  = var.sqs_OCPPMessageQueueGV_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_OCPPMessageQueueGV_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_OCPPMessageQueueGV_fifo_queue
  sqs_content_based_deduplication    = var.sqs_OCPPMessageQueueGV_content_based_deduplication
}

module "TF_Module_SQS_OCPPFaults" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_OCPPFaults_env_tag
  sqs_name                           = var.sqs_OCPPFaults_name
  sqs_visibility_timeout_seconds     = var.sqs_OCPPFaults_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_OCPPFaults_message_retention_seconds
  sqs_max_message_size               = var.sqs_OCPPFaults_max_message_size
  sqs_delay_seconds                  = var.sqs_OCPPFaults_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_OCPPFaults_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_OCPPFaults_fifo_queue
  sqs_content_based_deduplication    = var.sqs_OCPPFaults_content_based_deduplication
}

module "TF_Module_SQS_OCPPHistogramJobQueue" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_OCPPHistogramJobQueue_env_tag
  sqs_name                           = var.sqs_OCPPHistogramJobQueue_name
  sqs_visibility_timeout_seconds     = var.sqs_OCPPHistogramJobQueue_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_OCPPHistogramJobQueue_message_retention_seconds
  sqs_max_message_size               = var.sqs_OCPPHistogramJobQueue_max_message_size
  sqs_delay_seconds                  = var.sqs_OCPPHistogramJobQueue_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_OCPPHistogramJobQueue_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_OCPPHistogramJobQueue_fifo_queue
  sqs_content_based_deduplication    = var.sqs_OCPPHistogramJobQueue_content_based_deduplication
}

module "TF_Module_SQS_OCPPHourlyJobQueue" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_OCPPHourlyJobQueue_env_tag
  sqs_name                           = var.sqs_OCPPHourlyJobQueue_name
  sqs_visibility_timeout_seconds     = var.sqs_OCPPHourlyJobQueue_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_OCPPHourlyJobQueue_message_retention_seconds
  sqs_max_message_size               = var.sqs_OCPPHourlyJobQueue_max_message_size
  sqs_delay_seconds                  = var.sqs_OCPPHourlyJobQueue_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_OCPPHourlyJobQueue_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_OCPPHourlyJobQueue_fifo_queue
  sqs_content_based_deduplication    = var.sqs_OCPPHourlyJobQueue_content_based_deduplication
}

module "TF_Module_SQS_OCPPSessionJobQueue" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_OCPPSessionJobQueue_env_tag
  sqs_name                           = var.sqs_OCPPSessionJobQueue_name
  sqs_visibility_timeout_seconds     = var.sqs_OCPPSessionJobQueue_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_OCPPSessionJobQueue_message_retention_seconds
  sqs_max_message_size               = var.sqs_OCPPSessionJobQueue_max_message_size
  sqs_delay_seconds                  = var.sqs_OCPPSessionJobQueue_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_OCPPSessionJobQueue_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_OCPPSessionJobQueue_fifo_queue
  sqs_content_based_deduplication    = var.sqs_OCPPSessionJobQueue_content_based_deduplication
}

module "TF_Module_SQS_ReindexRawDataQueue" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_ReindexRawDataQueue_env_tag
  sqs_name                           = var.sqs_ReindexRawDataQueue_name
  sqs_visibility_timeout_seconds     = var.sqs_ReindexRawDataQueue_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_ReindexRawDataQueue_message_retention_seconds
  sqs_max_message_size               = var.sqs_ReindexRawDataQueue_max_message_size
  sqs_delay_seconds                  = var.sqs_ReindexRawDataQueue_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_ReindexRawDataQueue_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_ReindexRawDataQueue_fifo_queue
  sqs_content_based_deduplication    = var.sqs_ReindexRawDataQueue_content_based_deduplication
}

module "TF_Module_SQS_RoadRunnerDataStream" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_RoadRunnerDataStream_env_tag
  sqs_name                           = var.sqs_RoadRunnerDataStream_name
  sqs_visibility_timeout_seconds     = var.sqs_RoadRunnerDataStream_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_RoadRunnerDataStream_message_retention_seconds
  sqs_max_message_size               = var.sqs_RoadRunnerDataStream_max_message_size
  sqs_delay_seconds                  = var.sqs_RoadRunnerDataStream_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_RoadRunnerDataStream_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_RoadRunnerDataStream_fifo_queue
  sqs_content_based_deduplication    = var.sqs_RoadRunnerDataStream_content_based_deduplication
}

module "TF_Module_SQS_ToucanBackward" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_ToucanBackward_env_tag
  sqs_name                           = var.sqs_ToucanBackward_name
  sqs_visibility_timeout_seconds     = var.sqs_ToucanBackward_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_ToucanBackward_message_retention_seconds
  sqs_max_message_size               = var.sqs_ToucanBackward_max_message_size
  sqs_delay_seconds                  = var.sqs_ToucanBackward_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_ToucanBackward_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_ToucanBackward_fifo_queue
  sqs_content_based_deduplication    = var.sqs_ToucanBackward_content_based_deduplication
}

module "TF_Module_SQS_ToucanRawDataStream" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_ToucanRawDataStream_env_tag
  sqs_name                           = var.sqs_ToucanRawDataStream_name
  sqs_visibility_timeout_seconds     = var.sqs_ToucanRawDataStream_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_ToucanRawDataStream_message_retention_seconds
  sqs_max_message_size               = var.sqs_ToucanRawDataStream_max_message_size
  sqs_delay_seconds                  = var.sqs_ToucanRawDataStream_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_ToucanRawDataStream_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_ToucanRawDataStream_fifo_queue
  sqs_content_based_deduplication    = var.sqs_ToucanRawDataStream_content_based_deduplication
}

module "TF_Module_SQS_chargereventfaults" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_chargereventfaults_env_tag
  sqs_name                           = var.sqs_chargereventfaults_name
  sqs_visibility_timeout_seconds     = var.sqs_chargereventfaults_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_chargereventfaults_message_retention_seconds
  sqs_max_message_size               = var.sqs_chargereventfaults_max_message_size
  sqs_delay_seconds                  = var.sqs_chargereventfaults_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_chargereventfaults_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_chargereventfaults_fifo_queue
  sqs_content_based_deduplication    = var.sqs_chargereventfaults_content_based_deduplication
}

module "TF_Module_SQS_ChargerEventFaultsAggregated" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_ChargerEventFaultsAggregated_env_tag
  sqs_name                           = var.sqs_ChargerEventFaultsAggregated_name
  sqs_visibility_timeout_seconds     = var.sqs_ChargerEventFaultsAggregated_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_ChargerEventFaultsAggregated_message_retention_seconds
  sqs_max_message_size               = var.sqs_ChargerEventFaultsAggregated_max_message_size
  sqs_delay_seconds                  = var.sqs_ChargerEventFaultsAggregated_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_ChargerEventFaultsAggregated_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_ChargerEventFaultsAggregated_fifo_queue
  sqs_content_based_deduplication    = var.sqs_ChargerEventFaultsAggregated_content_based_deduplication
}

module "TF_Module_SQS_EnergyManagerOcppQueueProd" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_EnergyManagerOcppQueueProd_env_tag
  sqs_name                           = var.sqs_EnergyManagerOcppQueueProd_name
  sqs_visibility_timeout_seconds     = var.sqs_EnergyManagerOcppQueueProd_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_EnergyManagerOcppQueueProd_message_retention_seconds
  sqs_max_message_size               = var.sqs_EnergyManagerOcppQueueProd_max_message_size
  sqs_delay_seconds                  = var.sqs_EnergyManagerOcppQueueProd_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_EnergyManagerOcppQueueProd_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_EnergyManagerOcppQueueProd_fifo_queue
  sqs_content_based_deduplication    = var.sqs_EnergyManagerOcppQueueProd_content_based_deduplication
}

module "TF_Module_SQS_EnergyManagerOcppQueueTest" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_EnergyManagerOcppQueueTest_env_tag
  sqs_name                           = var.sqs_EnergyManagerOcppQueueTest_name
  sqs_visibility_timeout_seconds     = var.sqs_EnergyManagerOcppQueueTest_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_EnergyManagerOcppQueueTest_message_retention_seconds
  sqs_max_message_size               = var.sqs_EnergyManagerOcppQueueTest_max_message_size
  sqs_delay_seconds                  = var.sqs_EnergyManagerOcppQueueTest_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_EnergyManagerOcppQueueTest_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_EnergyManagerOcppQueueTest_fifo_queue
  sqs_content_based_deduplication    = var.sqs_EnergyManagerOcppQueueTest_content_based_deduplication
}

module "TF_Module_SQS_energymanagersqs" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_energymanagersqs_env_tag
  sqs_name                           = var.sqs_energymanagersqs_name
  sqs_visibility_timeout_seconds     = var.sqs_energymanagersqs_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_energymanagersqs_message_retention_seconds
  sqs_max_message_size               = var.sqs_energymanagersqs_max_message_size
  sqs_delay_seconds                  = var.sqs_energymanagersqs_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_energymanagersqs_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_energymanagersqs_fifo_queue
  sqs_content_based_deduplication    = var.sqs_energymanagersqs_content_based_deduplication
}

module "TF_Module_SQS_ExpOcppChargerStatus" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_ExpOcppChargerStatus_env_tag
  sqs_name                           = var.sqs_ExpOcppChargerStatus_name
  sqs_visibility_timeout_seconds     = var.sqs_ExpOcppChargerStatus_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_ExpOcppChargerStatus_message_retention_seconds
  sqs_max_message_size               = var.sqs_ExpOcppChargerStatus_max_message_size
  sqs_delay_seconds                  = var.sqs_ExpOcppChargerStatus_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_ExpOcppChargerStatus_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_ExpOcppChargerStatus_fifo_queue
  sqs_content_based_deduplication    = var.sqs_ExpOcppChargerStatus_content_based_deduplication
}

module "TF_Module_SQS_ExpOcppMessages" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_ExpOcppMessages_env_tag
  sqs_name                           = var.sqs_ExpOcppMessages_name
  sqs_visibility_timeout_seconds     = var.sqs_ExpOcppMessages_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_ExpOcppMessages_message_retention_seconds
  sqs_max_message_size               = var.sqs_ExpOcppMessages_max_message_size
  sqs_delay_seconds                  = var.sqs_ExpOcppMessages_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_ExpOcppMessages_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_ExpOcppMessages_fifo_queue
  sqs_content_based_deduplication    = var.sqs_ExpOcppMessages_content_based_deduplication
}

module "TF_Module_SQS_FromDevIngestor" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_FromDevIngestor_env_tag
  sqs_name                           = var.sqs_FromDevIngestor_name
  sqs_visibility_timeout_seconds     = var.sqs_FromDevIngestor_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_FromDevIngestor_message_retention_seconds
  sqs_max_message_size               = var.sqs_FromDevIngestor_max_message_size
  sqs_delay_seconds                  = var.sqs_FromDevIngestor_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_FromDevIngestor_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_FromDevIngestor_fifo_queue
  sqs_content_based_deduplication    = var.sqs_FromDevIngestor_content_based_deduplication
}

module "TF_Module_SQS_FromOcppProdIngestorm" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_FromOcppProdIngestorm_env_tag
  sqs_name                           = var.sqs_FromOcppProdIngestorm_name
  sqs_visibility_timeout_seconds     = var.sqs_FromOcppProdIngestorm_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_FromOcppProdIngestorm_message_retention_seconds
  sqs_max_message_size               = var.sqs_FromOcppProdIngestorm_max_message_size
  sqs_delay_seconds                  = var.sqs_FromOcppProdIngestorm_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_FromOcppProdIngestorm_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_FromOcppProdIngestorm_fifo_queue
  sqs_content_based_deduplication    = var.sqs_FromOcppProdIngestorm_content_based_deduplication
}

module "TF_Module_SQS_OcppWriteQueueAssetcustomerdetails" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_OcppWriteQueueAssetcustomerdetails_env_tag
  sqs_name                           = var.sqs_OcppWriteQueueAssetcustomerdetails_name
  sqs_visibility_timeout_seconds     = var.sqs_OcppWriteQueueAssetcustomerdetails_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_OcppWriteQueueAssetcustomerdetails_message_retention_seconds
  sqs_max_message_size               = var.sqs_OcppWriteQueueAssetcustomerdetails_max_message_size
  sqs_delay_seconds                  = var.sqs_OcppWriteQueueAssetcustomerdetails_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_OcppWriteQueueAssetcustomerdetails_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_OcppWriteQueueAssetcustomerdetails_fifo_queue
  sqs_content_based_deduplication    = var.sqs_OcppWriteQueueAssetcustomerdetails_content_based_deduplication
}

module "TF_Module_SQS_OcppWriteQueueCategories" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_OcppWriteQueueCategories_env_tag
  sqs_name                           = var.sqs_OcppWriteQueueCategories_name
  sqs_visibility_timeout_seconds     = var.sqs_OcppWriteQueueCategories_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_OcppWriteQueueCategories_message_retention_seconds
  sqs_max_message_size               = var.sqs_OcppWriteQueueCategories_max_message_size
  sqs_delay_seconds                  = var.sqs_OcppWriteQueueCategories_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_OcppWriteQueueCategories_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_OcppWriteQueueCategories_fifo_queue
  sqs_content_based_deduplication    = var.sqs_OcppWriteQueueCategories_content_based_deduplication
}

module "TF_Module_SQS_expChargerEventFaults1" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_expChargerEventFaults1_env_tag
  sqs_name                           = var.sqs_expChargerEventFaults1_name
  sqs_visibility_timeout_seconds     = var.sqs_expChargerEventFaults1_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_expChargerEventFaults1_message_retention_seconds
  sqs_max_message_size               = var.sqs_expChargerEventFaults1_max_message_size
  sqs_delay_seconds                  = var.sqs_expChargerEventFaults1_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_expChargerEventFaults1_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_expChargerEventFaults1_fifo_queue
  sqs_content_based_deduplication    = var.sqs_expChargerEventFaults1_content_based_deduplication
}

module "TF_Module_SQS_expChargerEventFaultsAggregated" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_expChargerEventFaultsAggregated_env_tag
  sqs_name                           = var.sqs_expChargerEventFaultsAggregated_name
  sqs_visibility_timeout_seconds     = var.sqs_expChargerEventFaultsAggregated_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_expChargerEventFaultsAggregated_message_retention_seconds
  sqs_max_message_size               = var.sqs_expChargerEventFaultsAggregated_max_message_size
  sqs_delay_seconds                  = var.sqs_expChargerEventFaultsAggregated_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_expChargerEventFaultsAggregated_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_expChargerEventFaultsAggregated_fifo_queue
  sqs_content_based_deduplication    = var.sqs_expChargerEventFaultsAggregated_content_based_deduplication
}

module "TF_Module_SQS_expChargerEvent1" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_expChargerEvent1_env_tag
  sqs_name                           = var.sqs_expChargerEvent1_name
  sqs_visibility_timeout_seconds     = var.sqs_expChargerEvent1_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_expChargerEvent1_message_retention_seconds
  sqs_max_message_size               = var.sqs_expChargerEvent1_max_message_size
  sqs_delay_seconds                  = var.sqs_expChargerEvent1_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_expChargerEvent1_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_expChargerEvent1_fifo_queue
  sqs_content_based_deduplication    = var.sqs_expChargerEvent1_content_based_deduplication
}

module "TF_Module_SQS_NonAbbOCPPSubmitMessageRequestQueue" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_env_tag
  sqs_name                           = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_name
  sqs_visibility_timeout_seconds     = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_message_retention_seconds
  sqs_max_message_size               = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_max_message_size
  sqs_delay_seconds                  = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_fifo_queue
  sqs_content_based_deduplication    = var.sqs_NonAbbOCPPSubmitMessageRequestQueue_content_based_deduplication
}

module "TF_Module_SQS_NonAbbOCPPSubmitMessageResponseQueue" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_env_tag
  sqs_name                           = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_name
  sqs_visibility_timeout_seconds     = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_message_retention_seconds
  sqs_max_message_size               = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_max_message_size
  sqs_delay_seconds                  = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_fifo_queue
  sqs_content_based_deduplication    = var.sqs_NonAbbOCPPSubmitMessageResponseQueue_content_based_deduplication
}

module "TF_Module_SQS_NonAbbOCPPChargerEventFaults1" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_NonAbbOCPPChargerEventFaults1_env_tag
  sqs_name                           = var.sqs_NonAbbOCPPChargerEventFaults1_name
  sqs_visibility_timeout_seconds     = var.sqs_NonAbbOCPPChargerEventFaults1_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_NonAbbOCPPChargerEventFaults1_message_retention_seconds
  sqs_max_message_size               = var.sqs_NonAbbOCPPChargerEventFaults1_max_message_size
  sqs_delay_seconds                  = var.sqs_NonAbbOCPPChargerEventFaults1_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_NonAbbOCPPChargerEventFaults1_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_NonAbbOCPPChargerEventFaults1_fifo_queue
  sqs_content_based_deduplication    = var.sqs_NonAbbOCPPChargerEventFaults1_content_based_deduplication
}

module "TF_Module_SQS_NonAbbOCPPChargerEventFaultsaggregated" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_env_tag
  sqs_name                           = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_name
  sqs_visibility_timeout_seconds     = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_message_retention_seconds
  sqs_max_message_size               = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_max_message_size
  sqs_delay_seconds                  = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_fifo_queue
  sqs_content_based_deduplication    = var.sqs_NonAbbOCPPChargerEventFaultsaggregated_content_based_deduplication
}

module "TF_Module_SQS_NonAbbOCPPChargerEvents1" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_NonAbbOCPPChargerEvents1_env_tag
  sqs_name                           = var.sqs_NonAbbOCPPChargerEvents1_name
  sqs_visibility_timeout_seconds     = var.sqs_NonAbbOCPPChargerEvents1_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_NonAbbOCPPChargerEvents1_message_retention_seconds
  sqs_max_message_size               = var.sqs_NonAbbOCPPChargerEvents1_max_message_size
  sqs_delay_seconds                  = var.sqs_NonAbbOCPPChargerEvents1_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_NonAbbOCPPChargerEvents1_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_NonAbbOCPPChargerEvents1_fifo_queue
  sqs_content_based_deduplication    = var.sqs_NonAbbOCPPChargerEvents1_content_based_deduplication
}

module "TF_Module_SQS_NonAbbOCPPMessages" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_NonAbbOCPPMessages_env_tag
  sqs_name                           = var.sqs_NonAbbOCPPMessages_name
  sqs_visibility_timeout_seconds     = var.sqs_NonAbbOCPPMessages_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_NonAbbOCPPMessages_message_retention_seconds
  sqs_max_message_size               = var.sqs_NonAbbOCPPMessages_max_message_size
  sqs_delay_seconds                  = var.sqs_NonAbbOCPPMessages_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_NonAbbOCPPMessages_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_NonAbbOCPPMessages_fifo_queue
  sqs_content_based_deduplication    = var.sqs_NonAbbOCPPMessages_content_based_deduplication
}

module "TF_Module_SQS_v2gChargerEventFaults1" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gChargerEventFaults1_env_tag
  sqs_name                           = var.sqs_v2gChargerEventFaults1_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gChargerEventFaults1_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gChargerEventFaults1_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gChargerEventFaults1_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gChargerEventFaults1_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gChargerEventFaults1_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gChargerEventFaults1_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gChargerEventFaults1_content_based_deduplication
}

module "TF_Module_SQS_v2gChargerEventFaultsAggregated" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gChargerEventFaultsAggregated_env_tag
  sqs_name                           = var.sqs_v2gChargerEventFaultsAggregated_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gChargerEventFaultsAggregated_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gChargerEventFaultsAggregated_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gChargerEventFaultsAggregated_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gChargerEventFaultsAggregated_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gChargerEventFaultsAggregated_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gChargerEventFaultsAggregated_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gChargerEventFaultsAggregated_content_based_deduplication
}

module "TF_Module_SQS_v2gChargerEvents1" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gChargerEvents1_env_tag
  sqs_name                           = var.sqs_v2gChargerEvents1_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gChargerEvents1_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gChargerEvents1_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gChargerEvents1_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gChargerEvents1_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gChargerEvents1_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gChargerEvents1_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gChargerEvents1_content_based_deduplication
}

module "TF_Module_SQS_v2gDnp3Reporting" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gDnp3Reporting_env_tag
  sqs_name                           = var.sqs_v2gDnp3Reporting_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gDnp3Reporting_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gDnp3Reporting_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gDnp3Reporting_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gDnp3Reporting_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gDnp3Reporting_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gDnp3Reporting_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gDnp3Reporting_content_based_deduplication
}

module "TF_Module_SQS_v2gLib" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gLib_env_tag
  sqs_name                           = var.sqs_v2gLib_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gLib_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gLib_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gLib_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gLib_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gLib_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gLib_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gLib_content_based_deduplication
}

module "TF_Module_SQS_v2gOCPPChargerStatus" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gOCPPChargerStatus_env_tag
  sqs_name                           = var.sqs_v2gOCPPChargerStatus_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gOCPPChargerStatus_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gOCPPChargerStatus_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gOCPPChargerStatus_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gOCPPChargerStatus_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gOCPPChargerStatus_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gOCPPChargerStatus_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gOCPPChargerStatus_content_based_deduplication
}

module "TF_Module_SQS_v2gOCPPMessages" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gOCPPMessages_env_tag
  sqs_name                           = var.sqs_v2gOCPPMessages_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gOCPPMessages_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gOCPPMessages_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gOCPPMessages_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gOCPPMessages_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gOCPPMessages_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gOCPPMessages_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gOCPPMessages_content_based_deduplication
}

module "TF_Module_SQS_v2gDevIngestor" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gDevIngestor_env_tag
  sqs_name                           = var.sqs_v2gDevIngestor_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gDevIngestor_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gDevIngestor_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gDevIngestor_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gDevIngestor_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gDevIngestor_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gDevIngestor_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gDevIngestor_content_based_deduplication
}

module "TF_Module_SQS_v2gtest13" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gtest13_env_tag
  sqs_name                           = var.sqs_v2gtest13_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gtest13_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gtest13_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gtest13_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gtest13_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gtest13_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gtest13_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gtest13_content_based_deduplication
}

module "TF_Module_SQS_v2gtest16" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gtest16_env_tag
  sqs_name                           = var.sqs_v2gtest16_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gtest16_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gtest16_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gtest16_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gtest16_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gtest16_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gtest16_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gtest16_content_based_deduplication
}

module "TF_Module_SQS_v2gtest58" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gtest58_env_tag
  sqs_name                           = var.sqs_v2gtest58_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gtest58_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gtest58_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gtest58_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gtest58_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gtest58_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gtest58_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gtest58_content_based_deduplication
}

module "TF_Module_SQS_v2gTestReppaOutbound" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gTestReppaOutbound_env_tag
  sqs_name                           = var.sqs_v2gTestReppaOutbound_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gTestReppaOutbound_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gTestReppaOutbound_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gTestReppaOutbound_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gTestReppaOutbound_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gTestReppaOutbound_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gTestReppaOutbound_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gTestReppaOutbound_content_based_deduplication
}

module "TF_Module_SQS_v2gTestReppa" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_v2gTestReppa_env_tag
  sqs_name                           = var.sqs_v2gTestReppa_name
  sqs_visibility_timeout_seconds     = var.sqs_v2gTestReppa_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_v2gTestReppa_message_retention_seconds
  sqs_max_message_size               = var.sqs_v2gTestReppa_max_message_size
  sqs_delay_seconds                  = var.sqs_v2gTestReppa_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_v2gTestReppa_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_v2gTestReppa_fifo_queue
  sqs_content_based_deduplication    = var.sqs_v2gTestReppa_content_based_deduplication
}

module "TF_Module_SQS_PCANFaultEvents" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_PCANFaultEvents_env_tag
  sqs_name                           = var.sqs_PCANFaultEvents_name
  sqs_visibility_timeout_seconds     = var.sqs_PCANFaultEvents_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_PCANFaultEvents_message_retention_seconds
  sqs_max_message_size               = var.sqs_PCANFaultEvents_max_message_size
  sqs_delay_seconds                  = var.sqs_PCANFaultEvents_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_PCANFaultEvents_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_PCANFaultEvents_fifo_queue
  sqs_content_based_deduplication    = var.sqs_PCANFaultEvents_content_based_deduplication
}

#qa-launch2020-PCANServiceOperation
module "TF_Module_SQS_PCANServiceOperation" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_PCANServiceOperation_env_tag
  sqs_name                           = var.sqs_PCANServiceOperation_name
  sqs_visibility_timeout_seconds     = var.sqs_PCANServiceOperation_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_PCANServiceOperation_message_retention_seconds
  sqs_max_message_size               = var.sqs_PCANServiceOperation_max_message_size
  sqs_delay_seconds                  = var.sqs_PCANServiceOperation_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_PCANServiceOperation_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_PCANServiceOperation_fifo_queue
  sqs_content_based_deduplication    = var.sqs_PCANServiceOperation_content_based_deduplication
}

#qa-launch2020-APEX_MDC_TO_OTA.fifo
module "TF_Module_SQS_APEX_MDC_TO_OTA" {
  source = "../../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_APEX_MDC_TO_OTA_env_tag
  sqs_name                           = var.sqs_APEX_MDC_TO_OTA_name
  sqs_visibility_timeout_seconds     = var.sqs_APEX_MDC_TO_OTA_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs_APEX_MDC_TO_OTA_message_retention_seconds
  sqs_max_message_size               = var.sqs_APEX_MDC_TO_OTA_max_message_size
  sqs_delay_seconds                  = var.sqs_APEX_MDC_TO_OTA_delay_seconds
  sqs_receive_wait_time_seconds      = var.sqs_APEX_MDC_TO_OTA_receive_wait_time_seconds
  sqs_fifo_queue                     = var.sqs_APEX_MDC_TO_OTA_fifo_queue
  sqs_content_based_deduplication    = var.sqs_APEX_MDC_TO_OTA_content_based_deduplication
}
################### S3 ######################

module "TF_Module_S3_ccssEcms_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_ccssEcms_bucket_name
  s3_bucket_policy_file_name = var.s3_ccssEcms_bucket_policy_file_name
}

module "TF_Module_S3_ams_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_ams_bucket_name
  s3_bucket_policy_file_name = var.s3_ams_bucket_policy_file_name
  enable_ploicy              = false
}

module "TF_Module_S3_apex_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_apex_bucket_name
  s3_bucket_policy_file_name = var.s3_apex_bucket_policy_file_name
}

module "TF_Module_S3_backwardToucan_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_backwardToucan_bucket_name
  s3_bucket_policy_file_name = var.s3_backwardToucan_bucket_policy_file_name
  enable_ploicy              = false
}

module "TF_Module_S3_commissioning_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_commissioning_bucket_name
  s3_bucket_policy_file_name = var.s3_commissioning_bucket_policy_file_name
}

module "TF_Module_S3_dataConnected_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_dataConnected_bucket_name
  s3_bucket_policy_file_name = var.s3_dataConnected_bucket_policy_file_name
}

module "TF_Module_S3_reports_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_reports_bucket_name
  s3_bucket_policy_file_name = var.s3_reports_bucket_policy_file_name
  enable_ploicy              = false
}

module "TF_Module_S3_toucanData_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_toucanData_bucket_name
  s3_bucket_policy_file_name = var.s3_toucanData_bucket_policy_file_name
}

module "TF_Module_S3_toucanDataTest_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_toucanDataTest_bucket_name
  s3_bucket_policy_file_name = var.s3_toucanDataTest_bucket_policy_file_name
}

module "TF_Module_S3_toucan_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_toucan_bucket_name
  s3_bucket_policy_file_name = var.s3_toucan_bucket_policy_file_name
}

module "TF_Module_S3_multiplexer_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_multiplexer_bucket_name
  s3_bucket_policy_file_name = var.s3_multiplexer_bucket_policy_file_name
}

module "TF_Module_S3_v2g_apex_Buckets" {
  source = "../../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.s3_v2g_apex_bucket_name
  s3_bucket_policy_file_name = var.s3_v2g_apex_bucket_policy_file_name
}

#################  Cloud Front ###############

module "TF_Module_CLOUDFRONT_APEX" {
   source             = "../../modules/cloudfront"
   aliases            = var.cf_apex_aliases
   origin_domain_name = module.TF_Module_S3_apex_Buckets.s3_bucket_domain_name
   origin_id          =  join("-", list("S3", module.TF_Module_S3_apex_Buckets.s3_bucket_name))
   acm_certificate_arn  = var.acm_certificate_arn
}

module "TF_Module_CLOUDFRONT_EOL" {
   source             = "../../modules/cloudfront"
   aliases            = var.cf_eol_aliases
   origin_domain_name = module.TF_Module_S3_commissioning_Buckets.s3_bucket_domain_name
   origin_id          =  join("-", list("S3", module.TF_Module_S3_commissioning_Buckets.s3_bucket_name))
   acm_certificate_arn  = var.acm_certificate_arn
}


################### cognito ######################
module "TF_Module_COGNITO_AMS" {
  source = "../../modules/TF_Module_COGNITO_AMS"
  cognito_user_pool_name          = var.ams_cognito_user_pool_name
  cognito_email_verif_subject     = var.ams_cognito_email_verif_subject
  cognito_sms_verif_message       = var.ams_cognito_sms_verif_message
  cognito_email_verif_message     = var.ams_cognito_email_verif_message
  cognito_unused_account_days     = var.ams_cognito_unused_account_days
  cognito_email_subject           = var.ams_cognito_email_subject
  cognito_sms_email_message       = var.ams_cognito_sms_email_message
  cognito_app_client_name         = var.ams_cognito_app_client_name
  callback_urls                   = var.ams_callback_urls
  logout_urls                     = var.ams_logout_urls
  allowed_oauth_scopes            = var.ams_allowed_oauth_scopes
  explicit_auth_flows             = var.ams_explicit_auth_flows
  supported_identity_providers    = var.ams_supported_identity_providers
  string_schemas                  = var.string_schemas
  number_schemas                  = var.number_schemas
  #schemas                        = var.schemas                    
}

################### lambda ######################

module "TF_Module_Lambda_RoadRunnerDataProcessor" {
 source = "../../modules/TF_Module_LAMBDA"
 enable_role   =  var.enable_RoadRunnerDataProcessor_role
 iam_role_name =  var.lambda_RoadRunnerDataProcessor_iam_role_name
 iam_assume_role_policy  = var.lambda_RoadRunnerDataProcessor_iam_assume_role_policy
 iam_role_policy = var.lambda_RoadRunnerDataProcessor_iam_role_policy
 policy_name = var.RoadRunnerDataProcessor_policy_name 
 path = var.RoadRunnerDataProcessor_path
 s3_bucket = var.RoadRunnerDataProcessor_s3_bucket
 s3_key = var.RoadRunnerDataProcessor_s3_key
 lambda_funcname = var.lambda_RoadRunnerDataProcessor_function_name
 timeout	  = var.lambda_RoadRunnerDataProcessor_timeout
 mem_size	  = var.lambda_RoadRunnerDataProcessor_mem_size
 handler	  = var.lambda_RoadRunnerDataProcessor_handler
 runtime    =var.lambda_RoadRunnerDataProcessor_runtime
 statement_id  =  var.RoadRunnerDataProcessor_statement_id
 action        =  var.RoadRunnerDataProcessor_action 
 principal     = var.RoadRunnerDataProcessor_principal
 source_arn = var.RoadRunnerDataProcessor_source_arn
 #source_arn = [module.TF_Module_S3_toucanData_Buckets.arn, module.TF_Module_S3_toucanDataTest_Buckets.arn]
}

module "TF_Module_Lambda_DecompressLambda" {
 source = "../../modules/TF_Module_LAMBDA"
 enable_role   =  var.enable_DecompressLambda_role
 iam_role_name =  var.lambda_DecompressLambda_iam_role_name
 iam_assume_role_policy  = var.lambda_DecompressLambda_iam_assume_role_policy
 iam_role_policy = var.lambda_DecompressLambda_iam_role_policy
 policy_name = var.DecompressLambda_policy_name 
 path = var.DecompressLambda_path
 s3_bucket = var.DecompressLambda_s3_bucket
 s3_key = var.DecompressLambda_s3_key
 lambda_funcname = var.lambda_DecompressLambda_function_name
 timeout	  = var.lambda_DecompressLambda_timeout
 mem_size	  = var.lambda_DecompressLambda_mem_size
 handler	  = var.lambda_DecompressLambda_handler
 runtime    =var.lambda_DecompressLambda_runtime
 statement_id  =  var.DecompressLambda_statement_id
 action        =  var.DecompressLambda_action 
 principal     = var.DecompressLambda_principal
 source_arn = var.DecompressLambda_source_arn
 #source_arn = [module.TF_Module_S3_toucanData_Buckets.arn, module.TF_Module_S3_toucanDataTest_Buckets.arn]
}

module "TF_Module_Lambda_BackwardToucanProcessor" {
 source = "../../modules/TF_Module_LAMBDA"
 enable_role   =  var.enable_BackwardToucanProcessor_role
 iam_role_name =  var.lambda_BackwardToucanProcessor_iam_role_name
 iam_assume_role_policy  = var.lambda_BackwardToucanProcessor_iam_assume_role_policy
 iam_role_policy = var.lambda_BackwardToucanProcessor_iam_role_policy
 policy_name = var.BackwardToucanProcessor_policy_name 
 path = var.BackwardToucanProcessor_path
 s3_bucket = var.BackwardToucanProcessor_s3_bucket
 s3_key = var.BackwardToucanProcessor_s3_key
 lambda_funcname = var.lambda_BackwardToucanProcessor_function_name
 timeout	  = var.lambda_BackwardToucanProcessor_timeout
 mem_size	  = var.lambda_BackwardToucanProcessor_mem_size
 handler	  = var.lambda_BackwardToucanProcessor_handler
 runtime    =var.lambda_BackwardToucanProcessor_runtime
 statement_id  =  var.BackwardToucanProcessor_statement_id
 action        =  var.BackwardToucanProcessor_action 
 principal     = var.BackwardToucanProcessor_principal
 source_arn = var.BackwardToucanProcessor_source_arn
 #source_arn = [module.TF_Module_S3_toucanData_Buckets.arn, module.TF_Module_S3_toucanDataTest_Buckets.arn]
}

module "TF_Module_Lambda_ToucanRawProcessor" {
 source = "../../modules/TF_Module_LAMBDA"
 enable_role   =  var.enable_ToucanRawProcessor_role
 iam_role_name =  var.lambda_ToucanRawProcessor_iam_role_name
 iam_assume_role_policy  = var.lambda_ToucanRawProcessor_iam_assume_role_policy
 iam_role_policy = var.lambda_ToucanRawProcessor_iam_role_policy
 policy_name = var.ToucanRawProcessor_policy_name 
 path = var.ToucanRawProcessor_path
 s3_bucket = var.ToucanRawProcessor_s3_bucket
 s3_key = var.ToucanRawProcessor_s3_key
 lambda_funcname = var.lambda_ToucanRawProcessor_function_name
 timeout	  = var.lambda_ToucanRawProcessor_timeout
 mem_size	  = var.lambda_ToucanRawProcessor_mem_size
 handler	  = var.lambda_ToucanRawProcessor_handler
 runtime    =var.lambda_ToucanRawProcessor_runtime
 statement_id  =  var.ToucanRawProcessor_statement_id
 action        =  var.ToucanRawProcessor_action 
 principal     = var.ToucanRawProcessor_principal
 source_arn = var.ToucanRawProcessor_source_arn
 #source_arn = [module.TF_Module_S3_toucanData_Buckets.arn, module.TF_Module_S3_toucanDataTest_Buckets.arn]
}

#########  API Gateway ############

#QA_LAUNCH2020_EOL_STARPOINT

module "TF_Module_EOL_STARPOINT" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_eol_startpoint
  template_file_map=  merge(var.template_file_map_eol_startpoint,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_eol_startpoint["stageName"]
  api_name = var.template_file_map_eol_startpoint["title"]
  api_description = var.template_file_map_eol_startpoint["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_eol_startpoint
}

#QA_LAUNCH2020_BUSINESS_TIER

module "TF_Module_API_BUSINESS_TIER" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_business_tier
  template_file_map=  merge(var.template_file_map_business_tier,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_business_tier["stageName"]
  api_name = var.template_file_map_business_tier["title"]
  api_description = var.template_file_map_business_tier["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_business_tier
}

#QA_LAUNCH2020_CCSS

module "TF_Module_API_CCSS" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_ccss
  template_file_map=  merge(var.template_file_map_ccss,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_ccss["stageName"]
  api_name = var.template_file_map_ccss["title"]
  api_description = var.template_file_map_ccss["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_ccss
}

#QA_LAUNCH2020_CCSS_TRACK

module "TF_Module_API_CCSS_TRACK" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_ccss_track
  template_file_map=  merge(var.template_file_map_ccss_track,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_ccss_track["stageName"]
  api_name = var.template_file_map_ccss_track["title"]
  api_description = var.template_file_map_ccss_track["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_ccss_track
}

#QA_LAUNCH2020_CHARGEMONITOR

module "TF_Module_API_CHARGEMONITOR" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_chargemonitor
  template_file_map=  merge(var.template_file_map_chargemonitor,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_chargemonitor["stageName"]
  api_name = var.template_file_map_chargemonitor["title"]
  api_description = var.template_file_map_chargemonitor["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_chargemonitor
}

#QA_LAUNCH2020_CIM

module "TF_Module_API_CIM" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_cim
  template_file_map=  merge(var.template_file_map_cim,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_cim["stageName"]
  api_name = var.template_file_map_cim["title"]
  api_description = var.template_file_map_cim["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_cim
}

#QA_LAUNCH2020_DATALAKE

module "TF_Module_API_DATALAKE" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_datalake
  template_file_map=  merge(var.template_file_map_datalake,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_datalake["stageName"]
  api_name = var.template_file_map_datalake["title"]
  api_description = var.template_file_map_datalake["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_datalake
}

#QA_LAUNCH2020_DEMAND_MANAGEMENT

module "TF_Module_API_DEMAND_MANAGEMENT" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_demand_management
  template_file_map=  merge(var.template_file_map_demand_management,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_demand_management["stageName"]
  api_name = var.template_file_map_demand_management["title"]
  api_description = var.template_file_map_demand_management["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_demand_management
}

#QA_LAUNCH2020_IQ

module "TF_Module_API_IQ" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_iq
  template_file_map=  merge(var.template_file_map_iq,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_iq["stageName"]
  api_name = var.template_file_map_iq["title"]
  api_description = var.template_file_map_iq["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_iq
}

#QA_LAUNCH2020_MISCEL_API

module "TF_Module_API_MISCEL_API" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_miscel
  template_file_map=  merge(var.template_file_map_miscel,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_miscel["stageName"]
  api_name = var.template_file_map_miscel["title"]
  api_description = var.template_file_map_miscel["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_miscel
}

#QA_LAUNCH2020_MULTIPLEXER

module "TF_Module_API_MULTIPLEXER" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_multiplexer
  template_file_map=  merge(var.template_file_map_multiplexer,
  {
  #providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_multiplexer["stageName"]
  api_name = var.template_file_map_multiplexer["title"]
  api_description = var.template_file_map_multiplexer["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_multiplexer
}

#QA_LAUNCH2020_MULTIPLEXER_CP

module "TF_Module_API_MULTIPLEXER_CP" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_multiplexer_cp
  template_file_map=  merge(var.template_file_map_multiplexer_cp,
  {
  #providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_multiplexer_cp["stageName"]
  api_name = var.template_file_map_multiplexer_cp["title"]
  api_description = var.template_file_map_multiplexer_cp["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_multiplexer_cp
}

#QA_LAUNCH2020_OCPP

module "TF_Module_API_OCPP" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_ocpp
  template_file_map=  merge(var.template_file_map_ocpp,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_ocpp["stageName"]
  api_name = var.template_file_map_ocpp["title"]
  api_description = var.template_file_map_ocpp["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_ocpp
}

#QA_LAUNCH2020_PDF_FILE_PROTERRA_AMS

module "TF_Module_API_PDF_FILE_PROTERRA_AMS" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_pdf_file_proterra_ams
  template_file_map=  merge(var.template_file_map_pdf_file_proterra_ams,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_pdf_file_proterra_ams["stageName"]
  api_name = var.template_file_map_pdf_file_proterra_ams["title"]
  api_description = var.template_file_map_pdf_file_proterra_ams["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_pdf_file_proterra_ams
}

#QA_LAUNCH2020_PROTERRA_AMS

module "TF_Module_API_PROTERRA_AMS" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_proterra_ams
  template_file_map=  merge(var.template_file_map_proterra_ams,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_proterra_ams["stageName"]
  api_name = var.template_file_map_proterra_ams["title"]
  api_description = var.template_file_map_proterra_ams["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_proterra_ams
}

#QA_LAUNCH2020_V2G_WEBSERVICES

module "TF_Module_API_V2G_WEBSERVICES" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_v2g_webservices
  template_file_map=  merge(var.template_file_map_v2g_webservices,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_v2g_webservices["stageName"]
  api_name = var.template_file_map_v2g_webservices["title"]
  api_description = var.template_file_map_v2g_webservices["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_v2g_webservices
}

#QA_LAUNCH2020_RUNS

module "TF_Module_API_RUNS" {
  source = "../../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_runs
  template_file_map=  merge(var.template_file_map_runs,
  {
  providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_YLu50ozIk"
  }
  ) 
  api_stage_name = var.template_file_map_runs["stageName"]
  api_name = var.template_file_map_runs["title"]
  api_description = var.template_file_map_runs["description"]
  exp_api_or_prod=  var.templates_exp_path 
  stage_variables=  var.local_stage_variables_runs
}

########################  SNS  #####################
#ocpppowersetsns 
module "TF_Module_SNS_OCPP_POWERSET_SNS" {
  source = "../../modules/TF_Module_SNS"

  sns_name=var.ocpppowersetsns_sns_name
  sns_subscription_arn_list =concat (var.ocpppowersetsns_sns_subscription_arn_list,  [
    {
      protocol = "sqs"
      arn = module.TF_Module_SQS_energymanagersqs.sqs_queue_arn
    } 
    ]
  )
}

#ocpp-messages
module "TF_Module_SNS_OCPP_MESSAGES" {
  source = "../../modules/TF_Module_SNS"

  sns_name=var.ocpp_messages_sns_name
  sns_subscription_arn_list = concat (var.ocpp_messages_sns_subscription_arn_list)

}

#v2g-ocpp-messages
module "TF_Module_SNS_V2G_OCPP_MESSAGES" {
  source = "../../modules/TF_Module_SNS"

  sns_name=var.v2g_ocpp_messages_sns_name
  sns_subscription_arn_list = concat (var.v2g_ocpp_messages_sns_subscription_arn_list)

}
#v2g-sns
module "TF_Module_SNS_V2G_SNS" {
  source = "../../modules/TF_Module_SNS"

  sns_name=var.v2g_sns_sns_name
  sns_subscription_arn_list = concat (var.v2g_sns_sns_subscription_arn_list)

}
#v2g-sns-reppa
module "TF_Module_SNS_V2G_SNS_REPPA" {
  source = "../../modules/TF_Module_SNS"

  sns_name=var.v2g_sns_reppa_sns_name
  sns_subscription_arn_list = concat (var.v2g_sns_reppa_sns_subscription_arn_list)

}




