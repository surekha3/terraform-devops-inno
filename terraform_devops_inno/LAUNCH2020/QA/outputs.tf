output "config_map_aws_auth" {
  value = module.TF_Module_EKS.config_map_aws_auth
}

output "kubeconfig" {
  value = module.TF_Module_EKS.kubeconfig
}
