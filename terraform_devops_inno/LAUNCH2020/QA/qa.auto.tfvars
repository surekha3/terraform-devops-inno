# Base.tfvars
aws_region              = "us-east-1"
aws_profile             = "proterra-exp"

################## VPC #################

vpc_cidr              = "172.30.16.0/20"
vpc_public_subnets    = ["172.30.16.0/23", "172.30.18.0/23", "172.30.20.0/23", "172.30.22.0/23"]
vpc_private_subnets   = ["172.30.24.0/23", "172.30.26.0/23", "172.30.28.0/23", "172.30.30.0/23"]
vpc_availabilityzones = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
subnet_additional_tags = {
  "Environment" = "Qa"
  "kubernetes.io/cluster/qa-launch2020-eks-cluster" = "shared"
}
vpc_project               = "launch2020"
vpc_environment           = "qa"     
vpc_endpoint_tag_name         = "LAUNCH2020-QA-EC2-DynamoDB"

##############  EKS ##############
cluster-name       = "qa-launch2020-eks-cluster"

#regular worker node
worker-nodes-data = {
cluster-node-name  = "launch2020-eks-node"
node-instance-type = "t2.medium"
autoscaling-desired-capacity = 0
autoscaling-max-size =0
autoscaling-min-size = 0
ec2_ssh_key = "qa-launch2020"
}

#node-group
launch2020-node-group-data= {
  create_launch_template = true 
  ami_id = "ami-091f6e311130bd848"
  node_group_ec2_ssh_key= "qa-launch2020"
  node_group_name = "qa-launch2020"
  node-group-autoscaling-desired-capacity = 4
  node-group-autoscaling-max-size = 6
  node-group-autoscaling-min-size = 4
  node-group-instance-types = "t2.large"
}
#mdc-node-group-data= {
#  create_launch_template = false 
#  ami_id = ""
#  node_group_ec2_ssh_key= "qa-launch2020"
#  node_group_name = "qa-mdc-launch2020"
#  node-group-autoscaling-desired-capacity = 1
#  node-group-autoscaling-max-size = 1
#  node-group-autoscaling-min-size = 1
#  node-group-instance-types = "t2.medium"
#}


#################### ECR #############
ecr_name = "qa-launch2020"


##########  Redis ###############
cluster_id                    = "redis-qa-launch2020-prod-rg"
node_type                     = "cache.t2.micro"
num_cache_nodes               = 1
parameter_group_name          = "default.redis5.0"
engine_version                = "5.0.6"
port_number                   = 6397 
cluster_security_group_name   ="redis-qa-launch2020-sg"
cluster_subnet_group          = "redis-qa-launch2020-parameter-group"
replication_group_id          = "redis-qa-launch2020-cluster"
number_cache_clusters         = 2

########### SQS ###################

#sqs_BusRealTimeDataStream_env_tag                    = 
sqs_BusRealTimeDataStream_name                        = "qa-launch2020-BusRealTimeDataStream"
sqs_BusRealTimeDataStream_visibility_timeout_seconds  = 30
sqs_BusRealTimeDataStream_message_retention_seconds   = 86400
sqs_BusRealTimeDataStream_max_message_size            = 256000
sqs_BusRealTimeDataStream_delay_seconds               = 0
sqs_BusRealTimeDataStream_receive_wait_time_seconds   = 0
sqs_BusRealTimeDataStream_fifo_queue                  = false
sqs_BusRealTimeDataStream_content_based_deduplication = false

######
#sqs_BusShadowUpdatesV2_env_tag                   = 
sqs_BusShadowUpdatesV2_name                       = "qa-launch2020-BusShadowUpdatesV2"
sqs_BusShadowUpdatesV2_visibility_timeout_seconds = 30
sqs_BusShadowUpdatesV2_message_retention_seconds  = 86400
sqs_BusShadowUpdatesV2_max_message_size           = 256000
sqs_BusShadowUpdatesV2_delay_seconds              = 0
sqs_BusShadowUpdatesV2_receive_wait_time_seconds  = 0
sqs_BusShadowUpdatesV2_fifo_queue                 = false
sqs_BusShadowUpdatesV2_content_based_deduplication = false

######
#sqs_LogstashLambdaTest_env_tag                   = 
sqs_LogstashLambdaTest_name                       = "qa-launch2020-LogstashLambdaTest"
sqs_LogstashLambdaTest_visibility_timeout_seconds = 30
sqs_LogstashLambdaTest_message_retention_seconds  = 345600
sqs_LogstashLambdaTest_max_message_size           = 256000
sqs_LogstashLambdaTest_delay_seconds              = 0
sqs_LogstashLambdaTest_receive_wait_time_seconds  = 0
sqs_LogstashLambdaTest_fifo_queue                 = false
sqs_LogstashLambdaTest_content_based_deduplication = false

######
#sqs_OCPPMessageQueue_env_tag                    = 
sqs_OCPPMessageQueue_name                        = "qa-launch2020-OCPP-MessageQueue"
sqs_OCPPMessageQueue_visibility_timeout_seconds  = 30
sqs_OCPPMessageQueue_message_retention_seconds   = 1209600
sqs_OCPPMessageQueue_max_message_size            = 256000
sqs_OCPPMessageQueue_delay_seconds               = 0
sqs_OCPPMessageQueue_receive_wait_time_seconds   = 0
sqs_OCPPMessageQueue_fifo_queue                  = false
sqs_OCPPMessageQueue_content_based_deduplication = false

######
#sqs_OCPPMessageQueueGV_env_tag                    = 
sqs_OCPPMessageQueueGV_name                        = "qa-launch2020-OCPP-MessageQueue-GV"
sqs_OCPPMessageQueueGV_visibility_timeout_seconds  = 30
sqs_OCPPMessageQueueGV_message_retention_seconds   = 1209600
sqs_OCPPMessageQueueGV_max_message_size            = 256000
sqs_OCPPMessageQueueGV_delay_seconds               = 0
sqs_OCPPMessageQueueGV_receive_wait_time_seconds   = 0
sqs_OCPPMessageQueueGV_fifo_queue                  = false
sqs_OCPPMessageQueueGV_content_based_deduplication = false

######
#sqs_OCPPFaults_env_tag                    = 
sqs_OCPPFaults_name                        = "qa-launch2020-OCPPFaults"
sqs_OCPPFaults_visibility_timeout_seconds  = 30
sqs_OCPPFaults_message_retention_seconds   = 345600
sqs_OCPPFaults_max_message_size            = 256000
sqs_OCPPFaults_delay_seconds               = 0
sqs_OCPPFaults_receive_wait_time_seconds   = 0
sqs_OCPPFaults_fifo_queue                  = false
sqs_OCPPFaults_content_based_deduplication = false

######
#sqs_OCPPHistogramJobQueue_env_tag                    = 
sqs_OCPPHistogramJobQueue_name                        = "qa-launch2020-OCPPHistogramJobQueue"
sqs_OCPPHistogramJobQueue_visibility_timeout_seconds  = 30
sqs_OCPPHistogramJobQueue_message_retention_seconds   = 345600
sqs_OCPPHistogramJobQueue_max_message_size            = 256000
sqs_OCPPHistogramJobQueue_delay_seconds               = 0
sqs_OCPPHistogramJobQueue_receive_wait_time_seconds   = 0
sqs_OCPPHistogramJobQueue_fifo_queue                  = false
sqs_OCPPHistogramJobQueue_content_based_deduplication = false

######
#sqs_OCPPHourlyJobQueue_env_tag                    = 
sqs_OCPPHourlyJobQueue_name                        = "qa-launch2020-OCPPHourlyJobQueue"
sqs_OCPPHourlyJobQueue_visibility_timeout_seconds  = 30
sqs_OCPPHourlyJobQueue_message_retention_seconds   = 345600
sqs_OCPPHourlyJobQueue_max_message_size            = 256000
sqs_OCPPHourlyJobQueue_delay_seconds               = 0
sqs_OCPPHourlyJobQueue_receive_wait_time_seconds   = 0
sqs_OCPPHourlyJobQueue_fifo_queue                  = false
sqs_OCPPHourlyJobQueue_content_based_deduplication = false

######
#sqs_OCPPSessionJobQueue_env_tag                    = 
sqs_OCPPSessionJobQueue_name                        = "qa-launch2020-OCPPSessionJobQueue"
sqs_OCPPSessionJobQueue_visibility_timeout_seconds  = 30
sqs_OCPPSessionJobQueue_message_retention_seconds   = 345600
sqs_OCPPSessionJobQueue_max_message_size            = 256000
sqs_OCPPSessionJobQueue_delay_seconds               = 0
sqs_OCPPSessionJobQueue_receive_wait_time_seconds   = 0
sqs_OCPPSessionJobQueue_fifo_queue                  = false
sqs_OCPPSessionJobQueue_content_based_deduplication = false

######
#sqs_ReindexRawDataQueue_env_tag                    = 
sqs_ReindexRawDataQueue_name                        = "qa-launch2020-ReindexRawDataQueue"
sqs_ReindexRawDataQueue_visibility_timeout_seconds  = 30
sqs_ReindexRawDataQueue_message_retention_seconds   = 345600
sqs_ReindexRawDataQueue_max_message_size            = 256000
sqs_ReindexRawDataQueue_delay_seconds               = 0
sqs_ReindexRawDataQueue_receive_wait_time_seconds   = 0
sqs_ReindexRawDataQueue_fifo_queue                  = false
sqs_ReindexRawDataQueue_content_based_deduplication = false

######
#sqs_RoadRunnerDataStream_env_tag                    = 
sqs_RoadRunnerDataStream_name                        = "qa-launch2020-RoadRunnerDataStream"
sqs_RoadRunnerDataStream_visibility_timeout_seconds  = 30
sqs_RoadRunnerDataStream_message_retention_seconds   = 1209600
sqs_RoadRunnerDataStream_max_message_size            = 256000
sqs_RoadRunnerDataStream_delay_seconds               = 0
sqs_RoadRunnerDataStream_receive_wait_time_seconds   = 0
sqs_RoadRunnerDataStream_fifo_queue                  = false
sqs_RoadRunnerDataStream_content_based_deduplication = false

######
#sqs_ToucanBackward_env_tag                    = 
sqs_ToucanBackward_name                        = "qa-launch2020-ToucanBackward"
sqs_ToucanBackward_visibility_timeout_seconds  = 30
sqs_ToucanBackward_message_retention_seconds   = 1209600
sqs_ToucanBackward_max_message_size            = 256000
sqs_ToucanBackward_delay_seconds               = 0
sqs_ToucanBackward_receive_wait_time_seconds   = 0
sqs_ToucanBackward_fifo_queue                  = false
sqs_ToucanBackward_content_based_deduplication = false

######
#sqs_ToucanRawDataStream_env_tag                    = 
sqs_ToucanRawDataStream_name                        = "qa-launch2020-ToucanRawDataStream"
sqs_ToucanRawDataStream_visibility_timeout_seconds  = 30
sqs_ToucanRawDataStream_message_retention_seconds   = 604800
sqs_ToucanRawDataStream_max_message_size            = 256000
sqs_ToucanRawDataStream_delay_seconds               = 0
sqs_ToucanRawDataStream_receive_wait_time_seconds   = 0
sqs_ToucanRawDataStream_fifo_queue                  = false
sqs_ToucanRawDataStream_content_based_deduplication = false

######
#sqs_chargereventfaults_env_tag                    = 
sqs_chargereventfaults_name                        = "qa-launch2020-charger-event-faults"
sqs_chargereventfaults_visibility_timeout_seconds  = 30
sqs_chargereventfaults_message_retention_seconds   = 345600
sqs_chargereventfaults_max_message_size            = 256000
sqs_chargereventfaults_delay_seconds               = 0
sqs_chargereventfaults_receive_wait_time_seconds   = 0
sqs_chargereventfaults_fifo_queue                  = false
sqs_chargereventfaults_content_based_deduplication = false

######
#sqs_ChargerEventFaultsAggregated_env_tag                    = 
sqs_ChargerEventFaultsAggregated_name                        = "qa-launch2020-charger-event-faults_aggregated"
sqs_ChargerEventFaultsAggregated_visibility_timeout_seconds  = 30
sqs_ChargerEventFaultsAggregated_message_retention_seconds   = 345600
sqs_ChargerEventFaultsAggregated_max_message_size            = 256000
sqs_ChargerEventFaultsAggregated_delay_seconds               = 0
sqs_ChargerEventFaultsAggregated_receive_wait_time_seconds   = 0
sqs_ChargerEventFaultsAggregated_fifo_queue                  = false
sqs_ChargerEventFaultsAggregated_content_based_deduplication = false

######
#sqs_EnergyManagerOcppQueueProd_env_tag                    = 
sqs_EnergyManagerOcppQueueProd_name                        = "qa-launch2020-energy_manager_ocpp_queue_prod.fifo"
sqs_EnergyManagerOcppQueueProd_visibility_timeout_seconds  = 30
sqs_EnergyManagerOcppQueueProd_message_retention_seconds   = 1209600
sqs_EnergyManagerOcppQueueProd_max_message_size            = 256000
sqs_EnergyManagerOcppQueueProd_delay_seconds               = 0
sqs_EnergyManagerOcppQueueProd_receive_wait_time_seconds   = 0
sqs_EnergyManagerOcppQueueProd_fifo_queue                  = true
sqs_EnergyManagerOcppQueueProd_content_based_deduplication = true

######
#sqs_EnergyManagerOcppQueueTest_env_tag                    = 
sqs_EnergyManagerOcppQueueTest_name                        = "qa-launch2020-energy_manager_ocpp_queue_test.fifo"
sqs_EnergyManagerOcppQueueTest_visibility_timeout_seconds  = 30
sqs_EnergyManagerOcppQueueTest_message_retention_seconds   = 345600
sqs_EnergyManagerOcppQueueTest_max_message_size            = 256000
sqs_EnergyManagerOcppQueueTest_delay_seconds               = 0
sqs_EnergyManagerOcppQueueTest_receive_wait_time_seconds   = 0
sqs_EnergyManagerOcppQueueTest_fifo_queue                  = true
sqs_EnergyManagerOcppQueueTest_content_based_deduplication = true

######
#sqs_energymanagersqs_env_tag                    = 
sqs_energymanagersqs_name                        = "qa-launch2020-energymanagersqs"
sqs_energymanagersqs_visibility_timeout_seconds  = 30
sqs_energymanagersqs_message_retention_seconds   = 345600
sqs_energymanagersqs_max_message_size            = 256000
sqs_energymanagersqs_delay_seconds               = 0
sqs_energymanagersqs_receive_wait_time_seconds   = 0
sqs_energymanagersqs_fifo_queue                  = false
sqs_energymanagersqs_content_based_deduplication = false

######
#sqs_ExpOcppChargerStatus_env_tag                    = 
sqs_ExpOcppChargerStatus_name                        = "qa-launch2020-exp-ocpp-charger-status.fifo"
sqs_ExpOcppChargerStatus_visibility_timeout_seconds  = 30
sqs_ExpOcppChargerStatus_message_retention_seconds   = 345600
sqs_ExpOcppChargerStatus_max_message_size            = 256000
sqs_ExpOcppChargerStatus_delay_seconds               = 0
sqs_ExpOcppChargerStatus_receive_wait_time_seconds   = 0
sqs_ExpOcppChargerStatus_fifo_queue                  = true
sqs_ExpOcppChargerStatus_content_based_deduplication = true

######
#sqs_ExpOcppMessages_env_tag                    = 
sqs_ExpOcppMessages_name                        = "qa-launch2020-exp-ocpp-messages.fifo"
sqs_ExpOcppMessages_visibility_timeout_seconds  = 30
sqs_ExpOcppMessages_message_retention_seconds   = 345600
sqs_ExpOcppMessages_max_message_size            = 256000
sqs_ExpOcppMessages_delay_seconds               = 0
sqs_ExpOcppMessages_receive_wait_time_seconds   = 0
sqs_ExpOcppMessages_fifo_queue                  = true
sqs_ExpOcppMessages_content_based_deduplication = true

######
#sqs_FromDevIngestor_env_tag                    = 
sqs_FromDevIngestor_name                        = "qa-launch2020-from-dev-ingestor"
sqs_FromDevIngestor_visibility_timeout_seconds  = 30
sqs_FromDevIngestor_message_retention_seconds   = 345600
sqs_FromDevIngestor_max_message_size            = 256000
sqs_FromDevIngestor_delay_seconds               = 0
sqs_FromDevIngestor_receive_wait_time_seconds   = 0
sqs_FromDevIngestor_fifo_queue                  = false
sqs_FromDevIngestor_content_based_deduplication = false

######
#sqs_FromOcppProdIngestorm_env_tag                    = 
sqs_FromOcppProdIngestorm_name                        = "qa-launch2020-from-ocpp-prod-ingestor"
sqs_FromOcppProdIngestorm_visibility_timeout_seconds  = 30
sqs_FromOcppProdIngestorm_message_retention_seconds   = 345600
sqs_FromOcppProdIngestorm_max_message_size            = 256000
sqs_FromOcppProdIngestorm_delay_seconds               = 0
sqs_FromOcppProdIngestorm_receive_wait_time_seconds   = 0
sqs_FromOcppProdIngestorm_fifo_queue                  = false
sqs_FromOcppProdIngestorm_content_based_deduplication = false

######
#sqs_OcppWriteQueueAssetcustomerdetails_env_tag                    = 
sqs_OcppWriteQueueAssetcustomerdetails_name                        = "qa-launch2020-ocpp-write-queue-assetcustomerdetails"
sqs_OcppWriteQueueAssetcustomerdetails_visibility_timeout_seconds  = 30
sqs_OcppWriteQueueAssetcustomerdetails_message_retention_seconds   = 345600
sqs_OcppWriteQueueAssetcustomerdetails_max_message_size            = 256000
sqs_OcppWriteQueueAssetcustomerdetails_delay_seconds               = 0
sqs_OcppWriteQueueAssetcustomerdetails_receive_wait_time_seconds   = 0
sqs_OcppWriteQueueAssetcustomerdetails_fifo_queue                  = false
sqs_OcppWriteQueueAssetcustomerdetails_content_based_deduplication = false

######
#sqs_OcppWriteQueueCategories_env_tag                    = 
sqs_OcppWriteQueueCategories_name                        = "qa-launch2020-ocpp-write-queue-categories"
sqs_OcppWriteQueueCategories_visibility_timeout_seconds  = 30
sqs_OcppWriteQueueCategories_message_retention_seconds   = 345600
sqs_OcppWriteQueueCategories_max_message_size            = 256000
sqs_OcppWriteQueueCategories_delay_seconds               = 0
sqs_OcppWriteQueueCategories_receive_wait_time_seconds   = 0
sqs_OcppWriteQueueCategories_fifo_queue                  = false
sqs_OcppWriteQueueCategories_content_based_deduplication = false

######
#sqs_expChargerEventFaults1_env_tag                    = 
sqs_expChargerEventFaults1_name                        = "qa-launch2020-exp-charger-event-faults1"
sqs_expChargerEventFaults1_visibility_timeout_seconds  = 30
sqs_expChargerEventFaults1_message_retention_seconds   = 345600
sqs_expChargerEventFaults1_max_message_size            = 256000
sqs_expChargerEventFaults1_delay_seconds               = 0
sqs_expChargerEventFaults1_receive_wait_time_seconds   = 0
sqs_expChargerEventFaults1_fifo_queue                  = false
sqs_expChargerEventFaults1_content_based_deduplication = false

######
#sqs_expChargerEventFaultsAggregated_env_tag                    = 
sqs_expChargerEventFaultsAggregated_name                        = "qa-launch2020-exp-charger-event-faults_aggregated"
sqs_expChargerEventFaultsAggregated_visibility_timeout_seconds  = 30
sqs_expChargerEventFaultsAggregated_message_retention_seconds   = 345600
sqs_expChargerEventFaultsAggregated_max_message_size            = 256000
sqs_expChargerEventFaultsAggregated_delay_seconds               = 0
sqs_expChargerEventFaultsAggregated_receive_wait_time_seconds   = 0
sqs_expChargerEventFaultsAggregated_fifo_queue                  = false
sqs_expChargerEventFaultsAggregated_content_based_deduplication  = false

######
#sqs_expChargerEvent1_env_tag                    = 
sqs_expChargerEvent1_name                        = "qa-launch2020-exp-charger-events1"
sqs_expChargerEvent1_visibility_timeout_seconds  = 30
sqs_expChargerEvent1_message_retention_seconds   = 345600
sqs_expChargerEvent1_max_message_size            = 256000
sqs_expChargerEvent1_delay_seconds               = 0
sqs_expChargerEvent1_receive_wait_time_seconds   = 0
sqs_expChargerEvent1_fifo_queue                  = false
sqs_expChargerEvent1_content_based_deduplication = false

######
#sqs_NonAbbOCPPSubmitMessageRequestQueue_env_tag                    = 
sqs_NonAbbOCPPSubmitMessageRequestQueue_name                        = "qa-launch2020-non-abb-OCPP-SubmitMessageRequestQueue"
sqs_NonAbbOCPPSubmitMessageRequestQueue_visibility_timeout_seconds  = 30
sqs_NonAbbOCPPSubmitMessageRequestQueue_message_retention_seconds   = 345600
sqs_NonAbbOCPPSubmitMessageRequestQueue_max_message_size            = 256000
sqs_NonAbbOCPPSubmitMessageRequestQueue_delay_seconds               = 0
sqs_NonAbbOCPPSubmitMessageRequestQueue_receive_wait_time_seconds   = 0
sqs_NonAbbOCPPSubmitMessageRequestQueue_fifo_queue                  = false
sqs_NonAbbOCPPSubmitMessageRequestQueue_content_based_deduplication = false

######
#sqs_NonAbbOCPPSubmitMessageResponseQueue_env_tag                    = 
sqs_NonAbbOCPPSubmitMessageResponseQueue_name                        = "qa-launch2020-non-abb-OCPP-SubmitMessageResponseQueue"
sqs_NonAbbOCPPSubmitMessageResponseQueue_visibility_timeout_seconds  = 30
sqs_NonAbbOCPPSubmitMessageResponseQueue_message_retention_seconds   = 345600
sqs_NonAbbOCPPSubmitMessageResponseQueue_max_message_size            = 256000
sqs_NonAbbOCPPSubmitMessageResponseQueue_delay_seconds               = 0
sqs_NonAbbOCPPSubmitMessageResponseQueue_receive_wait_time_seconds   = 0
sqs_NonAbbOCPPSubmitMessageResponseQueue_fifo_queue                  = false
sqs_NonAbbOCPPSubmitMessageResponseQueue_content_based_deduplication = false

######
#sqs_NonAbbOCPPChargerEventFaults1_env_tag                    = 
sqs_NonAbbOCPPChargerEventFaults1_name                        = "qa-launch2020-non-abb-exp-charger-event-faults1"
sqs_NonAbbOCPPChargerEventFaults1_visibility_timeout_seconds  = 30
sqs_NonAbbOCPPChargerEventFaults1_message_retention_seconds   = 345600
sqs_NonAbbOCPPChargerEventFaults1_max_message_size            = 256000
sqs_NonAbbOCPPChargerEventFaults1_delay_seconds               = 0
sqs_NonAbbOCPPChargerEventFaults1_receive_wait_time_seconds   = 0
sqs_NonAbbOCPPChargerEventFaults1_fifo_queue                  = false
sqs_NonAbbOCPPChargerEventFaults1_content_based_deduplication = false

######
#sqs_NonAbbOCPPChargerEventFaultsaggregated_env_tag                    = 
sqs_NonAbbOCPPChargerEventFaultsaggregated_name                        = "qa-launch2020-non-abb-exp-charger-event-faults_aggregated"
sqs_NonAbbOCPPChargerEventFaultsaggregated_visibility_timeout_seconds  = 30
sqs_NonAbbOCPPChargerEventFaultsaggregated_message_retention_seconds   = 345600
sqs_NonAbbOCPPChargerEventFaultsaggregated_max_message_size            = 256000
sqs_NonAbbOCPPChargerEventFaultsaggregated_delay_seconds               = 0
sqs_NonAbbOCPPChargerEventFaultsaggregated_receive_wait_time_seconds   = 0
sqs_NonAbbOCPPChargerEventFaultsaggregated_fifo_queue                  = false
sqs_NonAbbOCPPChargerEventFaultsaggregated_content_based_deduplication = false

######
#sqs_NonAbbOCPPChargerEvents1_env_tag                    = 
sqs_NonAbbOCPPChargerEvents1_name                        = "qa-launch2020-non-abb-exp-charger-events1"
sqs_NonAbbOCPPChargerEvents1_visibility_timeout_seconds  = 30
sqs_NonAbbOCPPChargerEvents1_message_retention_seconds   = 345600
sqs_NonAbbOCPPChargerEvents1_max_message_size            = 256000
sqs_NonAbbOCPPChargerEvents1_delay_seconds               = 0
sqs_NonAbbOCPPChargerEvents1_receive_wait_time_seconds   = 0
sqs_NonAbbOCPPChargerEvents1_fifo_queue                  = false
sqs_NonAbbOCPPChargerEvents1_content_based_deduplication = false

######
#sqs_NonAbbOCPPMessages_env_tag                    = 
sqs_NonAbbOCPPMessages_name                        = "qa-launch2020-non-abb-exp-ocpp-messages.fifo"
sqs_NonAbbOCPPMessages_visibility_timeout_seconds  = 30
sqs_NonAbbOCPPMessages_message_retention_seconds   = 345600
sqs_NonAbbOCPPMessages_max_message_size            = 256000
sqs_NonAbbOCPPMessages_delay_seconds               = 0
sqs_NonAbbOCPPMessages_receive_wait_time_seconds   = 0
sqs_NonAbbOCPPMessages_fifo_queue                  = true
sqs_NonAbbOCPPMessages_content_based_deduplication = true

######
#sqs_v2gChargerEventFaults1_env_tag                    = 
sqs_v2gChargerEventFaults1_name                        = "qa-launch2020-v2g-exp-charger-event-faults1"
sqs_v2gChargerEventFaults1_visibility_timeout_seconds  = 30
sqs_v2gChargerEventFaults1_message_retention_seconds   = 345600
sqs_v2gChargerEventFaults1_max_message_size            = 256000
sqs_v2gChargerEventFaults1_delay_seconds               = 0
sqs_v2gChargerEventFaults1_receive_wait_time_seconds   = 0
sqs_v2gChargerEventFaults1_fifo_queue                  = false
sqs_v2gChargerEventFaults1_content_based_deduplication = false

######
#sqs_v2gChargerEventFaultsAggregated_env_tag                    = 
sqs_v2gChargerEventFaultsAggregated_name                        = "qa-launch2020-v2g-exp-charger-event-faults_aggregated"
sqs_v2gChargerEventFaultsAggregated_visibility_timeout_seconds  = 30
sqs_v2gChargerEventFaultsAggregated_message_retention_seconds   = 345600
sqs_v2gChargerEventFaultsAggregated_max_message_size            = 256000
sqs_v2gChargerEventFaultsAggregated_delay_seconds               = 0
sqs_v2gChargerEventFaultsAggregated_receive_wait_time_seconds   = 0
sqs_v2gChargerEventFaultsAggregated_fifo_queue                  = false
sqs_v2gChargerEventFaultsAggregated_content_based_deduplication = false

######
#sqs_v2gChargerEvents1_env_tag                    = 
sqs_v2gChargerEvents1_name                        = "qa-launch2020-v2g-exp-charger-events1"
sqs_v2gChargerEvents1_visibility_timeout_seconds  = 30
sqs_v2gChargerEvents1_message_retention_seconds   = 345600
sqs_v2gChargerEvents1_max_message_size            = 256000
sqs_v2gChargerEvents1_delay_seconds               = 0
sqs_v2gChargerEvents1_receive_wait_time_seconds   = 0
sqs_v2gChargerEvents1_fifo_queue                  = false
sqs_v2gChargerEvents1_content_based_deduplication = false

######
#sqs_v2gDnp3Reporting_env_tag                     = 
sqs_v2gDnp3Reporting_name                        = "qa-launch2020-v2g-exp-dnp3-reporting.fifo"
sqs_v2gDnp3Reporting_visibility_timeout_seconds  = 30
sqs_v2gDnp3Reporting_message_retention_seconds   = 345600
sqs_v2gDnp3Reporting_max_message_size            = 256000
sqs_v2gDnp3Reporting_delay_seconds               = 0
sqs_v2gDnp3Reporting_receive_wait_time_seconds   = 0
sqs_v2gDnp3Reporting_fifo_queue                  = true
sqs_v2gDnp3Reporting_content_based_deduplication = true

######
#sqs_v2gLib_env_tag                    = 
sqs_v2gLib_name                        = "qa-launch2020-v2g-exp-lib.fifo"
sqs_v2gLib_visibility_timeout_seconds  = 30
sqs_v2gLib_message_retention_seconds   = 345600
sqs_v2gLib_max_message_size            = 256000
sqs_v2gLib_delay_seconds               = 0
sqs_v2gLib_receive_wait_time_seconds   = 0
sqs_v2gLib_fifo_queue                  = true
sqs_v2gLib_content_based_deduplication = true

######
#sqs_v2gOCPPChargerStatus_env_tag                    = 
sqs_v2gOCPPChargerStatus_name                        = "qa-launch2020-v2g-exp-ocpp-charger-status.fifo"
sqs_v2gOCPPChargerStatus_visibility_timeout_seconds  = 30
sqs_v2gOCPPChargerStatus_message_retention_seconds   = 345600
sqs_v2gOCPPChargerStatus_max_message_size            = 256000
sqs_v2gOCPPChargerStatus_delay_seconds               = 0
sqs_v2gOCPPChargerStatus_receive_wait_time_seconds   = 0
sqs_v2gOCPPChargerStatus_fifo_queue                  = true
sqs_v2gOCPPChargerStatus_content_based_deduplication = true

######
#sqs_v2gOCPPMessages_env_tag                    = 
sqs_v2gOCPPMessages_name                        = "qa-launch2020-v2g-exp-ocpp-messages.fifo"
sqs_v2gOCPPMessages_visibility_timeout_seconds  = 30
sqs_v2gOCPPMessages_message_retention_seconds   = 345600
sqs_v2gOCPPMessages_max_message_size            = 256000
sqs_v2gOCPPMessages_delay_seconds               = 0
sqs_v2gOCPPMessages_receive_wait_time_seconds   = 0
sqs_v2gOCPPMessages_fifo_queue                  = true
sqs_v2gOCPPMessages_content_based_deduplication = true

######
#sqs_v2gDevIngestor_env_tag                    = 
sqs_v2gDevIngestor_name                        = "qa-launch2020-v2g-from-dev-ingestor"
sqs_v2gDevIngestor_visibility_timeout_seconds  = 30
sqs_v2gDevIngestor_message_retention_seconds   = 345600
sqs_v2gDevIngestor_max_message_size            = 256000
sqs_v2gDevIngestor_delay_seconds               = 0
sqs_v2gDevIngestor_receive_wait_time_seconds   = 0
sqs_v2gDevIngestor_fifo_queue                  = false
sqs_v2gDevIngestor_content_based_deduplication = false

######
#sqs_v2gtest13_env_tag                    = 
sqs_v2gtest13_name                        = "qa-launch2020-v2g-queue-test13.fifo"
sqs_v2gtest13_visibility_timeout_seconds  = 30
sqs_v2gtest13_message_retention_seconds   = 345600
sqs_v2gtest13_max_message_size            = 256000
sqs_v2gtest13_delay_seconds               = 0
sqs_v2gtest13_receive_wait_time_seconds   = 0
sqs_v2gtest13_fifo_queue                  = true
sqs_v2gtest13_content_based_deduplication = true

######
#sqs_v2gtest16_env_tag                    = 
sqs_v2gtest16_name                        = "qa-launch2020-v2g-queue-test16.fifo"
sqs_v2gtest16_visibility_timeout_seconds  = 30
sqs_v2gtest16_message_retention_seconds   = 345600
sqs_v2gtest16_max_message_size            = 256000
sqs_v2gtest16_delay_seconds               = 0
sqs_v2gtest16_receive_wait_time_seconds   = 0
sqs_v2gtest16_fifo_queue                  = true
sqs_v2gtest16_content_based_deduplication = true

######
#sqs_v2gtest58_env_tag                    = 
sqs_v2gtest58_name                        = "qa-launch2020-v2g-queue-test58.fifo"
sqs_v2gtest58_visibility_timeout_seconds  = 30
sqs_v2gtest58_message_retention_seconds   = 345600
sqs_v2gtest58_max_message_size            = 256000
sqs_v2gtest58_delay_seconds               = 0
sqs_v2gtest58_receive_wait_time_seconds   = 0
sqs_v2gtest58_fifo_queue                  = true
sqs_v2gtest58_content_based_deduplication = true

######
#sqs_v2gTestReppaOutbound_env_tag                    = 
sqs_v2gTestReppaOutbound_name                        = "qa-launch2020-v2g-test-reppa-outbound.fifo"
sqs_v2gTestReppaOutbound_visibility_timeout_seconds  = 30
sqs_v2gTestReppaOutbound_message_retention_seconds   = 345600
sqs_v2gTestReppaOutbound_max_message_size            = 256000
sqs_v2gTestReppaOutbound_delay_seconds               = 0
sqs_v2gTestReppaOutbound_receive_wait_time_seconds   = 0
sqs_v2gTestReppaOutbound_fifo_queue                  = true
sqs_v2gTestReppaOutbound_content_based_deduplication = true

######
#sqs_v2gTestReppa_env_tag                    = 
sqs_v2gTestReppa_name                        = "qa-launch2020-v2g-test-reppa.fifo"
sqs_v2gTestReppa_visibility_timeout_seconds  = 30
sqs_v2gTestReppa_message_retention_seconds   = 345600
sqs_v2gTestReppa_max_message_size            = 256000
sqs_v2gTestReppa_delay_seconds               = 0
sqs_v2gTestReppa_receive_wait_time_seconds   = 0
sqs_v2gTestReppa_fifo_queue                  = true
sqs_v2gTestReppa_content_based_deduplication = true

######
#sqs_PCANFaultEvents_env_tag                    = 
sqs_PCANFaultEvents_name                        = "qa-launch2020-PCANFaultEvents"
sqs_PCANFaultEvents_visibility_timeout_seconds  = 30
sqs_PCANFaultEvents_message_retention_seconds   = 345600
sqs_PCANFaultEvents_max_message_size            = 256000
sqs_PCANFaultEvents_delay_seconds               = 0
sqs_PCANFaultEvents_receive_wait_time_seconds   = 0
sqs_PCANFaultEvents_fifo_queue                  = false
sqs_PCANFaultEvents_content_based_deduplication = false

###### qa-launch2020-PCANServiceOperation
#sqs_PCANServiceOperation_env_tag                    = 
sqs_PCANServiceOperation_name                        = "qa-launch2020-PCANServiceOperation"
sqs_PCANServiceOperation_visibility_timeout_seconds  = 30
sqs_PCANServiceOperation_message_retention_seconds   = 345600
sqs_PCANServiceOperation_max_message_size            = 256000
sqs_PCANServiceOperation_delay_seconds               = 0
sqs_PCANServiceOperation_receive_wait_time_seconds   = 0
sqs_PCANServiceOperation_fifo_queue                  = false
sqs_PCANServiceOperation_content_based_deduplication = false


######
#sqs_APEX_MDC_TO_OTA_env_tag                    = 
sqs_APEX_MDC_TO_OTA_name                        = "qa-launch2020-APEX_MDC_TO_OTA.fifo"
sqs_APEX_MDC_TO_OTA_visibility_timeout_seconds  = 30
sqs_APEX_MDC_TO_OTA_message_retention_seconds   = 345600
sqs_APEX_MDC_TO_OTA_max_message_size            = 256000
sqs_APEX_MDC_TO_OTA_delay_seconds               = 0
sqs_APEX_MDC_TO_OTA_receive_wait_time_seconds   = 0
sqs_APEX_MDC_TO_OTA_fifo_queue                  = true
sqs_APEX_MDC_TO_OTA_content_based_deduplication = false

##############  S3  ##############

s3_ccssEcms_bucket_name               = "qa-launch2020-ccss-ecms.connected.proterra.com"
s3_ccssEcms_bucket_policy_file_name   = "files/s3/s3_ccssEcms_bucket_policy.json"

s3_ams_bucket_name               = "qa-launch2020-ams.proterra.com"
s3_ams_bucket_policy_file_name   = "files/s3/s3_ams_bucket_policy.json"

s3_apex_bucket_name               = "qa-launch2020-apex.proterra.com"
s3_apex_bucket_policy_file_name   = "files/s3/s3_apex_bucket_policy.json"

s3_backwardToucan_bucket_name               = "qa-launch2020-backward.toucan.proterra.com"
s3_backwardToucan_bucket_policy_file_name   = "files/s3/s3_backwardToucan_bucket_policy.json"

s3_commissioning_bucket_name               = "qa-launch2020-commissioning.proterra.com"
s3_commissioning_bucket_policy_file_name   = "files/s3/s3_commissioning_bucket_policy.json"

s3_dataConnected_bucket_name               = "qa-launch2020-data.connected.proterra.com"
s3_dataConnected_bucket_policy_file_name   = "files/s3/s3_dataConnected_bucket_policy.json"

s3_reports_bucket_name               = "qa-launch2020-reports.proterra.com"
s3_reports_bucket_policy_file_name   = "files/s3/s3_reports_bucket_policy.json"

s3_toucanData_bucket_name               = "qa-launch2020-toucan-data.proterra.com"
s3_toucanData_bucket_policy_file_name   = "files/s3/s3_toucanData_bucket_policy.json"

s3_toucanDataTest_bucket_name               = "qa-launch2020-toucan-data.proterra.com-test"
s3_toucanDataTest_bucket_policy_file_name   = "files/s3/s3_toucanDataTest_bucket_policy.json"

s3_toucan_bucket_name               = "qa-launch2020-toucan.proterra.com"
s3_toucan_bucket_policy_file_name   = "files/s3/s3_toucan_bucket_policy.json"

s3_multiplexer_bucket_name               = "qa-launch2020-multiplexer-cp.apex.proterra.com"
s3_multiplexer_bucket_policy_file_name   = "files/s3/s3_multiplexer_bucket_policy.json"

s3_v2g_apex_bucket_name               = "qa-launch2020-v2g-apex.proterra.com"
s3_v2g_apex_bucket_policy_file_name   = "files/s3/s3_v2g_apex_bucket_policy.json"

#################  Cloud Front ###############

acm_certificate_arn="arn:aws:acm:us-east-1:493000495847:certificate/4d0492b7-b063-4ce1-b231-c87286e66e2b"

# APEX 
#cf_apex_origin_domain_name =  "qa-launch2020-apex.proterra.com.s3.amazonaws.com"
#cf_apex_origin_id =  "S3-qa-launch2020-apex.proterra.com"
cf_apex_aliases = ["qa-launch2020.proterra.com"]

# EOL commissioning 
cf_eol_aliases = ["qa-launch2020-eol.proterra.com"]


##############  cognito ###############

ams_cognito_app_client_name      = "exp_proterra_ams_app_client_launch2020_qa"
ams_cognito_user_pool_name       = "exp_proterra_ams_cognito_pool_launch2020_qa"
ams_cognito_email_verif_subject  = "APEX verification code"
ams_cognito_email_verif_message  = "APEX verification code is {####}"
ams_cognito_unused_account_days  = 30
ams_cognito_email_subject        = "Welcome to Proterra APEX"
ams_cognito_sms_verif_message    = "Your authentication code is {####}. "
ams_callback_urls                =   ["https://exp-qa-launch2020.proterra.com"]
ams_logout_urls                  =   ["https://exp-qa-launch2020.proterra.com"]
ams_allowed_oauth_scopes         =   [ "email","openid","profile" ]
ams_explicit_auth_flows          =   [ "ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_SRP_AUTH"]
ams_supported_identity_providers =   ["COGNITO"]

#schemas = []
number_schemas = [
  {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "createdTime"
    required                 = false

    number_attribute_constraints = {
       # min_value = 2
       # max_value = 6
      }
 
   },
 {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "internalUser"
    required                 = false

    number_attribute_constraints = {
       # min_value = 2
       # max_value = 6
      }
   },
 {
    attribute_data_type      = "Number"
    developer_only_attribute = false
    mutable                  = true
    name                     = "lastModifiedTime"
    required                 = false

	number_attribute_constraints  ={
       # min_value = 2
       # max_value = 6
      }
	  
   }
   ]


string_schemas = [
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "department"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
   {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "firstName"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },

 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "lastName"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
{
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "middleName"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "phone"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "roleInformation"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userId"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userName"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userType"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "userRole"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressAbbreviation"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
{
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressId"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressLine1"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "addressLine2"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "assignedRole"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
	
   },
   {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "city"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "country"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "latitude"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "longitude"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "zipCode"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "tenantName"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "tenantId"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "state"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "zoneinfo"
    required                 = true

    string_attribute_constraints =  {
      min_length = 0
      max_length = 2048
    }
   },
 {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "email"
    required                 = true

    string_attribute_constraints =  {
      min_length = 0
      max_length = 2048
    }
   },
{
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "fedUserRole"
    required                 = false

    string_attribute_constraints =  {
      min_length = 1
      max_length = 256
    }  
   }

]
############## Lambda ###############

# For RoadRunnerDataProcessor
enable_RoadRunnerDataProcessor_role = true
RoadRunnerDataProcessor_s3_bucket = "launch2020-foundation"
RoadRunnerDataProcessor_s3_key = "qa-lambda/GettestChargerStatus.zip"
lambda_RoadRunnerDataProcessor_iam_role_name    = "RoadRunnerDataProcessor-role"
lambda_RoadRunnerDataProcessor_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_RoadRunnerDataProcessor_iam_role_policy  = "files/iam/iam-policy-RoadRunnerDataProcessor.json"
RoadRunnerDataProcessor_path = "/"
RoadRunnerDataProcessor_policy_name = "RoadRunnerDataProcessor-policy"
lambda_RoadRunnerDataProcessor_function_name    = "qa-launch2020-RoadRunnerDataProcessor"
lambda_RoadRunnerDataProcessor_timeout	       = 300
lambda_RoadRunnerDataProcessor_mem_size	       = 256
lambda_RoadRunnerDataProcessor_handler	        = "lambda_function.lambda_handler"
lambda_RoadRunnerDataProcessor_runtime          = "python3.7"
RoadRunnerDataProcessor_statement_id  =  ["RoadRunnerDataProcessor1", "RoadRunnerDataProcessor2"]
RoadRunnerDataProcessor_action        =  "lambda:InvokeFunction"
RoadRunnerDataProcessor_principal     = ["s3.amazonaws.com", "s3.amazonaws.com"]
RoadRunnerDataProcessor_source_arn = ["arn:aws:s3:::qa-launch2020-toucan-data.proterra.com", "arn:aws:s3:::qa-launch2020-toucan-data.proterra.com-test"]

# For DecompressLambda
enable_DecompressLambda_role = true
DecompressLambda_s3_bucket = "launch2020-foundation"
DecompressLambda_s3_key = "qa-lambda/GettestChargerStatus.zip"
lambda_DecompressLambda_iam_role_name    = "DecompressLambda-role"
lambda_DecompressLambda_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_DecompressLambda_iam_role_policy  = "files/iam/iam-policy-DecompressLambda.json"
DecompressLambda_path = "/"
DecompressLambda_policy_name = "DecompressLambda-policy"
lambda_DecompressLambda_function_name    = "qa-launch2020-DecompressLambda"
lambda_DecompressLambda_timeout	       = 30
lambda_DecompressLambda_mem_size	       = 256
lambda_DecompressLambda_handler	        = "lambda_function.lambda_handler"
lambda_DecompressLambda_runtime          = "python3.6"
DecompressLambda_statement_id  =  []
DecompressLambda_action        =  ""
DecompressLambda_principal     = []
DecompressLambda_source_arn = []

# For BackwardToucanProcessor
enable_BackwardToucanProcessor_role = true
BackwardToucanProcessor_s3_bucket = "launch2020-foundation"
BackwardToucanProcessor_s3_key = "qa-lambda/GettestChargerStatus.zip"
lambda_BackwardToucanProcessor_iam_role_name    = "BackwardToucanProcessor-role"
lambda_BackwardToucanProcessor_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_BackwardToucanProcessor_iam_role_policy  = "files/iam/iam-policy-BackwardToucanProcessor.json"
BackwardToucanProcessor_path = "/"
BackwardToucanProcessor_policy_name = "BackwardToucanProcessor-policy"
lambda_BackwardToucanProcessor_function_name    = "qa-launch2020-BackwardToucanProcessor"
lambda_BackwardToucanProcessor_timeout	       = 30
lambda_BackwardToucanProcessor_mem_size	       = 256
lambda_BackwardToucanProcessor_handler	        = "lambda_function.lambda_handler"
lambda_BackwardToucanProcessor_runtime          = "python3.6"
BackwardToucanProcessor_statement_id  =  []
BackwardToucanProcessor_action        =  ""
BackwardToucanProcessor_principal     = []
BackwardToucanProcessor_source_arn = []

# For ToucanRawProcessor
enable_ToucanRawProcessor_role = true
ToucanRawProcessor_s3_bucket = "launch2020-foundation"
ToucanRawProcessor_s3_key = "qa-lambda/GettestChargerStatus.zip"
lambda_ToucanRawProcessor_iam_role_name    = "ToucanRawProcessor-role"
lambda_ToucanRawProcessor_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_ToucanRawProcessor_iam_role_policy  = "files/iam/iam-policy-ToucanRawProcessor.json"
ToucanRawProcessor_path = "/"
ToucanRawProcessor_policy_name = "ToucanRawProcessor-policy"
lambda_ToucanRawProcessor_function_name    = "qa-launch2020-ToucanRawProcessor"
lambda_ToucanRawProcessor_timeout	       = 30
lambda_ToucanRawProcessor_mem_size	       = 256
lambda_ToucanRawProcessor_handler	        = "lambda_function.lambda_handler"
lambda_ToucanRawProcessor_runtime          = "python3.6"
ToucanRawProcessor_statement_id  =  []
ToucanRawProcessor_action        =  ""
ToucanRawProcessor_principal     = []
ToucanRawProcessor_source_arn = []

##################  API gateway ###############


templates_exp_path = "templates-launch2020"

#QA_LAUNCH2020_EOL_STARPOINT
template_file_name_api_eol_startpoint = "LAUNCH2020_EOL_STARPOINT-swagger-apigateway.json"
template_file_map_eol_startpoint = {
  title = "QA_LAUNCH2020_EOL_STARPOINT"
  basePath = "/qa-launch2020-eol-startpoint"
  stageName = "qa-launch2020"
  description = "API gateway for EOL STARPOINT"
  host = "exp-api.proterra.com"
}
local_stage_variables_eol_startpoint = { }


#QA_LAUNCH2020_BUSINESS_TIER
template_file_name_api_business_tier = "LAUNCH2020_BUSINESS_TIER-swagger-apigateway.json"
template_file_map_business_tier = {
  title = "QA_LAUNCH2020_BUSINESS_TIER"
  basePath = "/qa-launch2020-businesstier"
  stageName = "qa-launch2020"
  description = "API gateway for Business-Tier"
  host = "exp-api.proterra.com"
}
local_stage_variables_business_tier = {
baseURL = "a5196b70e817c4be4af75c206eaab55b-663784988.us-east-1.elb.amazonaws.com:8080"	 
}

#QA_LAUNCH2020_CCSS
template_file_name_api_ccss = "LAUNCH2020_CCSS-swagger-apigateway.json"
template_file_map_ccss = {
  title = "QA_LAUNCH2020_CCSS"
  basePath = "/qa-launch2020-ccss"
  stageName = "qa-launch2020"
  description = "API gateway for CCSS"
  host = "exp-api.proterra.com"
}
local_stage_variables_ccss = {
m5statusApiKey = "QBRvZlEq1Ric9D08pgqd5rj1NPYGplX1jNSWWWW8"	 
baseURL = "qa-launch2020-svc-alb.proterra.com/ccss"	 
runsApiKey = "auJXtUZXIj89oc5y4LxcO3scnqUwQaPc2gVIw6ey"	 
m5statusURL = "qa-launch2020-svc-alb.proterra.com/cim"	 
apiKey = "USVD773SAl7Rj4NNanngn49D6wqalu7T4JtW3BvB"	 
runsURL = "qa-launch2020-svc-alb.proterra.com/api"
}

#QA_LAUNCH2020_CCSS_TRACK
template_file_name_api_ccss_track = "LAUNCH2020_CCSS_TRACK-swagger-apigateway.json"
template_file_map_ccss_track = {
  title = "QA_LAUNCH2020_CCSS_TRACK"
  basePath = "/qa-launch2020-ccss-track"
  stageName = "qa-launch2020"
  description = "API gateway for CCSS TRACK"
  host = "exp-api.proterra.com"
}
local_stage_variables_ccss_track = {
baseURL = "qa-launch2020-svc-alb.proterra.com/ccss"	 
runsURL = "qa-launch2020-svc-alb.proterra.com/api"
m5statusURL = "qa-launch2020-svc-alb.proterra.com"
}

#QA_LAUNCH2020_CHARGEMONITOR
template_file_name_api_chargemonitor = "LAUNCH2020_CHARGEMONITOR-swagger-apigateway.json"
template_file_map_chargemonitor = {
  title = "QA_LAUNCH2020_CHARGEMONITOR"
  basePath = "/qa-launch2020-chargemonitor"
  stageName = "qa-launch2020"
  description = "API gateway for CHARGEMONITOR"
  host = "exp-api.proterra.com"
}
local_stage_variables_chargemonitor = { 
baseURL = "a5196b70e817c4be4af75c206eaab55b-663784988.us-east-1.elb.amazonaws.com:8080"
}

#QA_LAUNCH2020_CIM
template_file_name_api_cim = "LAUNCH2020_CIM-swagger-apigateway.json"
template_file_map_cim = {
  title = "QA_LAUNCH2020_CIM"
  basePath = "/qa-launch2020-cim"
  stageName = "qa-launch2020"
  description = "API gateway for CIM"
  host = "exp-api.proterra.com"
}
local_stage_variables_cim = { 
baseURL = "qa-launch2020-svc-alb.proterra.com"
}


#QA_LAUNCH2020_DATALAKE
template_file_name_api_datalake = "LAUNCH2020_DATALAKE-swagger-apigateway.json"
template_file_map_datalake= {
  title = "QA_LAUNCH2020_DATALAKE"
  basePath = "/qa-launch2020-datalake"
  stageName = "qa-launch2020"
  description = "API gateway for DATALAKE"
  host = "exp-api.proterra.com"
}
local_stage_variables_datalake= { 
apikey = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
readOnlyApiKey = "cHJvdGVycmEtcmVhZG9ubHk6cHJvdGVycmEtcmVhZG9ubHk="	 
chargeSessionAPIKey = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
dataLakeUrl = "qa-launch2020-svc-alb.proterra.com/dataLake/v1"	 
apiKeyProd = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
latestAuth = "Basic ZWxhc3RpYzpKeGdWMlpXZG9KMU9mSkN2c0licVdxYmk="	 
edmontonApiKey = "b09389c4-fd73-4cad-ad5a-c443a963d1f7"	 
internalDataLakeUrl = "a858c9f760c2148fa865eeea7d874a10-777558906.us-east-1.elb.amazonaws.com:8091"	 
baseURL = "52.23.215.14:8080"	 
reportBaseUrl = "qa-launch2020-svc-alb.proterra.com/dataLake/v1"	 
chargeSessionsURL = "qa-launch2020-svc-alb.proterra.com/dataLake/v1"	 
runsURL = "qa-launch2020-svc-alb.proterra.com/api/v1"	 
elasticSearchURL = "ec2-67-202-49-169.compute-1.amazonaws.com:9200"	 
rtApiKey = "7eb92260-e5c8-11ea-adc1-0242ac120002"	 
elasticSerchAuthKey = "Basic ZWxhc3RpYzplbGFzdGljX2RldmVz"
}


#QA_LAUNCH2020_DEMAND_MANAGEMENT
template_file_name_api_demand_management = "LAUNCH2020_DEMAND_MANAGEMENT-swagger-apigateway.json"
template_file_map_demand_management= {
  title = "QA_LAUNCH2020_DEMAND_MANAGEMENT"
  basePath = "/qa-launch2020-demand-management"
  stageName = "qa-launch2020"
  description = "API gateway for DEMAND MANAGEMENT"
  host = "exp-api.proterra.com"
}
local_stage_variables_demand_management= { }


#QA_LAUNCH2020_IQ
template_file_name_api_iq = "LAUNCH2020_IQ-swagger-apigateway.json"
template_file_map_iq= {
  title = "QA_LAUNCH2020_IQ"
  basePath = "/qa-launch2020-iq"
  stageName = "qa-launch2020"
  description = "API gateway for IQ"
  host = "exp-api.proterra.com"
}
local_stage_variables_iq= {
baseURL ="qa-launch2020-svc-alb.proterra.com/"
}


#QA_LAUNCH2020_MISCEL_API
template_file_name_api_miscel = "LAUNCH2020_MISCELAPI-swagger-apigateway.json"
template_file_map_miscel= {
  title = "QA_LAUNCH2020_MISCEL_API"
  basePath = "/qa-launch2020-miscel"
  stageName = "qa-launch2020"
  description = "API gateway for MISCEL"
  host = "exp-api.proterra.com"
}
local_stage_variables_miscel= {}


#QA_LAUNCH2020_MULTIPLEXER
template_file_name_api_multiplexer= "LAUNCH2020_MULTIPLEXER-swagger-apigateway.json"
template_file_map_multiplexer= {
  title = "QA_LAUNCH2020_MULTIPLEXER"
  basePath = "/qa-launch2020-multiplexer"
  stageName = "qa-launch2020"
  description = "API gateway for MULTIPLEXER"
  host = "exp-api.proterra.com"
}
local_stage_variables_multiplexer= {
  baseURL = "aabfa6f38ff444aa794176161c2728b4-660568574.us-east-1.elb.amazonaws.com:8887"
}



#QA_LAUNCH2020_MULTIPLEXER_CP
template_file_name_api_multiplexer_cp= "LAUNCH2020_MULTIPLEXER_CP-swagger-apigateway.json"
template_file_map_multiplexer_cp= {
  title = "QA_LAUNCH2020_MULTIPLEXER_CP"
  basePath = "/qa-launch2020-multiplexer-cp"
  stageName = "qa-launch2020"
  description = "API gateway for MULTIPLEXER_CP"
  host = "exp-api.proterra.com"
}
local_stage_variables_multiplexer_cp= {
   baseURL = "a4a274fa30ed84296bfd56a9a0f98f80-548781120.us-east-1.elb.amazonaws.com:8088"
}


#QA_LAUNCH2020_OCPP
template_file_name_api_ocpp= "LAUNCH2020_OCPP-swagger-apigateway.json"
template_file_map_ocpp= {
  title = "QA_LAUNCH2020_OCPP"
  basePath = "/qa-launch2020-ocpp"
  stageName = "qa-launch2020"
  description = "API gateway for OCPP"
  host = "exp-api.proterra.com"
}
local_stage_variables_ocpp= {
baseOCPPURL = "a0a9544af97924605b828fee8fc84a33-1058822887.us-east-1.elb.amazonaws.com:8080"	 
baseURL = "a0a9544af97924605b828fee8fc84a33-1058822887.us-east-1.elb.amazonaws.com:8080"
}

#QA_LAUNCH2020_PDF_FILE_PROTERRA_AMS
template_file_name_api_pdf_file_proterra_ams= "LAUNCH2020_PDF_FILE_PROTERRA_AMS-swagger-apigateway.json"
template_file_map_pdf_file_proterra_ams= {
  title = "QA_LAUNCH2020_PDF_FILE_PROTERRA_AMS"
  basePath = "/qa-launch2020-pdf"
  stageName = "qa-launch2020"
  description = "API gateway for PDF FILE PROTERRA AMS"
  host = "exp-api.proterra.com"
}
local_stage_variables_pdf_file_proterra_ams= {
baseCustomReportURL = "qa-launch2020-svc-alb.proterra.com/api"	 
baseURL = "qa-launch2020-svc-alb.proterra.com/ams" 
dataLakeURL = "qa-launch2020-svc-alb.proterra.com"
}

#QA_LAUNCH2020_PROTERRA_AMS
template_file_name_api_proterra_ams= "LAUNCH2020_PROTERRA_AMS-swagger-apigateway.json"
template_file_map_proterra_ams= {
  title = "QA_LAUNCH2020_PROTERRA_AMS"
  basePath = "/qa-launch2020-ams"
  stageName = "qa-launch2020"
  description = "API gateway for PROTERRA AMS"
  host = "exp-api.proterra.com"
}
local_stage_variables_proterra_ams= {
baseCustomReportURL = "qa-launch2020-svc-alb.proterra.com/api"	 
baseURL = "qa-launch2020-svc-alb.proterra.com/ams" 
dataLakeURL = "qa-launch2020-svc-alb.proterra.com"
}

#QA_LAUNCH2020_V2G_WEBSERVICES
template_file_name_api_v2g_webservices= "LAUNCH2020_V2G_WEBSERVICES-swagger-apigateway.json"
template_file_map_v2g_webservices= {
  title = "QA_LAUNCH2020_V2G_WEBSERVICES"
  basePath = "/qa-launch2020-v2g-webservices"
  stageName = "qa-launch2020"
  description = "API gateway for V2G WEBSERVICES"
  host = "exp-api.proterra.com"
}
local_stage_variables_v2g_webservices= {
apiUrl = "qa-launch2020-svc-alb.proterra.com/"	 
apiKey = "Rrww0oUksG8AjQ4UNS8xa5NKWI8TnBLU9V8fPHv9"	 
webserviceURL	 = "a84a35c106e294b4ca7c46924550bbdd-832380154.us-east-1.elb.amazonaws.com:8080/v2gws/"
}


#QA_LAUNCH2020_RUNS
template_file_name_api_runs= "LAUNCH2020_RUNS-swagger-apigateway.json"
template_file_map_runs= {
  title = "QA_LAUNCH2020_RUNS"
  basePath = "/qa-launch2020-runs"
  stageName = "qa-launch2020"
  description = "API gateway for RUNS"
  host = "exp-api.proterra.com"
}
local_stage_variables_runs= {	 
apiKey = "KhgHxin5W32b86AgHoGro8xSfTTg6uSo4rLAJDjK"	 
baseURL	 = "qa-launch2020-svc-alb.proterra.com/api"
}



############ SNS  #####
#ocpppowersetsns 
ocpppowersetsns_sns_name="qa-launch2020-ocpppowersetsns"
#ocpppowersetsns_sns_subscription_arn_list = []

#ocpp-messages
ocpp_messages_sns_name="qa-launch2020-ocpp-messages"
ocpp_messages_sns_subscription_arn_list =  [
    {
      protocol = "http"
      arn      = "http://a479887d694cf4221aeae58019a02b44-603124970.us-east-1.elb.amazonaws.com:8080/notification"
    }  
    ]
#v2g_ocpp-messages
v2g_ocpp_messages_sns_name="qa-launch2020-v2g-ocpp-messages"
v2g_ocpp_messages_sns_subscription_arn_list =  [
    {
      protocol = "http"
      arn      = "http://imuser-18ba19d9.localhost.run/notification"
    } , 
    {
      protocol = "http"
      arn      = "http://imuser-75dc5a66.localhost.run/notification"
    } , 
        {
      protocol = "http"
      arn      = "http://imuser-1e26d5d9.localhost.run/notification"
    } , 
        {
      protocol = "http"
      arn      = "http://imuser-78a75758.localhost.run/notification"
    } , 
        {
      protocol = "http"
      arn      = "http://imuser-bf745506.localhost.run/notification"
    } , 
        {
      protocol = "http"
      arn      = "http://a6c7cbe854f1e4b20919b2f0bd5e7f6c-237393569.us-east-1.elb.amazonaws.com:8080/notification"
    } , 
        {
      protocol = "http"
      arn      = "http://imuser-7d7aa833.localhost.run/notification"
    } 
]

#v2g_sns
v2g_sns_sns_name="qa-launch2020-v2g-sns"
v2g_sns_sns_subscription_arn_list =  [
    {
      protocol = "http"
      arn      = "http://imuser-f04a792e.localhost.run/notification"
    }    
]
#v2g-sns-reppa
v2g_sns_reppa_sns_name="qa-launch2020-v2g-sns-reppa"
v2g_sns_reppa_sns_subscription_arn_list =  [
    {
      protocol = "https"
      arn      = "https://ff5b4445197081.localhost.run/notification"
    }   
]





