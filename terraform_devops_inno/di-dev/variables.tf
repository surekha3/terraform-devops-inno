variable "aws_region" {
  type = string
}

variable "aws_profile" {
  type = string
}


############### S3 ################

variable "s3_bucket_env" {
  default = ""
  type    = string
}

variable "toucan_s3_bucket_name" {
    type = string
} 

variable "backward_toucan_s3_bucket_name" {
    type = string
}

variable "toucan_data_s3_bucket_name" {
    type = string
}

variable "data_connected_s3_bucket_name" {
    type = string
}

############ SQS #########################

variable "sqs_env_tag" {
  type        = string
   default     = ""
}
######### SQS 1  ToucanBackward 
variable "sqs1_name" {
  type        = string
   default     = ""
}
variable "sqs1_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs1_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs1_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs1_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs1_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
######### SQS 2   ToucanRawDataStream  #######
variable "sqs2_name" {
  type        = string
   default     = ""
}
variable "sqs2_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs2_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs2_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs2_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs2_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
######### SQS 3  RoadRunnerDataStream  #######
variable "sqs3_name" {
  type        = string
   default     = ""
}
variable "sqs3_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs3_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs3_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs3_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs3_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
######### SQS 4  BusShadowUpdates  #######
variable "sqs4_name" {
  type        = string
   default     = ""
}
variable "sqs4_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs4_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs4_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs4_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs4_receive_wait_time_seconds" {
  type        = number
   default     = 0
}

################ Lambda #################

# DecompressLambda

variable "lambda_dl_iam_role_name" {}
variable "lambda_dl_iam_role_policy" {}
variable "lambda_dl_filename" {}
variable "lambda_dl_function_name" {}
variable "lambda_dl_timeout" {}
variable "lambda_dl_mem_size" {}
variable "lambda_dl_handler" {}
variable "lambda_dl_runtime" {}
#variable "lambda_dl_s3_bucket" {}
#variable "lambda_dl_s3_key" {}

#ToucanRawProcessor
variable "lambda_trp_iam_role_name" {}
variable "lambda_trp_iam_role_policy" {}
variable "lambda_trp_filename" {}
variable "lambda_trp_function_name" {}
variable "lambda_trp_timeout" {}
variable "lambda_trp_mem_size" {}
variable "lambda_trp_handler" {}
variable "lambda_trp_runtime" {}

#BackwardToucanProcessor
variable "lambda_btw_iam_role_name" {}
variable "lambda_btw_iam_role_policy" {}
variable "lambda_btw_filename" {}
variable "lambda_btw_function_name" {}
variable "lambda_btw_timeout" {}
variable "lambda_btw_mem_size" {}
variable "lambda_btw_handler" {}
variable "lambda_btw_runtime" {}

#RoadRunnerDataProcessor

variable "lambda_rrdp_iam_role_name" {}
variable "lambda_rrdp_iam_role_policy" {}
variable "lambda_rrdp_filename" {}
variable "lambda_rrdp_function_name" {}
variable "lambda_rrdp_timeout" {}
variable "lambda_rrdp_mem_size" {}
variable "lambda_rrdp_handler" {}
variable "lambda_rrdp_runtime" {}