#####################################################
# Base Level Terraform Code.
# - Modify Account specific items.
#####################################################
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
  }
}

#################### S3 ######################

module "TF_Module_S3_toucan" {
  source = "../modules/TF_Module_S3_SIMPLE"
  s3_bucket_name             = var.toucan_s3_bucket_name
}

module "TF_Module_S3_backward_toucan" {
  source = "../modules/TF_Module_S3_SIMPLE"
  s3_bucket_name             = var.backward_toucan_s3_bucket_name
}
module "TF_Module_S3_toucan_data" {
  source = "../modules/TF_Module_S3_SIMPLE"
  s3_bucket_name             = var.toucan_data_s3_bucket_name
}

module "TF_Module_S3_data_connected" {
  source = "../modules/TF_Module_S3_SIMPLE"
  s3_bucket_name             = var.data_connected_s3_bucket_name
}

################### SQS #####################

module "TF_Module_SQS_ToucanBackward" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs1_name
  #sqs_visibility_timeout_seconds     = var.sqs1_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs1_message_retention_seconds
  #sqs_max_message_size               = var.sqs1_max_message_size
  #sqs_delay_seconds                  = var.sqs1_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs1_receive_wait_time_seconds
}

module "TF_Module_SQS_ToucanRawDataStream" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs2_name
  #sqs_visibility_timeout_seconds     = var.sqs2_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs2_message_retention_seconds
  #sqs_max_message_size               = var.sqs2_max_message_size
  #sqs_delay_seconds                  = var.sqs2_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs2_receive_wait_time_seconds
}
module "TF_Module_SQS_RoadRunnerDataStream" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs3_name
  #sqs_visibility_timeout_seconds     = var.sqs3_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs3_message_retention_seconds
  #sqs_max_message_size               = var.sqs3_max_message_size
  #sqs_delay_seconds                  = var.sqs3_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs3_receive_wait_time_seconds
}
module "TF_Module_SQS_BusShadowUpdates" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs4_name
  #sqs_visibility_timeout_seconds     = var.sqs4_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs4_message_retention_seconds
  #sqs_max_message_size               = var.sqs4_max_message_size
  #sqs_delay_seconds                  = var.sqs4_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs4_receive_wait_time_seconds
}

##################### Lambda #########################

# For DecompressLambda
module "TF_Module_Lambda_DecompressLambda" {
 source = "../modules/TF_Module_LAMBDA_DI"

 iam_role_name =  var.lambda_dl_iam_role_name
 iam_role_policy  = var.lambda_dl_iam_role_policy

 lambda_filename  = var.lambda_dl_filename
 #lambda_s3_bucket = var.lambda_dl_s3_bucket
 #lambda_s3_key    =  var.lambda_dl_s3_key
 lambda_function_name = var.lambda_dl_function_name
 timeout	  = var.lambda_dl_timeout
 mem_size	  = var.lambda_dl_mem_size
 handler	  = var.lambda_dl_handler
 runtime    =var.lambda_dl_runtime
 lambda_env = {
  targetS3Bucket = "exp-dev-launch2020-backward.toucan.proterra.com"
}
}

# For ToucanRawProcessor
module "TF_Module_Lambda_ToucanRawProcessor" {
 source = "../modules/TF_Module_LAMBDA_DI"

 iam_role_name =  var.lambda_trp_iam_role_name
 iam_role_policy  = var.lambda_trp_iam_role_policy

 lambda_filename  = var.lambda_trp_filename
 lambda_function_name = var.lambda_trp_function_name
 timeout	  = var.lambda_trp_timeout
 mem_size	  = var.lambda_trp_mem_size
 handler	  = var.lambda_trp_handler
 runtime    =var.lambda_trp_runtime
 lambda_env = {
  assetDBBucket = "exp-dev-launch2020-data.connected.proterra.com",
  SQSName="dev-launch2020-ToucanRawDataStream"
}
}

# For BackwardToucanProcessor
module "TF_Module_Lambda_BackwardToucanProcessor" {
 source = "../modules/TF_Module_LAMBDA_DI"

 iam_role_name =  var.lambda_btw_iam_role_name
 iam_role_policy  = var.lambda_btw_iam_role_policy

 lambda_filename  = var.lambda_btw_filename
 lambda_function_name = var.lambda_btw_function_name
 timeout	  = var.lambda_btw_timeout
 mem_size	  = var.lambda_btw_mem_size
 handler	  = var.lambda_btw_handler
 runtime    =var.lambda_btw_runtime
 lambda_env = {
  assetDBBucket = "exp-dev-launch2020-data.connected.proterra.com",
  SQSName="dev-launch2020-ToucanRawDataStream"
}
}
#RoadRunnerDataProcessor

module "TF_Module_Lambda_RoadRunnerDataProcessor" {
 source = "../modules/TF_Module_LAMBDA_DI"

 iam_role_name =  var.lambda_rrdp_iam_role_name
 iam_role_policy  = var.lambda_rrdp_iam_role_policy

 lambda_filename  = var.lambda_rrdp_filename
 lambda_function_name = var.lambda_rrdp_function_name
 timeout	  = var.lambda_rrdp_timeout
 mem_size	  = var.lambda_rrdp_mem_size
 handler	  = var.lambda_rrdp_handler
 runtime    = var.lambda_rrdp_runtime
 lambda_env = {
  assetDBBucket = "exp-dev-launch2020-data.connected.proterra.com",
  sqsQueueName="dev-launch2020-RoadRunnerDataStream"
}
}
