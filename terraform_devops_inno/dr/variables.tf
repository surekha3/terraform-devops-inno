variable "aws_region" {
  type = string
}

variable "aws_profile" {
  type = string
}

variable "state_bucket" {
  type = string
}

############## S3 ################

variable "s3_bucket_env" {
  default = ""
  type    = string
}

variable "toucan_s3_bucket_name" {
    type = string
} 

variable "backward_toucan_s3_bucket_name" {
    type = string
}

variable "toucan_data_s3_bucket_name" {
    type = string
}

variable "data_connected_s3_bucket_name" {
    type = string
}

############ SQS #########################

variable "sqs_env_tag" {
  type        = string
   default     = ""
}
######### SQS 1  ToucanBackward 
variable "sqs1_name" {
  type        = string
   default     = ""
}
variable "sqs1_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs1_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs1_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs1_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs1_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
######### SQS 2   ToucanRawDataStream  #######
variable "sqs2_name" {
  type        = string
   default     = ""
}
variable "sqs2_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs2_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs2_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs2_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs2_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
######### SQS 3  RoadRunnerDataStream  #######
variable "sqs3_name" {
  type        = string
   default     = ""
}
variable "sqs3_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs3_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs3_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs3_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs3_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
######### SQS 4  BusShadowUpdates  #######
variable "sqs4_name" {
  type        = string
   default     = ""
}
variable "sqs4_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs4_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs4_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs4_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs4_receive_wait_time_seconds" {
  type        = number
   default     = 0
}

##########  MDC Queues ########
######### SQS 5  mdc-device-status
variable "sqs5_name" {
  type        = string
   default     = ""
}
variable "sqs5_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs5_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs5_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs5_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs5_receive_wait_time_seconds" {
  type        = number
   default     = 0
}
######### SQS 6  mdc-devices-messages
variable "sqs6_name" {
  type        = string
   default     = ""
}
variable "sqs6_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs6_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs6_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs6_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs6_receive_wait_time_seconds" {
  type        = number
   default     = 0
}

######### SQS 7  mdc-jobs
variable "sqs7_name" {
  type        = string
   default     = ""
}
variable "sqs7_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs7_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs7_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs7_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs7_receive_wait_time_seconds" {
  type        = number
   default     = 0
}

######### SQS 8 mdc-status
variable "sqs8_name" {
  type        = string
   default     = ""
}
variable "sqs8_visibility_timeout_seconds" {
  type        = number
   default     = 30
}
variable "sqs8_message_retention_seconds" {
  type        = number
   default     = 345600
}
variable "sqs8_max_message_size" {
  type        = number
   default     = 262144
}
variable "sqs8_delay_seconds" {
  type        = number
   default     = 0
}
variable "sqs8_receive_wait_time_seconds" {
  type        = number
   default     = 0
}


################ Lambda #################
# GetPCSChargerStatus
variable "pcs_s3_bucket" {}
variable "pcs_s3_key" {}
variable "lambda_pcs_iam_role_name" {}
variable "lambda_pcs_iam_role_policy" {}
variable "lambda_pcs_iam_assume_role_policy" {}
variable "pcs_path" {}
#variable "enable_role" {}
variable "pcs_policy_name" {}
#variable "lambda_pcs_filename" {}
variable "lambda_pcs_function_name" {}
variable "lambda_pcs_timeout" {}
variable "lambda_pcs_mem_size" {}
variable "lambda_pcs_handler" {}
variable "lambda_pcs_runtime" {}
variable "pcs_statement_id" {
  type = list(string)
}
variable "pcs_action"  {}
variable "pcs_principal" {
  type = list(string)
}
variable "pcs_source_arn" {
  type = list(string)
}

# getGlossaryFromS3
variable "get_glossy_s3_bucket" {}
variable "get_glossy_s3_key" {}
variable "lambda_get_glossy_iam_role_name" {}
variable "lambda_get_glossy_iam_role_policy" {}
variable "lambda_get_glossy_iam_assume_role_policy" {}
variable "get_glossy_path" {}
#variable "enable_role" {}
variable "get_glossy_policy_name" {}
#variable "lambda_get_glossy_filename" {}
variable "lambda_get_glossy_function_name" {}
variable "lambda_get_glossy_timeout" {}
variable "lambda_get_glossy_mem_size" {}
variable "lambda_get_glossy_handler" {}
variable "lambda_get_glossy_runtime" {}
variable "get_glossy_statement_id" {
  type = list(string)
}
variable "get_glossy_action"  {}
variable "get_glossy_principal" {
  type = list(string)
}
variable "get_glossy_source_arn" {
  type = list(string)
}

# saveGlossaryToS3
variable "save_glossy_s3_bucket" {}
variable "save_glossy_s3_key" {}
variable "lambda_save_glossy_iam_role_name" {}
variable "lambda_save_glossy_iam_role_policy" {}
variable "lambda_save_glossy_iam_assume_role_policy" {}
variable "save_glossy_path" {}
#variable "enable_role" {}
variable "save_glossy_policy_name" {}
#variable "lambda_save_glossy_filename" {}
variable "lambda_save_glossy_function_name" {}
variable "lambda_save_glossy_timeout" {}
variable "lambda_save_glossy_mem_size" {}
variable "lambda_save_glossy_handler" {}
variable "lambda_save_glossy_runtime" {}
variable "save_glossy_statement_id" {
  type = list(string)
}
variable "save_glossy_action"  {}
variable "save_glossy_principal" {
  type = list(string)
}
variable "save_glossy_source_arn" {
  type = list(string)
}

# LogstashLambda
variable "logstash_s3_bucket" {}
variable "logstash_s3_key" {}
variable "lambda_logstash_iam_role_name" {}
variable "lambda_logstash_iam_role_policy" {}
variable "lambda_logstash_iam_assume_role_policy" {}
variable "logstash_path" {}
#variable "enable_role" {}
variable "logstash_policy_name" {}
#variable "lambda_logstash_filename" {}
variable "lambda_logstash_function_name" {}
variable "lambda_logstash_timeout" {}
variable "lambda_logstash_mem_size" {}
variable "lambda_logstash_handler" {}
variable "lambda_logstash_runtime" {}
variable "logstash_statement_id" {
  type = list(string)
}
variable "logstash_action"  {}
variable "logstash_principal" {
  type = list(string)
}
variable "logstash_source_arn" {
  type = list(string)
}

# ToucanRawProcessor
variable "ToucanRawProcessor_s3_bucket" {}
variable "ToucanRawProcessor_s3_key" {}
variable "lambda_ToucanRawProcessor_iam_role_name" {}
variable "lambda_ToucanRawProcessor_iam_role_policy" {}
variable "lambda_ToucanRawProcessor_iam_assume_role_policy" {}
variable "ToucanRawProcessor_path" {}
#variable "enable_role" {}
variable "ToucanRawProcessor_policy_name" {}
#variable "lambda_ToucanRawProcessor_filename" {}
variable "lambda_ToucanRawProcessor_function_name" {}
variable "lambda_ToucanRawProcessor_timeout" {}
variable "lambda_ToucanRawProcessor_mem_size" {}
variable "lambda_ToucanRawProcessor_handler" {}
variable "lambda_ToucanRawProcessor_runtime" {}
variable "ToucanRawProcessor_statement_id" {
  type = list(string)
}
variable "ToucanRawProcessor_action"  {}
variable "ToucanRawProcessor_principal" {
  type = list(string)
}
variable "ToucanRawProcessor_source_arn" {
  type = list(string)
}

# DecompressLambda
variable "DecompressLambda_s3_bucket" {}
variable "DecompressLambda_s3_key" {}
variable "lambda_DecompressLambda_iam_role_name" {}
variable "lambda_DecompressLambda_iam_role_policy" {}
variable "lambda_DecompressLambda_iam_assume_role_policy" {}
variable "DecompressLambda_path" {}
#variable "enable_role" {}
variable "DecompressLambda_policy_name" {}
#variable "lambda_DecompressLambda_filename" {}
variable "lambda_DecompressLambda_function_name" {}
variable "lambda_DecompressLambda_timeout" {}
variable "lambda_DecompressLambda_mem_size" {}
variable "lambda_DecompressLambda_handler" {}
variable "lambda_DecompressLambda_runtime" {}
variable "DecompressLambda_statement_id" {
  type = list(string)
}
variable "DecompressLambda_action"  {}
variable "DecompressLambda_principal" {
  type = list(string)
}
variable "DecompressLambda_source_arn" {
  type = list(string)
}

# BackwardToucanProcessor
variable "BackwardToucanProcessor_s3_bucket" {}
variable "BackwardToucanProcessor_s3_key" {}
variable "lambda_BackwardToucanProcessor_iam_role_name" {}
variable "lambda_BackwardToucanProcessor_iam_role_policy" {}
variable "lambda_BackwardToucanProcessor_iam_assume_role_policy" {}
variable "BackwardToucanProcessor_path" {}
#variable "enable_role" {}
variable "BackwardToucanProcessor_policy_name" {}
#variable "lambda_BackwardToucanProcessor_filename" {}
variable "lambda_BackwardToucanProcessor_function_name" {}
variable "lambda_BackwardToucanProcessor_timeout" {}
variable "lambda_BackwardToucanProcessor_mem_size" {}
variable "lambda_BackwardToucanProcessor_handler" {}
variable "lambda_BackwardToucanProcessor_runtime" {}
variable "BackwardToucanProcessor_statement_id" {

  type = list(string)
}
variable "BackwardToucanProcessor_action"  {}
variable "BackwardToucanProcessor_principal" {
  type = list(string)
}
variable "BackwardToucanProcessor_source_arn" {

  type = list(string)
}

# RoadRunnerDataProcessor
variable "RoadRunnerDataProcessor_s3_bucket" {}
variable "RoadRunnerDataProcessor_s3_key" {}
variable "lambda_RoadRunnerDataProcessor_iam_role_name" {}
variable "lambda_RoadRunnerDataProcessor_iam_role_policy" {}
variable "lambda_RoadRunnerDataProcessor_iam_assume_role_policy" {}
variable "RoadRunnerDataProcessor_path" {}
#variable "enable_role" {}
variable "RoadRunnerDataProcessor_policy_name" {}
#variable "lambda_RoadRunnerDataProcessor_filename" {}
variable "lambda_RoadRunnerDataProcessor_function_name" {}
variable "lambda_RoadRunnerDataProcessor_timeout" {}
variable "lambda_RoadRunnerDataProcessor_mem_size" {}
variable "lambda_RoadRunnerDataProcessor_handler" {}
variable "lambda_RoadRunnerDataProcessor_runtime" {}
variable "RoadRunnerDataProcessor_statement_id" {
  type = list(string)
}
variable "RoadRunnerDataProcessor_action"  {}
variable "RoadRunnerDataProcessor_principal" {
  type = list(string)
}
variable "RoadRunnerDataProcessor_source_arn" {
  type = list(string)
}

# realtimeIntoApex
variable "realtimeIntoApex_s3_bucket" {}
variable "realtimeIntoApex_s3_key" {}
variable "lambda_realtimeIntoApex_iam_role_name" {}
variable "lambda_realtimeIntoApex_iam_role_policy" {}
variable "lambda_realtimeIntoApex_iam_assume_role_policy" {}
variable "realtimeIntoApex_path" {}
#variable "enable_role" {}
variable "realtimeIntoApex_policy_name" {}
#variable "lambda_realtimeIntoApex_filename" {}
variable "lambda_realtimeIntoApex_function_name" {}
variable "lambda_realtimeIntoApex_timeout" {}
variable "lambda_realtimeIntoApex_mem_size" {}
variable "lambda_realtimeIntoApex_handler" {}
variable "lambda_realtimeIntoApex_runtime" {}
variable "realtimeIntoApex_statement_id" {
  type = list(string)
}
variable "realtimeIntoApex_action"  {}
variable "realtimeIntoApex_principal" {
  type = list(string)
}
variable "realtimeIntoApex_source_arn" {
  type = list(string)
}

# mdc-enrollment
variable "mdcEnrollment_s3_bucket" {}
variable "mdcEnrollment_s3_key" {}
variable "lambda_mdcEnrollment_iam_role_name" {}
variable "lambda_mdcEnrollment_iam_role_policy" {}
variable "lambda_mdcEnrollment_iam_assume_role_policy" {}
variable "mdcEnrollment_path" {}
#variable "enable_role" {}
variable "mdcEnrollment_policy_name" {}
#variable "lambda_mdcEnrollment_filename" {}
variable "lambda_mdcEnrollment_function_name" {}
variable "lambda_mdcEnrollment_timeout" {}
variable "lambda_mdcEnrollment_mem_size" {}
variable "lambda_mdcEnrollment_handler" {}
variable "lambda_mdcEnrollment_runtime" {}
variable "mdcEnrollment_statement_id" {
  type = list(string)
}
variable "mdcEnrollment_action"  {}
variable "mdcEnrollment_principal" {
  type = list(string)
}
variable "mdcEnrollment_source_arn" {
  type = list(string)
}

# prod_mdc_s3_edge_headers
variable "mdcS3EdgeHeaders_s3_bucket" {}
variable "mdcS3EdgeHeaders_s3_key" {}
variable "lambda_mdcS3EdgeHeaders_iam_role_name" {}
variable "lambda_mdcS3EdgeHeaders_iam_role_policy" {}
variable "lambda_mdcS3EdgeHeaders_iam_assume_role_policy" {}
variable "mdcS3EdgeHeaders_path" {}
#variable "enable_role" {}
variable "mdcS3EdgeHeaders_policy_name" {}
#variable "lambda_mdcS3EdgeHeaders_filename" {}
variable "lambda_mdcS3EdgeHeaders_function_name" {}
variable "lambda_mdcS3EdgeHeaders_timeout" {}
variable "lambda_mdcS3EdgeHeaders_mem_size" {}
variable "lambda_mdcS3EdgeHeaders_handler" {}
variable "lambda_mdcS3EdgeHeaders_runtime" {}
variable "mdcS3EdgeHeaders_statement_id" {
  type = list(string)
}
variable "mdcS3EdgeHeaders_action"  {}
variable "mdcS3EdgeHeaders_principal" {
  type = list(string)
}
variable "mdcS3EdgeHeaders_source_arn" {
  type = list(string)
}



#EOL
variable "lambda_eol_iam_role_name" {}
variable "lambda_eol_iam_role_policy" {}
variable "eol_statement_id" {}
variable "eol_s3_bucket" {}
variable "eol_s3_key" {}
variable "eol_commissioning_principal" {}
variable "eol_commissioning_source_arn" {}
variable "eol_commissioning_path" {}
variable "eol_commissioning_action" {} 
#variable "lambda_eol_filename" {}
variable "lambda_eol_function_name" {}
variable "lambda_eol_timeout" {}
variable "lambda_eol_mem_size" {}
variable "lambda_eol_handler" {}
variable "lambda_eol_runtime" {}

variable "environment" {
 type = string
}
###################################
# Variables added with VPC module.
variable "vpc_cidr" {
  type = string
}

variable "vpc_public_subnets" {
  type = list
}

variable "vpc_private_subnets" {
  type = list
}

variable "vpc_availabilityzones" {
  type = list
}

variable "project" {
  type = string
}

#variable "vpc_flow_log_cloudwatch_log_group" {
#  type = string
#}

variable "vpc_account_owner_ip" {
  type = list
  default = [" "]
}

variable "subnet_additional_tags" {
 type = map(string)
 default = {}
}



###################################################


variable "apex_ui_s3_bucket_name" {
    type = string
}

variable "apex_ui_s3_bucket_policy_file_name" {
    type = string
}

variable "ccss_ui_s3_bucket_name" {
    type = string
}

variable "ccss_ui_s3_bucket_policy_file_name" {
    type = string
}



variable "snapshot_identifier" {}




###########

variable "ams_cognito_user_pool_name" {
  type = string
  default = ""
}

variable "ams_cognito_email_verif_subject" {
  type = string
  default = ""
}

variable "ams_cognito_email_verif_message" {
  type = string
  default = ""
}

variable "ams_cognito_sms_verif_message" {
  type = string
  default = ""
}

variable "ams_cognito_unused_account_days" {
  type = number
  default = 30
}

variable "ams_cognito_email_subject" {
  type = string
  default = ""
}

variable "ams_cognito_sms_email_message" {
  type = string
  default = "\u003c!DOCTYPE html\n  PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"\u003e\n\u003chtml xmlns=\"http://www.w3.org/1999/xhtml\"\u003e\n\n\u003chead\u003e\n  \u003cmeta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /\u003e\n  \u003cmeta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /\u003e\n  \u003ctitle\u003eSetup a new password\u003c/title\u003e\n  \u003c!-- \n    The style block is collapsed on page load to save you some scrolling.\n    Postmark automatically inlines all CSS properties for maximum email client \n    compatibility. You can just update styles here, and Postmark does the rest.\n    --\u003e\n  \u003cstyle type=\"text/css\" rel=\"stylesheet\" media=\"all\"\u003e\n    /* Base ------------------------------ */\n\n    *:not(br):not(tr):not(html) {\n      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n      box-sizing: border-box;\n    }\n\n    body {\n      width: 100% !important;\n      height: 100%;\n      margin: 0;\n      line-height: 1.4;\n      background-color: #F2F4F6;\n      color: #74787E;\n      -webkit-text-size-adjust: none;\n    }\n\n    p,\n    ul,\n    ol,\n    blockquote {\n      line-height: 1.4;\n      text-align: left;\n    }\n\n    a {\n      color: #3869D4;\n    }\n\n    a img {\n      border: none;\n    }\n\n    td {\n      word-break: break-word;\n    }\n\n    /* Layout ------------------------------ */\n\n    .email-wrapper {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #F2F4F6;\n    }\n\n    .email-content {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    /* Masthead ----------------------- */\n\n    .email-masthead {\n      padding: 25px 0;\n      text-align: center;\n    }\n\n    .email-masthead_logo {\n      width: 94px;\n    }\n\n    .email-masthead_name {\n      font-size: 16px;\n      font-weight: bold;\n      color: #bbbfc3;\n      text-decoration: none;\n      text-shadow: 0 1px 0 white;\n    }\n\n    /* Body ------------------------------ */\n\n    .email-body {\n      width: 100%;\n      margin: 0;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      border-top: 1px solid #EDEFF2;\n      border-bottom: 1px solid #EDEFF2;\n      background-color: #FFFFFF;\n    }\n\n    .email-body_inner {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #FFFFFF;\n    }\n\n    .email-footer {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      -premailer-width: 570px;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .email-footer p {\n      color: #AEAEAE;\n    }\n\n    .body-action {\n      width: 100%;\n      margin: 30px auto;\n      padding: 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      text-align: center;\n    }\n\n    .body-sub {\n      margin-top: 25px;\n      padding-top: 25px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .content-cell {\n      padding: 35px;\n    }\n\n    .preheader {\n      display: none !important;\n      visibility: hidden;\n      mso-hide: all;\n      font-size: 1px;\n      line-height: 1px;\n      max-height: 0;\n      max-width: 0;\n      opacity: 0;\n      overflow: hidden;\n    }\n\n    /* Attribute list ------------------------------ */\n\n    .attributes {\n      margin: 0 0 21px;\n    }\n\n    .attributes_content {\n      background-color: #EDEFF2;\n      padding: 16px;\n    }\n\n    .attributes_item {\n      padding: 0;\n    }\n\n    /* Related Items ------------------------------ */\n\n    .related {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .related_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .related_item-title {\n      display: block;\n      margin: .5em 0 0;\n    }\n\n    .related_item-thumb {\n      display: block;\n      padding-bottom: 10px;\n    }\n\n    .related_heading {\n      border-top: 1px solid #EDEFF2;\n      text-align: center;\n      padding: 25px 0 10px;\n    }\n\n    /* Discount Code ------------------------------ */\n\n    .discount {\n      width: 100%;\n      margin: 0;\n      padding: 24px;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n      background-color: #EDEFF2;\n      border: 2px dashed #9BA2AB;\n    }\n\n    .discount_heading {\n      text-align: center;\n    }\n\n    .discount_body {\n      text-align: center;\n      font-size: 15px;\n    }\n\n    /* Social Icons ------------------------------ */\n\n    .social {\n      width: auto;\n    }\n\n    .social td {\n      padding: 0;\n      width: auto;\n    }\n\n    .social_icon {\n      height: 20px;\n      margin: 0 8px 10px 8px;\n      padding: 0;\n    }\n\n    /* Data table ------------------------------ */\n\n    .purchase {\n      width: 100%;\n      margin: 0;\n      padding: 35px 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_content {\n      width: 100%;\n      margin: 0;\n      padding: 25px 0 0 0;\n      -premailer-width: 100%;\n      -premailer-cellpadding: 0;\n      -premailer-cellspacing: 0;\n    }\n\n    .purchase_item {\n      padding: 10px 0;\n      color: #74787E;\n      font-size: 15px;\n      line-height: 18px;\n    }\n\n    .purchase_heading {\n      padding-bottom: 8px;\n      border-bottom: 1px solid #EDEFF2;\n    }\n\n    .purchase_heading p {\n      margin: 0;\n      color: #9BA2AB;\n      font-size: 12px;\n    }\n\n    .purchase_footer {\n      padding-top: 15px;\n      border-top: 1px solid #EDEFF2;\n    }\n\n    .purchase_total {\n      margin: 0;\n      text-align: right;\n      font-weight: bold;\n      color: #2F3133;\n    }\n\n    .purchase_total--label {\n      padding: 0 15px 0 0;\n    }\n\n    /* Utilities ------------------------------ */\n\n    .align-right {\n      text-align: right;\n    }\n\n    .align-left {\n      text-align: left;\n    }\n\n    .align-center {\n      text-align: center;\n    }\n\n    /*Media Queries ------------------------------ */\n\n    @media only screen and (max-width: 600px) {\n\n      .email-body_inner,\n      .email-footer {\n        width: 100% !important;\n      }\n    }\n\n    @media only screen and (max-width: 500px) {\n      .button {\n        width: 100% !important;\n      }\n    }\n\n    /* Buttons ------------------------------ */\n\n    .button {\n      background-color: #3869D4;\n      border-top: 10px solid #3869D4;\n      border-right: 18px solid #3869D4;\n      border-bottom: 10px solid #3869D4;\n      border-left: 18px solid #3869D4;\n      display: inline-block;\n      color: #FFF;\n      text-decoration: none;\n      border-radius: 3px;\n      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);\n      -webkit-text-size-adjust: none;\n    }\n\n    .button--green {\n      background-color: #62a744;\n      border-top: 10px solid #62a744;\n      border-right: 18px solid #62a744;\n      border-bottom: 10px solid #62a744;\n      border-left: 18px solid #62a744;\n    }\n\n    .button--red {\n      background-color: #FF6136;\n      border-top: 10px solid #FF6136;\n      border-right: 18px solid #FF6136;\n      border-bottom: 10px solid #FF6136;\n      border-left: 18px solid #FF6136;\n    }\n\n    /* Type ------------------------------ */\n\n    h1 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 19px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h2 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 16px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    h3 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 14px;\n      font-weight: bold;\n      text-align: left;\n    }\n\n    p {\n      margin-top: 0;\n      color: #74787E;\n      font-size: 16px;\n      line-height: 1.5em;\n      text-align: left;\n    }\n\n    p.sub {\n      font-size: 12px;\n    }\n\n    p.center {\n      text-align: center;\n    }\n  \u003c/style\u003e\n\u003c/head\u003e\n\n\u003cbody\u003e\n  \u003cspan class=\"preheader\"\u003eUse this link to reset your password. The link is only valid for 24 hours.\u003c/span\u003e\n  \u003ctable class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n    \u003ctr\u003e\n      \u003ctd align=\"center\"\u003e\n        \u003ctable class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-masthead\"\u003e\n              \u003cdiv class=\"navbar-header\" style=\"width:100%; height:100px\"\u003e\n\n                \u003ca class=\"navbar-brand\" rel=\"home\" href=\"#\" title=\"\"\u003e\n                  \u003cimg style=\"margin-top: 10px;\" src=\"http://dr-prod.apex.proterra.com.s3-website-us-east-1.amazonaws.com/images/logo.png\"\u003e\n                \u003c/a\u003e\n              \u003c/div\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003c!-- Email Body --\u003e\n          \u003ctr\u003e\n            \u003ctd class=\"email-body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n              \u003ctable class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003c!-- Body content --\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\"\u003e\n                    \u003ch1\u003eHi {username},\u003c/h1\u003e\n                    \u003cp\u003eYour Proterra APEX administrator has created an account for you.\u003c/p\u003e\n                    \u003cp\u003eClick below to complete your account setup. You will be asked to set your password.\u003c/p\u003e\n                    \u003c!-- Action --\u003e\n                    \u003ctable class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd align=\"center\"\u003e\n                          \u003c!-- Border based button\n                       https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design --\u003e\n                          \u003ctable width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                            \u003ctr\u003e\n                              \u003ctd align=\"center\"\u003e\n                                \u003ctable border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\u003e\n                                  \u003ctr\u003e\n                                    \u003ctd\u003e\n                                      \u003ca href=\"http://dr-prod.apex.proterra.com.s3-website-us-east-1.amazonaws.com/changePassword.html?username={username}\u0026temporaryPassword={####}\"\n                                        class=\"button button--green\" target=\"_blank\"\n                                        style=\"color: white !important;\"\u003eSet up new password\u003c/a\u003e\n                                    \u003c/td\u003e\n                                  \u003c/tr\u003e\n                                \u003c/table\u003e\n                              \u003c/td\u003e\n                            \u003c/tr\u003e\n                          \u003c/table\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                    \u003cp\u003eIf you have issues or questions please contact your admin or \u003ca\n                        href=\"mailto:connected@proterra.com\"\u003eAPEX support\u003c/a\u003e.\u003c/p\u003e\n                    \u003cp\u003eThank you,\n                      \u003cbr\u003eThe APEX Team\u003c/p\u003e\n                    \u003c!-- Sub copy --\u003e\n                    \u003ctable class=\"body-sub\"\u003e\n                      \u003ctr\u003e\n                        \u003ctd\u003e\n                          \u003cp class=\"sub\"\u003eIf you’re having trouble with the button above, copy and paste the URL below\n                            into your web browser.\u003c/p\u003e\n                          \u003cp class=\"sub\"\u003e\n                            http://dr-prod.apex.proterra.com.s3-website-us-east-1.amazonaws.com/changePassword.html?username={username}\u0026temporaryPassword={####}\n                          \u003c/p\u003e\n                        \u003c/td\u003e\n                      \u003c/tr\u003e\n                    \u003c/table\u003e\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n          \u003ctr\u003e\n            \u003ctd\u003e\n              \u003ctable class=\"email-footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\"\u003e\n                \u003ctr\u003e\n                  \u003ctd class=\"content-cell\" align=\"center\"\u003e\n                    \u003cp class=\"sub align-center\"\u003e\u0026copy; 2019 Proterra. All rights reserved.\u003c/p\u003e\n\n                  \u003c/td\u003e\n                \u003c/tr\u003e\n              \u003c/table\u003e\n            \u003c/td\u003e\n          \u003c/tr\u003e\n        \u003c/table\u003e\n      \u003c/td\u003e\n    \u003c/tr\u003e\n  \u003c/table\u003e\n\u003c/body\u003e\n\n\u003c/html\u003e"

}



######### cognito client #########

variable "ams_cognito_app_client_name" {
  type = string
  default = ""
}


variable "ams_callback_urls" {
  type = list
  default = []
}
variable "ams_logout_urls" {
  type = list
  default = []
}

variable "ams_allowed_oauth_scopes" {
  type = list
  default = []
}
variable "ams_explicit_auth_flows" {
  type = list
  default = []
}
variable "ams_supported_identity_providers"  {
  type = list
  default = []
}


################ API #####################

variable "is_dr_purpose" {
	default = false
}

variable "templates_dr_path" {
  type = string
  default = "templates-dr"
}

variable "templates_exp_path" {
  type = string
  default = "templates-exp"
}

### CCSS 
variable "template_file_name_api_ccss" {
   type = string
   default = ""
}
variable "template_file_map_ccss" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_ccss" {
 type = map(string)
 default = {}
}
variable "dr_ccss_stage_variables_file" {
   type = string
   default = ""
}

### CIM
variable "template_file_name_api_cim" {
   type = string
   default = ""
}
variable "template_file_map_cim" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_cim" {
 type = map(string)
 default = {}
}
variable "dr_cim_stage_variables_file" {
   type = string
   default = ""
}

### IQ
variable "template_file_name_api_iq" {
   type = string
   default = ""
}
variable "template_file_map_iq" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_iq" {
 type = map(string)
 default = {}
}
variable "dr_iq_stage_variables_file" {
    type = string
   default = ""
}

### DATALAKE
variable "template_file_name_api_datalake" {
  type = string
   default = ""
}
variable "template_file_map_datalake" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_datalake" {
 type = map(string)
 default = {}
}
variable "dr_datalake_stage_variables_file" {
   type = string
   default = ""
}


### RUNS
variable "template_file_name_api_runs" {
   type = string
   default = ""
}
variable "template_file_map_runs" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_runs" {
 type = map(string)
 default = {}
}
variable "dr_runs_stage_variables_file" {
   type = string
   default = ""
}

### AMS
variable "template_file_name_api_ams" {
   type = string
   default = ""
}
variable "template_file_map_ams" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_ams" {
 type = map(string)
 default = {}
}
variable "dr_ams_stage_variables_file" {
   type = string
   default = ""
}

### MISCEL
variable "template_file_name_api_miscel" {
   type = string
   default = ""
}
variable "template_file_map_miscel" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_miscel" {
 type = map(string)
 default = {}
}
variable "dr_miscel_stage_variables_file" {
   type = string
   default = ""
}

### OCPP
variable "template_file_name_api_ocpp" {
   type = string
   default = ""
}
variable "template_file_map_ocpp" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_ocpp" {
 type = map(string)
 default = {}
}
variable "dr_ocpp_stage_variables_file" {
   type = string
   default = ""
}

### FILE
variable "template_file_name_api_file" {
   type = string
   default = ""
}
variable "template_file_map_file" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_file" {
 type = map(string)
 default = {}
}
variable "dr_file_stage_variables_file" {
   type = string
   default = ""
}

### FILE AMS
variable "template_file_name_api_fileams" {
   type = string
   default = ""
}
variable "template_file_map_fileams" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_fileams" {
 type = map(string)
 default = {}
}
variable "dr_fileams_stage_variables_file" {
   type = string
   default = ""
}

### MDC 
variable "template_file_name_api_mdc" {
   type = string
   default = ""
}
variable "template_file_map_mdc" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_mdc" {
 type = map(string)
 default = {}
}
variable "dr_mdc_stage_variables_file" {
   type = string
   default = ""
}


### STARPOINT
variable "template_file_name_api_starpoint" {
   type = string
   default = ""
}
variable "template_file_map_starpoint" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_starpoint" {
 type = map(string)
 default = {}
}
variable "dr_starpoint_stage_variables_file" {
   type = string
   default = ""
}


### MULTIPLEXER
variable "template_file_name_api_multiplexer" {
   type = string
   default = ""
}
variable "template_file_map_multiplexer" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_multiplexer" {
 type = map(string)
 default = {}
}
variable "dr_multiplexer_stage_variables_file" {
   type = string
   default = ""
}

### CONTROL PANEL 
variable "template_file_name_api_controlpanel" {
   type = string
   default = ""
}
variable "template_file_map_controlpanel" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_controlpanel" {
 type = map(string)
 default = {}
}
variable "dr_controlpanel_stage_variables_file" {
   type = string
   default = ""
}

### STARPOINT_LOGGER
variable "template_file_name_api_starpointlogger" {
   type = string
   default = ""
}
variable "template_file_map_starpointlogger" {
  type = map(string)
  default = {}
}
variable "local_stage_variables_starpointlogger" {
 type = map(string)
 default = {}
}
variable "dr_starpointlogger_stage_variables_file" {
   type = string
   default = ""
}


#############  API Keys #######

####  Usage Plan :  External Customer	API Key : E-dl  API : Proterra Data Lake

variable "is_usage_plan_required_ec" {
	default = false
}

variable "is_api_key_required_ec" {
	default = false
}
variable "is_associated_usage_plans_required_ec" {
	default = false
}

variable "local_usage_plan_map_file_name_ec" {
   type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_ec" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_ec" {
   type = string
   default = ""
}
variable "local_api_keys_data_ec" {
 type = map(string)
 default = {}
}

####  Usage Plan :  External_data_lake_production 	API Key : edmonton-datalake  API : Proterra Data Lake
variable "is_usage_plan_required_ed" {
	default = false
}

variable "is_api_key_required_ed" {
	default = false
}
variable "is_associated_usage_plans_required_ed" {
	default = false
}

variable "local_usage_plan_map_file_name_ed" {
    type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_ed" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_ed" {
   type = string
   default = ""
}
variable "local_api_keys_data_ed" {
 type = map(string)
 default = {}
}

####  Usage Plan :  GatewayCreateProd 	API Key : misc-api  API : Miscel API
variable "is_usage_plan_required_gcp" {
	default = false
}

variable "is_api_key_required_gcp" {
	default = false
}
variable "is_associated_usage_plans_required_gcp" {
	default = false
}

variable "local_usage_plan_map_file_name_gcp" {
   type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_gcp" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_gcp" {
   type = string
   default = ""
}
variable "local_api_keys_data_gcp" {
 type = map(string)
 default = {}
}

####  Usage Plan :  starpoint 	API Key : starpoint  API : starpoint

variable "is_usage_plan_required_sp" {
	default = false
}

variable "is_api_key_required_sp" {
	default = false
}
variable "is_associated_usage_plans_required_sp" {
	default = false
}

variable "local_usage_plan_map_file_name_sp" {
   type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_sp" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_sp" {
   type = string
   default = ""
}
variable "local_api_keys_data_sp" {
 type = map(string)
 default = {}
}

####  Usage Plan :  chargersessions 	API Key :  N/A  API : Proterra Data Lake

variable "is_usage_plan_required_cs" {
	default = false
}

variable "is_api_key_required_cs" {
	default = false
}
variable "is_associated_usage_plans_required_cs" {
	default = false
}

variable "local_usage_plan_map_file_name_cs" {
   type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_cs" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_cs" {
   type = string
   default = ""
}
variable "local_api_keys_data_cs" {
 type = map(string)
 default = {}
}

####  Usage Plan :  logger_usage_plan 	API Key :  N/A  API : Exp-Prod-Starpointlogger

variable "is_usage_plan_required_lup" {
	default = false
}

variable "is_api_key_required_lup" {
	default = false
}
variable "is_associated_usage_plans_required_lup" {
	default = false
}

variable "local_usage_plan_map_file_name_lup" {
   type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_lup" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_lup" {
   type = string
   default = ""
}
variable "local_api_keys_data_lup" {
 type = map(string)
 default = {}
}

####  Usage Plan : multiplexer-prod-fetch-charger-cp 	API Key :  N/A  API : Proterra AMS

variable "is_usage_plan_required_mp" {
	default = false
}

variable "is_api_key_required_mp" {
	default = false
}
variable "is_associated_usage_plans_required_mp" {
	default = false
}

variable "local_usage_plan_map_file_name_mp" {
   type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_mp" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_mp" {
   type = string
   default = ""
}
variable "local_api_keys_data_mp" {
 type = map(string)
 default = {}
}

####  Usage Plan : prod-mp-up 	API Key :  N/A  API : control-panel-prod

variable "is_usage_plan_required_mpup" {
	default = false
}

variable "is_api_key_required_mpup" {
	default = false
}
variable "is_associated_usage_plans_required_mpup" {
	default = false
}

variable "local_usage_plan_map_file_name_mpup" {
   type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_mpup" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_mpup" {
   type = string
   default = ""
}
variable "local_api_keys_data_mpup" {
 type = map(string)
 default = {}
}

####  Usage Plan : N/A  	API Key :  Dominion  API : N/A

variable "is_usage_plan_required_dominion" {
	default = false
}

variable "is_api_key_required_dominion" {
	default = false
}
variable "is_associated_usage_plans_required_dominion" {
	default = false
}

variable "local_usage_plan_map_file_name_dominion" {
   type = string
   default = ""
}
variable "dr_usage_plan_map_file_name_dominion" {
   type = string
   default = ""
}
variable "dr_api_keys_map_file_name_dominion" {
   type = string
   default = ""
}
variable "local_api_keys_data_dominion" {
 type = map(string)
 default = {}
}


############ SNS  #####
variable "mdc_sns_name" {
  type        = string
   default     = ""
}

##############  IOT Core ###############

# mdcmessagestosns

variable "iot_rule_mdc_messages_provisioning_name"   {
    type = string
    default =""
}
variable "iot_rule_mdc_messages_description" {
    type = string
    default =""
}
variable "iot_rule_mdc_messages_enabled" {
    default = true
}
variable "iot_rule_mdc_messages_sql_query"  {
    type = string
    default =""
}

variable "iot_rule_mdc_messages_sql_version"  {
    type = string
    default = "2016-03-23"
}


#mdcpresencetosns

variable "iot_rule_mdc_presence_provisioning_name"   {
    type = string
    default =""
}
variable "iot_rule_mdc_presence_description" {
    type = string
    default =""
}
variable "iot_rule_mdc_presence_enabled" {
    default = true
}
variable "iot_rule_mdc_presence_sql_query"  {
    type = string
    default =""
}

variable "iot_rule_mdc_presence_sql_version"  {
    type = string
    default = "2016-03-23"
}


#spprovisioning

variable "iot_rule_mdc_spprovisioning_provisioning_name"   {
    type = string
    default =""
}
variable "iot_rule_mdc_spprovisioning_description" {
    type = string
    default =""
}
variable "iot_rule_mdc_spprovisioning_enabled" {
    default = true
}
variable "iot_rule_mdc_spprovisioning_sql_query"  {
    type = string
    default =""
}

variable "iot_rule_mdc_spprovisioning_sql_version"  {
    type = string
    default = "2016-03-23"
}
