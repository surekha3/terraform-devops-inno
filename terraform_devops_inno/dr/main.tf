locals {
snapshot = "${var.snapshot_identifier}"
}
#####################################################
# Base Level Terraform Code.
# - Modify Account specific items.
#####################################################
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

terraform {
  required_version = ">= 0.11.11"
  backend "s3" {
  }
}

#################### S3 ######################

module "TF_Module_S3_toucan" {
  source = "../modules/TF_Module_S3_SIMPLE"
  s3_bucket_name             = var.toucan_s3_bucket_name
}

module "TF_Module_S3_backward_toucan" {
  source = "../modules/TF_Module_S3_SIMPLE"
  s3_bucket_name             = var.backward_toucan_s3_bucket_name
}
module "TF_Module_S3_toucan_data" {
  source = "../modules/TF_Module_S3_SIMPLE"
  s3_bucket_name             = var.toucan_data_s3_bucket_name
}

module "TF_Module_S3_data_connected" {
  source = "../modules/TF_Module_S3_SIMPLE"
  s3_bucket_name             = var.data_connected_s3_bucket_name
}

################### SQS #####################

module "TF_Module_SQS_ToucanBackward" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs1_name
  #sqs_visibility_timeout_seconds     = var.sqs1_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs1_message_retention_seconds
  #sqs_max_message_size               = var.sqs1_max_message_size
  #sqs_delay_seconds                  = var.sqs1_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs1_receive_wait_time_seconds
}

module "TF_Module_SQS_ToucanRawDataStream" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs2_name
  #sqs_visibility_timeout_seconds     = var.sqs2_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs2_message_retention_seconds
  #sqs_max_message_size               = var.sqs2_max_message_size
  #sqs_delay_seconds                  = var.sqs2_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs2_receive_wait_time_seconds
}
module "TF_Module_SQS_RoadRunnerDataStream" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs3_name
  #sqs_visibility_timeout_seconds     = var.sqs3_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs3_message_retention_seconds
  #sqs_max_message_size               = var.sqs3_max_message_size
  #sqs_delay_seconds                  = var.sqs3_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs3_receive_wait_time_seconds
}
module "TF_Module_SQS_BusShadowUpdates" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs4_name
  #sqs_visibility_timeout_seconds     = var.sqs4_visibility_timeout_seconds
  sqs_message_retention_seconds      = var.sqs4_message_retention_seconds
  #sqs_max_message_size               = var.sqs4_max_message_size
  #sqs_delay_seconds                  = var.sqs4_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs4_receive_wait_time_seconds
}
# MDC
module "TF_Module_SQS_MDC_DEVICE_STATUS" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs5_name
  sqs_visibility_timeout_seconds     = var.sqs5_visibility_timeout_seconds
  #sqs_message_retention_seconds      = var.sqs5_message_retention_seconds
  #sqs_max_message_size               = var.sqs5_max_message_size
  #sqs_delay_seconds                  = var.sqs5_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs5_receive_wait_time_seconds
}

module "TF_Module_SQS_MDC_DEVICE_MESSAGES" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs6_name
  sqs_visibility_timeout_seconds     = var.sqs6_visibility_timeout_seconds
  #sqs_message_retention_seconds      = var.sqs6_message_retention_seconds
  #sqs_max_message_size               = var.sqs6_max_message_size
  #sqs_delay_seconds                  = var.sqs6_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs6_receive_wait_time_seconds
}

module "TF_Module_SQS_MDC_JOBS" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs7_name
  #sqs_visibility_timeout_seconds     = var.sqs7_visibility_timeout_seconds
  #sqs_message_retention_seconds      = var.sqs7_message_retention_seconds
  #sqs_max_message_size               = var.sqs7_max_message_size
  #sqs_delay_seconds                  = var.sqs7_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs7_receive_wait_time_seconds
}

module "TF_Module_SQS_MDC_STATUS" {
  source = "../modules/TF_Module_SQS"
  sqs_env_tag                        = var.sqs_env_tag
  sqs_name                           = var.sqs8_name
  #sqs_visibility_timeout_seconds     = var.sqs8_visibility_timeout_seconds
  #sqs_message_retention_seconds      = var.sqs8_message_retention_seconds
  #sqs_max_message_size               = var.sqs8_max_message_size
  #sqs_delay_seconds                  = var.sqs8_delay_seconds
  #sqs_receive_wait_time_seconds      = var.sqs8_receive_wait_time_seconds
}


##################### Lambda #########################
# For GetPCSChargerStatus
module "TF_Module_Lambda_GetPCSChargerStatus" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_pcs_iam_role_name
 iam_assume_role_policy  = var.lambda_pcs_iam_assume_role_policy
 iam_role_policy = var.lambda_pcs_iam_role_policy
 #iam_role_policy = var.lambda_pcs_iam_role_policy
 policy_name = var.pcs_policy_name 
 path = var.pcs_path
 s3_bucket = var.pcs_s3_bucket
 s3_key = var.pcs_s3_key
 lambda_funcname = var.lambda_pcs_function_name
 timeout	  = var.lambda_pcs_timeout
 mem_size	  = var.lambda_pcs_mem_size
 handler	  = var.lambda_pcs_handler
 runtime    =var.lambda_pcs_runtime
 statement_id  =  var.pcs_statement_id
 action        =  var.pcs_action 
 principal     = var.pcs_principal
 source_arn = var.pcs_source_arn
}

# For getGlossaryFromS3
module "TF_Module_Lambda_GetGlossaryFromS3" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_get_glossy_iam_role_name
 iam_assume_role_policy = var.lambda_get_glossy_iam_assume_role_policy
 iam_role_policy  = var.lambda_get_glossy_iam_role_policy
 policy_name = var.get_glossy_policy_name
 path = var.get_glossy_path
 s3_bucket = var.get_glossy_s3_bucket
 s3_key = var.get_glossy_s3_key
 lambda_funcname = var.lambda_get_glossy_function_name
 timeout	  = var.lambda_get_glossy_timeout
 mem_size	  = var.lambda_get_glossy_mem_size
 handler	  = var.lambda_get_glossy_handler
 runtime    =var.lambda_get_glossy_runtime
 statement_id  =  var.get_glossy_statement_id
 action        =  var.get_glossy_action 
 principal     = var.get_glossy_principal
 source_arn = var.get_glossy_source_arn
}

# For saveGlossaryToS3
module "TF_Module_Lambda_saveGlossaryToS3" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_save_glossy_iam_role_name
 iam_assume_role_policy = var.lambda_save_glossy_iam_assume_role_policy
 iam_role_policy  = var.lambda_save_glossy_iam_role_policy
 path = var.save_glossy_path
 policy_name = var.save_glossy_policy_name
 s3_bucket = var.save_glossy_s3_bucket
 s3_key = var.save_glossy_s3_key
 lambda_funcname = var.lambda_save_glossy_function_name
 timeout	  = var.lambda_save_glossy_timeout
 mem_size	  = var.lambda_save_glossy_mem_size
 handler	  = var.lambda_save_glossy_handler
 runtime    =var.lambda_save_glossy_runtime
 statement_id  =  var.save_glossy_statement_id
 action        =  var.save_glossy_action 
 principal     = var.save_glossy_principal
 source_arn = var.save_glossy_source_arn
}


# For LogstashLambda
module "TF_Module_Lambda_LogstashLambda" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_logstash_iam_role_name
 iam_assume_role_policy = var.lambda_logstash_iam_assume_role_policy
 iam_role_policy  = var.lambda_logstash_iam_role_policy
 path = var.logstash_path
 policy_name = var.logstash_policy_name
 s3_bucket = var.logstash_s3_bucket
 s3_key = var.logstash_s3_key
 lambda_funcname = var.lambda_logstash_function_name
 timeout	  = var.lambda_logstash_timeout
 mem_size	  = var.lambda_logstash_mem_size
 handler	  = var.lambda_logstash_handler
 runtime    =var.lambda_logstash_runtime
 statement_id  =  var.logstash_statement_id
 action        =  var.logstash_action 
 principal     = var.logstash_principal
 source_arn = var.logstash_source_arn
}

# For ToucanRawProcessor
module "TF_Module_Lambda_ToucanRawProcessor" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_ToucanRawProcessor_iam_role_name
 iam_assume_role_policy = var.lambda_ToucanRawProcessor_iam_assume_role_policy
 iam_role_policy  = var.lambda_ToucanRawProcessor_iam_role_policy
 path = var.ToucanRawProcessor_path
 policy_name = var.ToucanRawProcessor_policy_name
 s3_bucket = var.ToucanRawProcessor_s3_bucket
 s3_key = var.ToucanRawProcessor_s3_key
 lambda_funcname = var.lambda_ToucanRawProcessor_function_name
 timeout	  = var.lambda_ToucanRawProcessor_timeout
 mem_size	  = var.lambda_ToucanRawProcessor_mem_size
 handler	  = var.lambda_ToucanRawProcessor_handler
 runtime    =var.lambda_ToucanRawProcessor_runtime
 statement_id  =  var.ToucanRawProcessor_statement_id
 action        =  var.ToucanRawProcessor_action 
 principal     = var.ToucanRawProcessor_principal
 source_arn = var.ToucanRawProcessor_source_arn
}

# For DecompressLambda
module "TF_Module_Lambda_DecompressLambda" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_DecompressLambda_iam_role_name
 iam_assume_role_policy = var.lambda_DecompressLambda_iam_assume_role_policy
 iam_role_policy  = var.lambda_DecompressLambda_iam_role_policy
 path = var.DecompressLambda_path
 policy_name = var.DecompressLambda_policy_name
 s3_bucket = var.DecompressLambda_s3_bucket
 s3_key = var.DecompressLambda_s3_key
 lambda_funcname = var.lambda_DecompressLambda_function_name
 timeout	  = var.lambda_DecompressLambda_timeout
 mem_size	  = var.lambda_DecompressLambda_mem_size
 handler	  = var.lambda_DecompressLambda_handler
 runtime    =var.lambda_DecompressLambda_runtime
 statement_id  =  var.DecompressLambda_statement_id
 action        =  var.DecompressLambda_action 
 principal     = var.DecompressLambda_principal
 source_arn = var.DecompressLambda_source_arn
}

# For BackwardToucanProcessor
module "TF_Module_Lambda_BackwardToucanProcessor" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_BackwardToucanProcessor_iam_role_name
 iam_assume_role_policy = var.lambda_BackwardToucanProcessor_iam_assume_role_policy
 iam_role_policy  = var.lambda_BackwardToucanProcessor_iam_role_policy
 path = var.BackwardToucanProcessor_path
 policy_name = var.BackwardToucanProcessor_policy_name
 s3_bucket = var.BackwardToucanProcessor_s3_bucket
 s3_key = var.BackwardToucanProcessor_s3_key
 lambda_funcname = var.lambda_BackwardToucanProcessor_function_name
 timeout	  = var.lambda_BackwardToucanProcessor_timeout
 mem_size	  = var.lambda_BackwardToucanProcessor_mem_size
 handler	  = var.lambda_BackwardToucanProcessor_handler
 runtime    =var.lambda_BackwardToucanProcessor_runtime
 statement_id  =  var.BackwardToucanProcessor_statement_id
 action        =  var.BackwardToucanProcessor_action 
 principal     = var.BackwardToucanProcessor_principal
 source_arn = var.BackwardToucanProcessor_source_arn
}

# For RoadRunnerDataProcessor
module "TF_Module_Lambda_RoadRunnerDataProcessor" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_RoadRunnerDataProcessor_iam_role_name
 iam_assume_role_policy = var.lambda_RoadRunnerDataProcessor_iam_assume_role_policy
 iam_role_policy  = var.lambda_RoadRunnerDataProcessor_iam_role_policy
 path = var.RoadRunnerDataProcessor_path
 policy_name = var.RoadRunnerDataProcessor_policy_name
 s3_bucket = var.RoadRunnerDataProcessor_s3_bucket
 s3_key = var.RoadRunnerDataProcessor_s3_key
 lambda_funcname = var.lambda_RoadRunnerDataProcessor_function_name
 timeout	  = var.lambda_RoadRunnerDataProcessor_timeout
 mem_size	  = var.lambda_RoadRunnerDataProcessor_mem_size
 handler	  = var.lambda_RoadRunnerDataProcessor_handler
 runtime    =var.lambda_RoadRunnerDataProcessor_runtime
 statement_id  =  var.RoadRunnerDataProcessor_statement_id
 action        =  var.RoadRunnerDataProcessor_action 
 principal     = var.RoadRunnerDataProcessor_principal
 source_arn = var.RoadRunnerDataProcessor_source_arn
}

# For realtimeIntoApex
module "TF_Module_Lambda_realtimeIntoApex" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_realtimeIntoApex_iam_role_name
 iam_assume_role_policy = var.lambda_realtimeIntoApex_iam_assume_role_policy
 iam_role_policy  = var.lambda_realtimeIntoApex_iam_role_policy
 path = var.realtimeIntoApex_path
 policy_name = var.realtimeIntoApex_policy_name
 s3_bucket = var.realtimeIntoApex_s3_bucket
 s3_key = var.realtimeIntoApex_s3_key
 lambda_funcname = var.lambda_realtimeIntoApex_function_name
 timeout	  = var.lambda_realtimeIntoApex_timeout
 mem_size	  = var.lambda_realtimeIntoApex_mem_size
 handler	  = var.lambda_realtimeIntoApex_handler
 runtime    =var.lambda_realtimeIntoApex_runtime
 statement_id  =  var.realtimeIntoApex_statement_id
 action        =  var.realtimeIntoApex_action 
 principal     = var.realtimeIntoApex_principal
 source_arn = var.realtimeIntoApex_source_arn
}

# For mdc-enrollment
module "TF_Module_Lambda_mdcEnrollment" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_mdcEnrollment_iam_role_name
 iam_assume_role_policy = var.lambda_mdcEnrollment_iam_assume_role_policy
 iam_role_policy  = var.lambda_mdcEnrollment_iam_role_policy
 path = var.mdcEnrollment_path
 policy_name = var.mdcEnrollment_policy_name
 s3_bucket = var.mdcEnrollment_s3_bucket
 s3_key = var.mdcEnrollment_s3_key
 lambda_funcname = var.lambda_mdcEnrollment_function_name
 timeout	  = var.lambda_mdcEnrollment_timeout
 mem_size	  = var.lambda_mdcEnrollment_mem_size
 handler	  = var.lambda_mdcEnrollment_handler
 runtime    =var.lambda_mdcEnrollment_runtime
 statement_id  =  var.mdcEnrollment_statement_id
 action        =  var.mdcEnrollment_action 
 principal     = var.mdcEnrollment_principal
 source_arn = var.mdcEnrollment_source_arn
 lambda_env = {
    API_ENDPOINT  = "https://mdc-device.proterra.com/api/v1.0/private/certificates/"
  }
}

# For prod_mdc_s3_edge_headers
module "TF_Module_Lambda_mdc_s3_edge_headers" {
 source = "../modules/TF_Module_LAMBDA"
 enable_role		= "true"
 iam_role_name =  var.lambda_mdcS3EdgeHeaders_iam_role_name
 iam_assume_role_policy = var.lambda_mdcS3EdgeHeaders_iam_assume_role_policy
 iam_role_policy  = var.lambda_mdcS3EdgeHeaders_iam_role_policy
 path = var.mdcS3EdgeHeaders_path
 policy_name = var.mdcS3EdgeHeaders_policy_name
 s3_bucket = var.mdcS3EdgeHeaders_s3_bucket
 s3_key = var.mdcS3EdgeHeaders_s3_key
 lambda_funcname = var.lambda_mdcS3EdgeHeaders_function_name
 timeout	  = var.lambda_mdcS3EdgeHeaders_timeout
 mem_size	  = var.lambda_mdcS3EdgeHeaders_mem_size
 handler	  = var.lambda_mdcS3EdgeHeaders_handler
 runtime    =var.lambda_mdcS3EdgeHeaders_runtime
 statement_id  =  var.mdcS3EdgeHeaders_statement_id
 action        =  var.mdcS3EdgeHeaders_action 
 principal     = var.mdcS3EdgeHeaders_principal
 source_arn = var.mdcS3EdgeHeaders_source_arn
}

#####
data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}
/*
provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.11"
}
*/
data "aws_availability_zones" "available" {
}

locals {
  cluster_name = "Dr-EKS-Prod"
}


#################### VPC ####################

module "vpc" {
  source = "../modules/TF_Module_VPC"

  vpc_cidr              = var.vpc_cidr
  vpc_public_subnets    = var.vpc_public_subnets
  vpc_private_subnets   = var.vpc_private_subnets
  vpc_availabilityzones = var.vpc_availabilityzones
  vpc_account_owner_ip  = var.vpc_account_owner_ip

  vpc_tag_project       = var.project
  vpc_tag_environment   = var.environment
  subnet_additional_tags   = var.subnet_additional_tags
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "12.2.0"

  cluster_name    = "${local.cluster_name}"
  cluster_version = "1.18"
  subnets         = module.vpc.vpc_private_subnet_ids

  vpc_id = module.vpc.vpc_id

  node_groups = {
    first = {
      desired_capacity = 4
      max_capacity     = 10
      min_capacity     = 1

      instance_type = "r5.xlarge"
    }
  }

  write_kubeconfig   = true
  config_output_path = "./"
}

######################################################################
#EDMONTON
######################################################################
module "TF_Module_S3_Buckets_APEX_UI" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.apex_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.apex_ui_s3_bucket_policy_file_name
}

module "TF_Module_S3_Buckets_CCSS_UI" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = var.ccss_ui_s3_bucket_name
  s3_bucket_policy_file_name = var.ccss_ui_s3_bucket_policy_file_name
}

module "apex-cdn" {
   source 	      = "../modules/cloudfront"
   aliases            = ["my-connected.proterra.com"]
   origin_domain_name = "dr-prod.apex.proterra.com.s3.amazonaws.com" 
   origin_id  	      = "S3-dr-prod.apex.proterra.com"
}

module "ccss-cdn" {
   source             = "../modules/cloudfront"
   aliases            = ["my-ccss-connected.proterra.com"]
   origin_domain_name = "dr-prod.ccss.proterra.com.s3.amazonaws.com"
   origin_id          = "S3-dr-prod.ccss.proterra.com"
}

module "proterra-prod-db" {
  source                          = "../modules/TF_Module_RDS"
  enabled                         = "true"
  rds_storage_encrypted           = "true"
  rds_availabilityzones           = ["us-east-1a","us-east-1b","us-east-1e"]
  env_short                       = "dr"
  rds_engine_version              = "5.6.mysql_aurora.1.22.2"
  rds_identifier                  = "dr-db-1"
  rds_security_group_name         = "dr-proterra-rds-sg"
  rds_security_group_description  = "rds Security Group for DR database cluster"
  rds_copy_tags_to_snapshot       = "false"
  rds_parameter_group_name        = "defaultaurora56"
  rds_db_name                     = "drdb-1"
  rds_skip_final_snapshot         = "false"
  project                         = "proterra-dr"
  rds_snapshot_identifier         = local.snapshot
  rds_instance_class              = "db.r5.large"
  rds_vpc_id                      = module.vpc.vpc_id
  rds_vpc_private_subnet_ids      =  module.vpc.vpc_private_subnet_ids
  rds_subnet_group_name            = "dr-rds-private-subnet"
}

module "encrypted-rds-cluster" {
  source                          = "../modules/TF_Module_RDS"
  enabled                         = "true"
  rds_storage_encrypted           = "true"
  rds_availabilityzones           = ["us-east-1a","us-east-1b","us-east-1e"]
  env_short                       = "dr"
  rds_engine_version              = "5.6.mysql_aurora.1.22.2"
  rds_identifier                  = "dr-db-2"
  rds_security_group_name         = "dr-proterra-rds-sg-1"
  rds_security_group_description  = "rds Security Group for DR database cluster"
  rds_copy_tags_to_snapshot       = "false"
  rds_parameter_group_name        = "defaultaurora56"
  rds_db_name                     = "drdb-2"
  rds_skip_final_snapshot         = "false"
  project                         = "proterra-dr"
  rds_snapshot_identifier         = local.snapshot
  rds_instance_class              = "db.r5.large"
  rds_vpc_id                      = module.vpc.vpc_id
  rds_vpc_private_subnet_ids      =  module.vpc.vpc_private_subnet_ids
  rds_subnet_group_name            = "dr-rds-private-subnet-1"
}


##########################

module "TF_Module_COGNITO_AMS" {
  source = "../modules/TF_Module_COGNITO_AMS"

  cognito_user_pool_name          = var.ams_cognito_user_pool_name
  cognito_email_verif_subject     = var.ams_cognito_email_verif_subject
  cognito_sms_verif_message       = var.ams_cognito_sms_verif_message
  cognito_email_verif_message     = var.ams_cognito_email_verif_message
  cognito_unused_account_days     = var.ams_cognito_unused_account_days
  cognito_email_subject           = var.ams_cognito_email_subject
  cognito_sms_email_message       = var.ams_cognito_sms_email_message
  cognito_app_client_name         = var.ams_cognito_app_client_name
  callback_urls                   = var.ams_callback_urls
  logout_urls                     = var.ams_logout_urls
  allowed_oauth_scopes            = var.ams_allowed_oauth_scopes
  explicit_auth_flows             = var.ams_explicit_auth_flows
  supported_identity_providers    = var.ams_supported_identity_providers

}



###########################
#MDC
###########################
module "mdc_ui" {
  source = "../modules/TF_Module_S3_Buckets"
  s3_bucket_name             = "dr-mdc-prod-webapp.proterra.com"
  s3_bucket_policy_file_name = "files-s3/mdc_ui_s3_bucket_policy.json"
}

module "mdc-cdn" {
   source             = "../modules/cloudfront"
   aliases            = ["my-mdc.proterra.com"]
   origin_domain_name = "dr-mdc-prod-webapp.proterra.com.s3.amazonaws.com"
   origin_id          = "S3-dr-mdc-prod-webapp.proterra.com"
}

module "mdc-redis" {
  source               = "../modules/redis"
  vpc_id               = module.vpc.vpc_id
  cluster_id           = "redis-mdc-dr"
  node_type            = "cache.t2.micro"
  num_cache_nodes      = "1"
  parameter_group_name = "default.redis5.0"
  engine_version       = "5.0.6"
  port_number          = "6397"
  cluster_subnet_group = "redis-mdc-dr-subnet-group"
  all-subnets-list     =  module.vpc.vpc_private_subnet_ids
  cluster_security_group_name = "redis-mdc-dr-sg"
}

module "mdc-sqs-status-updates" {
  source                                = "../modules/TF_Module_SQS"
  sqs_env_tag                           = var.sqs_env_tag
  sqs_name                              = "dr-mdc-status-updates"
  sqs_message_retention_seconds         = "345600"
  sqs_max_message_size                  = "262144"
  sqs_delay_seconds                     = "0"
  sqs_receive_wait_time_seconds         = "0"
  sqs_visibility_timeout_seconds         = "30"

}

module "mdc-sqs-status" {
  source                                = "../modules/TF_Module_SQS"
  sqs_env_tag                           = var.sqs_env_tag
  sqs_name                              = "dr-mdc-status"
  sqs_message_retention_seconds         = "345600"
  sqs_max_message_size                  = "262144"
  sqs_delay_seconds                     = "0"
  sqs_receive_wait_time_seconds         = "0"
  sqs_visibility_timeout_seconds         = "30"
 
}

module "mdc-sqs-apex-status-updates" {
  source                                = "../modules/TF_Module_SQS"
  sqs_env_tag                           = var.sqs_env_tag
  sqs_name                              = "dr-mdc-apex-status-updates"
  sqs_message_retention_seconds         = "345600"
  sqs_max_message_size                  = "262144"
  sqs_delay_seconds                     = "0"
  sqs_receive_wait_time_seconds         = "0"
  sqs_visibility_timeout_seconds         = "30"
}

module "mdc-sqs-device-status" {
  source                                = "../modules/TF_Module_SQS"
  sqs_env_tag                           = var.sqs_env_tag
  sqs_name                              = "dr-mdc-device-status"
  sqs_message_retention_seconds         = "345600"
  sqs_max_message_size                  = "262144"
  sqs_delay_seconds                     = "0"
  sqs_receive_wait_time_seconds         = "0"
  sqs_visibility_timeout_seconds         = "30"
}
module "mdc-sqs-devices-messagess" {
  source                                = "../modules/TF_Module_SQS"
  sqs_env_tag                           = var.sqs_env_tag
  sqs_name                              = "dr-mdc-devices-messagess"
  sqs_message_retention_seconds         = "345600"
  sqs_max_message_size                  = "262144"
  sqs_delay_seconds                     = "0"
  sqs_receive_wait_time_seconds         = "0"
  sqs_visibility_timeout_seconds         = "30"
}
module "mdc-sqs-jobs" {
  source                                = "../modules/TF_Module_SQS"
  sqs_env_tag                           = var.sqs_env_tag
  sqs_name                              = "dr-mdc-jobs"
  sqs_message_retention_seconds         = "345600"
  sqs_max_message_size                  = "262144"
  sqs_delay_seconds                     = "0"
  sqs_receive_wait_time_seconds         = "0"
  sqs_visibility_timeout_seconds         = "30"
}


###############################
#EOL
###############################
module "eol_commissioning" {
 source               = "../modules/TF_Module_LAMBDA"
 #lambda_filename     = var.lambda_eol_filename
 iam_role_name 	      = var.lambda_eol_iam_role_name
 iam_role_policy      = var.lambda_eol_iam_role_policy
 statement_id         =  var.eol_statement_id
 s3_bucket            = var.eol_s3_bucket
 s3_key               = var.eol_s3_key
 action		      = var.eol_commissioning_action 
 principal 	      = var.eol_commissioning_principal 
 source_arn 	      = var.eol_commissioning_source_arn 
 lambda_funcname = var.lambda_eol_function_name
 timeout              = var.lambda_eol_timeout
 mem_size             = var.lambda_eol_mem_size
 handler              = var.lambda_eol_handler
 runtime              = var.lambda_eol_runtime
 lambda_env = {
    APEX_ASSET_URL      = "https://api.proterra.com/ams/v1/tenants/31e12c78-dffc-4d87-a633-d36b6597e63b/chargers"
    APEX_LOGIN_URL       = "https://api.proterra.com/ams/v1/cognitoLogin"
    APEX_PASSWORD        = "WLqy`=4F+*z!3pfu1"
    APEX_USERNAME        = "api@proterra.com"
    ATT_AUTH             = "Basic Q2hhcmdlckVPTDpiZjQ0YzEyYS03YWM2LTQxMzEtOTdjMi02NmQyNzMzMTk0NzA="
    ATT_URL              = "https://api-iotdevice.att.com/rws/api/v1/devices/"
    CP_URL               = "https://www.cradlepointecm.com/api/v2/routers/"
    DEFAULT_CHARGER_SIZE = "1000"
    GROUP                = "138656"
    SP_GROUP             = "191453"
    X_CP_API_ID          = "e6545c62"
    X_CP_API_KEY         = "fac4495ef69ef9501cf5e3d63db0d758"
    X_ECM_API_ID         = "0d1ec983-8de9-408b-9aac-e9319d0ee2de"
    X_ECM_API_KEY        = "b2fe0563243c6ce1a563801553cfb80179fea41d"
  }
}
#####################  REST APIs ############

### CCSS  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "ccssstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_ccss_stage_variables_file
} 
data "external" "ccssjson" {
  program = ["echo", "${data.aws_s3_bucket_object.ccssstagevariables.body}"]
}
module "TF_Module_API_CCSS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_ccss
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_ccss,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_ccss
  api_stage_name = var.template_file_map_ccss["stageName"]
  api_name = var.template_file_map_ccss["title"]
  api_description = var.template_file_map_ccss["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.ccssjson.result}" : var.local_stage_variables_ccss
}

### CIM  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "cimstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_cim_stage_variables_file
} 
data "external" "cimjson" {
  program = ["echo", "${data.aws_s3_bucket_object.cimstagevariables.body}"]
}
module "TF_Module_API_CIM" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_cim
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_cim,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_cim
  api_stage_name = var.template_file_map_cim["stageName"]
  api_name = var.template_file_map_cim["title"]
  api_description = var.template_file_map_cim["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.cimjson.result}" : var.local_stage_variables_cim
}

### IQ  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "iqstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_iq_stage_variables_file
} 
data "external" "iqjson" {
  program = ["echo", "${data.aws_s3_bucket_object.iqstagevariables.body}"]
}
module "TF_Module_API_IQ" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_iq
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_iq,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_iq
  api_stage_name = var.template_file_map_iq["stageName"]
  api_name = var.template_file_map_iq["title"]
  api_description = var.template_file_map_iq["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.iqjson.result}" : var.local_stage_variables_iq
}

### DataLake  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "datalakestagevariables" {
  bucket = var.state_bucket
  key    = var.dr_datalake_stage_variables_file
} 
data "external" "datalakejson" {
  program = ["echo", "${data.aws_s3_bucket_object.datalakestagevariables.body}"]
}
module "TF_Module_API_DATALAKE" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_datalake
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_datalake,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_datalake
  api_stage_name = var.template_file_map_datalake["stageName"]
  api_name = var.template_file_map_datalake["title"]
  api_description = var.template_file_map_datalake["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.datalakejson.result}" : var.local_stage_variables_datalake
}

### RUNS  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "runsstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_runs_stage_variables_file
} 
data "external" "runsjson" {
  program = ["echo", "${data.aws_s3_bucket_object.runsstagevariables.body}"]
}
module "TF_Module_API_RUNS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_runs
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_runs,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_runs
  api_stage_name = var.template_file_map_runs["stageName"]
  api_name = var.template_file_map_runs["title"]
  api_description = var.template_file_map_runs["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.runsjson.result}" : var.local_stage_variables_runs
}

### AMS  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "amsstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_ams_stage_variables_file
} 
data "external" "amsjson" {
  program = ["echo", "${data.aws_s3_bucket_object.amsstagevariables.body}"]
}
module "TF_Module_API_AMS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_ams
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_ams,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_ams
  api_stage_name = var.template_file_map_ams["stageName"]
  api_name = var.template_file_map_ams["title"]
  api_description = var.template_file_map_ams["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.amsjson.result}" : var.local_stage_variables_ams
}


### Miscel  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "miscelstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_miscel_stage_variables_file
} 
data "external" "misceljson" {
  program = ["echo", "${data.aws_s3_bucket_object.miscelstagevariables.body}"]
}
module "TF_Module_API_MISCEL" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_miscel
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_miscel,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_miscel
  api_stage_name = var.template_file_map_miscel["stageName"]
  api_name = var.template_file_map_miscel["title"]
  api_description = var.template_file_map_miscel["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.misceljson.result}" : var.local_stage_variables_miscel
}


### OCPP  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "ocppstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_ocpp_stage_variables_file
} 
data "external" "ocppjson" {
  program = ["echo", "${data.aws_s3_bucket_object.ocppstagevariables.body}"]
}
module "TF_Module_API_OCPP" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_ocpp
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_ocpp,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_ocpp
  api_stage_name = var.template_file_map_ocpp["stageName"]
  api_name = var.template_file_map_ocpp["title"]
  api_description = var.template_file_map_ocpp["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.ocppjson.result}" : var.local_stage_variables_ocpp
}

### FILE  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "filestagevariables" {
  bucket = var.state_bucket
  key    = var.dr_file_stage_variables_file
} 
data "external" "filejson" {
  program = ["echo", "${data.aws_s3_bucket_object.filestagevariables.body}"]
}
module "TF_Module_API_FILE" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_file
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_file,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_file
  api_stage_name = var.template_file_map_file["stageName"]
  api_name = var.template_file_map_file["title"]
  api_description = var.template_file_map_file["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.filejson.result}" : var.local_stage_variables_file
}

### FILE  AMS ###
# Collect stage varaibles
data "aws_s3_bucket_object" "fileamsstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_fileams_stage_variables_file
} 
data "external" "fileamsjson" {
  program = ["echo", "${data.aws_s3_bucket_object.fileamsstagevariables.body}"]
}
module "TF_Module_API_FILEAMS" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_fileams
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_fileams,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_fileams
  api_stage_name = var.template_file_map_fileams["stageName"]
  api_name = var.template_file_map_fileams["title"]
  api_description = var.template_file_map_fileams["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.fileamsjson.result}" : var.local_stage_variables_fileams
}


### MDC  AMS ###
# Collect stage varaibles
data "aws_s3_bucket_object" "mdcstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_mdc_stage_variables_file
} 
data "external" "mdcjson" {
  program = ["echo", "${data.aws_s3_bucket_object.mdcstagevariables.body}"]
}
module "TF_Module_API_MDC" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_mdc
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_mdc,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_mdc
  api_stage_name = var.template_file_map_mdc["stageName"]
  api_name = var.template_file_map_mdc["title"]
  api_description = var.template_file_map_mdc["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.mdcjson.result}" : var.local_stage_variables_mdc
}

### STAR POINT  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "starpointstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_starpoint_stage_variables_file
} 
data "external" "starpointjson" {
  program = ["echo", "${data.aws_s3_bucket_object.starpointstagevariables.body}"]
}
module "TF_Module_API_STARPOINT" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_starpoint
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_starpoint,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_starpoint
  api_stage_name = var.template_file_map_starpoint["stageName"]
  api_name = var.template_file_map_starpoint["title"]
  api_description = var.template_file_map_starpoint["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.starpointjson.result}" : var.local_stage_variables_starpoint
}

### multiplexer-rest-prod  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "multiplexerstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_multiplexer_stage_variables_file
} 
data "external" "multiplexerjson" {
  program = ["echo", "${data.aws_s3_bucket_object.multiplexerstagevariables.body}"]
}
module "TF_Module_API_MULTIPLEXER" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_multiplexer
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_multiplexer,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_multiplexer
  api_stage_name = var.template_file_map_multiplexer["stageName"]
  api_name = var.template_file_map_multiplexer["title"]
  api_description = var.template_file_map_multiplexer["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.multiplexerjson.result}" : var.local_stage_variables_multiplexer
}


### control-panel-prod ###
# Collect stage varaibles
data "aws_s3_bucket_object" "controlpanelstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_controlpanel_stage_variables_file
} 
data "external" "controlpaneljson" {
  program = ["echo", "${data.aws_s3_bucket_object.controlpanelstagevariables.body}"]
}
module "TF_Module_API_MP_CONTROL_PANEL" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_controlpanel
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_controlpanel,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_controlpanel
  api_stage_name = var.template_file_map_controlpanel["stageName"]
  api_name = var.template_file_map_controlpanel["title"]
  api_description = var.template_file_map_controlpanel["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.controlpaneljson.result}" : var.local_stage_variables_controlpanel
}

### Exp-Prod-Starpointlogger  ###
# Collect stage varaibles
data "aws_s3_bucket_object" "starpointloggerstagevariables" {
  bucket = var.state_bucket
  key    = var.dr_starpointlogger_stage_variables_file
} 
data "external" "starpointloggerjson" {
  program = ["echo", "${data.aws_s3_bucket_object.starpointloggerstagevariables.body}"]
}
module "TF_Module_API_STARPOINT_LOGGER" {
  source = "../modules/TF_Module_API"
  template_file_name = var.template_file_name_api_starpointlogger
  template_file_map= var.is_dr_purpose ? merge(var.template_file_map_starpointlogger,
  {
   #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_ALjhxO9eZ"
   providerARNs = "${module.TF_Module_COGNITO_AMS.cognito_pool_arn}"
  }
  ) : var.template_file_map_starpointlogger
  api_stage_name = var.template_file_map_starpointlogger["stageName"]
  api_name = var.template_file_map_starpointlogger["title"]
  api_description = var.template_file_map_starpointlogger["description"]
  exp_api_or_prod= var.is_dr_purpose ? var.templates_dr_path : var.templates_exp_path 
  stage_variables= var.is_dr_purpose ? "${data.external.starpointloggerjson.result}" : var.local_stage_variables_starpointlogger
}

################  API Keys ###############

####  Usage Plan :  External Customer  	API Key : E-dl  API : Proterra Data Lake


# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplanec" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_ec
} 
data "external" "drmapusageplanec" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplanec.body}"]
}
# Local usage plan 
data "external" "localmapusageplanec" {
  program = ["cat", "${var.local_usage_plan_map_file_name_ec}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeyec" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_ec
} 
data "external" "drmapapikeyec" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeyec.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_EXTERNAL_CUSTOMER" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_ec
  is_api_key_required = var.is_api_key_required_ec
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_ec

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplanec.result}" : "${data.external.localmapusageplanec.result}"
  dr_usage_plan_apiStages_data =  [
        {
            "apiId": "${module.TF_Module_API_DATALAKE.api_gateway_rest_api_id}",
            "stage": "${element(split("/",module.TF_Module_API_DATALAKE.api_gateway_stage_url),length(split("/",module.TF_Module_API_DATALAKE.api_gateway_stage_url))-1)}"
        }    
    ]

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeyec.result}" : var.local_api_keys_data_ec
  
}


####  Usage Plan :  External_data_lake_production 	API Key : edmonton-datalake  API : Proterra Data Lake
# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplaned" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_ed
} 
data "external" "drmapusageplaned" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplaned.body}"]
}
# Local usage plan 
data "external" "localmapusageplaned" {
  program = ["cat", "${var.local_usage_plan_map_file_name_ed}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeyed" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_ed
} 
data "external" "drmapapikeyed" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeyed.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_EXTERNAL_DATALAKE_PROD" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_ed
  is_api_key_required = var.is_api_key_required_ed
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_ed

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplaned.result}" : "${data.external.localmapusageplaned.result}"
  dr_usage_plan_apiStages_data =  [
        {
            "apiId": "${module.TF_Module_API_DATALAKE.api_gateway_rest_api_id}",
            "stage": "${element(split("/",module.TF_Module_API_DATALAKE.api_gateway_stage_url),length(split("/",module.TF_Module_API_DATALAKE.api_gateway_stage_url))-1)}"
        }    
    ]

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeyed.result}" : var.local_api_keys_data_ed
  
}

####  Usage Plan :  GatewayCreateProd 	API Key : misc-api  API : Miscel API

# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplangcp" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_gcp
} 
data "external" "drmapusageplangcp" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplangcp.body}"]
}
# Local usage plan 
data "external" "localmapusageplangcp" {
  program = ["cat", "${var.local_usage_plan_map_file_name_gcp}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeygcp" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_gcp
} 
data "external" "drmapapikeygcp" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeygcp.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_GATEWAY_CREATE_PROD" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_gcp
  is_api_key_required = var.is_api_key_required_gcp
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_gcp

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplangcp.result}" : "${data.external.localmapusageplangcp.result}"
  dr_usage_plan_apiStages_data =  [
        {
            "apiId": "${module.TF_Module_API_MISCEL.api_gateway_rest_api_id}",
            "stage": "${element(split("/",module.TF_Module_API_MISCEL.api_gateway_stage_url),length(split("/",module.TF_Module_API_MISCEL.api_gateway_stage_url))-1)}"
        }    
    ]

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeygcp.result}" : var.local_api_keys_data_gcp
  
}

####  Usage Plan :  starpoint 	API Key : starpoint  API : starpoint

# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplansp" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_sp
} 
data "external" "drmapusageplansp" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplansp.body}"]
}
# Local usage plan 
data "external" "localmapusageplansp" {
  program = ["cat", "${var.local_usage_plan_map_file_name_sp}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeysp" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_sp
} 
data "external" "drmapapikeysp" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeysp.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_STARPOINT" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_sp
  is_api_key_required = var.is_api_key_required_sp
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_sp

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplansp.result}" : "${data.external.localmapusageplansp.result}"
  dr_usage_plan_apiStages_data =  [
        {
            "apiId": "${module.TF_Module_API_STARPOINT.api_gateway_rest_api_id}",
            "stage": "${element(split("/",module.TF_Module_API_STARPOINT.api_gateway_stage_url),length(split("/",module.TF_Module_API_STARPOINT.api_gateway_stage_url))-1)}"
        }    
    ]

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeysp.result}" : var.local_api_keys_data_sp
  
}

####  Usage Plan :  chargersessions 	API Key :  N/A  API : Proterra Data Lake

# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplancs" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_cs
} 
data "external" "drmapusageplancs" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplancs.body}"]
}
# Local usage plan 
data "external" "localmapusageplancs" {
  program = ["cat", "${var.local_usage_plan_map_file_name_cs}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeycs" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_cs
} 
data "external" "drmapapikeycs" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeycs.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_CHARGER_SESSIONS" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_cs
  is_api_key_required = var.is_api_key_required_cs
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_cs

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplancs.result}" : "${data.external.localmapusageplancs.result}"
  dr_usage_plan_apiStages_data =  [
        {
            "apiId": "${module.TF_Module_API_DATALAKE.api_gateway_rest_api_id}",
            "stage": "${element(split("/",module.TF_Module_API_DATALAKE.api_gateway_stage_url),length(split("/",module.TF_Module_API_DATALAKE.api_gateway_stage_url))-1)}"
        }    
    ]

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeycs.result}" : var.local_api_keys_data_cs
  
}


####  Usage Plan :  logger_usage_plan 	API Key :  N/A  API : Exp-Prod-Starpointlogger

# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplanlup" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_lup
} 
data "external" "drmapusageplanlup" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplanlup.body}"]
}
# Local usage plan 
data "external" "localmapusageplanlup" {
  program = ["cat", "${var.local_usage_plan_map_file_name_lup}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeylup" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_lup
} 
data "external" "drmapapikeylup" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeylup.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_LOGGER_USAGE_PLAN" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_lup
  is_api_key_required = var.is_api_key_required_lup
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_lup

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplanlup.result}" : "${data.external.localmapusageplanlup.result}"
  dr_usage_plan_apiStages_data =  [
        {
            "apiId": "${module.TF_Module_API_STARPOINT_LOGGER.api_gateway_rest_api_id}",
            "stage": "${element(split("/",module.TF_Module_API_STARPOINT_LOGGER.api_gateway_stage_url),length(split("/",module.TF_Module_API_DATALAKE.api_gateway_stage_url))-1)}"
        }    
    ]

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeylup.result}" : var.local_api_keys_data_lup
  
}

####  Usage Plan : multiplexer-prod-fetch-charger-cp 	API Key :  N/A  API : Proterra AMS

# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplanmp" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_mp
} 
data "external" "drmapusageplanmp" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplanmp.body}"]
}
# Local usage plan 
data "external" "localmapusageplanmp" {
  program = ["cat", "${var.local_usage_plan_map_file_name_mp}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeymp" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_mp
} 
data "external" "drmapapikeymp" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeymp.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_MP_FETCH_CHARGER_CP" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_mp
  is_api_key_required = var.is_api_key_required_mp
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_mp

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplanmp.result}" : "${data.external.localmapusageplanmp.result}"
  dr_usage_plan_apiStages_data =  [
        {
            "apiId": "${module.TF_Module_API_AMS.api_gateway_rest_api_id}",
            "stage": "${element(split("/",module.TF_Module_API_AMS.api_gateway_stage_url),length(split("/",module.TF_Module_API_AMS.api_gateway_stage_url))-1)}"
        }    
    ]

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeymp.result}" : var.local_api_keys_data_mp
  
}


####  Usage Plan : prod-mp-up 	API Key :  N/A  API : control-panel-prod

# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplanmpup" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_mpup
} 
data "external" "drmapusageplanmpup" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplanmpup.body}"]
}
# Local usage plan 
data "external" "localmapusageplanmpup" {
  program = ["cat", "${var.local_usage_plan_map_file_name_mpup}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeympup" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_mpup
} 
data "external" "drmapapikeympup" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeympup.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_MP_UP" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_mpup
  is_api_key_required = var.is_api_key_required_mpup
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_mpup

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplanmpup.result}" : "${data.external.localmapusageplanmpup.result}"
  dr_usage_plan_apiStages_data =  [
        {
            "apiId": "${module.TF_Module_API_MP_CONTROL_PANEL.api_gateway_rest_api_id}",
            "stage": "${element(split("/",module.TF_Module_API_MP_CONTROL_PANEL.api_gateway_stage_url),length(split("/",module.TF_Module_API_MP_CONTROL_PANEL.api_gateway_stage_url))-1)}"
        }    
    ]

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeympup.result}" : var.local_api_keys_data_mpup
  
}


####  Usage Plan : N/A  	API Key :  Dominion  API : N/A

# DR Purpose usage plan
data "aws_s3_bucket_object" "usageplandominion" {
  bucket = var.state_bucket
  key    = var.dr_usage_plan_map_file_name_dominion
} 
data "external" "drmapusageplandominion" {
  program = ["echo", "${data.aws_s3_bucket_object.usageplandominion.body}"]
}
# Local usage plan 
data "external" "localmapusageplandominion" {
  program = ["cat", "${var.local_usage_plan_map_file_name_dominion}"]
}

# DR api keys
data "aws_s3_bucket_object" "apikeydominion" {
  bucket = var.state_bucket
  key    = var.dr_api_keys_map_file_name_dominion
} 
data "external" "drmapapikeydominion" {
  program = ["echo", "${data.aws_s3_bucket_object.apikeydominion.body}"]
}
module "TF_Module_API_KEYS_USAGEPLAN_DOMINION" {
  source = "../modules/TF_Module_API_KEYS"
  
  is_dr = var.is_dr_purpose ? true : false 
  is_usage_plan_required = var.is_usage_plan_required_dominion
  is_api_key_required = var.is_api_key_required_dominion
  is_associated_usage_plans_required =  var.is_associated_usage_plans_required_dominion

  #Usgae plan 
  usage_plan_map_data= var.is_dr_purpose ? "${data.external.drmapusageplandominion.result}" : "${data.external.localmapusageplandominion.result}"
  dr_usage_plan_apiStages_data =  []

  # API Keys 
  api_keys_map_data=var.is_dr_purpose ? "${data.external.drmapapikeydominion.result}" : var.local_api_keys_data_dominion
  
}

########################## SNS ################

module "TF_Module_SNS_MDC" {
  source = "../modules/TF_Module_SNS"

  sns_name=var.mdc_sns_name
  sns_subscription_arn_list = [
    {
      protocol = "sqs"
      arn      = module.TF_Module_SQS_MDC_DEVICE_STATUS.sqs_queue_arn
    }    
    ]
}

############### IoT Rules ##########
#mdcmessagestosns
module "TF_Module_IOT_MDC_MESSAGE_SNS" {
  source = "../modules/TF_Module_IOT"

  iot_rule_provisioning_name = var.iot_rule_mdc_messages_provisioning_name 
  iot_rule_description= var.iot_rule_mdc_messages_description
  iot_rule_sql_query=var.iot_rule_mdc_messages_sql_query

  iot_rule_action_sns =  [
        {
        "message_format" = "RAW",
        "target_arn"     = module.TF_Module_SNS_MDC.sns_arn         
        }    
    ]
}

#mdcpresencetosns
module "TF_Module_IOT_MDC_PRESENCE_SNS" {
  source = "../modules/TF_Module_IOT"

  iot_rule_provisioning_name = var.iot_rule_mdc_presence_provisioning_name 
  iot_rule_description= var.iot_rule_mdc_presence_description
  iot_rule_sql_query=var.iot_rule_mdc_presence_sql_query

  iot_rule_action_sns =  [
        {
        "message_format" = "RAW",
        "target_arn"     = module.TF_Module_SNS_MDC.sns_arn         
        }    
    ]
}
#spprovisioning

module "TF_Module_IOT_MDC_SPPROVISIONING_SNS" {
  source = "../modules/TF_Module_IOT"

  iot_rule_provisioning_name = var.iot_rule_mdc_spprovisioning_provisioning_name 
  iot_rule_description= var.iot_rule_mdc_spprovisioning_description
  iot_rule_sql_query=var.iot_rule_mdc_spprovisioning_sql_query

  iot_rule_action_lambda =  [
        {
       "arn"     = module.TF_Module_Lambda_mdcEnrollment.lambda_arn
       # "arn"     = "arn:aws:lambda:us-east-1:029126476216:function:stage-mdc-enrollment"        
        }    
    ]
}

