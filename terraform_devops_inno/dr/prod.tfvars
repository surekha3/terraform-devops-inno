# Base.tfvars
aws_region              = "us-east-1"
aws_profile             = "proterra-prod"
state_bucket          	= "dr-foundation"

####### S3 ####
s3_bucket_env                      = "dr-prod"
toucan_s3_bucket_name              = "dr-toucan.proterra.com"
backward_toucan_s3_bucket_name     = "dr-backward.toucan.proterra.com"
toucan_data_s3_bucket_name         = "dr-toucan-data.proterra.com"
data_connected_s3_bucket_name      = "dr-data.connected.proterra.com"
############## SQS ##############

# The default for visibility_timeout_seconds  is 30
# The default for message_retention_seconds is 345600 (4 days).
# The default for max_message_size is 262144 (256 KiB)
# The default for delay_seconds is 0 seconds.
# The default for receive_wait_time_seconds is 0, meaning that the call will return immediately.
sqs_env_tag                        = "dr-prod"

sqs1_name                          =  "dr-ToucanBackward"
sqs1_message_retention_seconds     = 1209600

sqs2_name                          =  "dr-ToucanRawDataStream"
sqs2_message_retention_seconds     = 604800

sqs3_name                          =  "dr-RoadRunnerDataStream"
sqs3_message_retention_seconds     = 1209600

sqs4_name                          =  "dr-BusShadowUpdates"
sqs4_message_retention_seconds     = 86400

# MDC 

sqs5_name                           = "dr-mdc-device-status"
sqs5_visibility_timeout_seconds     =  45

sqs6_name                           = "dr-mdc-devices-messages"
sqs6_visibility_timeout_seconds     =  45

sqs7_name                           = "dr-mdc-jobs"

sqs8_name                           = "dr-mdc-status"


############## Lambda ###############

# For GetPCSChargerStatus
#enable_role = true
pcs_s3_bucket = "dr-foundation"
pcs_s3_key = "lambda/GetPCSChargerStatus.zip"
lambda_pcs_iam_role_name    = "GetPCSChargerStatus-role-dr"
lambda_pcs_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_pcs_iam_role_policy  = "files/iam/iam-policy-ChargerStatusLambda.json"
pcs_path = "/service-role/"
pcs_policy_name = "GetPCSChargerStatus-dr-policy"
lambda_pcs_function_name    = "GetPCSChargerStatus-dr"
lambda_pcs_timeout	       = 10
lambda_pcs_mem_size	       = 128
lambda_pcs_handler	        = "index.handler"
lambda_pcs_runtime          = "nodejs10.x"
pcs_statement_id  =  ["a7f98c9f-c7bc-49de-97f1-71a24718c087"]
pcs_action        =  "lambda:InvokeFunction"
pcs_principal     = ["apigateway.amazonaws.com"]
pcs_source_arn = ["arn:aws:execute-api:us-east-1:029126476216:tpmdwrt4vi/*/GET/v1/ocpp-v16jw/chargersiotecha"]

# For getGlossaryFromS3
#enable_role = true
get_glossy_s3_bucket = "dr-foundation"
get_glossy_s3_key = "lambda/getGlossaryFromS3.zip"
lambda_get_glossy_iam_role_name    = "getGlossaryFromS3-role-dr"
lambda_get_glossy_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_get_glossy_iam_role_policy  = "files/iam/iam-policy-getGlossaryFromS3.json"
get_glossy_path = "/service-role/"
get_glossy_policy_name = "getGlossaryFromS3-dr-policy"
lambda_get_glossy_function_name    = "getGlossaryFromS3-dr"
lambda_get_glossy_timeout	       = 10
lambda_get_glossy_mem_size	       = 128
lambda_get_glossy_handler	        = "index.handler"
lambda_get_glossy_runtime          = "nodejs10.x"
get_glossy_statement_id  =  ["967dc152-3cee-4dff-bf97-3a50ce0bae62"]
get_glossy_action        =  "lambda:InvokeFunction"
get_glossy_principal     = ["apigateway.amazonaws.com"]
get_glossy_source_arn = ["arn:aws:execute-api:us-east-1:029126476216:b9phhfwkv4/*/GET/glossary"]

# For getGlossaryFromS3
#enable_role = true
save_glossy_s3_bucket = "dr-foundation"
save_glossy_s3_key = "lambda/saveGlossaryToS3.zip"
lambda_save_glossy_iam_role_name    = "saveGlossaryToS3-role-dr"
lambda_save_glossy_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_save_glossy_iam_role_policy  = "files/iam/iam-policy-saveGlossaryToS3.json"
save_glossy_path = "/service-role/"
save_glossy_policy_name = "saveGlossaryToS3-dr-policy"
lambea_save_glossy_function_name    = "saveGlossaryToS3-dr"
lambda_save_glossy_timeout	       = 10
lambda_save_glossy_mem_size	       = 128
lambda_save_glossy_handler	        = "index.handler"
lambda_save_glossy_runtime          = "nodejs10.x"
save_glossy_statement_id  =  ["32d01278-87ec-4244-a9b9-633841b2e455"]
save_glossy_action        =  "lambda:InvokeFunction"
save_glossy_principal     = ["apigateway.amazonaws.com"]
save_glossy_source_arn = ["arn:aws:execute-api:us-east-1:029126476216:b9phhfwkv4/*/POST/glossary"]


# For LogstashLambda
#enable_role = true
logstash_s3_bucket = "dr-foundation"
logstash_s3_key = "lambda/LogstashLambda.zip"
lambda_logstash_iam_role_name    = "LogstashLambda-role-dr"
lambda_logstash_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_logstash_iam_role_policy  = "files/iam/iam-policy-logstashlambda.json"
logstash_path = "/service-role/"
logstash_policy_name = "LogstashLambda-dr-policy"
lambda_logstash_function_name    = "LogstashLambda-dr"
lambda_logstash_timeout	       = 10
lambda_logstash_mem_size	       = 128
lambda_logstash_handler	        = "lambdalogstash.lambda_handler"
lambda_logstash_runtime          = "python3.6"
logstash_statement_id  =  ["lc-e14b158c-9fd0-4a1e-82c7-f6e797bb35c8"]
logstash_action        =  "lambda:InvokeFunction"
logstash_principal     = ["s3.amazonaws.com"]
logstash_source_arn = ["arn:aws:s3:::data.connected.proterra.com"]

# For ToucanRawProcessorLambda
#enable_role = true
ToucanRawProcessor_s3_bucket = "dr-foundation"
ToucanRawProcessor_s3_key = "lambda/ToucanRawProcessor.zip"
lambda_ToucanRawProcessor_iam_role_name    = "ToucanRawProcessor-role-dr"
lambda_ToucanRawProcessor_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_ToucanRawProcessor_iam_role_policy  = "files/iam/iam-policy-ToucanRawProcessor.json"
ToucanRawProcessor_path = "/service-role/"
ToucanRawProcessor_policy_name = "ToucanRawProcessor-dr-policy"
lambda_ToucanRawProcessor_function_name    = "ToucanRawProcessor-dr"
lambda_ToucanRawProcessor_timeout	       = 600
lambda_ToucanRawProcessor_mem_size	       = 256
lambda_ToucanRawProcessor_handler	        = "lambda_function.lambda_handler"
lambda_ToucanRawProcessor_runtime          = "python3.6"
ToucanRawProcessor_statement_id  =  ["lambda-2531bc80-a665-4efe-83f7-50f382d944b7"]
ToucanRawProcessor_action        =  "lambda:InvokeFunction"
ToucanRawProcessor_principal     = ["s3.amazonaws.com"]
ToucanRawProcessor_source_arn = ["arn:aws:s3:::toucan.proterra.com"]


# For DecompressLambda
#enable_role = true
DecompressLambda_s3_bucket = "dr-foundation"
DecompressLambda_s3_key = "lambda/DecompressLambda.zip"
lambda_DecompressLambda_iam_role_name    = "DecompressLambda-role-dr"
lambda_DecompressLambda_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_DecompressLambda_iam_role_policy  = "files/iam/iam-policy-DecompressLambda.json"
DecompressLambda_path = "/service-role/"
DecompressLambda_policy_name = "DecompressLambda-dr-policy"
lambda_DecompressLambda_function_name    = "DecompressLambda-dr"
lambda_DecompressLambda_timeout	       = 300
lambda_DecompressLambda_mem_size	       = 256
lambda_DecompressLambda_handler	        = "lambdaDecompress.lambda_handler"
lambda_DecompressLambda_runtime          = "python3.6"
DecompressLambda_statement_id  =  ["lambda-2ba9ed45-b705-422f-bb0c-851e86598af6"]
DecompressLambda_action        =  "lambda:InvokeFunction"
DecompressLambda_principal     = ["s3.amazonaws.com"]
DecompressLambda_source_arn = ["arn:aws:s3:::backward.toucan.proterra.com"]

# For BackwardToucanProcessorLambda
#enable_role = true
BackwardToucanProcessor_s3_bucket = "dr-foundation"
BackwardToucanProcessor_s3_key = "lambda/BackwardToucanProcessor.zip"
lambda_BackwardToucanProcessor_iam_role_name    = "BackwardToucanProcessor-role-dr"
lambda_BackwardToucanProcessor_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_BackwardToucanProcessor_iam_role_policy  = "files/iam/iam-policy-BackwardToucanProcessor.json"
BackwardToucanProcessor_path = "/service-role/"
BackwardToucanProcessor_policy_name = "BackwardToucanProcessor-dr-policy"
lambda_BackwardToucanProcessor_function_name    = "BackwardToucanProcessor-dr"
lambda_BackwardToucanProcessor_timeout	       = 300
lambda_BackwardToucanProcessor_mem_size	       = 256
lambda_BackwardToucanProcessor_handler	        = "lambda_function.lambda_handler"
lambda_BackwardToucanProcessor_runtime          = "python3.6"
BackwardToucanProcessor_statement_id  =  ["lambda-b7502b61-50c6-4833-b0ec-cad53f6af1fd"]
BackwardToucanProcessor_action        =  "lambda:InvokeFunction"
BackwardToucanProcessor_principal     = ["s3.amazonaws.com"]
BackwardToucanProcessor_source_arn = ["arn:aws:s3:::backward.toucan.proterra.com"]

# For RoadRunnerDataProcessor
#enable_role = true
RoadRunnerDataProcessor_s3_bucket = "dr-foundation"
RoadRunnerDataProcessor_s3_key = "lambda/RoadRunnerDataProcessor.zip"
lambda_RoadRunnerDataProcessor_iam_role_name    = "RoadRunnerDataProcessor-role-dr"
lambda_RoadRunnerDataProcessor_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_RoadRunnerDataProcessor_iam_role_policy  = "files/iam/iam-policy-RoadRunnerDataProcessor.json"
RoadRunnerDataProcessor_path = "/service-role/"
RoadRunnerDataProcessor_policy_name = "RoadRunnerDataProcessor-dr-policy"
lambda_RoadRunnerDataProcessor_function_name    = "RoadRunnerDataProcessor-dr"
lambda_RoadRunnerDataProcessor_timeout	       = 300
lambda_RoadRunnerDataProcessor_mem_size	       = 256
lambda_RoadRunnerDataProcessor_handler	        = "lambda_function.lambda_handler"
lambda_RoadRunnerDataProcessor_runtime          = "python3.7"
RoadRunnerDataProcessor_statement_id  =  ["lambda-270cead5-9fc2-430f-8543-a02fbda98b08"]
RoadRunnerDataProcessor_action        =  "lambda:InvokeFunction"
RoadRunnerDataProcessor_principal     = ["s3.amazonaws.com"]
RoadRunnerDataProcessor_source_arn = ["arn:aws:s3:::toucan-data.proterra.com"]

# For realtimeIntoApex
#enable_role = true
realtimeIntoApex_s3_bucket = "dr-foundation"
realtimeIntoApex_s3_key = "lambda/realtime-into-apex.zip"
lambda_realtimeIntoApex_iam_role_name    = "realtimeIntoApex-role-dr"
lambda_realtimeIntoApex_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_realtimeIntoApex_iam_role_policy  = "files/iam/iam-policy-realtimeIntoApex.json"
realtimeIntoApex_path = "/service-role/"
realtimeIntoApex_policy_name = "realtimeIntoApex-dr-policy"
lambda_realtimeIntoApex_function_name    = "realtime-into-apex-dr"
lambda_realtimeIntoApex_timeout	       = 49
lambda_realtimeIntoApex_mem_size	       = 4096
lambda_realtimeIntoApex_handler	        = "lambda_function.lambda_handler"
lambda_realtimeIntoApex_runtime          = "python3.7"
realtimeIntoApex_statement_id  = ["4a0cb40c-6b64-4bdc-b165-ff48719ebfb5", "c334a1e5-d976-4f21-9fa1-c28b68003aa6", "f4996718-474d-4960-b140-cc7e71543c2a"]
realtimeIntoApex_action        =  "lambda:InvokeFunction"
realtimeIntoApex_principal     = ["iot.amazonaws.com", "iot.amazonaws.com", "iot.amazonaws.com"] 
realtimeIntoApex_source_arn = ["arn:aws:iot:us-east-1:029126476216:rule/IoTToDyanamo", "arn:aws:iot:us-east-1:029126476216:rule/RTVINRULE", "arn:aws:iot:us-east-1:029126476216:rule/NewLiveStream"]

# For mdc-enrollment
#enable_role = true
mdcEnrollment_s3_bucket = "dr-foundation"
mdcEnrollment_s3_key = "lambda/mdc-enrollment.zip"
lambda_mdcEnrollment_iam_role_name    = "mdcEnrollment-role-dr"
lambda_mdcEnrollment_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_mdcEnrollment_iam_role_policy  = "files/iam/iam-policy-mdcEnrollment.json"
mdcEnrollment_path = "/service-role/"
mdcEnrollment_policy_name = "mdcEnrollment-dr-policy"
lambda_mdcEnrollment_function_name    = "mdc-enrollment-dr"
lambda_mdcEnrollment_timeout	       = 30
lambda_mdcEnrollment_mem_size	       = 128
lambda_mdcEnrollment_handler	        = "lambda_function.lambda_handler"
lambda_mdcEnrollment_runtime          = "python2.7"
mdcEnrollment_statement_id  = ["8f9e5973-f006-4725-a68e-77098528583c"]
mdcEnrollment_action        =  "lambda:InvokeFunction"
mdcEnrollment_principal     = ["iot.amazonaws.com"] 
mdcEnrollment_source_arn = ["arn:aws:iot:us-east-1:029126476216:rule/spprovisioning"]

# For prod_mdc_s3_edge_headers
#enable_role = true
mdcS3EdgeHeaders_s3_bucket = "dr-foundation"
mdcS3EdgeHeaders_s3_key = "lambda/prod_mdc_s3_edge_headers.zip"
lambda_mdcS3EdgeHeaders_iam_role_name    = "mdcS3EdgeHeaders-role-dr"
lambda_mdcS3EdgeHeaders_iam_assume_role_policy    = "files/iam/iam-policy-assumerole.json"
lambda_mdcS3EdgeHeaders_iam_role_policy  = "files/iam/iam-policy-mdcS3EdgeHeaders.json"
mdcS3EdgeHeaders_path = "/service-role/"
mdcS3EdgeHeaders_policy_name = "mdcS3EdgeHeaders-dr-policy"
lambda_mdcS3EdgeHeaders_function_name    = "prod_mdc_s3_edge_headers-dr"
lambda_mdcS3EdgeHeaders_timeout	       = 30
lambda_mdcS3EdgeHeaders_mem_size	       = 128
lambda_mdcS3EdgeHeaders_handler	        = "lambda_function.lambda_handler"
lambda_mdcS3EdgeHeaders_runtime          = "nodejs10.x"
mdcS3EdgeHeaders_statement_id  = []
mdcS3EdgeHeaders_action        =  ""
mdcS3EdgeHeaders_principal     = [] 
mdcS3EdgeHeaders_source_arn = []
# eol
eol_commissioning_path 		= "/service-role/"
lambda_eol_iam_role_name	= "dr-prod-eolcommisioning"
lambda_eol_iam_role_policy	= "files/iam/iam-policy-eolcc.json"
eol_statement_id		= ["sid-eol-1","sid-eol-2","sid-eol-3"]
eol_s3_bucket			= "dr-foundation"
eol_s3_key			= "lambda/prod_eol_commissioning.zip"
eol_commissioning_action        =  "lambda:InvokeFunction"
eol_commissioning_principal     = ["apigateway.amazonaws.com","apigateway.amazonaws.com","apigateway.amazonaws.com"]
eol_commissioning_source_arn    = ["arn:aws:execute-api:us-east-1:029126476216:j7t2tsw923/*/POST/v1/commissioning", "arn:aws:execute-api:us-east-1:029126476216:j7t2tsw923/*/POST/v1/commissioning", "arn:aws:execute-api:us-east-1:029126476216:j7t2tsw923/*/POST/v1/commissioning"]
lambda_eol_function_name       = "dr-commissioning_att_cp_apex" 
lambda_eol_timeout             = 10
lambda_eol_mem_size            = 128
lambda_eol_handler             = "index.handler"
lambda_eol_runtime             = "nodejs12.x"

environment             = "dr"
project                 = "proterra-dr"

#VPC Setting
vpc_cidr              = "172.35.0.0/16"
vpc_public_subnets    = ["172.35.0.0/20", "172.35.16.0/20", "172.35.32.0/20", "172.35.48.0/20"]
vpc_private_subnets   = ["172.35.64.0/20", "172.35.80.0/20", "172.35.96.0/20", "172.35.112.0/20"]
vpc_availabilityzones = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
subnet_additional_tags = {
  "kubernetes.io/cluster/c-6bhpp" = "shared"
  "kubernetes.io/cluster/c-kw7p2" = "shared"
  "kubernetes.io/cluster/c-vb2gh" = "shared"
}


########################################################################
apex_ui_s3_bucket_name               = "dr-prod.apex.proterra.com"
apex_ui_s3_bucket_policy_file_name   = "files-s3/apex_ui_s3_bucket_policy.json"
ccss_ui_s3_bucket_name               = "dr-prod.ccss.proterra.com"
ccss_ui_s3_bucket_policy_file_name   = "files-s3/ccss_ui_s3_bucket_policy.json"



### Edmonton AMS Cognito

ams_cognito_app_client_name      = "dr_proterra_ams_app_client"
ams_cognito_user_pool_name       = "dr_proterra_ams_cognito_pool_edmonton"
ams_cognito_email_verif_subject  = "APEX verification code"
ams_cognito_email_verif_message  = "APEX verification code is {####}"
ams_cognito_unused_account_days  = 30
ams_cognito_email_subject        = "Welcome to Proterra APEX"
ams_cognito_sms_verif_message    = "Your authentication code is {####}. "
ams_callback_urls                =   ["https://dr.proterra.com"]
ams_logout_urls                  =   ["https://dr.proterra.com"]
ams_allowed_oauth_scopes         =   [ "email","openid","profile" ]
ams_explicit_auth_flows          =   [ "ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_SRP_AUTH"]
ams_supported_identity_providers =   ["COGNITO"]

###########################################

#######  API ########
is_dr_purpose = true 

templates_dr_path = "templates-dr"
templates_exp_path = "templates-exp"

### CCSS 
template_file_name_api_ccss = "CCSS-swagger-apigateway.json"
template_file_map_ccss = {
  title = "CCSS_DR"
  basePath = "/ccss-dr"
  stageName = "dr"
  description = "DR API gateway for CCSS"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_ccss = {
m5statusApiKey =	"9e20617c-90a3-4ec6-8066-37d71cc13b78"	 
runsApiKey =	"53190a32-57ab-41be-be26-495632f3e1c6"	 
baseURL =	"svc-alb.proterra.com/ccss"	 
m5statusURL =	"svc-alb.proterra.com/cim"	 
apiKey =	"d5585123-1346-4852-b4c0-d4fd4c8fffd0"	 
runsURL = "svc-alb.proterra.com/api"
}
dr_ccss_stage_variables_file = "API_STAGES/CCSS-7ke4odh6yk-Prod-stage-variables.json"


### CIM
template_file_name_api_cim = "CIM-swagger-apigateway.json"
template_file_map_cim = {
  title = "CIM_DR"
  basePath = "/cim-dr"
  stageName = "dr"
  description = "DR API gateway for CIM"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_cim = {
apiKey =	"9e20617c-90a3-4ec6-8066-37d71cc13b78" 
baseURL =	"svc-alb.proterra.com/cim"
}
dr_cim_stage_variables_file = "API_STAGES/CIM-ika7vviuc9-Prod-stage-variables.json"


### IQ
template_file_name_api_iq = "IQ-swagger-apigateway.json"
template_file_map_iq = {
  title = "IQ_DR"
  basePath = "/iq-dr"
  stageName = "dr"
  description = "DR API gateway for IQ"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_iq = {
apiKey =	"ee50b20e-8f99-464c-b9c4-ab016e32095b"	 
baseURL =	"svc-alb.proterra.com"	 
adminApiKey =	"626b25ef-74d5-45ca-8dd9-0abf9907adc1"
}
dr_iq_stage_variables_file = "API_STAGES/IQ-k8un1k10v7-Prod-stage-variables.json"

### DATALAKE
template_file_name_api_datalake = "DATALAKE-swagger-apigateway.json"
template_file_map_datalake = {
  title = "DATALAKE_DR"
  basePath = "/datalake-dr"
  stageName = "dr"
  description = "DR API gateway for DATALAKE"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_datalake = {
chargerMetricsApiKey = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
apikey = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
chargeSessionAPIKey = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
readOnlyApiKey = "Basic cHJvdGVycmEtcmVhZG9ubHk6cHJvdGVycmEtcmVhZG9ubHk="	 
apiKey = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
dataLakeUrl = "svc-alb.proterra.com/dataLake/v1"	 
externalBaseURL = "svc-alb.proterra.com/dataLake/v1"	 
apiKeyProd = "6323872f-1637-4ebd-aaf6-61cd1239d155"	 
latestAuth = "Basic ZWxhc3RpYzpKeGdWMlpXZG9KMU9mSkN2c0licVdxYmk="	 
edmontonApiKey = "b09389c4-fd73-4cad-ad5a-c443a963d1f7" 
internalDataLakeUrl = "a497578fb200d11eb9d2f023d2113a77-1749519384.us-east-1.elb.amazonaws.com:8091"	 
baseURL = "52.23.215.14:8080"	 
ccssApiKey = "53190a32-57ab-41be-be26-495632f3e1c6"	 
ccssAuditURL = "svc-alb.proterra.com/api"	 
reportBaseUrl = "svc-alb.proterra.com/dataLake/v1"	 
runsURL = "ab4024b4ded2811ea9d2f023d2113a77-1766774454.us-east-1.elb.amazonaws.com:8080/api/v1"	 
chargeSessionsURL = "svc-alb.proterra.com/dataLake/v1" 
rtApiKey = "a62b0b62-ed28-11ea-adc1-0242ac120002"	 
chargerMetricsBaseURL = "svc-alb.proterra.com/dataLake/v1"	 
dominionApiKey = "9yIkDNVlPQ1e96xquWPXr8KFypuRYsj89GtAa1no"
}
dr_datalake_stage_variables_file = "API_STAGES/IQ-k8un1k10v7-Prod-stage-variables.json"


### RUNS
template_file_name_api_runs = "RUNS-swagger-apigateway.json"
template_file_map_runs = {
  title = "RUNS_DR"
  basePath = "/runs-dr"
  stageName = "dr"
  description = "DR API gateway for RUNS"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_runs = {
apiKey =	"53190a32-57ab-41be-be26-495632f3e1c6"	 
baseURL =	"svc-alb.proterra.com/api"	 
}
dr_runs_stage_variables_file = "API_STAGES/RUNS-chlbesg2p0-Prod-stage-variables.json"

### AMS
template_file_name_api_ams = "AMS-swagger-apigateway.json"
template_file_map_ams = {
  title = "AMS_DR"
  basePath = "/ams-dr"
  stageName = "dr"
  description = "DR API gateway for AMS"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_ams = {
apiKey =	"53190a32-57ab-41be-be26-495632f3e1c6"	 
baseURL =	"svc-alb.proterra.com/api"	 
}
dr_ams_stage_variables_file = "API_STAGES/AMS-hkcjr4dbcl-prod-stage-variables.json"


### Miscel
template_file_name_api_miscel = "MISCEL-swagger-apigateway.json"
template_file_map_miscel = {
  title = "MISCEL_DR"
  basePath = "/miscel-dr"
  stageName = "dr"
  description = "DR API gateway for MISCEL"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_miscel = {
auth =	"thisIsForProterraAdminOnly123"	 
baseURL =	"svc-alb.proterra.com/ams"	 
}
dr_miscel_stage_variables_file = "API_STAGES/MISCEL-b9phhfwkv4-prod-stage-variables.json"


### OCPP
template_file_name_api_ocpp = "OCPP-swagger-apigateway.json"
template_file_map_ocpp = {
  title = "OCPP_DR"
  basePath = "/ocpp-dr"
  stageName = "dr"
  description = "DR API gateway for OCPP"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_ocpp = {
baseOCPPURL = "54.224.248.38:8080" 
authkey = "19998bd3-03de-4f19-9bd6-af67f4401cbe"
baseURL = "52.45.139.95:8080"	 
ocppApiKey = "	19998bd3-03de-4f19-9bd6-af67f4401cbe"
pcsOCPPURL = "35.170.104.176:8080"
}
dr_ocpp_stage_variables_file = "API_STAGES/OCPP-tpmdwrt4vi-prod-stage-variables.json"


### FILE
template_file_name_api_file = "FILE-swagger-apigateway.json"
template_file_map_file = {
  title = "FILE_DR"
  basePath = "/file-dr"
  stageName = "dr"
  description = "DR API gateway for FILE"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_file = {
baseURL = "svc-alb.proterra.com/ams"	 
}
dr_file_stage_variables_file = "API_STAGES/FILE-q4j3xc8sxh-prod-stage-variables.json"

### FILE AMS 
template_file_name_api_fileams = "FILE-AMS-swagger-apigateway.json"
template_file_map_fileams = {
  title = "FILE_AMS_DR"
  basePath = "/fileams-dr"
  stageName = "dr"
  description = "DR API gateway for FILE AMS "
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_fileams = {
baseCustomReportURL =	"18.209.101.116:3000/api"	 
baseURL = "svc-alb.proterra.com/ams"	 
dataLakeURL =	 "svc-alb.proterra.com" 
}
dr_fileams_stage_variables_file = "API_STAGES/FILE-AMS-u4xquba38g-prod-stage-variables.json"


### MDC  
template_file_name_api_mdc = "MDC-swagger-apigateway.json"
template_file_map_mdc = {
  title = "MDC_DR"
  basePath = "/mdc-dr"
  stageName = "dr"
  description = "DR API gateway for MDC"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_mdc = {
DeviceURL =	 "mdc-device.proterra.com" 
}
dr_mdc_stage_variables_file = "API_STAGES/MDC-bakq37oidb-prod-stage-variables.json"


### STARPOINT 
template_file_name_api_starpoint = "starpoint-swagger-apigateway.json"
template_file_map_starpoint= {
  title = "STARPOINT_DR"
  basePath = "/starpoint-dr"
  stageName = "dr"
  description = "DR API gateway for STARPOINT"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_starpoint = {
}
dr_starpoint_stage_variables_file = "API_STAGES/starpoint-j7t2tsw923-prod-stage-variables.json"



### MULTIPLEXER 
template_file_name_api_multiplexer = "multiplexer-swagger-apigateway.json"
template_file_map_multiplexer = {
  title = "MULTIPLEXER_DR"
  basePath = "/multiplexer-dr"
  stageName = "dr"
  description = "DR API gateway for MULTIPLEXER"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_multiplexer = {
}
dr_multiplexer_stage_variables_file = "API_STAGES/multiplexer-o7nqlx6oy1-prod-mp-stage-variables.json"


### CONTROL_PANEL 
template_file_name_api_controlpanel = "control-panel-swagger-apigateway.json"
template_file_map_controlpanel = {
  title = "CONTROL_PANEL_DR"
  basePath = "/controlpanel-dr"
  stageName = "dr"
  description = "DR API gateway for CONTROL_PANEL"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_controlpanel = {
}
dr_controlpanel_stage_variables_file = "API_STAGES/control-panel-rlzu4mvlti-prod-cp-stage-variables.json"


### STARPOINT_LOGGER
template_file_name_api_starpointlogger = "Starpointlogger-swagger-apigateway.json"
template_file_map_starpointlogger = {
  title = "STARPOINT_LOGGER_DR"
  basePath = "/starpointlogger-dr"
  stageName = "dr"
  description = "DR API gateway for STARPOINT_LOGGER"
  #providerARNs = "arn:aws:cognito-idp:us-east-1:493000495847:userpool/us-east-1_1JCyT7jUY"
  host = "api.proterra.com"
}
local_stage_variables_starpointlogger = {
}
dr_starpointlogger_stage_variables_file = "API_STAGES/Starpointlogger-yqr4zi5swl-Prod-stage-variables.json"


################  API Keys ###############

####  Usage Plan :  External Customer	API Key : E-dl  API : Proterra Data Lake
 
is_usage_plan_required_ec = true
is_api_key_required_ec = true
is_associated_usage_plans_required_ec =  true
## Usage Plan
#dr 
dr_usage_plan_map_file_name_ec="API-USAGE-PLAN/API-USAGE-PLAN-DR/sxzogg-External_usageplan.json"
#local
local_usage_plan_map_file_name_ec="files/apikeys/UsagePlan-jason.json"

## Api keys
dr_api_keys_map_file_name_ec="API-KEYS/API-KEYS-DR/E-dl-is557l2qc1_apikey.json"
                           
# local_api_keys_data_ec = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }

####  Usage Plan :  External_data_lake_production 	API Key : edmonton-datalake  API : Proterra Data Lake
 
is_usage_plan_required_ed = true
is_api_key_required_ed = true
is_associated_usage_plans_required_ed =  true
## Usage Plan
#dr 
dr_usage_plan_map_file_name_ed="API-USAGE-PLAN/API-USAGE-PLAN-DR/xc290r-External_data_lake_production_usageplan.json"
#local
local_usage_plan_map_file_name_ed="files/apikeys/UsagePlan-jason.json"

## Api keys
dr_api_keys_map_file_name_ed="API-KEYS/API-KEYS-DR/edmonton-datalake-pn1jgdgn28_apikey.json"
                           
# local_api_keys_data_ed = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }

####  Usage Plan :  GatewayCreateProd 	API Key : misc-api  API : Miscel API
is_usage_plan_required_gcp = true
is_api_key_required_gcp = true
is_associated_usage_plans_required_gcp =  true
## Usage Plan
#dr 
dr_usage_plan_map_file_name_gcp="API-USAGE-PLAN/API-USAGE-PLAN-DR/hwoo59-GatewayCreateProd_usageplan.json"
#local
local_usage_plan_map_file_name_gcp="files/apikeys/UsagePlan-jason.json"

## Api keys
dr_api_keys_map_file_name_gcp="API-KEYS/API-KEYS-DR/misc-api-ukzcn20fji_apikey.json"
                           
# local_api_keys_data_gcp = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }

####  Usage Plan :  starpoint 	API Key : starpoint  API : starpoint

is_usage_plan_required_sp = true
is_api_key_required_sp = true
is_associated_usage_plans_required_sp =  true
## Usage Plan
#dr 
dr_usage_plan_map_file_name_sp="API-USAGE-PLAN/API-USAGE-PLAN-DR/2q4ca5-startpoint_usageplan.json"
#local
local_usage_plan_map_file_name_sp="files/apikeys/UsagePlan-jason.json"

## Api keys
dr_api_keys_map_file_name_sp="API-KEYS/API-KEYS-DR/starpoint-hj2q7g57j1_apikey.json"
                           
# local_api_keys_data_sp = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }

####  Usage Plan :  chargersessions 	API Key :  N/A  API : Proterra Data Lake

is_usage_plan_required_cs = true
is_api_key_required_cs = false
is_associated_usage_plans_required_cs =  false
## Usage Plan
#dr 
dr_usage_plan_map_file_name_cs="API-USAGE-PLAN/API-USAGE-PLAN-DR/seo5sb-chargersessions_usageplan.json"
#local
local_usage_plan_map_file_name_cs="files/apikeys/UsagePlan-jason.json"

## Api keys
# For dummy
dr_api_keys_map_file_name_cs="API-KEYS/API-KEYS-DR/starpoint-hj2q7g57j1_apikey.json"
                           
# local_api_keys_data_cs = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }

####  Usage Plan :  logger_usage_plan 	API Key :  N/A  API : Exp-Prod-Starpointlogger
is_usage_plan_required_lup = true
is_api_key_required_lup = false
is_associated_usage_plans_required_lup =  false
## Usage Plan
#dr 
dr_usage_plan_map_file_name_lup="API-USAGE-PLAN/API-USAGE-PLAN-DR/seo5sb-chargersessions_usageplan.json"
#local
local_usage_plan_map_file_name_lup="files/apikeys/UsagePlan-jason.json"

## Api keys
#for dummy
dr_api_keys_map_file_name_lup="API-KEYS/API-KEYS-DR/starpoint-hj2q7g57j1_apikey.json"
                           
# local_api_keys_data_lup = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }

####  Usage Plan : multiplexer-prod-fetch-charger-cp 	API Key :  N/A  API : Proterra AMS

is_usage_plan_required_mp = true
is_api_key_required_mp = false
is_associated_usage_plans_required_mp =  false
## Usage Plan
#dr 
dr_usage_plan_map_file_name_mp="API-USAGE-PLAN/API-USAGE-PLAN-DR/su5rb3-multiplexer-prod-fetch-charger-cp_usageplan.json"
#local
local_usage_plan_map_file_name_mp="files/apikeys/UsagePlan-jason.json"

## Api keys
## dummy
dr_api_keys_map_file_name_mp="API-KEYS/API-KEYS-DR/starpoint-hj2q7g57j1_apikey.json"
                           
# local_api_keys_data_mp = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }

####  Usage Plan : prod-mp-up 	API Key :  N/A  API : control-panel-prod

is_usage_plan_required_mpup = true
is_api_key_required_mpup = false
is_associated_usage_plans_required_mpup =  false
## Usage Plan
#dr 
dr_usage_plan_map_file_name_mpup="API-USAGE-PLAN/API-USAGE-PLAN-DR/lte5kx-prod-mp-up_usageplan.json"
#local
local_usage_plan_map_file_name_mpup="files/apikeys/UsagePlan-jason.json"

## Api keys
## dummy 
dr_api_keys_map_file_name_mpup="API-KEYS/API-KEYS-DR/starpoint-hj2q7g57j1_apikey.json"
                           
# local_api_keys_data_mpup = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }

####  Usage Plan : N/A  	API Key :  Dominion  API : N/A

is_usage_plan_required_dominion = false
is_api_key_required_dominion = true
is_associated_usage_plans_required_dominion =  false
## Usage Plan
#dr 
# dummy 
dr_usage_plan_map_file_name_dominion="API-USAGE-PLAN/API-USAGE-PLAN-DR/lte5kx-prod-mp-up_usageplan.json"
#local
local_usage_plan_map_file_name_dominion="files/apikeys/UsagePlan-jason.json"

## Api keys
dr_api_keys_map_file_name_dominion="API-KEYS/API-KEYS-DR/Dominion-86grvlzxad_apikey.json"
                           
# local_api_keys_data_dominion = {
# name = "devops_demo_apikey"	 
# description = "Managed by Terraform"
# enabled = "true"
# value = ""
# }


############ SNS  #####
mdc_sns_name="mdc-dr-test"

############  IoT Rules ##########
# mdcmessagestosns 
iot_rule_mdc_messages_provisioning_name = "dr_mdcmessagestosns" 
iot_rule_mdc_messages_description="mdc messages to sns and redirected to SQS"
iot_rule_mdc_messages_sql_query="SELECT * FROM 'starpoint/messages'"


#mdcpresencetosns
iot_rule_mdc_presence_provisioning_name = "dr_mdcpresencetosns" 
iot_rule_mdc_presence_description="mdc presence to sns and redirected to SQS"
iot_rule_mdc_presence_sql_query="SELECT * FROM '$aws/events/presence/#'"

#spprovisioning

iot_rule_mdc_spprovisioning_provisioning_name = "dr_spprovisioning" 
iot_rule_mdc_spprovisioning_description="spprovisioning"
iot_rule_mdc_spprovisioning_sql_query="SELECT * FROM '$aws/events/certificates/registered/14d1845fa77aa47c94c1169016854c75ce0524eb23b66030a1a739df13e8715e'"
