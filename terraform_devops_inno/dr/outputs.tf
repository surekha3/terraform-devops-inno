output "cf_apex_domain_name" {
  value       = module.apex-cdn.cf_domain_name
  description = "Domain name corresponding to the distribution"
}
output "cf_ccss_domain_name" {
  value       = module.ccss-cdn.cf_domain_name 
 }

output "prod_db_cluster_url" {
 value = module.proterra-prod-db.rds_cluster_endpoint
 }

output "encrypted_db_cluster_url" {
 value = module.encrypted-rds-cluster.rds_cluster_endpoint
}
output "public_subnets" {
  value  = "${jsonencode(split(",", module.vpc.vpc_public_subnet_ids))}"
}
output "worker_role_name" {
  value = module.eks.worker_iam_role_name
}
